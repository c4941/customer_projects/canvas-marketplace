#!/bin/bash
export DJANGO_SETTINGS_MODULE=project.settings.production
gunicorn --keep-alive=300 --timeout=300  --workers=2 --log-file=- -b :8080 project.wsgi:application
