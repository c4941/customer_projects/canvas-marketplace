#!/bin/bash
export DJANGO_SETTINGS_MODULE=project.settings.dev
python manage.py collectstatic --no-input
python manage.py runserver 0.0.0.0:8080