#!/bin/bash
export DJANGO_SETTINGS_MODULE=project.settings.production
python manage.py migrate --noinput
python manage.py collectstatic --no-input --clear
echo "Init Complete"
