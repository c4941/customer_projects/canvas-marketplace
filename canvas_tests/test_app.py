# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import pytest
from django.core.management import call_command
from django.test.client import Client
from django.urls import reverse


@pytest.mark.django_db
def test_index_page():
    call_command("init_project")
    call_command("load_config")

    client = Client()
    response = client.get(reverse("shuup:index"))
    assert response.status_code == 200
