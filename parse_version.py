import os

poetry_version = os.environ.get("POETRY_PACKAGE_VERSION")
print(poetry_version.split(" ")[1])
