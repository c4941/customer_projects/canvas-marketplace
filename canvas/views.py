# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.admin.views.select import MultiselectAjaxView
from shuup.front.checkout import VerticalCheckoutProcess
from shuup.front.views.checkout import CheckoutViewWithLoginAndRegister

from canvas.models import SustainabilityGoal

phase_specs = [
    "canvas.front.checkout_phases:ChooseYourCheckoutMethod",
    "shuup.front.checkout.checkout_method:RegisterPhase",
    "canvas.front.checkout_phases:AddAddresses",
    "canvas.front.checkout_phases:ChooseShippingAndPay",
    "canvas.front.checkout_phases:MultivendorChooseShippingAndPay",
    "shuup_multivendor.checkout:MultivendorShippingMethodSpawnerPhase",
    "shuup.front.checkout.methods:PaymentMethodPhase",
    "canvas.front.checkout_phases:FinalizeYourOrderPhase",
]


class CheckoutViewWithLoginAndRegisterVertical(CheckoutViewWithLoginAndRegister):
    process_class = VerticalCheckoutProcess
    phase_specs = phase_specs


class CanvasMultiselectAjaxView(MultiselectAjaxView):
    def get_data(self, request, *args, **kwargs):
        results = super().get_data(request, *args, **kwargs)
        if results and request.GET.get("model") == "canvas.sustainabilitygoal":
            # MultiselectAjaxView always sorts by name. But in the case of SDGs, they need to be sorted by identifier.
            # So, we'll have to do the query over again.
            results = list(
                [
                    {"id": sdg.id, "name": sdg.name}
                    for sdg in SustainabilityGoal.objects.filter(id__in=[data["id"] for data in results]).order_by(
                        "identifier"
                    )
                ]
            )
        return results
