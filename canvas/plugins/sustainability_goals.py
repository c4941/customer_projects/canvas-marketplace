# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.translation import ugettext_lazy as _
from enumfields import Enum, EnumField
from shuup.core import cache
from shuup.xtheme import TemplatedPlugin
from shuup.xtheme.plugins.forms import TranslatableField

from canvas.models import SustainabilityGoal


class SustainabilityGoalLayout(Enum):
    CAROUSEL = "carousel"
    LIST = "list"

    class Labels:
        CAROUSEL = _("SDG Carousel")
        LIST = _("SDGs as a list")


class SustainabilityGoalPlugin(TemplatedPlugin):
    identifier = "sustainability_goals"
    name = _("Sustainable Development Goals")
    template_name = "canvas/plugins/sustainability_goals.jinja"
    fields = [
        (
            "layout",
            EnumField(
                SustainabilityGoalLayout, default=SustainabilityGoalLayout.CAROUSEL, verbose_name=_("SDGs layout")
            ).formfield(),
        ),
        (
            "url",
            TranslatableField(
                label=_("SDGs external URL"),
                required=False,
                help_text=_("The UN website URL for SGSs. Leave empty to hide the link element"),
            ),
        ),
    ]

    def get_context_data(self, context):
        sdgs = cache.get(self.identifier)
        if not sdgs:
            sdgs = SustainabilityGoal.objects.select_related("image").all().order_by("identifier")
            cache.set(self.identifier, sdgs)
        return {
            "request": context["request"],
            "sustainability_goals": sdgs,
            "layout": self.config.get("layout"),
            "url": self.get_translated_value("url"),
        }
