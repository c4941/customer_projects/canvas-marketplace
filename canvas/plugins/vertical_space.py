# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.translation import ugettext_lazy as _
from shuup.xtheme import TemplatedPlugin


class VerticalSpacePlugin(TemplatedPlugin):
    identifier = "vertical_space"
    name = _("Vertical space")
    template_name = "canvas/plugins/vertical_space.jinja"
    fields = []

    def get_context_data(self, context):
        return {
            "request": context["request"],
        }
