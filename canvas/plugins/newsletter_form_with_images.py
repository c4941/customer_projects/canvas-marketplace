# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from filer.models import File
from shuup.admin.forms.widgets import ImageChoiceWidget
from shuup.xtheme import TemplatedPlugin
from shuup.xtheme.plugins.forms import TranslatableField


class ImagePluginChoiceWidget(ImageChoiceWidget):
    """
    Subclass of ImageChoiceWidget that will not raise an exception if
    given an invalid initial image ID (in case the image has been deleted).
    """

    def get_object(self, value):
        return File.objects.filter(pk=value).first()


class ImageIDField(forms.IntegerField):
    """
    A custom field that stores the ID value of a Filer image and presents
    Shuup admin's image popup widget.
    """

    widget = ImagePluginChoiceWidget(clearable=True)

    def clean(self, value):
        try:
            value = super(ImageIDField, self).clean(value)
        except ValidationError:
            raise ValidationError("Error! Invalid image ID.", code="invalid")
        return value


class NewsletterFormWithImagesPlugin(TemplatedPlugin):
    identifier = "newsletter_form_with_images"
    name = _("Canvas newsletter form with images")
    template_name = "canvas/plugins/newsletter_form_with_images.jinja"
    fields = [
        ("title_prefix", TranslatableField(label=_("Title Prefix"), required=False)),
        ("title", TranslatableField(label=_("Newsletter Title"), required=True)),
        ("image_1_id", ImageIDField(label=_("Main Image"), required=False)),
        ("image_2_id", ImageIDField(label=_("Image 2"), required=False)),
        ("image_3_id", ImageIDField(label=_("Image 3"), required=False)),
        ("image_4_id", ImageIDField(label=_("Image 4"), required=False)),
        ("image_5_id", ImageIDField(label=_("Image 5"), required=False)),
        ("image_6_id", ImageIDField(label=_("Image 6"), required=False)),
        ("image_7_id", ImageIDField(label=_("Image 7"), required=False)),
    ]

    def get_context_data(self, context):
        image_1 = None
        image_1_id = self.config.get("image_1_id", None)
        if image_1_id:
            image_1 = File.objects.filter(pk=image_1_id).first()

        image_2 = None
        image_2_id = self.config.get("image_2_id", None)
        if image_2_id:
            image_2 = File.objects.filter(pk=image_2_id).first()

        image_3 = None
        image_3_id = self.config.get("image_3_id", None)
        if image_3_id:
            image_3 = File.objects.filter(pk=image_3_id).first()

        image_4 = None
        image_4_id = self.config.get("image_4_id", None)
        if image_4_id:
            image_4 = File.objects.filter(pk=image_4_id).first()

        image_5 = None
        image_5_id = self.config.get("image_5_id", None)
        if image_5_id:
            image_5 = File.objects.filter(pk=image_5_id).first()

        image_6 = None
        image_6_id = self.config.get("image_6_id", None)
        if image_6_id:
            image_6 = File.objects.filter(pk=image_6_id).first()

        image_7 = None
        image_7_id = self.config.get("image_7_id", None)
        if image_7_id:
            image_7 = File.objects.filter(pk=image_7_id).first()

        return {
            "request": context["request"],
            "title_prefix": self.get_translated_value("title_prefix"),
            "title": self.get_translated_value("title"),
            "image_1_url": (image_1.url if image_1 else None),
            "image_2_url": (image_2.url if image_2 else None),
            "image_3_url": (image_3.url if image_3 else None),
            "image_4_url": (image_4.url if image_4 else None),
            "image_5_url": (image_5.url if image_5 else None),
            "image_6_url": (image_6.url if image_6 else None),
            "image_7_url": (image_7.url if image_7 else None),
        }
