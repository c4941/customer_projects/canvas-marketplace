# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from filer.models import File
from shuup.admin.forms.widgets import ImageChoiceWidget
from shuup.xtheme import TemplatedPlugin


class ImagePluginChoiceWidget(ImageChoiceWidget):
    """
    Subclass of ImageChoiceWidget that will not raise an exception if
    given an invalid initial image ID (in case the image has been deleted).
    """

    def get_object(self, value):
        return File.objects.filter(pk=value).first()


class ImageIDField(forms.IntegerField):
    """
    A custom field that stores the ID value of a Filer image and presents
    Shuup admin's image popup widget.
    """

    widget = ImagePluginChoiceWidget(clearable=True)

    def clean(self, value):
        try:
            value = super(ImageIDField, self).clean(value)
        except ValidationError:
            raise ValidationError("Error! Invalid image ID.", code="invalid")
        return value


class FullWidthImagePlugin(TemplatedPlugin):
    identifier = "full_width_image"
    name = _("Canvas full page width image")
    template_name = "canvas/plugins/full_width_image.jinja"
    fields = [
        ("image_id", ImageIDField(label=_("Image"), required=True)),
    ]

    def get_context_data(self, context):
        image = None
        image_id = self.config.get("image_id", None)
        if image_id:
            image = File.objects.filter(pk=image_id).first()

        return {
            "request": context["request"],
            "image_url": (image.url if image else None),
        }
