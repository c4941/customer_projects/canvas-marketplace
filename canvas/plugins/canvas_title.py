# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django import forms
from django.utils.translation import ugettext_lazy as _
from shuup.xtheme import TemplatedPlugin
from shuup.xtheme.plugins.forms import TranslatableField


class CanvasTitlePlugin(TemplatedPlugin):
    identifier = "canvas_title"
    name = _("Canvas Title")
    template_name = "canvas/plugins/canvas_title.jinja"
    fields = [
        ("subtitle", TranslatableField(label=_("Subtitle"), required=False)),
        ("title_prefix", TranslatableField(label=_("Title Prefix"), required=False)),
        ("title", TranslatableField(label=_("Title"), required=True)),
        ("has_line_break", forms.BooleanField(label=_("Line break after title prefix"), required=False, initial=False)),
        ("is_heading_1", forms.BooleanField(label=_("Render as H1"), required=False, initial=False)),
    ]

    def get_context_data(self, context):
        return {
            "request": context["request"],
            "subtitle": self.get_translated_value("subtitle"),
            "title_prefix": self.get_translated_value("title_prefix"),
            "title": self.get_translated_value("title"),
            "has_line_break": self.config.get("has_line_break"),
            "is_heading_1": self.config.get("is_heading_1"),
        }
