# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.translation import ugettext_lazy as _
from shuup.xtheme import TemplatedPlugin
from shuup.xtheme.plugins.forms import TranslatableField


class CheckboxItemPlugin(TemplatedPlugin):
    identifier = "checkbox_item"
    name = _("Canvas checkbox item")
    template_name = "canvas/plugins/checkbox_item.jinja"
    fields = [
        ("text", TranslatableField(label=_("Checkbox Item Text"), required=True)),
    ]

    def get_context_data(self, context):
        return {
            "request": context["request"],
            "text": self.get_translated_value("text"),
        }
