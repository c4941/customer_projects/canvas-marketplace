# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from enumfields import Enum, EnumField
from filer.models import File
from shuup.admin.forms.widgets import ImageChoiceWidget, TextEditorWidget
from shuup.xtheme import TemplatedPlugin
from shuup.xtheme.plugins.forms import TranslatableField


class AccordionLayout(Enum):
    IMAGE_RIGHT = "image_right"
    IMAGE_LEFT = "image_left"
    FULL_WIDTH_IMAGE_FIRST = "full_width_image_first"
    FULL_WIDTH_TEXT_FIRST = "full_width_text_first"

    class Labels:
        IMAGE_RIGHT = _("Split: Image on right, text left")
        IMAGE_LEFT = _("Split: Image on left, text right")
        FULL_WIDTH_IMAGE_FIRST = _("Full width: Image on top of the text")
        FULL_WIDTH_TEXT_FIRST = _("Full width: Text on top of the image")


class ImagePluginChoiceWidget(ImageChoiceWidget):
    """
    Subclass of ImageChoiceWidget that will not raise an exception if
    given an invalid initial image ID (in case the image has been deleted).
    """

    def get_object(self, value):
        return File.objects.filter(pk=value).first()


class ImageIDField(forms.IntegerField):
    """
    A custom field that stores the ID value of a Filer image and presents
    Shuup admin's image popup widget.
    """

    widget = ImagePluginChoiceWidget(clearable=True)

    def clean(self, value):
        try:
            value = super(ImageIDField, self).clean(value)
        except ValidationError:
            raise ValidationError("Error! Invalid image ID.", code="invalid")
        return value


class AccordionPlugin(TemplatedPlugin):
    identifier = "accordion_default"
    name = _("Accordion")
    template_name = "canvas/plugins/accordion_default.jinja"
    fields = [
        ("title", TranslatableField(label=_("Accordion Title"), required=True)),
        (
            "layout",
            EnumField(
                AccordionLayout, default=AccordionLayout.IMAGE_RIGHT, verbose_name=_("accordion layout")
            ).formfield(),
        ),
        ("is_open", forms.BooleanField(label=_("Accordion is initially open"), required=False, initial=False)),
        (
            "content",
            TranslatableField(label=_("Text Content"), required=False, initial="", widget=TextEditorWidget),
        ),
        (
            "limit_text_width",
            forms.BooleanField(
                label=_("Limit content width"),
                required=False,
                initial=False,
                help_text=_("Limit text and image width to max 750px."),
            ),
        ),
        ("image_id", ImageIDField(label=_("Image"), required=False)),
    ]

    def get_context_data(self, context):
        image = None
        image_id = self.config.get("image_id", None)
        if image_id:
            image = File.objects.filter(pk=image_id).first()

        return {
            "request": context["request"],
            "title": self.get_translated_value("title"),
            "layout": self.config.get("layout"),
            "is_open": self.config.get("is_open"),
            "content": self.get_translated_value("content"),
            "limit_text_width": self.config.get("limit_text_width"),
            "image_url": (image.url if image else None),
        }
