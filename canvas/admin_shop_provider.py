# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from contextlib import contextmanager
from typing import Generator, Optional

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpRequest
from django.utils.translation import ugettext_lazy as _
from shuup.admin.shop_provider import set_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Shop, ShopStatus
from shuup.core.utils.shops import get_shop_from_host
from shuup_multivendor.models import SupplierUser

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER

SHOP_SESSION_KEY = "admin_shop"


class AdminShopProvider(object):
    def get_shop(self, request):
        if not request.user.is_staff:
            return None

        # take the first if multishop is disabled
        if not settings.SHUUP_ENABLE_MULTIPLE_SHOPS:
            return Shop.objects.first()

        supplier_users = SupplierUser.objects.filter(user=request.user)
        if supplier_users:
            # In this project we have the whole vendor admin work across the shops at
            # all times. Thus we should always route vendors to the main shop because
            # that only will for example contain all of the Categories that the vendors
            # can select for their products, and only by having the shop as active will
            # the Category select fields return those results.
            return Shop.objects.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER, status=ShopStatus.ENABLED).first()

        # user is a staff member or a superuser
        permitted_shops = Shop.objects.get_for_user(request.user).filter(status=ShopStatus.ENABLED)

        if SHOP_SESSION_KEY in request.session:
            shop = Shop.objects.filter(pk=request.session[SHOP_SESSION_KEY]).first()
            if shop and shop in permitted_shops:
                return shop

        # try loading the shop from the host
        host = request.META.get("HTTP_HOST")
        shop = get_shop_from_host(host) if host else None
        if shop and shop in permitted_shops:
            return shop

        # no shop set, fetch the first shop available
        first_available_shop = permitted_shops.first()
        if first_available_shop:
            return first_available_shop

        # so return the first if we are superuser
        if request.user.is_superuser:
            return Shop.objects.first()

    def set_shop(self, request, shop=None):
        if not request.user.is_staff:
            raise PermissionDenied(_("You must have the Access to Admin Panel permission."))

        if shop:
            supplier = get_supplier(request)

            if (
                shop.staff_members.filter(pk=request.user.pk).exists()
                or request.user.is_superuser
                or (supplier and shop.vendor_users.filter(supplier=supplier).exists())
            ):
                request.session[SHOP_SESSION_KEY] = shop.id
            else:
                raise PermissionDenied(_("You must have the Access to Admin Panel permissions to this shop."))

        else:
            self.unset_shop(request)

    def unset_shop(self, request):
        if SHOP_SESSION_KEY in request.session:
            del request.session[SHOP_SESSION_KEY]


_SHOP_NOT_SET = object()


@contextmanager
def switch_shop(request: HttpRequest, shop: Optional[Shop] = None) -> Generator[None, None, None]:
    """Use as a context manager to temporarily switch the request's shop."""
    initial_session_shop = request.session.get(SHOP_SESSION_KEY, _SHOP_NOT_SET)
    initial_request_shop = getattr(request, "shop", _SHOP_NOT_SET)

    set_shop(request, shop)
    # Some code reads the value from this instead of the session, for example:
    #   `shuup.admin.modules.sales_dashboard.dashboard.OrderValueChartDashboardBlock.__init__`
    request.shop = shop

    try:
        yield
    finally:
        # Let's get everything back how it was.
        if initial_session_shop is not _SHOP_NOT_SET:
            request.session[SHOP_SESSION_KEY] = initial_session_shop
        if initial_request_shop is not _SHOP_NOT_SET:
            request.shop = initial_request_shop
