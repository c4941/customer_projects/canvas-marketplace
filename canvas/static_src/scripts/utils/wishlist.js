/*
* This file is part of Shuup.
*
* Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
*
* This source code is licensed under the Shuup Commerce Inc -
* SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
* and the Licensee.
 */

let wishlistUrl = `/wishlist/default/product/`

// Allow users to add from wishlist at will.
export function addWishlistItem(shopProductId, callback) {
    $.ajax({
        url: `${wishlistUrl}/${shopProductId}/`,
        method: "POST",
        success: function(resp) {
            if (resp.ok && typeof callback === "function") callback();
            else alert("{% trans %}Something went wrong. Please try again shortly.{% endtrans %}");
        }
    });
}

// Allow users to delete from wishlist at will.
export function removeWishlistItem(shopProductId, callback) {
    $.ajax({
        url: `${wishlistUrl}/${shopProductId}/remove/`,
        method: "POST",
        success: function(resp) {
            if (resp.ok && typeof callback === "function") callback();
            else alert("{% trans %}Something went wrong. Please try again shortly.{% endtrans %}");
        }
    });
}
