const setFocus = $elem => {
  $elem.focus();
  setTimeout(function() {
    // 50ms timeout so that it actually gets focused if it wasn't already
    $elem.focus();
  }, 50);
};

export default setFocus;
