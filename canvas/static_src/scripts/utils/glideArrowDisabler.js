const glideArrowDisabler = (Glide, Components) => {
  const carouselControlClasses = {
    'controls': 'glide__controls',
    'backArrow': 'glide__controls__back',
    'nextArrow': 'glide__controls__next',
  };

  return {
    mount() {
      // Only in effect when rewinding is disabled
      if (Glide.settings.rewind) {
        return
      }

      Glide.on(['mount.after', 'run', 'resize'], () => {
        // Filter out arrows_control
        for (let controlItem of Components.Controls.items) {
          if (controlItem.className !== carouselControlClasses.controls) {
            continue
          }

          // Set left arrow state
          var left = controlItem.querySelector('.' + carouselControlClasses.backArrow)
          if (left) {
            if (Components.Sizes.length <= Glide.settings.perView) {
              left.setAttribute('disabled', '') // Disable if not enough slides
            } else {
              if (Glide.index === 0) {
                left.setAttribute('disabled', '') // Disable on first slide
              } else {
                left.removeAttribute('disabled') // Enable on other slides
              }
            }
          }

          // Set right arrow state
          var right = controlItem.querySelector('.' + carouselControlClasses.nextArrow)
          if (right) {
            // Glide.index is based on the active slide
            // For bound: true, there will be no empty space & the last slide will never become active
            // Hence add perView to correctly calculate the last slide
            const lastSlideIndex = Glide.settings.bound
              ? Glide.index + (Glide.settings.perView - 1)
              : Glide.index;

            if (Components.Sizes.length <= Glide.settings.perView) {
              right.setAttribute('disabled', '') // Disable if not enough slides
            } else {
              if (lastSlideIndex === Components.Sizes.length - 1) {
                right.setAttribute('disabled', '') // Disable on last slide
              } else {
                right.removeAttribute('disabled') // Enable on other slides
              }
            }
          }
        }
      });
    }
  }
};

export default glideArrowDisabler;
