import { gsap } from 'gsap';

export function searchTransitionIn(elementClass) {
  const element = document.querySelector(elementClass);

  return gsap
    .to(element, {
      duration: .2,
      autoAlpha: 1,
      ease: 'power1.out',
    });
}

export function searchTransitionOut(elementClass, callBackFunc) {
  const element = document.querySelector(elementClass);

  return gsap
    .timeline()
    .to(element, {
      duration: .2,
      autoAlpha: 0,
      ease: 'power1.out',
      onComplete: function() {
        return callBackFunc();
      },
    });
  }
