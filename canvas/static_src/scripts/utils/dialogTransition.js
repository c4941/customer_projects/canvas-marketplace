import { gsap } from 'gsap';

export function dialogTransitionIn(elementClass) {
  const element = document.querySelector(elementClass);

  const scrollBarWidth = window.innerWidth - document.body.clientWidth;

  if (scrollBarWidth > 0) {
    $('body').css({ paddingRight: scrollBarWidth });
  }

  return gsap
    .to(element, {
      duration: .2,
      transform: 'translateX(0)',
      ease: 'power1.out',
    });
}

export function dialogTransitionOut(elementClass, callBackFunc) {
  const element = document.querySelector(elementClass);

  return gsap
    .timeline()
    .to(element, {
      duration: .2,
      transform: 'translateX(100%)',
      ease: 'power1.out',
      onComplete: function() {
        $('body').css({ paddingRight: 0 });
        return callBackFunc();
      },
    });
  }
