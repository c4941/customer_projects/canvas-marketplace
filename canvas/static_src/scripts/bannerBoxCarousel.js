import Glide from '@glidejs/glide';
import glideArrowDisabler from './utils/glideArrowDisabler';

const CanvasBannerBoxCarousel = () => {
  const carouselElems = document.querySelectorAll('.canvas-carousel__banners__mobile .glide');

  carouselElems.forEach((element) => {
    new Glide(element, {
      type: 'slider',
      startAt: 0,
      perView: 1,
      gap: 15,
      rewind: false,
      bound: true,
      breakpoints: {
        991: {
          peek: {
            before: 15,
            after: 200
          },
        },
        769: {
          peek: {
            before: 15,
            after: 100,
          },
        },
        500: {
          peek: {
            before: 15,
            after: 60,
          },
        },
      },
    }).mount({ glideArrowDisabler });
  });
};

export default CanvasBannerBoxCarousel;
