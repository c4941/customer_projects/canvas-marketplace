import { dialogTransitionIn, dialogTransitionOut } from './utils/dialogTransition';
import setFocus from './utils/setFocus';

const categoryFilters = () => {
  const filtersElementSelector = '#canvas-filters-dialog';
  const $filtersElement = $(filtersElementSelector);
  const filtersTogglerSelector = '#canvas-filters-dialog__toggler';
  const $filtersToggler = $(filtersTogglerSelector);
  const filtersCloseBtnSelector = '#canvas-filters-dialog__close-btn';
  const $filtersCloseBtn = $(filtersCloseBtnSelector);
  const filtersOpenClassname = 'canvas-filters-dialog__open';

  const isFiltersOpen = () => {
    return !!$('body').hasClass(filtersOpenClassname);
  };

  const openFilters = () => {
    dialogTransitionIn(filtersElementSelector);
    $('body').addClass(filtersOpenClassname);
    setFocus($filtersElement);
  };

  const removeFiltersClass = () => {
    $('body').removeClass(filtersOpenClassname);
  }

  const closeFilters = () => {
    dialogTransitionOut(filtersElementSelector, removeFiltersClass);
    setFocus($filtersToggler);
  };

  $filtersToggler.on('click', function() {
    openFilters();
  });

  $filtersCloseBtn.on('click', function() {
    closeFilters();
  });

  // Handle closing dialogs if clicked outside the elements
  $(document).on('click', function(e) {
    // check if filters is open and user has not clicked on the filters toggle button
    if (isFiltersOpen() && !$(e.target).closest(filtersTogglerSelector).length) {
      // Don't do anything if clicked inside the filters element
      if (!$(e.target).closest(filtersElementSelector).length) {
        closeFilters();
      }
    }
  });

  $(document).keydown(function(e) {
    // check if dialog element is open and close it after ESCAPE key press
    if (e.keyCode == 27) {
      isFiltersOpen() && closeFilters();
    }
  });
};

export default categoryFilters;
