import Glide from '@glidejs/glide';
import glideArrowDisabler from './utils/glideArrowDisabler';

const canvasSdgCarousel = () => {
  const carouselElems = document.querySelectorAll('.sdg-plugin__carousel');

  carouselElems.forEach((element) => {
    new Glide(element, {
      type: 'slider',
      startAt: 0,
      perView: 6,
      gap: 30,
      rewind: false,
      bound: true,
      peek: {
        before: 30,
        after: 75,
      },
      breakpoints: {
        1300: {
          perView: 5,
          gap: 20,
          peek: {
            before: 30,
            after: 75,
          },
        },
        992: {
          perView: 4,
          gap: 15,
          peek: {
            before: 15,
            after: 75,
          },
        },
        769: {
          perView: 3,
          gap: 15,
          peek: {
            before: 15,
            after: 75,
          },
        },
        500: {
          perView: 2,
          gap: 15,
          peek: {
            before: 15,
            after: 75,
          },
        },
      },
    }).mount({ glideArrowDisabler });
  });
};

export default canvasSdgCarousel;
