var $ = require('jquery');
require('paroller.js');

const parallax = () => (
  $(".parallax-background").paroller({
    factor: 0.5,
    factorXs: 0.2,
  })
);

export default parallax;
