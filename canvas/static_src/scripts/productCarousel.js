import Glide from '@glidejs/glide';
import glideArrowDisabler from './utils/glideArrowDisabler';

const canvasProductCarousel = () => {
  const carouselElems = document.querySelectorAll('.canvas-product-carousel');

  carouselElems.forEach((element) => {
    new Glide(element, {
      type: 'slider',
      startAt: 0,
      perView: 3,
      gap: 30,
      rewind: false,
      bound: true,
      peek: {
        before: 30,
        after: 200
      },
      breakpoints: {
        992: {
          perView: 2,
          peek: {
            before: 15,
            after: 100,
          },
        },
        769: {
          perView: 1,
          peek: {
            before: 15,
            after: 100,
          },
        },
        500: {
          perView: 1,
          peek: {
            before: 15,
            after: 60,
          },
        },
      },
    }).mount({ glideArrowDisabler });
  });
};

export default canvasProductCarousel;
