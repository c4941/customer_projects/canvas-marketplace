var $ = require('jquery');
require('jquery.marquee');

const marquee = () => {
  const topBar = $("#canvas-top-bar");
  const topBarText = $("#canvas-top-bar__text");
  const topBarDismiss = $("#canvas-top-bar__dismiss");

  // Marquee is hidden by default
  const marqueeHidden = sessionStorage.getItem('marquee');

  // If user hasn't dismissed the marquee in the current session
  if (!marqueeHidden) {
    topBar.addClass('is-visible');

    // Adjust body navigation padding when marquee is shown
    $('body').css({ paddingTop: $('.navigation-wrapper').outerHeight() });

    topBarText.marquee({
      //duration in milliseconds of the marquee
      duration: 20000,
      //gap in pixels between the tickers
      gap: 300,
      //time in milliseconds before the marquee will start animating
      delayBeforeStart: 0,
      //'left' or 'right'
      direction: 'left',
      //true or false - should the marquee be duplicated to show an effect of continues flow
      duplicated: true,
      //pause on hover
      pauseOnHover: true,
      //the marquee is visible initially positioned next to the border towards it will be moving
      startVisible: false,
    });
  }

  topBarDismiss.on('click', function() {
    // Add marquee bar hiding to session storage
    sessionStorage.setItem('marquee', 'hidden');

    // Adjust body navigation padding when marquee is hidden
    $('body').css({ paddingTop: ($('.navigation-wrapper').outerHeight() - topBar.outerHeight()) });

    // Animate hiding the marquee bar
    topBar.stop().animate({
      height: "0px",
    }, 300, function() {
      topBar.removeClass('is-visible');
    });
  });
};

export default marquee;
