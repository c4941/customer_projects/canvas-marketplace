import Glide from '@glidejs/glide';

export const productPage = () => {
  // Enable lightbox for products with multiple images. Uses arrows to navigate through images.
  const selector_multiple = "a[data-imagelightbox='image-multiple']";

  if ($(selector_multiple).length > 0) {
    $(selector_multiple).simpleLightbox({ history: false });
  }

  // Enable lightbox for products with only one image.
  const selector = "a[data-imagelightbox='image']";

  if ($(selector).length > 0) {
    $(selector).simpleLightbox({ history: false });
  }

  // Smooth scroll
  $('.product-image-thumbnails.desktop').find('a').on('click', function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: (target.offset().top - 30)
        }, 500, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
};

export const productScrollSpy = () => {
  // Add scrollspy for product image thumbnails on desktop
  if ($('.product-page__main').length) {
    if ($(window).width() > 991) {
      $('body').scrollspy({
        target: '#thumbnail-scrollspy',
        offset: 31,
      });
    }
  }
};

export const productImageMobileCarousel = () => {
  const element = document.querySelector('.product-images__mobile .glide');
  const $navElement = $('#product-images__controls a');

  if (element) {
    const carousel = new Glide(element, {
      type: 'carousel',
      startAt: 0,
      perView: 1,
      gap: 0,
      rewind: true,
      dots: '#product-images__controls',
    }).mount();

    $navElement.on('click', function(e) {
      e.preventDefault();
      const targetSlide = $(this).attr('data-glide-id');
      carousel.go(`=${targetSlide}`);
    });
  }
};
