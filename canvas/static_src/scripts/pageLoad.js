import { gsap } from 'gsap';

const pageLoad = () => {
  const loadingScreen = document.querySelector('.loading-screen');
  const mainNavigationElements = document.querySelectorAll('.canvas-nav__is-animated');
  const mainFooter = document.querySelector('.main-footer');
  const mainContainer = document.querySelector('#main');

  // Function to run on the first page load
  function firstLoadAnimation() {
    return gsap
      .timeline({ delay: .5 })
      .add('start')
      .to(loadingScreen, {
        duration: .5,
        opacity: 0,
        visibility: 'hidden',
        ease: 'power1.out',
      }, 'start')
      .call(contentAnimation, [mainContainer], 'start');
  }

  // Function to animate the content of each page
  function contentAnimation(elem) {
    return gsap
      .timeline()
      .from(mainNavigationElements, {
        duration: .5,
        translateY: -10,
        autoAlpha: 0,
        stagger: 0.05,
        clearProps: 'transform',
      })
      .from([
        elem.querySelector('.is-animated'),
        mainFooter,
      ], {
          duration: .5,
          translateY: 10,
          opacity: 0,
          stagger: 0.05,
        }
      );
  }

  return firstLoadAnimation();
};

export default pageLoad;
