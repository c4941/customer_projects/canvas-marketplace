function loadImage(entries, observer){
    entries.forEach(entry=>{
        if (entry.isIntersecting){
            if (entry.target.tagName.toLowerCase() === "div"){
                entry.target.setAttribute("style", `background-image: url(${entry.target.getAttribute("data-image-src")})`)
            } else if (entry.target.tagName.toLowerCase() === "img"){
                entry.target.setAttribute("src", entry.target.getAttribute("data-image-src"))
            } else {
                console.log(entry.target, entry.target.tagName.toLowerCase())
                throw Error(`${entry.target}can only be implemented on divs and imgs`)
            }
        }
    })
}

export default function lazyLoad(){
    const observer = new IntersectionObserver(loadImage)
    const images = document.querySelectorAll('[data-image-src]')
    Array.from(images).forEach(ele=>{
        observer.observe(ele)
    })   
}
