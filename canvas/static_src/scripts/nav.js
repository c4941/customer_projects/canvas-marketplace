import { dialogTransitionIn, dialogTransitionOut } from './utils/dialogTransition';
import { searchTransitionIn, searchTransitionOut } from './utils/searchTransition';
import setFocus from './utils/setFocus';
import throttle from './utils/throttle';

const mainNav = () => {
  const navWrapperSelector = '.navigation-wrapper';
  const $navWrapperElement = $(navWrapperSelector);

  const navSelector = '#canvas-nav';
  const $navElement = $(navSelector);

  const mobileNavBreakpoint = 991; // Screen-sm-max

  const pageFadeSelector = '.canvas-dialog__page-fade';
  const $pageFadeElement = $(pageFadeSelector);

  // Add classnames for cms links if they have child pages active
  $('li.cms-link.current').parents('li').addClass('current-ancestor');

  // ----------------------------------------------------
  // SEARCH
  // ----------------------------------------------------
  const searchElementSelector = '#canvas-search-dialog';
  const searchTogglerSelector = '.canvas-search-dialog__toggler';
  const $searchToggler = $(searchTogglerSelector);
  const searchCloseBtnSelector = '#canvas-search-dialog__close-btn';
  const $searchCloseBtn = $(searchCloseBtnSelector);
  const searchInputSelector = '#canvas-search-dialog__form-input';
  const $searchInput = $(searchInputSelector);
  const searchSuggestionBtnSelector = '.canvas-search-dialog__popular-terms button';
  const $searchSuggestionBtn = $(searchSuggestionBtnSelector);
  const searchOpenClassname = 'canvas-search-dialog__open';
  const searchPageTogglerSelector = '#canvas-search-page-toggler';
  const $searchPageToggler = $(searchPageTogglerSelector);

  const isSearchOpen = () => {
    return !!$('body').hasClass(searchOpenClassname);
  };

  const openSearch = () => {
    searchTransitionIn(searchElementSelector);
    $('body').addClass(searchOpenClassname);
    setFocus($searchInput);
  };

  const removeSearchClass = () => {
    $('body').removeClass(searchOpenClassname);
  }

  const closeSearch = () => {
    searchTransitionOut(searchElementSelector, removeSearchClass);
    setFocus($searchToggler);
  };

  $searchToggler.on('click', function() {
    openSearch();
  });

  $searchCloseBtn.on('click', function() {
    closeSearch();
  });

  $searchPageToggler.on('click', function() {
    closeSearch();
  });

  // Set suggested search term to search input when clicked
  $searchSuggestionBtn.on('click', function(e) {
    e.preventDefault();
    $searchInput.attr('value', $(this).attr('value'));
    setFocus($searchInput);
  });
  // ----------------------------------------------------
  // END SEARCH
  // ----------------------------------------------------


  // ----------------------------------------------------
  // WISHLIST
  // ----------------------------------------------------
  const wishlistElementSelector = '#canvas-wishlist-dialog';
  const $wishlistElement = $(wishlistElementSelector);
  const wishlistTogglerSelector = '.canvas-wishlist-dialog__toggler';
  const $wishlistToggler = $(wishlistTogglerSelector);
  const wishlistCloseBtnSelector = '#canvas-wishlist-dialog__close-btn';
  const $wishlistCloseBtn = $(wishlistCloseBtnSelector);
  const wishlistOpenClassname = 'canvas-wishlist-dialog__open';

  const isWishlistOpen = () => {
    return !!$('body').hasClass(wishlistOpenClassname);
  };

  const openWishlist = () => {
    dialogTransitionIn(wishlistElementSelector);
    $('body').addClass(wishlistOpenClassname);
    setFocus($wishlistElement);
  };

  const removeWishlistClass = () => {
    $('body').removeClass(wishlistOpenClassname);
  }

  const closeWishlist = () => {
    dialogTransitionOut(wishlistElementSelector, removeWishlistClass);
    setFocus($wishlistToggler);
  };

  $wishlistToggler.on('click', function() {
    openWishlist();
  });

  $wishlistCloseBtn.on('click', function() {
    closeWishlist();
  });
  // ----------------------------------------------------
  // END WISHLIST
  // ----------------------------------------------------


  // ----------------------------------------------------
  // BASKET
  // ----------------------------------------------------
  const basketElementSelector = '#canvas-basket-dialog';
  const $basketElement = $(basketElementSelector);
  const basketTogglerSelector = '.canvas-basket-dialog__toggler';
  const $basketToggler = $(basketTogglerSelector);
  const basketCloseBtnSelector = '#canvas-basket-dialog__close-btn';
  const $basketCloseBtn = $(basketCloseBtnSelector);
  const basketOpenClassname = 'canvas-basket-dialog__open';

  const isBasketOpen = () => {
    return !!$('body').hasClass(basketOpenClassname);
  };

  const openBasket = () => {
    dialogTransitionIn(basketElementSelector);
    $('body').addClass(basketOpenClassname);
    setFocus($basketElement);
  };

  const removeBasketClass = () => {
    $('body').removeClass(basketOpenClassname);
  }

  const closeBasket = () => {
    dialogTransitionOut(basketElementSelector, removeBasketClass);
    setFocus($basketToggler);
  };

  $basketToggler.on('click', function() {
    openBasket();
  });

  $basketCloseBtn.on('click', function() {
    closeBasket();
  });
  // ----------------------------------------------------
  // END BASKET
  // ----------------------------------------------------


  // ----------------------------------------------------
  // MOBILE NAV
  // ----------------------------------------------------
  const mobilenavElementSelector = '#canvas-mobile-nav';
  const $mobilenavElement = $(mobilenavElementSelector);
  const mobilenavTogglerSelector = '.canvas-mobile-nav__toggler';
  const $mobilenavToggler = $(mobilenavTogglerSelector);
  const mobilenavCloseBtnSelector = '#canvas-mobile-nav__close-btn';
  const $mobilenavCloseBtn = $(mobilenavCloseBtnSelector);
  const mobilenavOpenClassname = 'canvas-mobile-nav__open';

  const $mobileNavChildToggler = $('#canvas-mobile-nav .toggle-icon');

  const isMobilenavOpen = () => {
    return !!$('body').hasClass(mobilenavOpenClassname);
  };

  const openMobilenav = () => {
    dialogTransitionIn(mobilenavElementSelector);
    $('body').addClass(mobilenavOpenClassname);
    setFocus($mobilenavElement);
  };

  const removeMobileNavClass = () => {
    $('body').removeClass(mobilenavOpenClassname);
  }

  const closeMobilenav = () => {
    dialogTransitionOut(mobilenavElementSelector, removeMobileNavClass);
    setFocus($mobilenavToggler);
  };

  $mobilenavToggler.on('click', function() {
    openMobilenav();
  });

  $mobilenavCloseBtn.on('click', function() {
    closeMobilenav();
  });

  $pageFadeElement.on('click', function() {
    if (isMobilenavOpen) {
      closeMobilenav();
    }
  });

  $mobileNavChildToggler.on('click', function() {
    const $parent = $(this).parent('li');
    const $child = $parent.find('ul').first();

    if ($parent.hasClass('is-open')) {
      $($child).slideUp(200, function() {
        $(this).attr('aria-expanded', false);
        $parent.removeClass('is-open');
      });
    } else {
      $($child).slideDown(200, function() {
        $(this).attr('aria-expanded', true);
        $parent.addClass('is-open');
      });
    }
  });

  // ----------------------------------------------------
  // END MOBILE NAV
  // ----------------------------------------------------

  // Handle closing dialogs if clicked outside the elements
  $(document).on('click', function(e) {
    // check if search is open and user has not clicked on the search toggle button
    if (isSearchOpen() && !$(e.target).closest(searchTogglerSelector).length) {
      // Don't do anything if clicked inside the search element
      if (!$(e.target).closest(searchElementSelector).length) {
        closeSearch();
      }
    }

    // check if wishlist is open and user has not clicked on the wishlist toggle button
    if (isWishlistOpen() && !$(e.target).closest(wishlistTogglerSelector).length) {
      // Don't do anything if clicked inside the wishlist element
      if (!$(e.target).closest(wishlistElementSelector).length) {
        closeWishlist();
      }
    }

    // check if basket is open and user has not clicked on the basket toggle button
    if (isBasketOpen() && !$(e.target).closest(basketTogglerSelector).length) {
      // Don't do anything if clicked inside the basket element
      if (!$(e.target).closest(basketElementSelector).length) {
        closeBasket();
      }
    }
  });

  $(document).keydown(function(e) {
    // check if dialog element is open and close it after ESCAPE key press
    if (e.keyCode == 27) {
      isSearchOpen() && closeSearch();
      isWishlistOpen() && closeWishlist();
      isBasketOpen() && closeBasket();
      isMobilenavOpen() && closeMobilenav();
    }
  });

  const checkNavPadding = () => {
    const navHeight = $navWrapperElement.outerHeight();
    $('body').css({ paddingTop: navHeight });
  }

  checkNavPadding();

  // Check after window resize if mobile nav needs to be hidden
  throttle("resize", "mobileNavVisibility");
  window.addEventListener("mobileNavVisibility", function() {
    checkNavPadding();

    if (window.outerWidth > mobileNavBreakpoint) {
      closeMobilenav();
    }
  });
};

export default mainNav;
