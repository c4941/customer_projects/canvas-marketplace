import lodash from 'lodash';

// Hide Header on on scroll down
const stickyNav = () => {
  const $navWrapperElem = $('.navigation-wrapper');
  const $navElem = $('#canvas-nav');
  const $marqueeElem = $('#canvas-top-bar');
  const delta = 5;
  let navbarHeight = $navElem.outerHeight();
  let lastScrollTop = 0;

  if ($marqueeElem.hasClass('is-visible')) {
    navbarHeight += $marqueeElem.outerHeight();
  }

  const scrolled = lodash.throttle(hasScrolled, 150);

  $(window).on('scroll', () => {
    scrolled();
  });

  function hasScrolled() {
    const st = $(window).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta) {
      return;
    }

    // Apply background effects after starting to scroll
    if (st > 0) {
      $navElem.addClass('nav-scrolled');
    } else {
      $navElem.removeClass('nav-scrolled');
    }

    if (st > lastScrollTop && st > (navbarHeight)){
      // Scroll Down
      if ($navElem.hasClass('megamenu-open') || $('body').hasClass('canvas-search-dialog__open')) {
        return;
      }
      $navWrapperElem.css({ top: -navbarHeight });
    } else {
      // Scroll Up
      if (st + $(window).height() < $(document).height()) {
        $navWrapperElem.css({ top: 0 });
      }
    }

    lastScrollTop = st;
  }
}

export default stickyNav;
