import lodash from 'lodash';

var $ = require('jquery');
require('jquery-hoverintent/jquery.hoverIntent');

const megaMenu = () => {
  const $mainNav = $('#canvas-nav');
  const $megamenuListElement = $('.canvas-nav__category-menu-items');
  const $megamenuParentListItem = $('.canvas-nav__category-menu-items > li.is-parent');
  const $megamenuBackground = $('#canvas-megamenu__bg');
  const $megamenuContentElem = $('.canvas-megamenu__container');

  const isTouch = 'ontouchstart' in window || !!(navigator.msMaxTouchPoints);

  let megamenuContentMaxHeight = 0;

  const handleMenuItemOpenState = (elem) => {
    elem.addClass('is-open');
  };

  const handleMenuItemCloseState = (elem) => {
    elem.removeClass('is-open');
  };

  const openMegamenu = (bgElem) => {
    $mainNav.addClass('megamenu-open');
    const totalHeightWithNav = $mainNav.outerHeight() + getTallestMenuHeight();
    bgElem.height(totalHeightWithNav);
  };

  const closeMegamenu = (bgElem) => {
    $mainNav.removeClass('megamenu-open');
    bgElem.height(0);
  };

  const getTallestMenuHeight = () => {
    if ($(window).outerWidth() < 992) {
      return 'auto';
    }
    let maxHeight = 0;
    $megamenuContentElem.each((index, item) => {
      var $subcontent = $(item).find('.canvas-megamenu__content').get(0);
      var height = $($subcontent).outerHeight();
      if (height > maxHeight) {
        maxHeight = height;
      }
    });
    return maxHeight;
  };

  const setEachMenuPositionAndHeight = (height) => {
    let offSetLeft = $megamenuListElement.offset().left;
    const offSetRight = 30;

    if ($(window).width() < 1300) {
      offSetLeft = 30;
    }

    if ($mainNav.hasClass('megamenu-open')){
      $megamenuBackground.height($mainNav.outerHeight() + getTallestMenuHeight());
    }

    $megamenuContentElem.each((index, item) => {
      $(item).height(height);
      $(item).css({
        paddingLeft: offSetLeft,
        paddingRight: offSetRight,
      });
    });
  };

  const debouncedClose = lodash.debounce(closeMegamenu, 400);
  const throttledContentHeightCount = lodash.throttle(getTallestMenuHeight, 100);

  window.onload = () =>{
      window.counter = 0;
      setTimeout(function(){
        window.counter++;
      }, 750); // on first page load delay the menu by .75 second.
      $megamenuParentListItem.each((index, listItem) => {
        if (!isTouch) {
          $(listItem).hoverIntent({
            sensitivity: 10,
            interval: 50,
            over: () => {
              debouncedClose.cancel();
              $megamenuParentListItem.removeClass('is-open');
              if (window.counter < 1){
                setTimeout(function(){
                  handleMenuItemOpenState($(listItem));
                  openMegamenu($megamenuBackground);
                  window.counter++;
                }, 750); // on first page load delay the menu by .75 second.
              } else{
                handleMenuItemOpenState($(listItem));
                openMegamenu($megamenuBackground); 
              }
            },
            out: () => {
              handleMenuItemCloseState($(listItem));
              debouncedClose($megamenuBackground, 0);
            },
          });
        } else {
          $(listItem).find('a').first().on('click touch', function(event) {
            if ($(this).hasClass('touched-menuitem')) {
              // If was already touched, open the link
              return;
            } else {
              event.preventDefault(); // Don't open up the link on the first click
    
              $('.touched-menuitem').removeClass('touched-menuitem'); // Clear all touched menuitem classes
              $(this).addClass('touched-menuitem'); // Add touched for this link
    
              if (!$(listItem).hasClass('is-open')) {
                $megamenuParentListItem.removeClass('is-open'); // Clear all other opened menu links
                handleMenuItemOpenState($(listItem));
                openMegamenu($megamenuBackground);
              } else {
                handleMenuItemCloseState($(listItem));
                closeMegamenu($megamenuBackground, 0);
              }
            }
          });
        }
      });
  }

  window.onresize = () => {
    megamenuContentMaxHeight = throttledContentHeightCount();
    setEachMenuPositionAndHeight(megamenuContentMaxHeight);
  };

  megamenuContentMaxHeight = getTallestMenuHeight();
  setEachMenuPositionAndHeight(megamenuContentMaxHeight);

  $(document).on('click', function(e) {
    if (!$(e.target).closest('.canvas-nav__category-menu-items > li.is-parent').length) {
      $('.touched-menuitem').removeClass('touched-menuitem');
      $megamenuParentListItem.removeClass('is-open');
      closeMegamenu($megamenuBackground, 0);
    }
  });
};

export default megaMenu;
