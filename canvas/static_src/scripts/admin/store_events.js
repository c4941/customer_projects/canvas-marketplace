/*
* This file is part of Shuup.
*
* Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
*
* This source code is licensed under the Shuup Commerce Inc -
* SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
* and the Licensee.
 */

// Manage the formsets and shuffle names around to keep them just as Django expects as users
// add/delete formset rows before the final submit event.
function recalculateForms() {
    const rows = $(".events-table .event-row:not(.template-row)");
    const totalForms = rows.length;
    $(".events-table input[name*='-TOTAL_FORMS']").val(totalForms);
    $(".events-table input[name*='-INITIAL_FORMS']").val(0);

    rows.each(function(i, element) {
        $(element).find("input,select").each(function(j, input) {
            const field = $(input);
            ["id", "name"].forEach(function(prop) {
                const value = field.prop(prop);
                if (value.indexOf("__prefix__") >= 0) {
                    field.prop(prop, value.replace("__prefix__", i));
                } else {
                    field.prop(prop, value.replace(/-\d+-/, `-${i}-`));
                }
            });
        });
    });
}

function setUpEvents() {
    $(".events-table .event-row .btn-delete").off().on("click", function(event) {
        $(event.target).closest(".event-row").remove();
        recalculateForms();
    });
}

window.setUpStoreEventScripts = function setUpStoreEventScripts() {
    $(".events-table .btn-add").on("click", function() {
        const newRow = $("#events-template").html().replace("template-row", "");
        let tableBody = $(".events-table table tbody")
        tableBody.append(newRow);
        const dateInput = tableBody.children().children().first().children().children(".form-input-group").children().first();
        // dateInput.datepicker({
        //     "todayHighlight": true,
        // });
        recalculateForms();
        setUpEvents();
    });
    $(() => {
        recalculateForms();
        setUpEvents();
    });
}

export default setUpStoreEventScripts;
