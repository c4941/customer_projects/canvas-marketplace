/** import default javascript from definite theme */
import "intersection-observer"
import("simplelightbox/dist/simple-lightbox.js")
import("../../shuup_definite_theme/static_src/js/custom/update_price.js")
import("../../shuup_definite_theme/static_src/js/custom/product_actions.js")
import("../../shuup_definite_theme/static_src/js/custom/custom.js")

/** add and/or import your custom javascript here */
const mainNav = import('./nav.js')
const marquee = import('./marquee.js')
const megaMenu = import('./megaMenu.js')
import pageLoad from './pageLoad.js'
const parallax = import('./parallax.js')
const lazyLoading = import('./lazyLoading.js')
const stickyNav = import('./stickyNav.js')
const categoryFilters = import('./categoryFilters.js')
const canvasProductCarousel = import('./productCarousel.js')
const { productPage, productScrollSpy, productImageMobileCarousel } = import('./productPage.js')
const CanvasBannerBoxCarousel = import('./bannerBoxCarousel')
const canvasSdgCarousel = ('./sdgCaroconst')
$(() => {
    const isAdminMode = $('body').hasClass('admin-tools-visible');
    if (!isAdminMode) {
        pageLoad();
    }
    lazyLoading.then((res)=>{res.default()})
    marquee.then((res)=>{res.default()})
    parallax.then((res)=>{res.default()})
    mainNav.then((res)=>{res.default()})
    megaMenu.then((res)=>{res.default()})
    stickyNav.then((res)=>{res.default()})
    categoryFilters.then((res)=>{res.default()})
    canvasProductCarousel.then((res)=>{res.default()})
    productPage.then((res)=>{res.default()})
    productScrollSpy.then((res)=>{res.default()})
    productImageMobileCarousel.then((res)=>{res.default()})
    CanvasBannerBoxCarousel.then((res)=>{res.default()})
    canvasSdgCarousel.then((res)=>{res.default()})
    $('[data-toggle="popover"]').popover();
});

$(window).resize(function() {
  productScrollSpy();
});
