import moment from 'moment-timezone';
import "@glidejs/glide/dist/css/glide.core.min.css"
import './styles/index.less';
import './scripts/index';

window.moment = moment;
