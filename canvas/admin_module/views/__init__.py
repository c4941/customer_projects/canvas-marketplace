# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from .dashboard import MultishopSupplierDashboardView
from .messages import CanvasMessageEditView
from .multivendor_orders import CanvasMultivendorShipmentListView, CanvasOrderListView
from .product import (
    CanvasBarcodePrintView,
    MultishopApproveProductView,
    MultishopProductEditView,
    MultishopProductListView,
    MultishopProductsApprovals,
    MultishopRevokeProductApprovalView,
    MultishopVendorProductEditView,
    MultishopVendorProductListView,
)
from .services import (
    MultishopVendorServiceProviderDeleteView,
    MultishopVendorServiceProviderEditView,
    MultishopVendorServiceProviderListView,
    MultishopVendorShippingMethodDeleteView,
    MultishopVendorShippingMethodEditView,
    MultishopVendorShippingMethodListView,
)

__all__ = [
    "CanvasBarcodePrintView",
    "CanvasMessageEditView",
    "CanvasOrderListView",
    "CanvasMultivendorShipmentListView",
    "MultishopApproveProductView",
    "MultishopProductListView",
    "MultishopProductEditView",
    "MultishopProductsApprovals",
    "MultishopRevokeProductApprovalView",
    "MultishopSupplierDashboardView",
    "MultishopVendorProductEditView",
    "MultishopVendorProductListView",
    "MultishopVendorServiceProviderDeleteView",
    "MultishopVendorServiceProviderEditView",
    "MultishopVendorServiceProviderListView",
    "MultishopVendorShippingMethodDeleteView",
    "MultishopVendorShippingMethodEditView",
    "MultishopVendorShippingMethodListView",
]
