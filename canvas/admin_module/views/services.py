# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Any, Union

from django.db.models import QuerySet
from django.http import HttpResponse
from django.utils.translation import gettext as _
from django.views.generic.detail import SingleObjectMixin
from shuup.admin.supplier_provider import get_supplier
from shuup.admin.utils.picotable import Column
from shuup.core.models import ServiceProvider, ShippingMethod
from shuup_multivendor.admin_module.forms.service_providers import MultivendorCustomCarrierForm
from shuup_multivendor.admin_module.views.service_providers import (
    ServiceProviderDeleteView,
    ServiceProviderEditView,
    ServiceProviderListView,
)

from canvas.admin_module.form_parts.services import MultishopVendorShippingMethodBaseFormPart
from canvas.constants import CANVAS_PICK_UP_CARRIER_IDENTIFIER

__all__ = [
    "MultishopVendorServiceProviderEditView",
    "MultishopVendorServiceProviderDeleteView",
    "MultishopVendorServiceProviderListView",
    "MultishopVendorShippingMethodEditView",
    "MultishopVendorShippingMethodListView",
    "MultishopVendorShippingMethodDeleteView",
]

from shuup_multivendor.admin_module.views.services import (
    ShippingMethodDeleteView,
    ShippingMethodEditView,
    ShippingMethodListView,
)


class MultishopVendorGetQuerysetMixin:
    def get_queryset(self) -> "QuerySet[Union[ServiceProvider, ShippingMethod]]":
        qs = SingleObjectMixin.get_queryset(self).filter(supplier=get_supplier(self.request))
        if issubclass(self.model, ShippingMethod):
            qs = qs.exclude(carrier__identifier=CANVAS_PICK_UP_CARRIER_IDENTIFIER)
        return qs


class MultishopVendorServiceProviderEditView(MultishopVendorGetQuerysetMixin, ServiceProviderEditView):
    def form_valid(self, form: MultivendorCustomCarrierForm) -> HttpResponse:
        # assign the shop and supplier for the saved provider
        """Skip the parent, the form handles setting the shops and the supplier."""
        form.instance.supplier = get_supplier(self.request)
        response = super(ServiceProviderEditView, self).form_valid(form)
        return response


class MultishopVendorServiceProviderListView(MultishopVendorGetQuerysetMixin, ServiceProviderListView):
    pass


class MultishopVendorServiceProviderDeleteView(MultishopVendorGetQuerysetMixin, ServiceProviderDeleteView):
    pass


class MultishopVendorShippingMethodEditView(MultishopVendorGetQuerysetMixin, ShippingMethodEditView):
    base_form_part_classes = [MultishopVendorShippingMethodBaseFormPart]


class MultishopVendorShippingMethodListView(MultishopVendorGetQuerysetMixin, ShippingMethodListView):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.columns.append(Column("shop", _("Shop")))


class MultishopVendorShippingMethodDeleteView(MultishopVendorGetQuerysetMixin, ShippingMethodDeleteView):
    pass
