# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import base64
import io
import json
from typing import Any, Callable, Dict, Iterable, Optional, Union
from django.contrib.staticfiles.templatetags.staticfiles import static
import barcode.writer
import six
from django import forms
from django.conf import settings
from django.contrib import messages
from django.db.models import F, OuterRef, Q, QuerySet, Subquery
from django.db.models.functions import Coalesce
from django.http import HttpRequest
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from django.utils.translation import activate, get_language
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView
from django.views.generic.detail import BaseDetailView
from shuup.admin.base import MenuEntry
from shuup.admin.modules.products.views import ProductEditView as BaseProductEditView
from shuup.admin.modules.products.views import ProductListView as BaseProductListview
from shuup.admin.modules.products.views.edit import (
    ProductAttributeFormPart,
    ProductImageMediaFormPart,
    ProductMediaFormPart,
)
from shuup.admin.shop_provider import get_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.admin.toolbar import Toolbar, get_default_edit_toolbar
from shuup.admin.utils.picotable import Column, DateRangeFilter, PicotableMassAction, TextFilter
from shuup.admin.utils.views import CreateOrUpdateView, PicotableListView
from shuup.core.models import Product, ShopProduct, Supplier
from shuup.core.utils import context_cache
from shuup.utils.djangoenv import has_installed
from shuup.utils.form_group import FormGroup
from shuup_multivendor.admin_module.views import ProductEditView as VendorProductEditView
from shuup_multivendor.admin_module.views.products import ProductListView as VendorProductListView
from shuup_multivendor.utils.product import (
    filter_not_approved_shop_products_from_other_vendors,
    filter_product_waiting_approval,
    is_shop_product_approval_required_for_vendor,
    revoke_shop_product_approval,
    set_shop_product_approved,
)

from canvas.admin_module.form_parts import MultishopProductBaseFormPart, MultishopShopProductFormPart
from canvas.models import FailedSyncs, SyncedShopProduct
from canvas.square_sync.client import InventoryClient
from canvas.utils import (
    get_canvas_brick_and_mortar_suppliers,
    get_request_supplier_shops,
    is_canvas_supplier,
    is_request_canvas_supplier,
)

__all__ = [
    "CanvasBarcodePrintView",
    "CanvasProductCombinationsView",
    "MultishopApproveProductView",
    "MultishopProductEditView",
    "MultishopProductListView",
    "MultishopProductsApprovals",
    "MultishopRevokeProductApprovalView",
    "MultishopVendorProductEditView",
    "MultishopVendorProductListView",
]

from shuup_product_variations.admin.views.products import ProductCombinationsView


def format_shops(instance: ShopProduct) -> str:
    return ", ".join(shop_product.shop.name for shop_product in instance.product.shop_products.order_by("shop__pk"))


def format_canvas_stores(instance: ShopProduct) -> str:
    canvas_suppliers = instance.suppliers.filter(pk__in=get_canvas_brick_and_mortar_suppliers())
    return ", ".join(supplier.name for supplier in canvas_suppliers)


def format_stocks(stock_name: str) -> Callable[[ShopProduct], str]:
    def display(instance: ShopProduct) -> str:
        product_id = instance.product.pk
        supplier = instance.suppliers.exclude(pk__in=get_canvas_brick_and_mortar_suppliers()).first()
        if supplier:
            stock_count = getattr(supplier.get_stock_status(product_id=product_id), stock_name)
            return str(round(stock_count, 2))
        return ""

    return display


SHOPS_COLUMN = Column(
    "shops",
    _("Shops"),
    sortable=False,
    display=format_shops,
)

CANVAS_STORES_COLUMN = Column(
    "canvas_stores",
    _("Canvas Stores"),
    sortable=False,
    display=format_canvas_stores,
)

PHYSICAL_STOCKS_COLUMN = Column(
    "physical_stock",
    _("Physical Stock"),
    sortable=False,
    display=format_stocks("physical_count"),
)

LOGICAL_STOCKS_COLUMN = Column(
    "logical_stock",
    _("Logical Stock"),
    sortable=False,
    display=format_stocks("logical_count"),
)


class MultishopProductListView(BaseProductListview):
    default_columns = [
        *BaseProductListview.default_columns,
        SHOPS_COLUMN,
        CANVAS_STORES_COLUMN,
        PHYSICAL_STOCKS_COLUMN,
        LOGICAL_STOCKS_COLUMN,
    ]

    def get_queryset(self) -> "QuerySet[ShopProduct]":
        """
        Only thing that's changed is that we're showing shop products from all
        shops. we still only show one shop product per product to avoid duplication.
        """
        filter = self.get_filter()
        qs = ShopProduct.objects.filter(product__deleted=False)
        q = Q()
        for mode in filter.get("modes", []):
            q |= Q(product__mode=mode)
        manufacturer_ids = filter.get("manufacturers")
        if manufacturer_ids:
            q |= Q(product__manufacturer_id__in=manufacturer_ids)
        qs = qs.filter(q)

        supplier = get_supplier(self.request)
        if supplier:
            qs = qs.filter(suppliers=supplier)

        qs = qs.distinct()

        # Only show the first ShopProduct from each Product.
        # The same Product will exist in multiple Shops, but we want to manage them as one.
        return qs.filter(
            pk__in=Subquery(
                ShopProduct.objects.distinct("product").values("pk"),
            )
        )

    def format_suppliers(self, instance: ShopProduct) -> str:
        return ", ".join(
            supplier.name for supplier in instance.suppliers.exclude(pk__in=get_canvas_brick_and_mortar_suppliers())
        )

    def get_primary_image(self, instance):
        if instance.product.primary_image:
            thumbnail = instance.product.primary_image.get_thumbnail()
            if thumbnail:
                return "<img src='{}://{}{}'>".format(settings.MEDIA_URL, thumbnail)
        return "<img src='%s'>" % static("shuup_admin/img/no_image_thumbnail.png")


class MultishopProductEditView(BaseProductEditView):
    base_form_part_classes = [
        # Edited ones.
        MultishopProductBaseFormPart,
        MultishopShopProductFormPart,
        # Originals.
        ProductAttributeFormPart,
        ProductImageMediaFormPart,
        ProductMediaFormPart,
    ]

    def get_queryset(self) -> "QuerySet[ShopProduct]":
        qs = super(BaseProductEditView, self).get_queryset()  # Skip immediate parent.

        supplier = get_supplier(self.request)
        if supplier:
            qs = qs.filter(suppliers=supplier)

        return qs


class MultishopVendorProductListView(VendorProductListView):
    """Customizes the parent to handle showing products from all shops at once."""

    default_columns = [
        *VendorProductListView.default_columns[:7],  # Slice away the default stocks column.
        SHOPS_COLUMN,
        CANVAS_STORES_COLUMN,
        PHYSICAL_STOCKS_COLUMN,
        LOGICAL_STOCKS_COLUMN,
    ]

    def get_queryset(self) -> "QuerySet[ShopProduct]":
        """
        Only thing that's changed is that we're showing shop products from all
        shops. we still only show one shop product per product to avoid duplication.
        """
        filter = self.get_filter()
        qs = ShopProduct.objects.filter(product__deleted=False)

        query = Q()
        for mode in filter.get("modes", []):
            query |= Q(product__mode=mode)

        manufacturer_ids = filter.get("manufacturers")
        if manufacturer_ids:
            query |= Q(product__manufacturer_id__in=manufacturer_ids)

        qs = qs.filter(query)

        vendor = get_supplier(self.request)
        if not settings.VENDOR_CAN_SHARE_PRODUCTS:
            qs = qs.filter(suppliers=vendor)

        if is_shop_product_approval_required_for_vendor(vendor):
            # Approval required so filter out products that is waiting approval
            # for some other vendor
            qs = filter_not_approved_shop_products_from_other_vendors(qs, vendor)

        qs = qs.distinct()

        # Only take the first ShopProduct from each Product.
        # The same Product will exist in multiple Shops, but we want to manage them as one.
        return qs.filter(
            pk__in=Subquery(
                ShopProduct.objects.distinct("product").values("pk"),
            )
        )

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["title"] = "Products"
        return context

    def get_toolbar(self) -> Optional[Toolbar]:
        if not is_request_canvas_supplier(self.request):
            return super().get_toolbar()
        return None  # Canvas stores don't need to create products.


class MultishopVendorProductEditView(VendorProductEditView):
    def get_queryset(self) -> "QuerySet[ShopProduct]":
        """Overridden to show shop products from all shops."""
        qs = super(BaseProductEditView, self).get_queryset()  # Skipping two parents.
        qs = qs.filter(suppliers=get_supplier(self.request))
        return qs

    def form_valid(self, form: FormGroup) -> HttpResponse:
        shops = get_request_supplier_shops(self.request)
        assert shops

        for shop in shops:
            if form.forms[f"shop{shop.pk}"].cleaned_data.get("supplier_price") is not None:
                break
        else:
            error = forms.ValidationError(_("You need to select at least one shop to sell your product in."))
            for shop in shops:
                form.forms[f"shop{shop.pk}"].add_error("supplier_price", error)
            return self.form_invalid(form)

        return super().form_valid(form)

    def get_success_url(self) -> Optional[str]:
        next_url = self.request.POST.get("__next")
        if next_url == "return":
            return reverse("shuup_admin:shuup_multivendor.products_list")
        elif next_url == "new":
            return reverse("shuup_admin:shuup_multivendor.products_new")

        try:
            shop_product = self.object.get_shop_instance(get_shop(self.request), allow_cache=True)
        except ShopProduct.DoesNotExist:
            shop_product = self.object.shop_products.first()
            if not shop_product:
                raise

        return reverse("shuup_admin:shuup_multivendor.products_edit", kwargs=dict(pk=shop_product.pk))

    def get_toolbar(self) -> Optional[Toolbar]:
        """Overridden to remove everything but the delete and save buttons from the toolbar."""
        if self.object.pk:
            delete_url = reverse("shuup_admin:shuup_multivendor.products_delete", kwargs=dict(pk=self.object.pk))
        else:
            delete_url = None
        return get_default_edit_toolbar(self, self.get_save_form_id(), delete_url=delete_url)


class MultishopApproveMassAction(PicotableMassAction):
    label = _("Set approved")
    identifier = "mass_action_product_approved"

    def process(self, request: HttpRequest, ids: Union[str, Iterable[int]]) -> None:
        shop_products = ShopProduct.objects.all()

        if not isinstance(ids, str) or ids != "all":
            # Given ShopProduct ids of e.g. [1], we find all other ShopProducts
            # that share the same Products as the given ids.
            product_ids = ShopProduct.objects.filter(pk__in=ids).values("product__pk")
            shop_products = shop_products.filter(product__pk__in=product_ids)

        for shop_product in shop_products.iterator():
            set_shop_product_approved(shop_product, request.user)
            context_cache.bump_cache_for_shop_product(shop_product)


class MultishopProductsApprovals(MultishopProductListView):
    mass_actions = ["canvas.admin_module.views.product:MultishopApproveMassAction"]
    toolbar_buttons_provider_key = ""
    mass_actions_provider_key = ""

    def get_queryset(self) -> "QuerySet[ShopProduct]":
        queryset = super().get_queryset().filter(suppliers__in=Supplier.objects.enabled())
        return filter_product_waiting_approval(queryset)

    def get_toolbar(self):
        return None

    @staticmethod
    def get_breadcrumb_parents():
        return [MenuEntry(text=_("Approve Vendor Products"), url="shuup_admin:shuup_multivendor.products_approvals")]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = _("Vendor products waiting for approval")
        return context


class MultishopApprovalsBaseView(DetailView):
    model = Product
    context_object_name = "product"

    def get_queryset(self):
        return super().get_queryset().filter(shop_products__shop=get_shop(self.request))

    def do_action(self, request):
        pass

    def get(self, request, *args, **kwargs):
        return self.do_action(request)


class MultishopApproveProductView(MultishopApprovalsBaseView):
    def do_action(self, request):
        """Overridden to approved the product in all shops at once."""
        shop_product = None
        for shop_product in self.get_object().shop_products.iterator():
            set_shop_product_approved(shop_product, request.user)
            context_cache.bump_cache_for_shop_product(shop_product)

        if shop_product:
            messages.success(request, _("%s has been approved successfully.") % shop_product.product)
            return HttpResponseRedirect(reverse("shuup_admin:shop_product.edit", kwargs=dict(pk=shop_product.pk)))


class MultishopRevokeProductApprovalView(MultishopApprovalsBaseView):
    def do_action(self, request):
        """Overridden to unapproved the product in all shops at once."""
        shop_product = None
        for shop_product in self.get_object().shop_products.iterator():
            revoke_shop_product_approval(shop_product, request.user)
            context_cache.bump_cache_for_shop_product(shop_product)

        if shop_product:
            messages.success(request, _("Approval for %s has been revoked successfully.") % shop_product.product)
            return HttpResponseRedirect(reverse("shuup_admin:shop_product.edit", kwargs=dict(pk=shop_product.pk)))


class CanvasBarcodePrintView(BaseDetailView):
    model = Product

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        code = self.get_object().barcode or "-"
        svg = self._get_barcode_svg(code)
        data_url = mark_safe(self._svg_to_data_url(svg))
        quantity = int(request.GET.get("qty", 1))
        html = render_to_string(
            "canvas/admin/barcodes/barcode_print_pdf.jinja",
            context=dict(
                request=request,
                quantity=quantity,
                barcode_url=data_url,
            ),
        )
        # return HttpResponse(html)
        return self._barcode_html_to_pdf_response(html)

    @staticmethod
    def _barcode_html_to_pdf_response(html: str) -> HttpResponse:
        try:
            import weasyprint
        except ImportError:
            return JsonResponse({"error": _("Couldn't print labels. Weasyprint not installed.")}, status=400)

        if not html:
            return JsonResponse({"error": _("Couldn't generate label. Invalid html.")}, status=400)

        pdf_data = weasyprint.HTML(string=html).write_pdf()
        return HttpResponse(content=pdf_data, content_type="application/pdf")

    @staticmethod
    def _get_barcode_svg(code: str) -> bytes:
        """
        :param code: The barcode string, e.g. "ABCD123456"
        :return: The barcode SVG image as a safe string that can be rendered with `{{ svg }}` in a template.
        """
        buffer = io.BytesIO()
        svg_code = barcode.get("upc", code, writer=barcode.writer.SVGWriter())
        svg_code.write(buffer, options={"write_text": True})
        return buffer.getvalue()

    @staticmethod
    def _svg_to_data_url(svg: bytes) -> str:
        """
        Allows Weasyprint to render an SVG image.

        Reference: https://github.com/Kozea/WeasyPrint/issues/75#issuecomment-60531025
        """
        encoded = base64.b64encode(svg).decode()
        return f"data:image/svg+xml;charset=utf-8;base64,{encoded}"


class CanvasProductCombinationsView(ProductCombinationsView):
    def _sync_inventory(self):
        combinations = json.loads(self.request.body)
        if combinations:
            shop_product = ShopProduct.objects.filter(product__sku=combinations[0]["sku"]).first()
            if hasattr(shop_product, "synced_shop_product"):
                updated_count: str = combinations[0]["stock_count"]
                supplier = get_supplier(self.request)
                if is_canvas_supplier(supplier):
                    InventoryClient().sync_inventory(shop_product, supplier, updated_count)

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        self._sync_inventory()
        return response

    def get(self, request, *args, **kwargs):
        """The parent method has 'simple_supplier' hardcoded in its check for stock management status.
        We need 'canvas_supplier' instead.
        """
        self.object = self.get_object()
        combinations_data = []
        product_data = []
        product_ids = set()
        old_language = get_language()
        activate(settings.PARLER_DEFAULT_LANGUAGE_CODE)

        shop = get_shop(request)
        shop_product = self.object.get_shop_instance(shop)
        supplier = get_supplier(request)
        if not supplier:
            supplier = shop_product.suppliers.first()

        if not supplier:
            return JsonResponse({"combinations": [], "product_data": []})

        for combination in self.object.get_all_available_combinations():
            product_id = combination["result_product_pk"]
            if not product_id:
                continue

            product_ids.add(product_id)
            combinations_data.append(
                {
                    "product": product_id,
                    "sku_part": combination["sku_part"],
                    "hash": combination["hash"],
                    "combination": {
                        force_text(k): force_text(v) for k, v in six.iteritems(combination["variable_to_value"])
                    },
                }
            )

        is_simple_supplier_installed = has_installed("shuup.simple_supplier")

        stock_managed = bool(
            is_simple_supplier_installed
            and supplier.supplier_modules.filter(module_identifier="canvas_supplier").exists()
            and supplier.stock_managed
        )

        is_multivendor_installed = has_installed("shuup_multivendor")

        base_queryset = ShopProduct.objects.filter(shop=shop, product_id__in=product_ids)
        if stock_managed and is_multivendor_installed:
            from shuup.simple_supplier.models import StockCount
            from shuup_multivendor.models import SupplierPrice

            product_data = base_queryset.annotate(
                sku=F("product__sku"),
                price=Coalesce(
                    Subquery(
                        SupplierPrice.objects.filter(
                            shop=shop, supplier=supplier, product_id=OuterRef("product_id")
                        ).values("amount_value")[:1]
                    ),
                    0,
                ),
                stock_count=Coalesce(
                    Subquery(
                        StockCount.objects.filter(product_id=OuterRef("product_id"), supplier=supplier).values(
                            "logical_count"
                        )[:1]
                    ),
                    0,
                ),
            ).values("pk", "product_id", "sku", "price", "stock_count")
        elif stock_managed:
            from shuup.simple_supplier.models import StockCount

            product_data = base_queryset.annotate(
                sku=F("product__sku"),
                price=F("default_price_value"),
                stock_count=Coalesce(
                    Subquery(StockCount.objects.filter(product_id=OuterRef("product_id")).values("logical_count")[:1]),
                    0,
                ),
            ).values("pk", "product_id", "sku", "price", "stock_count")
        elif is_multivendor_installed:
            from shuup_multivendor.models import SupplierPrice

            product_data = base_queryset.annotate(
                sku=F("product__sku"),
                price=Coalesce(
                    Subquery(
                        SupplierPrice.objects.filter(
                            shop=shop, supplier=supplier, product_id=OuterRef("product_id")
                        ).values("amount_value")[:1]
                    ),
                    0,
                ),
            ).values("pk", "product_id", "sku", "price")
        else:
            product_data = base_queryset.annotate(
                sku=F("product__sku"),
                price=F("default_price_value"),
            ).values("pk", "product_id", "sku", "price")

        activate(old_language)
        return JsonResponse({"combinations": combinations_data, "product_data": list(product_data)})


class FailedProductSyncsListView(PicotableListView):
    model = FailedSyncs
    url_identifier = "failed_syncs"
    mass_actions = ["canvas.admin_module.views.product:FailedSyncsRetryMassAction"]
    columns = [
        Column(
            "shop_product",
            _("Product"),
            ordering=1,
            sortable=True,
            filter_config=TextFilter(
                filter_field="shop_product__translations__name", placeholder="Filter by product..."
            ),
        ),
        Column(
            "timestamp",
            _("Time of failure"),
            ordering=2,
            sortable=True,
            filter_config=DateRangeFilter(),
        ),
        Column(
            "problem",
            _("Reason of failure"),
            ordering=3,
            sortable=True,
        ),
    ]


class FailedSyncsRetryMassAction(PicotableMassAction):
    label = _("Attempt to sync again")
    identifier = "resync"

    def process(self, request: HttpRequest, ids: Union[str, Iterable[int]]) -> None:
        failed_syncs = FailedSyncs.objects.all()

        if not isinstance(ids, str) or ids != "all":
            failed_syncs = failed_syncs.filter(pk__in=ids)

        shop_product_ids = failed_syncs.values("shop_product")

        # Set the SyncedShopProduct to sync next time and delete the FailedSyncs
        SyncedShopProduct.objects.filter(shop_product_id__in=shop_product_ids).update(is_synced=False)
        failed_syncs.delete()


class FailedProductSyncsEditView(CreateOrUpdateView):
    model = FailedSyncs
    fields = ("shop_product", "problem")
    template_name = "canvas/admin/products/failed_syncs.jinja"
    context_object_name = "failed_sync"
