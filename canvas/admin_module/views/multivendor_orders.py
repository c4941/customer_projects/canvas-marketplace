# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.conf import settings
from django.db.models import Max, Q
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Order, OrderLine, Shipment, ShipmentStatus
from shuup_multivendor.admin_module.sections import VendorOrderDetailsSection, VendorOrderShipmentSection
from shuup_multivendor.admin_module.views import OrderLineListView, OrderListView
from shuup_multivendor.admin_module.views.shipments import (
    MultivendorOrderCreateShipmentView,
    MultivendorShipmentListView,
    MultivendorShipmentSetSentView,
)

from canvas.utils import is_canvas_supplier


class CanvasOrderListView(OrderListView):
    def get_queryset(self):
        supplier = get_supplier(self.request)
        if is_canvas_supplier(supplier):
            return (
                super(OrderListView, self)
                .get_queryset()
                .filter(shipping_method__identifier__contains=f"store-{supplier.pk}")
                .annotate(Max("id"))
                .order_by()
            )
        # List all orders that the vendor is responsible for handling.
        return super().get_queryset().exclude(shipping_method__identifier__contains="store")


class CanvasVendorOrderDetailsSection(VendorOrderDetailsSection):
    @classmethod
    def visible_for_object(cls, order, request):
        supplier = get_supplier(request)
        if is_canvas_supplier(supplier):
            return f"store-{supplier.pk}" in order.shipping_method.identifier

        return order.lines.filter(supplier=supplier).exists()


class CanvasVendorOrderShipmentSection(VendorOrderShipmentSection):
    @classmethod
    def visible_for_object(cls, order, request):
        if not order.shipping_method:
            return False
        if not order.shipping_method.carrier.uses_default_shipments_manager:
            return False

        supplier = get_supplier(request)
        if is_canvas_supplier(supplier):
            return f"store-{supplier.pk}" in order.shipping_method.identifier

        return (
            order.has_products_requiring_shipment(supplier=supplier)
            or Shipment.objects.filter(order=order, supplier=supplier).exists()
        )

    @classmethod
    def get_context_data(cls, order, request):
        context = super().get_context_data(order, request)
        supplier = context["suppliers"][0]
        if is_canvas_supplier(supplier):
            # We want the "real" supplier, the brand who supplied the store. Not the store itself.
            brand = order.supplier_order.supplier
            context["suppliers"] = [brand]
            context["create_urls"][brand.pk] = context["create_urls"][supplier.pk].replace(
                str(supplier.pk), str(brand.pk)
            )
        return context


class CanvasOrderLineListView(OrderLineListView):
    def get_queryset(self):
        supplier = get_supplier(self.request)
        if is_canvas_supplier(supplier):
            lines = (
                super(OrderLineListView, self)
                .get_queryset()
                .filter(order=self.order, type__in=settings.SHUUP_MULTIVENDOR_SHOW_VENDOR_ORDER_LINE_TYPES)
            )
            if not self.request.user.is_superuser:
                lines = lines.filter(order__shipping_method__identifier__contains=f"store-{supplier.pk}")

            return lines

        return super().get_queryset().exclude(order__shipping_method__identifier__contains=f"store-{supplier.pk}")


class CanvasMultivendorShipmentListView(MultivendorShipmentListView):
    def get_queryset(self):
        supplier = get_supplier(self.request)
        if supplier and is_canvas_supplier(supplier):
            queryset = (
                self.model.objects.filter(order__shipping_method__identifier__contains=f"store-{supplier.pk}")
                .exclude(status=ShipmentStatus.DELETED)
                .select_related("order", "order__shipping_method")
            )
        else:
            queryset = (
                super()
                .get_queryset()
                .exclude(Q(status=ShipmentStatus.DELETED) | Q(order__shipping_method__identifier__contains="store"))
                .select_related("order", "order__shipping_method")
            )
        return queryset


class CanvasMultivendorOrderCreateShipmentView(MultivendorOrderCreateShipmentView):
    def get_queryset(self):
        supplier_pk = self._get_supplier_id()
        supplier = get_supplier(self.request)
        supplier_is_store = is_canvas_supplier(supplier)
        if supplier_pk != supplier.pk and not supplier_is_store:
            # It's ok to not match if `supplier` is a store.
            return Order.objects.none()

        order_pk = int(self.kwargs["pk"])
        if supplier_is_store:
            if not OrderLine.objects.filter(
                order__shipping_method__identifier__contains=f"store-{supplier.pk}", order_id=order_pk
            ).exists():
                return Order.objects.none()

        else:
            if not OrderLine.objects.filter(supplier=supplier, order_id=order_pk).exists():
                return Order.objects.none()

        return Order.objects.all()


class CanvasMultivendorShipmentSetSentView(MultivendorShipmentSetSentView):
    def get_queryset(self):
        supplier = get_supplier(self.request)
        if is_canvas_supplier(supplier):
            return Shipment.objects.filter(order__shipping_method__identifier__contains=f"store-{supplier.pk}")
        return super().get_queryset()
