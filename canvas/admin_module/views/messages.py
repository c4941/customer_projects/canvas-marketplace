from shuup_messages.admin.edit import MessageEditView

from canvas.admin_module.forms import CanvasMessageForm

__all__ = [
    "CanvasMessageEditView",
]


class CanvasMessageEditView(MessageEditView):
    form_class = CanvasMessageForm
