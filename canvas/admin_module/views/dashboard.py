# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import datetime
from typing import Any, Dict

import shuup.utils.dates
from django.views.generic import TemplateView
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Order
from shuup_multivendor.models import WITHDRAW_ORDER_LINE_ACCOUNTING_IDENTIFIER

from canvas.admin_module.modules import MultishopVendorSalesDashboardModule
from canvas.admin_shop_provider import switch_shop
from canvas.dashboard.blocks import (
    get_favorite_categories_block,
    get_favorite_products_block,
    get_lifetime_sales_block,
    get_new_customers_block,
    get_returning_customers_block,
    get_vendor_order_chart_dashboard_block,
)
from canvas.utils import get_supplier_shops

__all__ = [
    "MultishopSupplierDashboardView",
]


class MultishopSupplierDashboardView(TemplateView):
    """
    Simplified vendor dashboard view which for now only shows vendor funds from all shops.

    In the future this will show aggregated order data from all of the vendor's shops as well.
    """

    template_name = "shuup_multivendor/admin/dashboard/dashboard.jinja"

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)

        supplier = get_supplier(self.request)
        if not supplier:
            return context

        shops = get_supplier_shops(supplier)

        # For daily sales blocks let's use 4am as a cutoff time
        # 1. Before 4am we show sales from 4am yesterday
        # 2. After 4am we show sales from 4am today
        local_now = shuup.utils.dates.local_now()
        four_am_today = local_now.replace(hour=4, minute=0, second=0)
        blocks = [
            ("block_30d", datetime.timedelta(days=30)),
            ("block_7d", datetime.timedelta(days=7)),
            (
                "block_daily",
                (four_am_today if local_now > four_am_today else four_am_today - datetime.timedelta(days=1)),
            ),
        ]

        context["blocks"] = MultishopVendorSalesDashboardModule().get_dashboard_blocks(request=self.request)

        for block_id, delta in blocks:
            cutoff_date = (local_now - delta) if isinstance(delta, datetime.timedelta) else delta

            completed_orders = (
                Order.objects.valid()
                .filter(order_date__gte=cutoff_date, lines__supplier=supplier)
                .exclude(lines__accounting_identifier=WITHDRAW_ORDER_LINE_ACCOUNTING_IDENTIFIER)
                .distinct()
            )

            completed_order_ids = completed_orders.values_list("id", flat=True)

            context[block_id] = [
                get_new_customers_block(completed_order_ids, supplier, cutoff_date),
                get_returning_customers_block(completed_order_ids, supplier, cutoff_date),
                get_favorite_categories_block(self.request, supplier, completed_order_ids),
                get_favorite_products_block(self.request, supplier, completed_order_ids),
            ]

            for i, shop in enumerate(shops):
                sales_block = get_lifetime_sales_block(
                    completed_orders.filter(shop=shop).values_list("id", flat=True), supplier, shop
                )
                context[block_id].insert(
                    2 + i, sales_block
                )  # between returning customers and favorite categories blocks
                if block_id == "block_30d":
                    with switch_shop(self.request, shop):
                        context[block_id].append(get_vendor_order_chart_dashboard_block(self.request, shop))

            return context
