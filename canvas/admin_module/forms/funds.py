# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Any, Dict, Iterable

from django import forms
from django.db.models import QuerySet
from django.utils.html import linebreaks
from django.utils.safestring import SafeText, mark_safe
from django.utils.translation import gettext as _
from shuup.core.models import Order, OrderLine, Shop
from shuup_multivendor.admin_module.form_parts.funds_pending import PendingWithdrawalsForm
from shuup_multivendor.admin_module.forms.funds import FundsBaseForm, PaypalForm, WireForm, WireFormEU
from shuup_multivendor.models import WITHDRAW_ORDER_LINE_ACCOUNTING_IDENTIFIER, VendorFunds
from shuup_multivendor.utils.funds import get_withdraw_help_text

__all__ = [
    "MultishopPendingWithdrawalsForm",
    "MultishopWireForm",
    "MultishopWireFormEU",
    "MultishopPaypalForm",
]


def get_help_text_or_default(shop: Shop) -> str:
    no_help_text_set = '<span class="text-muted">(no help text)</span>'
    return linebreaks(get_withdraw_help_text(shop).strip() or no_help_text_set)


class MultishopPendingWithdrawalsForm(PendingWithdrawalsForm):
    def __init__(self, *, shops: Iterable[Shop], **kwargs: Any) -> None:
        super().__init__(**kwargs)
        del self.shop
        self.shops = shops

    def get_withdraw_help_text(self) -> SafeText:
        """Overridden to get the help text from all of the shops."""
        help_texts = (f"<strong>{shop.name}</strong><br>{get_help_text_or_default(shop)}" for shop in self.shops)
        return mark_safe("<br>".join(help_texts))

    def get_withdrawals_queryset(self) -> "QuerySet[Order]":
        """Overridden to show withdrawals from all shops."""
        order_ids = OrderLine.objects.filter(
            order__shop__in=self.shops,
            supplier_id=self.vendor,
            accounting_identifier=WITHDRAW_ORDER_LINE_ACCOUNTING_IDENTIFIER,
        ).values_list("order_id", flat=True)
        return Order.objects.filter(id__in=order_ids)


class MultishopWithdrawalBaseForm(FundsBaseForm):
    def __init__(self, *args: Any, shops: Iterable[Shop], **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        del self.shop
        self.shops = shops
        # TODO: do that change also for vendor dashboard (show shop funds instead of currency funds)
        choices = [(shop.pk, f"{shop.name} ({shop.currency})") for shop in self.shops]
        self.fields["shop"] = forms.ChoiceField(choices=[("", "-" * 8)] + choices)
        self.fields.move_to_end("shop", last=False)

    def clean(self) -> Dict[str, Any]:
        self.shop = Shop.objects.filter(pk=self.cleaned_data.get("shop")).first()
        return super().clean()

    def get_withdraw_help_text(self) -> SafeText:
        """Overridden to get the help text from all of the shops

        This also include the available funds in the same text block for nice formatting.
        """
        help_texts = []
        for shop in self.shops:
            text = get_withdraw_help_text(shop)
            if text:
                help_texts.append((shop, linebreaks(text)))

        funds_title = _("available funds")
        template = "<strong>{shop_name}</strong> - {funds_title}: {available_funds}<br>{help_text}"
        help_texts = []
        for shop in self.shops:
            help_texts.append(
                template.format(
                    shop_name=shop.name,
                    funds_title=funds_title,
                    available_funds=VendorFunds.get_funds_formatted(shop, self.vendor),
                    help_text=get_help_text_or_default(shop),
                )
            )
        return mark_safe("<br>".join(help_texts))


class MultishopWireForm(WireForm, MultishopWithdrawalBaseForm):
    """
    Subclassed to allow selecting the shop to withdraw from.

    The method resolution order of this class is:
        - MultishopWireForm
        - WireForm
        - MultishopWithdrawalBaseForm
        - FundsBaseForm
        - Form
        - BaseForm
        - object
    """


class MultishopWireFormEU(WireFormEU, MultishopWithdrawalBaseForm):
    """Subclassed to allow selecting the shop to withdraw from."""


class MultishopPaypalForm(PaypalForm, MultishopWithdrawalBaseForm):
    """Subclassed to allow selecting the shop to withdraw from."""
