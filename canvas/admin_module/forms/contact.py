# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.translation import ugettext_lazy as _
from shuup.core.utils.forms import MutableAddressForm


class CanvasMutableAddressForm(MutableAddressForm):
    def __init__(self, **kwargs):
        super(CanvasMutableAddressForm, self).__init__(**kwargs)
        self.fields["name"].label = _("Full Name")
        self.fields["name_ext"].label = _("Suffix (Jr., Sr., etc.)")
