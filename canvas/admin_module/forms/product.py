# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Any, Optional

from django import forms
from django.db.models import Q
from django.forms import HiddenInput
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _
from shuup.admin.forms.fields import Select2ModelField, Select2ModelMultipleField
from shuup.admin.modules.products.forms import ProductBaseForm, ShopProductForm
from shuup.admin.shop_provider import get_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Category, Product, ShippingMethod, Shop, ShopProduct, Supplier
from shuup.customer_group_pricing.models import CgpDiscount
from shuup_multivendor.admin_module.forms.product import (
    VendorProductBaseForm,
    VendorShopProductForm,
    get_discount_contact_groups,
)
from shuup_multivendor.models import SupplierPrice
from shuup_multivendor.signals import vendor_shop_product_saved
from shuup_multivendor.utils.configuration import get_product_tax_class_options

from canvas.forms import CanvasProductExtraForm
from canvas.square_sync.client import set_barcode
from canvas.templatetags.canvas import round_stock_value
from canvas.utils import get_supplier_shops

__all__ = [
    "CanvasBarcodesForm",
    "MultishopProductBaseForm",
    "MultishopShopProductForm",
    "MultishopVendorProductBaseForm",
    "MultishopVendorShopProductForm",
    "ONLY_MAIN_SHOP_FIELDS",
]

ONLY_MAIN_SHOP_FIELDS = ("primary_category", "categories")


class MultishopProductBaseForm(ProductBaseForm):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        self.fields["type"].widget.attrs["data-minimum-input-length"] = 0

    def save(self, commit=True):
        if not self.instance.barcode:
            set_barcode(self.instance)
        return super().save()


class MultishopShopProductForm(ShopProductForm):
    def __init__(
        self, *, request: HttpRequest, shop: Optional[Shop] = None, is_main_shop: Optional[bool], **kwargs: Any
    ) -> None:

        # Need to be saved here before super().__init__() edits them.
        initial_primary_category = kwargs.get("initial", {}).get("primary_category")
        initial_categories = kwargs.get("initial", {}).get("categories")

        super().__init__(**kwargs)  # Don't pass the `request` arg here so that it will not use it to filter the shops.
        self.shop = shop
        self.request = request

        self.fields["default_price_value"].required = False
        self.fields["default_price_value"].label = _("Retail price ({currency})").format(currency=self.shop.currency)

        if self.instance.pk:
            # Restore these to their passed correct values.
            self.initial["primary_category"] = initial_primary_category
            self.initial["categories"] = initial_categories
        else:
            # Don't show 0 here, since only None means that the product is not sold in that shop.
            self.initial["default_price_value"] = None

        if is_main_shop:
            # Fetch the choice lists immediately without first needing to type 3 characters.
            self.fields["categories"].widget.attrs["data-minimum-input-length"] = 0
            self.fields["primary_category"].widget.attrs["data-minimum-input-length"] = 0
        else:
            for field in ONLY_MAIN_SHOP_FIELDS:
                self.fields.pop(field)

    def save(self, commit=True):
        default_price_value = self.cleaned_data.get("default_price_value")

        if default_price_value is None:  # Explicit comparison, since 0 is theoretically valid.
            # A None value means that this product is not sold in this shop at all.
            if self.instance.pk:
                self.instance.delete()
                self.instance.product.supplierprice_set.filter(shop=self.shop).delete()
            return None
        else:
            self.instance.product.supplierprice_set.filter(shop=self.shop).update(amount_value=default_price_value)

        super().save(commit)


class MultishopVendorShopProductForm(VendorShopProductForm):
    """
    Customizes the parent to enabling editing the product in any specific shop,
    instead of the one from the request.
    """

    def __init__(self, *, request: HttpRequest, shop: Shop, is_main_shop: bool, **kwargs: Any) -> None:
        super(VendorShopProductForm, self).__init__(**kwargs)  # On purpose skipping immediate parent.

        self.request = request
        self.shop = shop
        self.supplier = get_supplier(self.request)
        assert shop in get_supplier_shops(self.supplier)

        # An empty value for this means that the vendor has chosen not to sell in that shop at all.
        # Change: the above may be true in the future; for now, the US shop is required and the EU shop is not
        # (because the EU shop is not valid at the moment).
        self.fields["supplier_price"].required = is_main_shop
        self.fields["supplier_price"].label = _("Retail price ({currency})").format(currency=self.shop.currency)
        self.fields["supplier_price"].help_text = _(
            "This is the default individual base unit price of the product. "
            "All commission fees will be calculated from this price as well."
        )
        if self.instance:
            self.fields["supplier_price"].widget.initial = self.instance.default_price_value

        # Don't show field; however, it is required, so give it a default value.
        self.fields["purchase_multiple"].widget = HiddenInput()
        self.fields["purchase_multiple"].widget.initial = 0

        if self.instance.pk:
            supplier_price = SupplierPrice.objects.filter(
                shop=shop, supplier=self.supplier, product=self.instance.product
            ).first()

            if supplier_price:
                initial_price = supplier_price.amount_value
            else:
                initial_price = self.instance.default_price_value

            self.fields["supplier_price"].initial = initial_price

            groups = get_discount_contact_groups()
            discount = CgpDiscount.objects.filter(product=self.instance.product, shop=shop, group__in=groups).first()
            if discount:
                self.fields["discount_amount"].initial = discount.discount_amount.as_rounded().value

        shipping_methods_qs = ShippingMethod.objects.filter(
            Q(shop=shop) & Q(enabled=True) & (Q(supplier=self.supplier) | Q(supplier__isnull=True))
        )
        self.fields["shipping_methods"].queryset = shipping_methods_qs

        if is_main_shop:
            # These are fields that we want to select only once and copy to every shop product.
            self.fields["primary_category"] = Select2ModelField(model=Category, required=True)
            self.fields["primary_category"].widget.attrs["data-minimum-input-length"] = 0
            self.fields["primary_category"].widget.choices = [
                (cat.pk, cat.name) for cat in [self.initial.get("primary_category")] if cat
            ]

            self.fields["categories"] = Select2ModelMultipleField(model=Category, required=False)
            self.fields["categories"].widget.attrs["data-minimum-input-length"] = 0
            self.fields["categories"].widget.choices = [
                (cat.pk, cat.name) for cat in self.initial.get("categories", []) if cat
            ]
        else:
            for field in ONLY_MAIN_SHOP_FIELDS:
                self.fields.pop(field)

        # Remove fields that the client requested to be removed for vendors.
        self.fields.pop("minimum_purchase_quantity")
        self.fields.pop("visibility")

    def save(self, commit: bool = True) -> Optional[ShopProduct]:
        supplier_price_value = self.cleaned_data.get("supplier_price")
        if supplier_price_value is None:  # Explicit comparison, since 0 is theoretically valid.
            # A None value means that the vendor has chosen not to sell in this shop at all.
            if self.instance.pk:
                self.instance.delete()
            return None

        SupplierPrice.objects.update_or_create(
            shop=self.shop,
            supplier=self.supplier,
            product=self.instance.product,
            defaults=dict(amount_value=supplier_price_value),
        )

        # In this project we want to keep these two values in sync.
        self.instance.default_price_value = supplier_price_value

        instance = super(VendorShopProductForm, self).save(commit)  # On purpose skipping immediate parent.
        instance.suppliers.add(self.supplier)

        groups = get_discount_contact_groups()
        if self.cleaned_data.get("discount_amount"):
            for group in groups:
                CgpDiscount.objects.update_or_create(
                    product=instance.product,
                    shop=self.shop,
                    group=group,
                    defaults=dict(discount_amount_value=self.cleaned_data["discount_amount"]),
                )
        else:
            CgpDiscount.objects.filter(product=instance.product, shop=self.shop, group__in=groups).delete()

        vendor_shop_product_saved.send(ShopProduct, shop_product=self.instance, request=self.request)
        return instance


class MultishopVendorProductBaseForm(VendorProductBaseForm):
    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)

        # Remove fields that the client requested to be removed for vendors.
        self.fields.pop("tax_class")
        self.fields.pop("gtin")
        self.fields.pop("manufacturer")
        self.fields.pop("barcode")

        # Change labels
        self.fields["width"].label = _("Package width (mm)")
        self.fields["height"].label = _("Package height (mm)")
        self.fields["depth"].label = _("Package depth (mm)")

        if not self.instance.pk:
            # Don't show fields; however, some are required, so give them default values.
            self.fields["shipping_mode"].widget = HiddenInput()
            self.fields["width"].widget = HiddenInput()
            self.fields["width"].widget.initial = 0
            self.fields["height"].widget = HiddenInput()
            self.fields["height"].widget.initial = 0
            self.fields["depth"].widget = HiddenInput()
            self.fields["depth"].widget.initial = 0
            self.fields["sales_unit"].widget = HiddenInput()
            self.fields["net_weight"].widget = HiddenInput()
            self.fields["net_weight"].widget.initial = 0
            self.fields["gross_weight"].widget = HiddenInput()
            self.fields["gross_weight"].widget.initial = 0

    def save(self, commit=True):
        # `get_product_tax_class_options` is typed to return an Iterable, but actually returns a QuerySet.
        self.instance.tax_class = get_product_tax_class_options(get_shop(self.request)).first()
        if not self.instance.barcode:
            set_barcode(self.instance)
        return super().save()


class CanvasBarcodesForm(forms.ModelForm):
    quantity = forms.IntegerField(min_value=1, initial=1, label=_("Quantity to print"))

    class Meta:
        model = Product
        fields = ("barcode",)

    def __init__(self, *, request: HttpRequest, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.fields["barcode"].disabled = True

        supplier: Optional[Supplier] = get_supplier(request)
        if supplier:
            physical_count = supplier.get_stock_status(self.instance.pk).physical_count
            self.fields["physical_stock"] = forms.CharField(
                label=_("Current physical stock"),
                initial=round_stock_value(physical_count),
                disabled=True,
            )
            self.initial["quantity"] = int(physical_count)


class CanvasProductExtraSettingsForm(CanvasProductExtraForm):
    pass
