# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Any

from django.utils.translation import ugettext_lazy as _

from canvas.utils import get_request_supplier_shops

__all__ = ["MultishopVendorShippingMethodForm"]

from shuup_multivendor.admin_module.views.services import ShippingMethodForm


class MultishopVendorShippingMethodForm(ShippingMethodForm):
    class Meta(ShippingMethodForm.Meta):
        fields = ShippingMethodForm.Meta.fields + ["shop"]
        exclude = [field for field in ShippingMethodForm.Meta.exclude if field != "shop"]

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.fields["shop"].queryset = get_request_supplier_shops(self.request)
        self.fields["tax_class"].empty_label = None
        if not self.instance.pk:
            self.initial["enabled"] = True

        # Adjust labels
        self.fields["enabled"].label = _("Enable for Checkout")

    def save(self, commit=True):
        shop = self.cleaned_data["shop"]  # `BaseMethodForm._save_master` will overwrite this, so back it up here.
        instance = super().save(commit=False)
        instance.shop = shop
        instance.save()
        return instance
