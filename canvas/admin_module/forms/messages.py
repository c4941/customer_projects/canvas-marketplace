from django import forms
from django.conf import settings
from django.db.models import QuerySet
from django.utils.translation import gettext as _
from shuup.admin.shop_provider import get_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Supplier
from shuup_messages.admin.edit import FIXED_TARGET_KEYS, MessageForm

from canvas.utils import get_canvas_brick_and_mortar_suppliers, is_request_canvas_supplier

__all__ = [
    "CanvasMessageForm",
]

from shuup_messages.multishop_shops_provider import get_multishop_shops


class CanvasMessageForm(MessageForm):
    def __init__(self, *args, **kwargs):  # noqa C901
        """
        Overridden to allow vendors to message to Canvas store vendors and vice versa.

        Most of this is copied as is from the parent class.
        Check the Git blame to see changed parts that were done in a second commit.
        """
        self.request = kwargs.pop("request")
        self.supplier = get_supplier(self.request)
        self.shop = get_shop(self.request)
        found_target = False
        for target_key in FIXED_TARGET_KEYS:
            val = kwargs.pop(target_key, None)
            if val:
                setattr(self, target_key, val)
                found_target = True
                break

        super(MessageForm, self).__init__(*args, **kwargs)  # On purpose skipping immediate parent.
        if settings.SHUUP_ENABLE_MULTIPLE_SHOPS:
            self.fields["to_shop"] = forms.ModelChoiceField(
                label=_("To Shop"),
                queryset=get_multishop_shops(self.request.user),
                required=False,
            )

        if not found_target:
            # No fixed target for this message so let's figure out available
            # options for shop staff or supplier.
            user = self.request.user
            if self.supplier:
                # We do not have fixed to_shop but supplier is selected.
                #
                # This means we allow current user to select shop to send
                # this message to.
                self.fields["to_shop"] = forms.ModelChoiceField(
                    label=_("To Shop"),
                    queryset=self.supplier.shops.all(),
                    required=False,
                )
            elif getattr(user, "is_superuser", False):
                pass  # Superuser can send to all suppliers
            elif not settings.SHUUP_ENABLE_MULTIPLE_SHOPS:
                self.fields.pop("to_shop")

            if not settings.SHUUP_ENABLE_MULTIPLE_SUPPLIERS:
                self.fields.pop("to_supplier")  # No multiple suppliers means only shop to shop messaging
            elif self.shop and self.shop.staff_members.filter(id=self.request.user.pk).exists():
                # We do not have fixed to_supplier but this message is from
                # shop staff.
                #
                # This means we allow current user to select supplier to send
                # this message to.
                self.fields["to_supplier"] = forms.ModelChoiceField(
                    label=_("To Supplier"),
                    queryset=self.shop.suppliers.enabled(),
                    required=False,
                )
            elif getattr(user, "is_superuser", False):
                pass  # Superuser can send to all suppliers
            else:
                self._build_to_supplier_field()
        else:
            self.fields.pop("to_shop")
            self.fields.pop("to_supplier")

    def _build_to_supplier_field(self) -> "QuerySet[Supplier]":
        """
        Return the suppliers that the current supplier is allowed to message to.

        For a normal vendor this will return all the Canvas store suppliers.
        For a Canvas store supplier this will return all the normal vendors.
        """
        canvas_store_suppliers = get_canvas_brick_and_mortar_suppliers()
        if is_request_canvas_supplier(self.request):
            self.fields["to_supplier"].queryset = Supplier.objects.exclude(pk__in=canvas_store_suppliers)
        else:
            self.fields["to_supplier"].label = _("To Canvas store")
            self.fields["to_supplier"].queryset = canvas_store_suppliers
