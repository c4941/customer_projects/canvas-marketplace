# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import re
from typing import Any

from django.forms import CharField, DateField, Form, HiddenInput, ModelForm, formset_factory
from django.forms.fields import URLField
from django.http import HttpRequest
from django.utils.translation import ugettext_lazy as _
from shuup.admin.supplier_provider import get_supplier

from canvas.forms import CanvasSupplierExtraForm
from canvas.models import CanvasStoreExtra

__all__ = [
    "CanvasSupplierExtraSettingsForm",
    "CanvasStoreExtraForm",
    "CanvasStoreEventsForm",
    "CanvasStoreEventsFormSet",
]


class CanvasSupplierExtraSettingsForm(CanvasSupplierExtraForm):
    def __init__(self, *, request: HttpRequest, **kwargs: Any) -> None:
        self.vendor = kwargs.pop("vendor", None)
        super().__init__(**kwargs)
        if get_supplier(request):
            self.fields["referrer"].disabled = True

    def clean_social_media_handles(self):
        handles = self.cleaned_data["social_media_handles"]
        return ", ".join(re.findall(r"@[0-9a-zA-Z._][0-9a-zA-Z._]*", handles))

    def save(self, commit=True):
        super().save(commit=commit)
        self.instance.set_sustainability_goals(self.cleaned_data.get("sustainability_goals"))


class CanvasStoreExtraForm(ModelForm):
    class Meta:
        model = CanvasStoreExtra
        exclude = ("supplier",)

    def __init__(self, *args, **kwargs):
        self.vendor = kwargs.pop("vendor", None)
        super().__init__(*args, **kwargs)
        self.fields["spotify_uri"].label = _("Spotify album or playlist URI")

    def save(self, commit=True):
        self.instance.supplier = self.vendor
        return super().save(commit=commit)


class CanvasStoreEventsForm(Form):
    date = DateField()
    location = CharField(max_length=64)
    eventbrite_url = URLField(max_length=256)
    event_id = CharField(required=False, widget=HiddenInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["date"].widget.input_type = "date"


CanvasStoreEventsFormSet = formset_factory(CanvasStoreEventsForm, extra=1)
