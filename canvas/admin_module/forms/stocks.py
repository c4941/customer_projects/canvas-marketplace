import itertools
from typing import Any

from django import forms
from django.utils.translation import gettext as _
from shuup.core.models import Product, ProductMode, Supplier
from shuup.simple_supplier.admin_module.forms import SimpleSupplierForm
from shuup.simple_supplier.models import StockCount

from canvas.templatetags.canvas import round_stock_value
from canvas.utils import formfield, get_canvas_brick_and_mortar_suppliers

__all__ = [
    "CanvasSimpleSupplierForm",
    "CanvasStoreStocksForm",
]


class CanvasStoreStocksForm(forms.Form):
    delta = forms.DecimalField(
        label=_("Adjust stock quantity"),
        help_text=_(
            "Enter the amount that you want to adjust your stocks by. " "To decrease stocks, use a negative value."
        ),
        required=False,
    )

    def __init__(self, product: Product, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.product = product

        if not self.product.pk:
            raise TypeError("Error! This form can only be used with already saved Products.")

        canvas_suppliers = get_canvas_brick_and_mortar_suppliers()

        shop_product_suppliers = Supplier.objects.filter(pk__in=self.product.shop_products.values("suppliers__pk"))
        non_store_suppliers = shop_product_suppliers.exclude(pk__in=canvas_suppliers)

        if non_store_suppliers.count() != 1:
            raise AssertionError("Error! The product shouldn't be shared between vendors.")

        self.supplier = non_store_suppliers.first()

        # This is accessed in the template.
        self.initial_store_suppliers = canvas_suppliers.filter(pk__in=shop_product_suppliers)

        self.fields["canvas_stores"] = forms.ModelMultipleChoiceField(
            queryset=canvas_suppliers,
            initial=self.initial_store_suppliers,
            required=False,
            label=_("Canvas Stores"),
            widget=forms.CheckboxSelectMultiple,
            help_text=_("Select the Canvas brick and mortar store locations you want to supply your products to."),
        )

        if self.product.mode in {ProductMode.SIMPLE_VARIATION_PARENT, ProductMode.VARIABLE_VARIATION_PARENT}:
            self.products = self.product.variation_children.all()
        elif self.product.mode == ProductMode.VARIATION_CHILD:
            self.products = self.product.variation_parent.variation_children.all()
        else:  # Normal, non-variation product.
            for field in ("logical_count", "physical_count"):
                stocks_dict = self.supplier.get_stock_statuses(product_ids=[self.product.pk])
                initial = round_stock_value(getattr(stocks_dict[self.product.pk], field))
                self.fields[field] = formfield(StockCount, field, disabled=True, initial=initial)

            self.products = [self.product]

        self.product_stocks = {}

        for product in self.products:
            suppliers = {}
            for store_supplier in itertools.chain(self.initial_store_suppliers, [self.supplier]):
                suppliers[store_supplier] = store_supplier.get_stock_status(product_id=product.pk)
            self.product_stocks[product] = suppliers

    def clean_canvas_stores(self):
        canvas_stores = self.cleaned_data["canvas_stores"]
        for store_supplier in self.initial_store_suppliers:
            stocks = store_supplier.get_stock_statuses(product_ids=[p.pk for p in self.products])
            if any(stock.logical_count > 0 for stock in stocks.values()):
                if store_supplier not in canvas_stores:
                    raise forms.ValidationError(
                        _("You cannot deselect a store while it still has logical stock for this product.")
                    )
        return canvas_stores


class CanvasSimpleSupplierForm(SimpleSupplierForm):
    def get_suppliers(self, product):
        return Supplier.objects.filter(
            shop_products__product=product, supplier_modules__module_identifier="canvas_supplier"
        ).distinct()

    def can_manage_stock(self):
        return Supplier.objects.filter(
            supplier_modules__module_identifier="canvas_supplier", stock_managed=True
        ).exists()
