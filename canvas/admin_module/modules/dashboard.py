# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import datetime

from django.http import HttpRequest
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from shuup.admin.base import AdminModule
from shuup.admin.dashboard import DashboardMoneyBlock, DashboardNumberBlock
from shuup.admin.shop_provider import get_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.admin.utils.urls import admin_url
from shuup.core.models import Order, Shop, Supplier

from canvas.utils import get_supplier_shops

__all__ = [
    "MultishopVendorSalesDashboardModule",
]

from shuup_multivendor.models import VendorFunds
from shuup_multivendor.utils.funds import is_vendor_funds_enabled


class MultishopVendorSalesDashboardModule(AdminModule):
    """
    Vendor dashboard which shows aggregate data from all shops at once.

    At the moment this doesn't offer any vendor sales related blocks, those can/will be
    added at a later point.

    This uses the multivendor URL names so that all existing links in templates
    work fine. If this were to be pulled to its own addon, the URL names and paths
    should be changed for sure.
    """

    def get_dashboard_blocks(self, request):
        supplier = get_supplier(request)
        if supplier:
            shops = get_supplier_shops(supplier)
        else:
            shops = [get_shop(request)]

        if supplier:
            for shop in shops:
                if is_vendor_funds_enabled(shop):
                    yield get_vendor_funds_block(shop, supplier)
        else:
            yield get_active_vendors_block()

        yield get_recent_orders_block(request, _("New Orders"))
        yield get_recent_orders_block(request, _("In Delivery"), mode="non_delivered")

    name = _("Multishop Vendor Sales Dashboard")

    def get_urls(self):
        return [
            admin_url(
                r"^multivendor/dashboard/$",
                "canvas.admin_module.views:MultishopSupplierDashboardView",
                name="shuup_multivendor.dashboard.supplier",
            )
        ]


def get_vendor_funds_block(shop: Shop, vendor: Supplier) -> DashboardMoneyBlock:
    """
    Exactly like `shuup_multivendor.dashboards.funds.get_vendor_funds_block`,
    but takes customized the title and formats the shop's currency in it.
    """
    return DashboardMoneyBlock(
        id="vendor_funds_block",
        color="green",
        title=_("{shop} - Available Funds ({currency})").format(shop=shop.name, currency=shop.currency),
        value=VendorFunds.get_funds(shop, vendor).value,
        currency=shop.currency,
    )


# All of the below functions are redefined versions of functions from `shuup_multivendor.dashboards.module`
# that take the `shop` as an argument instead of taking it from the `request.


def get_active_vendors_block() -> DashboardNumberBlock:
    return DashboardNumberBlock(
        id="vendor_count_block",
        color="orange",
        title=_("Vendors"),
        value=Supplier.objects.enabled().count(),
        icon="fa fa-user",
        subtitle=_("All time"),
    )


def get_recent_orders_block(
    request: HttpRequest,
    title: str,
    hours_ago: int = 24,
    mode: str = "non_completed",
) -> DashboardNumberBlock:
    orders = Order.objects.all()

    if mode == "non_delivered":
        orders = orders.incomplete()
    elif mode == "non_completed":
        orders = orders.all()

    past = timezone.localtime(timezone.now()) - datetime.timedelta(hours=hours_ago)

    orders = orders.filter(created_on__gte=past)
    if not request.user.is_superuser:
        supplier = get_supplier(request)
        orders = orders.filter(lines__supplier=supplier)

    block = DashboardNumberBlock(
        id=f"recent_orders_{mode}",
        color="orange",
        title=title,
        value=orders.count(),
        icon="fa fa-user",
        subtitle=_("Recent %(type)s orders in the past %(number_of_hours)s hours")
        % {"type": mode.replace("_", " "), "number_of_hours": hours_ago},
    )
    return block
