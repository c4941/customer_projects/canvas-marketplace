# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import List

from shuup.admin.utils.urls import AdminRegexURLPattern, admin_url, get_edit_and_list_urls
from shuup_multivendor.admin_module.modules.service_providers import MultivendorServiceProviderModule
from shuup_multivendor.admin_module.modules.services import MultivendorShippingMethodModule

__all__ = [
    "MultishopVendorServiceProviderModule",
    "MultishopVendorShippingMethodModule",
]


class MultishopVendorServiceProviderModule(MultivendorServiceProviderModule):
    def get_urls(self) -> List[AdminRegexURLPattern]:
        return [
            admin_url(
                r"^multivendor/service_provider/(?P<pk>\d+)/delete/$",
                "canvas.admin_module.views:MultishopVendorServiceProviderDeleteView",
                name="multivendor.service_provider.delete",
            ),
        ] + get_edit_and_list_urls(
            url_prefix="^multivendor/service_provider",
            view_template="canvas.admin_module.views:MultishopVendorServiceProvider%sView",
            name_template="multivendor.service_provider.%s",
        )


class MultishopVendorShippingMethodModule(MultivendorShippingMethodModule):
    def get_urls(self) -> List[AdminRegexURLPattern]:
        return [
            admin_url(
                r"^multivendor/shipping_method/(?P<pk>\d+)/delete/$",
                "canvas.admin_module.views:MultishopVendorShippingMethodDeleteView",
                name="multivendor.shipping_method.delete",
            ),
        ] + get_edit_and_list_urls(
            url_prefix="^multivendor/shipping_method",
            view_template="canvas.admin_module.views:MultishopVendorShippingMethod%sView",
            name_template="multivendor.shipping_method.%s",
        )
