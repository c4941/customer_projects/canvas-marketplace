# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.translation import ugettext_lazy as _
from shuup.admin.base import AdminModule, MenuEntry
from shuup.admin.menu import SETTINGS_MENU_CATEGORY
from shuup.admin.utils.urls import admin_url, derive_model_url, get_edit_and_list_urls

from canvas.models import SustainabilityGoal


class SustainabilityGoalModule(AdminModule):
    name = _("Sustainable Development Goals")
    breadcrumbs_menu_entry = MenuEntry(name, url="shuup_admin:sustainability_goal.list")

    def get_urls(self):
        urls = get_edit_and_list_urls(
            url_prefix="^sustainability_goal",
            view_template="canvas.admin_module.views.sustainability_goals.SustainabilityGoal%sView",
            name_template="sustainability_goal.%s",
        ) + [
            admin_url(
                r"^sustainability_goal/delete/(?P<pk>\d+)/$",
                "canvas.admin_module.views.sustainability_goals.SustainabilityGoalDeleteView",
                name="sustainability_goal.delete",
            )
        ]
        return urls

    def get_menu_entries(self, request):
        return [
            MenuEntry(
                text=self.name,
                icon="fa fa-list",
                url="shuup_admin:sustainability_goal.list",
                category=SETTINGS_MENU_CATEGORY,
            )
        ]

    def get_model_url(self, object, kind, shop=None):
        return derive_model_url(SustainabilityGoal, "shuup_admin:sustainability_goal", object, kind)
