# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import List, Optional, Union

from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpRequest
from django.utils.translation import gettext_lazy as _
from shuup.admin.base import AdminModule, MenuEntry
from shuup.admin.menu import PRODUCTS_MENU_CATEGORY
from shuup.admin.modules.products import ProductModule
from shuup.admin.supplier_provider import get_supplier
from shuup.admin.utils.urls import AdminRegexURLPattern, admin_url, derive_model_url, get_edit_and_list_urls
from shuup.core.models import Product, Shop, ShopProduct
from shuup_multivendor.admin_module import MultivendorProductsAdminModule, MultivendorProductsApprovalAdminModule

__all__ = [
    "CanvasProductVariationsModule",
    "FailedProductSyncsModule",
    "MultishopProductModule",
    "MultishopVendorProductsAdminModule",
    "MultishopProductsApprovalAdminModule",
]

from shuup_product_variations.admin import ProductVariationsModule

from canvas.models import FailedSyncs


class MultishopProductModule(ProductModule):
    def get_urls(self) -> List[AdminRegexURLPattern]:
        blacklisted = {"shop_product.list", "shop_product.edit", "shop_product.new"}
        urls = super().get_urls()
        urls = [url for url in urls if url.name not in blacklisted]
        urls.extend(
            [
                admin_url(
                    r"^products/$",
                    "canvas.admin_module.views:MultishopProductListView",
                    name="shop_product.list",
                ),
                admin_url(
                    r"^products/(?P<pk>\d+)/edit/$",
                    "canvas.admin_module.views:MultishopProductEditView",
                    name="shop_product.edit",
                ),
                admin_url(
                    r"^products/new/$",
                    "canvas.admin_module.views:MultishopProductEditView",
                    name="shop_product.new",
                ),
                admin_url(
                    r"^products/(?P<pk>\d+)/barcodes/$",
                    "canvas.admin_module.views:CanvasBarcodePrintView",
                    name="canvas.barcode_print",
                ),
            ]
        )
        return urls

    def get_model_url(
        self, object: Union[Product, ShopProduct], kind: str, shop: Optional[Shop] = None
    ) -> Optional[str]:
        """
        Overridden to attempt to fall back to any shop product of the product didn't
        have a shop product in the passed shop.

        This is useful when a vendor's product only exists in the non-primary shop
        and we still want to be able to get an URL for it.
        """
        if isinstance(object, Product):
            shop_product = None
            if shop:
                try:
                    shop_product = object.get_shop_instance(shop)
                except ObjectDoesNotExist:
                    pass
            if not shop_product:
                shop_product = object.shop_products.first()
        else:
            shop_product = object

        if not shop_product:
            return None

        return derive_model_url(ShopProduct, "shuup_admin:shop_product", shop_product, kind)


class MultishopVendorProductsAdminModule(MultivendorProductsAdminModule):
    """
    Vendor admin module which handles managing products in all shops at the same time.

    This uses the multivendor URL names so that we can reuse most of the stuff from the
    existing views. If this were to be pulled to its own addon, the URL names and paths
    should be changed for sure.
    """

    name = _("Multishop Vendor Products")
    _public_name = _("Products")
    breadcrumbs_menu_entry = MenuEntry(_public_name, url="shuup_admin:shuup_multivendor.products_list")

    def get_urls(self) -> List[AdminRegexURLPattern]:
        return [
            admin_url(
                r"^multivendor/products/$",
                "canvas.admin_module.views.MultishopVendorProductListView",
                name="shuup_multivendor.products_list",
            ),
            admin_url(
                r"^multivendor/products/new/$",
                "canvas.admin_module.views.MultishopVendorProductEditView",
                name="shuup_multivendor.products_new",
                kwargs={"pk": None},
            ),
            admin_url(
                r"^multivendor/products/(?P<pk>\d+)/$",
                "canvas.admin_module.views.MultishopVendorProductEditView",
                name="shuup_multivendor.products_edit",
            ),
            admin_url(
                r"^multivendor/products/(?P<pk>\d+)/delete/$",
                # The standard delete view already works with shop products across all shops.
                "shuup_multivendor.admin_module.views.ProductDeleteView",
                name="shuup_multivendor.products_delete",
            ),
        ]

    def get_menu_entries(self, request: HttpRequest) -> List[MenuEntry]:
        if not get_supplier(request):
            return []

        return [
            MenuEntry(
                text=self._public_name,
                icon="fa fa-image",
                url="shuup_admin:shuup_multivendor.products_list",
                category=PRODUCTS_MENU_CATEGORY,
            )
        ]


class MultishopProductsApprovalAdminModule(MultivendorProductsApprovalAdminModule):
    """
    Admin module which handles managing approving products from all shops at the same time.

    This uses the multivendor URL names so that we can reuse most of the stuff from the
    existing views. If this were to be pulled to its own addon, the URL names and paths
    should be changed for sure.
    """

    def get_urls(self) -> List[AdminRegexURLPattern]:
        return [
            admin_url(
                r"^multivendor/products/approvals/$",
                "canvas.admin_module.views:MultishopProductsApprovals",
                name="shuup_multivendor.products_approvals",
            ),
            admin_url(
                r"^multivendor/products/approvals/(?P<pk>\d+)/approve/$",
                "canvas.admin_module.views:MultishopApproveProductView",
                name="shuup_multivendor.products_approvals_approve",
            ),
            admin_url(
                r"^multivendor/products/approvals/(?P<pk>\d+)/revoke/$",
                "canvas.admin_module.views:MultishopRevokeProductApprovalView",
                name="shuup_multivendor.products_approvals_revoke",
            ),
        ]


class CanvasProductVariationsModule(ProductVariationsModule):
    def get_urls(self):
        urls = super().get_urls()

        # There are a lot of admin urls. Replace the combinations one.
        for i, url in enumerate(urls):
            if "combination" in url.name:
                break
        urls.pop(i)
        urls.append(
            admin_url(
                r"^shuup_product_variations/(?P<pk>\d+)/combinations/$",
                "canvas.admin_module.views.product.CanvasProductCombinationsView",
                name="shuup_product_variations.product.combinations",
            )
        )

        return urls


class FailedProductSyncsModule(AdminModule):
    name = _("Failed Product Syncs")
    breadcrumbs_menu_entry = MenuEntry(text=name, url="shuup_admin:failed_syncs.list", category=PRODUCTS_MENU_CATEGORY)

    def get_urls(self):
        return get_edit_and_list_urls(
            url_prefix="^failed_syncs",
            view_template="canvas.admin_module.views.product.FailedProductSyncs%sView",
            name_template="failed_syncs.%s",
        )

    def get_menu_entries(self, request):
        return [
            MenuEntry(
                text=_("Failed product syncs"),
                icon="fa fa-times-circle",
                url="shuup_admin:failed_syncs.list",
                category=PRODUCTS_MENU_CATEGORY,
            )
        ]

    def get_model_url(self, object, kind, shop=None):
        return derive_model_url(FailedSyncs, "shuup_admin:failed_syncs", object, kind)
