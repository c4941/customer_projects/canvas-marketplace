from shuup.admin.urls import admin_url
from shuup_messages.admin import MessageModule

__all__ = [
    "CanvasMessageModule",
]


class CanvasMessageModule(MessageModule):
    def get_urls(self):
        """Overridden to change the `MessageEditView` class."""
        return [
            admin_url(r"^messages/all/$", "shuup_messages.admin.list.AllMessagesListView", name="message.all_messages"),
            admin_url(
                r"^messages/received/$", "shuup_messages.admin.list.MessageReceivedListView", name="message.received"
            ),
            admin_url(r"^messages/sent/$", "shuup_messages.admin.list.MessageSentListView", name="message.sent"),
            admin_url(
                r"^messages/new/$",
                "canvas.admin_module.views:CanvasMessageEditView",
                kwargs={"pk": None},
                name="message.new",
            ),
            admin_url(
                r"^messages/new/to_vendor/(?P<to_supplier_id>\d+)/$",
                "canvas.admin_module.views:CanvasMessageEditView",
                kwargs={"pk": None},
                name="message.new_to_supplier",
            ),
            admin_url(
                r"^messages/new/to_staff/(?P<to_shop_id>\d+)/$",
                "canvas.admin_module.views:CanvasMessageEditView",
                kwargs={"pk": None},
                name="message.new_to_staff",
            ),
            admin_url(
                r"^messages/new/to_contact/(?P<to_contact_id>\d+)/$",
                "canvas.admin_module.views:CanvasMessageEditView",
                kwargs={"pk": None},
                name="message.new_to_contact",
            ),
            admin_url(
                r"^messages/(?P<pk>\d+)/$", "canvas.admin_module.views:CanvasMessageEditView", name="message.edit"
            ),
            admin_url(
                r"^messages/(?P<pk>\d+)/mark_as_read/$",
                "shuup_messages.admin.actions.MarkAsReadAction",
                name="message.mark_as_read",
            ),
            admin_url(
                r"^messages/(?P<pk>\d+)/mark_as_unread/$",
                "shuup_messages.admin.actions.MarkAsUnreadAction",
                name="message.mark_as_unread",
            ),
            admin_url(
                r"^messages/(?P<pk>\d+)/delete/$",
                "shuup_messages.admin.actions.MessageDeleteView",
                name="message.delete",
            ),
        ]
