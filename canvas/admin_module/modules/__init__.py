# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from .dashboard import MultishopVendorSalesDashboardModule
from .messages import CanvasMessageModule
from .multivendor_orders import CanvasMultivendorOrdersAdminModule
from .product import (
    CanvasProductVariationsModule,
    FailedProductSyncsModule,
    MultishopProductModule,
    MultishopProductsApprovalAdminModule,
    MultishopVendorProductsAdminModule,
)
from .services import MultishopVendorServiceProviderModule, MultishopVendorShippingMethodModule
from .sustainability_goals import SustainabilityGoalModule

__all__ = [
    "CanvasMessageModule",
    "CanvasMultivendorOrdersAdminModule",
    "CanvasProductVariationsModule",
    "FailedProductSyncsModule",
    "MultishopProductModule",
    "MultishopProductsApprovalAdminModule",
    "MultishopVendorProductsAdminModule",
    "MultishopVendorSalesDashboardModule",
    "MultishopVendorServiceProviderModule",
    "MultishopVendorShippingMethodModule",
    "SustainabilityGoalModule",
]
