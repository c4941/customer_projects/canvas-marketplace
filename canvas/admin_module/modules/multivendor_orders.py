# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.admin.utils.urls import admin_url
from shuup_multivendor.admin_module import MultivendorOrdersAdminModule

__all__ = [
    "CanvasMultivendorOrdersAdminModule",
]


class CanvasMultivendorOrdersAdminModule(MultivendorOrdersAdminModule):
    blacklist = [
        "shuup_multivendor.order_list",
        "shuup_multivendor.order.shipments.list",
        "shuup_multivendor.order_line_list",
        "shuup_multivendor.create-shipment",
        "shuup_multivendor.set-shipment-sent",
    ]

    def get_urls(self):
        urls = [url for url in super().get_urls() if url.name not in self.blacklist]
        urls.append(
            admin_url(
                r"^multivendor/orders/$",
                "canvas.admin_module.views.multivendor_orders.CanvasOrderListView",
                name="shuup_multivendor.order_list",
            ),
        )
        urls.append(
            admin_url(
                r"^multivendor/shipments/$",
                "canvas.admin_module.views.multivendor_orders.CanvasMultivendorShipmentListView",
                name="shuup_multivendor.order.shipments.list",
            ),
        )
        urls.append(
            admin_url(
                r"^multivendor/orders/(?P<pk>\d+)/lines$",
                "canvas.admin_module.views.multivendor_orders.CanvasOrderLineListView",
                name="shuup_multivendor.order_line_list",
            ),
        )
        urls.append(
            admin_url(
                r"^multivendor/orders/(?P<pk>\d+)/create-shipment/(?P<supplier_pk>\d+)/$",
                "canvas.admin_module.views.multivendor_orders.CanvasMultivendorOrderCreateShipmentView",
                name="shuup_multivendor.create-shipment",
            ),
        )
        urls.append(
            admin_url(
                r"^multivendor/shipments/(?P<pk>\d+)/set-sent/$",
                "canvas.admin_module.views.multivendor_orders.CanvasMultivendorShipmentSetSentView",
                name="shuup_multivendor.set-shipment-sent",
            ),
        )
        return urls
