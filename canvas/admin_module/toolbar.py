# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.safestring import mark_safe
from shuup.admin.utils.forms import flatatt_filter
from shuup.admin.utils.permissions import has_permission
from shuup_multivendor.admin_module.toolbar import ApproveProductAction, RevokeProductApprovalAction

__all__ = [
    "MultishopApproveProductAction",
    "MultishopRevokeProductApprovalAction",
]

from shuup_multivendor.utils.product import is_shop_product_approved


class MultishopApproveProductAction(ApproveProductAction):
    def render(self, request):
        """
        Overridden to just check the first shop product, since all shop products will be approved
        and unapproved at once. We also might not always have shop instance for the current request shop.
        """
        shop_product = self.product.shop_products.first()
        if has_permission(
            request.user, "shuup_multivendor.products_approvals_approve"
        ) and not is_shop_product_approved(shop_product):
            attrs = {
                "class": "dropdown-item",
                "title": self.tooltip,
                "href": self.url,
                "onclick": (mark_safe(self.onclick) if self.onclick else None),
            }
            yield "<a %s>" % flatatt_filter(attrs)
            yield self.render_label()
            yield "</a>"


class MultishopRevokeProductApprovalAction(RevokeProductApprovalAction):
    def render(self, request):
        """
        Overridden to just check the first shop product, since all shop products will be approved
        and unapproved at once. We also might not always have shop instance for the current request shop.
        """
        shop_product = self.product.shop_products.first()
        if has_permission(request.user, "shuup_multivendor.products_approvals_revoke") and is_shop_product_approved(
            shop_product
        ):
            attrs = {
                "class": "dropdown-item",
                "title": self.tooltip,
                "href": self.url,
                "onclick": (mark_safe(self.onclick) if self.onclick else None),
            }
            yield "<a %s>" % flatatt_filter(attrs)
            yield self.render_label()
            yield "</a>"
