# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.forms import forms
from shuup.admin.form_part import TemplatedFormDef
from shuup.admin.forms.fields import Select2ModelMultipleField
from shuup.admin.modules.products.views.edit import ProductBaseFormPart
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import ShopProduct
from shuup_multivendor.admin_module.form_parts.product import VendorProductBaseFormPart

from canvas.models import ProductSustainabilityGoals, SustainabilityGoal
from canvas.utils import get_canvas_brick_and_mortar_suppliers, is_canvas_supplier


class SustainabilityGoalsMixin:
    name = "sustainability goals"

    def set_initial(self, vendor_sdgs):
        """All products are required to have SDGs. If there aren't any yet, default to the brand's SDGs."""
        self.object.product_sustainability_goals.goals.add(*vendor_sdgs)

    def form_valid(self, form_group):
        goals = form_group.cleaned_data.get(self.name)
        shop_product = ShopProduct.objects.filter(product=self.object, shop=self.request.shop).first()
        project_goals, __ = ProductSustainabilityGoals.objects.get_or_create(product=shop_product)

        # Associate only the goals selected just now.
        if goals:
            project_goals.goals.set(goals["sustainability_goals"])
        else:
            project_goals.goals.clear()
        return super().form_valid(form_group)

    def get_form_defs(self):
        if self.object.pk:
            yield TemplatedFormDef(
                self.name,
                SustainabilityGoalSelectionFormSet,
                template_name="canvas/admin/sustainability_goal/product_select_sdg.jinja",
                required=False,
                kwargs={"initial": self.get_initial()},
            )


class SustainabilityGoalSelectionFormSet(forms.Form):
    sustainability_goals = Select2ModelMultipleField(
        SustainabilityGoal,
        required=False,
        help_text="Select the Sustainable Development Goals you wish to feature.",
    )

    def __init__(self, *args, **kwargs):
        existing_goals = []
        if kwargs.get("initial"):
            existing_goals = [(goal.id, goal) for goal in kwargs.pop("initial").goals.all()]
        super().__init__(*args, **kwargs)
        # Note: odd as it looks, the following combination of initial/choices is necessary for the initial
        # value to render properly.
        self.fields["sustainability_goals"].initial = [goal[0] for goal in existing_goals]
        self.fields["sustainability_goals"].widget.choices = existing_goals
        # Show results immediately on open
        self.fields["sustainability_goals"].widget.attrs["data-minimum-input-length"] = 0


class ProductSustainabilityGoalFormPart(SustainabilityGoalsMixin, ProductBaseFormPart):
    def get_initial(self):
        sdgs, __ = ProductSustainabilityGoals.objects.get_or_create(product=self.object)
        if sdgs.goals.count() == 0:
            vendor = self.object.suppliers.exclude(id__in=get_canvas_brick_and_mortar_suppliers().values("id")).first()
            if getattr(vendor, "canvas_supplier", None):
                vendor_sdgs = vendor.canvas_supplier.sustainability_goals.all()
                self.set_initial(vendor_sdgs)
                sdgs.refresh_from_db()
        return sdgs


class VendorProductSustainabilityGoalFormPart(SustainabilityGoalsMixin, VendorProductBaseFormPart):
    def get_initial(self):
        sdgs, __ = ProductSustainabilityGoals.objects.get_or_create(product=self.object)
        vendor = get_supplier(self.request)
        if is_canvas_supplier(vendor):
            # We want the brand, not the store (if the store is logged in)
            vendor = self.object.suppliers.exclude(id__in=get_canvas_brick_and_mortar_suppliers().values("id")).first()
        if sdgs.goals.count() == 0 and getattr(vendor, "canvas_supplier", None):
            vendor_sdgs = vendor.canvas_supplier.sustainability_goals.all()
            self.set_initial(vendor_sdgs)
            sdgs.refresh_from_db()
        return sdgs
