# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Any, Dict, Generator, List, Optional

from django import forms
from django.conf import settings
from django.db.models import QuerySet
from django.http import HttpRequest
from shuup.admin.form_part import FormPart, TemplatedFormDef
from shuup.admin.modules.products.views.edit import ProductBaseFormPart
from shuup.admin.modules.products.views.edit import ShopProductFormPart as BaseShopProductFormPart
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Shop, ShopProduct, ShopStatus
from shuup.utils.form_group import FormGroup
from shuup_multivendor.admin_module.form_parts.product import VendorProductBaseFormPart, VendorShopProductFormPart

from canvas.admin_module.forms import (
    ONLY_MAIN_SHOP_FIELDS,
    CanvasBarcodesForm,
    MultishopProductBaseForm,
    MultishopShopProductForm,
    MultishopVendorProductBaseForm,
    MultishopVendorShopProductForm,
)
from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.forms import CanvasProductExtraForm
from canvas.models.product import CanvasProductExtra
from canvas.utils import get_supplier_shops

__all__ = [
    "CanvasBarcodesFormPart",
    "CanvasProductExtraFormPart",
    "MultishopShopProductFormPart",
    "MultishopVendorProductBaseFormPart",
    "MultishopVendorShopProductFormPart",
    "MultishopProductBaseFormPart",
]


class MultishopShopProductFormPartMixin:
    def __init__(self, request: HttpRequest, object: Optional[ShopProduct] = None) -> None:
        super().__init__(request, object)
        del self.shop
        self.supplier = get_supplier(self.request)
        self.shops = self._get_shops()

    def get_form_defs(self) -> Generator[TemplatedFormDef, None, None]:
        instances = []
        for shop in self.shops:
            if self.object.shop != shop:
                try:
                    instance = self.object.product.get_shop_instance(shop, allow_cache=True)
                except ShopProduct.DoesNotExist:
                    instance = ShopProduct(shop=shop)
            else:
                instance = self.object
            instances.append(instance)
        for instance in instances:
            shop = instance.shop
            is_main_shop = shop.identifier == CANVAS_MAIN_SHOP_IDENTIFIER

            yield TemplatedFormDef(
                name=f"shop{shop.pk}",
                form_class=self.form_class,
                template_name=self.template_name,
                required=True,  # Will appear in the 'basic information' tab.
                kwargs=dict(
                    instance=instance,
                    initial=self.get_initial(instance, instances),
                    request=self.request,
                    shop=shop,
                    is_main_shop=is_main_shop,
                    languages=settings.LANGUAGES,
                ),
            )

            yield TemplatedFormDef(
                name=f"shop{shop.pk}_extra",
                form_class=forms.Form,
                template_name=self.template_name_extra,
                required=False,  # Will appear as its own tab.
            )

    def get_initial(
        self, current_instance: Optional[ShopProduct] = None, all_instances: Optional[List[ShopProduct]] = None
    ) -> Dict[str, Any]:
        initial = {}
        if current_instance is None or all_instances is None:
            # Having these as optional args keeps the method fully compatible with superclasses.
            return initial

        for instance in all_instances:
            if instance.pk and hasattr(instance, "primary_category"):
                initial["primary_category"] = instance.primary_category
                initial["categories"] = instance.categories.all()
                break

        return initial

    def form_valid(self, form: FormGroup) -> None:
        main_shop = next(shop for shop in self.shops if shop.identifier == CANVAS_MAIN_SHOP_IDENTIFIER)
        main_shop_form = form[f"shop{main_shop.pk}"]

        for shop in self.shops:
            shop_product_form: MultishopVendorShopProductForm = form[f"shop{shop.pk}"]

            if not shop_product_form.instance.pk:
                # Note that `self.object` is a `Product` here, even though it was a
                # `ShopProduct` in `get_form_defs`. This happens because
                # `ProductBaseFormPart.form_valid` on purpose returns a `Product`
                # instance and thus `SaveFormPartsMixin.save_form_parts` takes that
                # value as the new `object`.
                shop_product_form.instance.product = self.object

            original_quantity = shop_product_form.instance.minimum_purchase_quantity
            rounded_quantity = self.object.sales_unit.round(original_quantity)
            shop_product_form.instance.minimum_purchase_quantity = rounded_quantity

            if shop != main_shop:
                # Copy these from the main shop's shop product to all other shop products.
                for field in ONLY_MAIN_SHOP_FIELDS:
                    data = main_shop_form.cleaned_data.get(field)
                    if data:
                        shop_product_form.cleaned_data[field] = data

            if shop_product_form.has_changed():
                shop_product_form.save()


class MultishopShopProductFormPart(MultishopShopProductFormPartMixin, BaseShopProductFormPart):

    form_class = MultishopShopProductForm
    template_name = "shuup/admin/products/_edit_shop_form.jinja"
    template_name_extra = "shuup/admin/products/_edit_extra_shop_form.jinja"

    def _get_shops(self) -> "QuerySet[Shop]":
        return Shop.objects.get_for_user(self.request.user).filter(status=ShopStatus.ENABLED).order_by("pk")


class MultishopVendorShopProductFormPart(MultishopShopProductFormPartMixin, VendorShopProductFormPart):
    """Customizes the parent to enabling editing the product in all of the vendor's shops at once."""

    form_class = MultishopVendorShopProductForm
    template_name = "shuup_multivendor/admin/products/_edit_shop_form.jinja"
    template_name_extra = ("shuup_multivendor/admin/products/_edit_extra_shop_form.jinja",)

    def _get_shops(self) -> "QuerySet[Shop]":
        return get_supplier_shops(self.supplier)


class MultishopVendorProductBaseFormPart(VendorProductBaseFormPart):
    def get_form_defs(self):
        """Overridden to just swap the form."""
        yield TemplatedFormDef(
            "base",
            MultishopVendorProductBaseForm,
            template_name="shuup_multivendor/admin/products/_edit_base_form.jinja",
            required=True,
            kwargs={
                "instance": self.object.product,
                "languages": settings.LANGUAGES,
                "initial": self.get_initial(),
                "request": self.request,
            },
        )
        if self.object.pk:
            yield TemplatedFormDef(
                "base_extra",
                forms.Form,
                template_name="shuup_multivendor/admin/products/_edit_extra_base_form.jinja",
                required=False,
            )


class CanvasProductExtraFormPart(ProductBaseFormPart):
    def get_form_defs(self):
        if self.object.pk:
            canvas_product_extra, _ = CanvasProductExtra.objects.get_or_create(product=self.object.product)
            yield TemplatedFormDef(
                "canvas_product_extra",
                CanvasProductExtraForm,
                template_name="shuup/admin/products/canvas_product_extra_form.jinja",
                required=False,
                kwargs={"instance": canvas_product_extra},
            )

    def form_valid(self, form):
        if "canvas_product_extra" in form.forms:
            vendor_extra_details_form = form["canvas_product_extra"]
            if vendor_extra_details_form.has_changed():
                vendor_extra_details_form.save()


class MultishopProductBaseFormPart(ProductBaseFormPart):
    def get_form_defs(self):
        """Overridden to just swap the form."""
        yield TemplatedFormDef(
            "base",
            MultishopProductBaseForm,
            template_name="shuup/admin/products/_edit_base_form.jinja",
            required=True,
            kwargs={
                "instance": self.object.product,
                "languages": settings.LANGUAGES,
                "initial": self.get_initial(),
                "request": self.request,
            },
        )

        yield TemplatedFormDef(
            "base_extra", forms.Form, template_name="shuup/admin/products/_edit_extra_base_form.jinja", required=False
        )


class CanvasBarcodesFormPart(FormPart):
    name = "canvas_barcodes"
    priority = 17  # Right after canvas stocks.

    def get_form_defs(self):
        if self.object.pk:  # Don't yield when creating a new product, similarly to `SimpleSupplierFormPart`.
            yield TemplatedFormDef(
                self.name,
                template_name="canvas/admin/vendor/canvas_barcodes.jinja",
                form_class=CanvasBarcodesForm,
                required=False,
                kwargs=dict(
                    request=self.request,
                    instance=self.object.product,
                ),
            )
