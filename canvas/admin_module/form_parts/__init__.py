# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from .funds import (
    MultishopVendorFundsPaypalFormPart,
    MultishopVendorFundsWireEUFormPart,
    MultishopVendorFundsWireFormPart,
    MultishopVendorPendingWithdrawalsFormPart,
)
from .product import (
    CanvasBarcodesFormPart,
    CanvasProductExtraFormPart,
    MultishopProductBaseFormPart,
    MultishopShopProductFormPart,
    MultishopVendorProductBaseFormPart,
    MultishopVendorShopProductFormPart,
)
from .stocks import CanvasSimpleSupplierFormPart, CanvasStoreStocksFormPart
from .sustainability_goals import (
    ProductSustainabilityGoalFormPart,
    SustainabilityGoalSelectionFormSet,
    VendorProductSustainabilityGoalFormPart,
)
from .vendor import (
    CanvasStoreEventFormPart,
    CanvasStoreExtraFormPart,
    CanvasSupplierExtraSettingsFormPart,
    CanvasSupplierOpeningPeriodsFormPart,
)

__all__ = [
    "CanvasBarcodesFormPart",
    "CanvasProductExtraFormPart",
    "CanvasSimpleSupplierFormPart",
    "CanvasStoreExtraFormPart",
    "CanvasStoreEventFormPart",
    "CanvasStoreStocksFormPart",
    "CanvasSupplierExtraSettingsFormPart",
    "CanvasSupplierOpeningPeriodsFormPart",
    "MultishopVendorFundsPaypalFormPart",
    "MultishopVendorFundsWireEUFormPart",
    "MultishopVendorFundsWireFormPart",
    "MultishopVendorPendingWithdrawalsFormPart",
    "MultishopProductBaseFormPart",
    "MultishopShopProductFormPart",
    "MultishopVendorProductBaseFormPart",
    "MultishopVendorShopProductFormPart",
    "ProductSustainabilityGoalFormPart",
    "SustainabilityGoalSelectionFormSet",
    "VendorProductSustainabilityGoalFormPart",
]
