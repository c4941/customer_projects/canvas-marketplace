# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Generator, Iterable

from django.contrib import messages
from django.db.models import Exists, OuterRef
from django.utils.translation import gettext as _
from shuup.admin.form_part import TemplatedFormDef
from shuup.core.models import ConfigurationItem, Shop, Supplier, get_person_contact
from shuup.utils.form_group import FormGroup
from shuup_multivendor.admin_module.form_parts.funds import (
    BaseVendorFundsFormPart,
    VendorFundsPaypalFormPart,
    VendorFundsWireEUFormPart,
    VendorFundsWireFormPart,
)
from shuup_multivendor.admin_module.form_parts.funds_pending import VendorPendingWithdrawalsFormPart
from shuup_multivendor.models import VendorFunds
from shuup_multivendor.utils.funds import (
    IS_VENDOR_FUND_ENABLED_KEY,
    is_paypal_withdraw_enabled,
    is_wire_transfer_eu_withdraw_enabled,
    is_wire_transfer_withdraw_enabled,
)

from canvas.admin_module.forms import (
    MultishopPaypalForm,
    MultishopPendingWithdrawalsForm,
    MultishopWireForm,
    MultishopWireFormEU,
)
from canvas.utils import get_supplier_shops, is_canvas_supplier

__all__ = [
    "MultishopVendorPendingWithdrawalsFormPart",
    "MultishopVendorFundsWireFormPart",
    "MultishopVendorFundsWireEUFormPart",
    "MultishopVendorFundsPaypalFormPart",
]


class MultishopBaseVendorFundsFormPart(BaseVendorFundsFormPart):
    def get_form_defs(self) -> Generator[TemplatedFormDef, None, None]:
        supplier = self.object
        shops = (
            get_supplier_shops(supplier)
            .annotate(
                is_vendor_funds_enabled=Exists(
                    ConfigurationItem.objects.filter(
                        shop=OuterRef("pk"),
                        key=IS_VENDOR_FUND_ENABLED_KEY,
                        value=True,
                    )
                )
            )
            .filter(
                is_vendor_funds_enabled=True,
            )
        )

        if not self.should_show(shops, self.object):
            return

        yield TemplatedFormDef(
            self.name,
            form_class=self.form_class,
            template_name=self.template_name,
            required=False,
            kwargs=dict(
                initial=self.get_initial(),
                vendor=supplier,
                shops=shops,
                shop=None,  # The original multivendor forms need this dummy arg.
            ),
        )

    def form_valid(self, form: FormGroup) -> None:
        if self.name not in form.forms:
            return

        withdraw_form = form[self.name]
        if withdraw_form.changed_data:
            data = withdraw_form.cleaned_data
            amount = data.pop("amount")
            customer = get_person_contact(self.request.user)

            # `MultishopPendingWithdrawalsForm` doesn't have this, but it also cannot have any `changed_data`.
            shop = withdraw_form.shop

            vendor = self.object
            self.save_initial_data(data)
            if amount:
                info = ""
                for key, value in data.items():
                    info += f"{key.capitalize()}: {value}\n"

                VendorFunds.create_withdrawal(shop, vendor, self.request.user, customer, amount, info)
                messages.success(self.request, _("Withdrawal created successfully."))


class MultishopVendorPendingWithdrawalsFormPart(VendorPendingWithdrawalsFormPart, MultishopBaseVendorFundsFormPart):
    """
    The method resolution order of this class is:
        - MultishopVendorPendingWithdrawalsFormPart
        - VendorPendingWithdrawalsFormPart
        - _MultishopBaseVendorFundsFormPart
        - BaseVendorFundsFormPart
        - FormPart
        - object
    """

    form_class = MultishopPendingWithdrawalsForm
    template_name = "canvas/admin/vendor/funds/multishop_open_withdrawals_template.jinja"

    def get_form_defs(self):
        """Overridden to skip immediate parent."""
        return super(VendorPendingWithdrawalsFormPart, self).get_form_defs()

    def should_show(self, shops: Iterable[Shop], vendor: Supplier) -> bool:
        # Removed the check from the original `get_form_defs` which doesn't show this
        # form part if there are no withdrawals for this vendor. It's simpler and more
        # logical that this is shown always.
        return not is_canvas_supplier(vendor)


class MultishopVendorFundsWireFormPart(VendorFundsWireFormPart, MultishopBaseVendorFundsFormPart):
    form_class = MultishopWireForm
    template_name = "canvas/admin/vendor/funds/multishop_withdraw_template_wire_transfer.jinja"

    def should_show(self, shops: Iterable[Shop], vendor: Supplier) -> bool:
        return not is_canvas_supplier(vendor) and any(is_wire_transfer_withdraw_enabled(shop) for shop in shops)


class MultishopVendorFundsWireEUFormPart(VendorFundsWireEUFormPart, MultishopBaseVendorFundsFormPart):
    form_class = MultishopWireFormEU
    template_name = "canvas/admin/vendor/funds/multishop_withdraw_template_eu_wire_transfer.jinja"

    def should_show(self, shops: Iterable[Shop], vendor: Supplier) -> bool:
        return not is_canvas_supplier(vendor) and any(is_wire_transfer_eu_withdraw_enabled(shop) for shop in shops)


class MultishopVendorFundsPaypalFormPart(VendorFundsPaypalFormPart, MultishopBaseVendorFundsFormPart):
    form_class = MultishopPaypalForm
    template_name = "canvas/admin/vendor/funds/multishop_withdraw_template_paypal.jinja"

    def should_show(self, shops: Iterable[Shop], vendor: Supplier) -> bool:
        return not is_canvas_supplier(vendor) and any(is_paypal_withdraw_enabled(shop) for shop in shops)
