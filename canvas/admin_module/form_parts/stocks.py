from shuup.admin.form_part import FormPart, TemplatedFormDef
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import ShopProduct
from shuup.simple_supplier.admin_module.forms import SimpleSupplierFormPart
from shuup.utils.form_group import FormGroup

from canvas.admin_module.forms.stocks import CanvasSimpleSupplierForm, CanvasStoreStocksForm
from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.models import SyncedShopProduct
from canvas.square_sync.client import InventoryClient
from canvas.utils import is_canvas_supplier

__all__ = [
    "CanvasSimpleSupplierFormPart",
    "CanvasStoreStocksFormPart",
]


class CanvasStoreStocksFormPart(FormPart):
    name = "canvas_store_stocks"
    priority = 16  # Right after product images.
    object: ShopProduct

    def get_form_defs(self):
        if self.object.pk:  # Don't yield when creating a new product, similarly to `SimpleSupplierFormPart`.
            yield TemplatedFormDef(
                self.name,
                template_name="canvas/admin/stocks/canvas_stocks.jinja",
                form_class=CanvasStoreStocksForm,
                required=False,
                kwargs=dict(
                    product=self.object.product,
                ),
            )

    def form_valid(self, form: FormGroup) -> None:
        if self.name in form.forms:
            stocks_form = form[self.name]
            if "canvas_stores" in stocks_form.changed_data:
                for shop_product in stocks_form.product.shop_products.iterator():
                    shop_product.suppliers.set(
                        [stocks_form.supplier, *stocks_form.cleaned_data.get("canvas_stores", [])]
                    )

            self._adjust_stocks(stocks_form)
        # We had no object yet so the FormDef was not yielded.

    def _adjust_stocks(self, stocks_form: CanvasStoreStocksForm) -> None:
        delta = stocks_form.cleaned_data.get("delta")

        if delta:
            supplier = get_supplier(self.request)
            if is_canvas_supplier(supplier):
                shop_product = stocks_form.product.get_shop_instance(self.request.shop)
                physical_count = stocks_form.cleaned_data["physical_count"]
                new_count = str(physical_count + delta)
                if self.request.shop.identifier == CANVAS_MAIN_SHOP_IDENTIFIER:
                    # Make sure there is something to sync, just in case.
                    SyncedShopProduct.get_sync(shop_product)
                    InventoryClient().sync_inventory(shop_product, supplier, new_count)
            supplier.adjust_stock(stocks_form.product.pk, delta=delta, created_by=self.request.user)


class CanvasSimpleSupplierFormPart(SimpleSupplierFormPart):
    form = CanvasSimpleSupplierForm
