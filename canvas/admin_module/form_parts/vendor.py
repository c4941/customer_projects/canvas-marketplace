# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Generator

from shuup.admin.form_part import FormPart, TemplatedFormDef
from shuup.utils.form_group import FormGroup

from canvas.admin_module.forms.vendor import (
    CanvasStoreEventsFormSet,
    CanvasStoreExtraForm,
    CanvasSupplierExtraSettingsForm,
)
from canvas.models import CanvasStoreEvent
from canvas.utils import ensure_store_extra, is_canvas_supplier

__all__ = [
    "CanvasStoreExtraFormPart",
    "CanvasStoreEventFormPart",
    "CanvasSupplierExtraSettingsFormPart",
    "CanvasSupplierOpeningPeriodsFormPart",
]

from shuup_opening_hours.admin_module.form_parts.vendor import SupplierOpeningPeriodsFormPart


class CanvasSupplierExtraSettingsFormPart(FormPart):
    name = "canvas_supplier"
    priority = 2  # Between 'Contact Address' and 'Images'.

    def get_form_defs(self) -> Generator[TemplatedFormDef, None, None]:
        if not self.should_show():
            return

        yield TemplatedFormDef(
            self.name,
            form_class=CanvasSupplierExtraSettingsForm,
            template_name="canvas/admin/vendor/extra_settings.jinja",
            required=True,
            kwargs=dict(
                instance=getattr(self.object, "canvas_supplier", None),
                request=self.request,
                vendor=self.object,
            ),
        )

    def form_valid(self, form: FormGroup) -> None:
        if self.name in form.forms:
            form[self.name].save()

    def should_show(self) -> bool:
        return not is_canvas_supplier(self.object)


class CanvasSupplierOpeningPeriodsFormPart(SupplierOpeningPeriodsFormPart):
    def get_form_defs(self) -> Generator[TemplatedFormDef, None, None]:
        """Overridden to show the template only for Canvas brick and mortar store owner vendors."""
        if not self.should_show():
            return
        yield from super().get_form_defs()

    def should_show(self) -> bool:
        return is_canvas_supplier(self.object)


class CanvasStoreExtraFormPart(FormPart):
    name = "store_extra"
    priority = 4  # After Opening Hours

    def get_initial(self):
        store_extra = ensure_store_extra(self.object)
        return dict(spotify_uri=store_extra.spotify_uri if store_extra else "")

    def get_form_defs(self):
        if not self.should_show():
            return

        extra_instance = self.object.canvas_store if hasattr(self.object, "canvas_store") else None
        yield TemplatedFormDef(
            self.name,
            form_class=CanvasStoreExtraForm,
            template_name="canvas/admin/vendor/store_extra.jinja",
            required=True,
            kwargs=dict(vendor=self.object, instance=extra_instance, initial=self.get_initial()),
        )

    def form_valid(self, form):
        if self.name in form.forms:
            form[self.name].save()

    def should_show(self) -> bool:
        return is_canvas_supplier(self.object)


class CanvasStoreEventFormPart(FormPart):
    name = "store_events"
    priority = 5

    def get_initial(self):
        store_extra = ensure_store_extra(self.object)
        return [
            {
                "date": event.date,
                "location": event.location,
                "eventbrite_url": event.eventbrite_url,
                "event_id": event.id,
            }
            for event in store_extra.store_events.order_by("-date")
        ]

    def get_form_defs(self):
        if not self.should_show():
            return

        yield TemplatedFormDef(
            self.name,
            form_class=CanvasStoreEventsFormSet,
            template_name="canvas/admin/vendor/store_events.jinja",
            required=True,
            kwargs=dict(initial=self.get_initial()),
        )

    def save(self, data):
        events = [event for event in data if event]
        to_create = []
        current_event_ids = []
        for event in events:
            event_id = event.pop("event_id", None)
            if event_id:
                CanvasStoreEvent.objects.update_or_create(id=int(event_id), defaults=event)
                current_event_ids.append(int(event_id))
            else:
                to_create.append(event)

        # Delete missing events before creating new events.
        CanvasStoreEvent.objects.exclude(id__in=current_event_ids).delete()
        CanvasStoreEvent.objects.bulk_create(
            [
                CanvasStoreEvent(
                    date=event["date"],
                    location=event["location"],
                    eventbrite_url=event["eventbrite_url"],
                    store=ensure_store_extra(self.object),
                )
                for event in to_create
            ]
        )

    def form_valid(self, form):
        if self.name in form.cleaned_data:
            data = form.cleaned_data[self.name]
            self.save(data)
        return super().form_valid(form)

    def should_show(self) -> bool:
        return is_canvas_supplier(self.object)
