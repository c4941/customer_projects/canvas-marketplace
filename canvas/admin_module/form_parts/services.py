# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import ShippingMethod
from shuup.utils.form_group import FormGroup
from shuup_multivendor.admin_module.views.services import ShippingMethodBaseFormPart

from canvas.admin_module.forms import MultishopVendorShippingMethodForm


class MultishopVendorShippingMethodBaseFormPart(ShippingMethodBaseFormPart):
    form = MultishopVendorShippingMethodForm

    def form_valid(self, form: FormGroup) -> ShippingMethod:
        """The form will allow the vendor to set the shop, so this doesn't have to set it."""
        self.object = form["base"].save(commit=False)
        self.object.supplier = get_supplier(self.request)
        self.object.save()
        return self.object
