# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from typing import Optional, Type

from django import forms
from django.db import models
from django.db.models import QuerySet
from django.http import HttpRequest
from django.utils.translation import gettext as _
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Shop, Supplier
from shuup.front.utils.product import get_default_product_context
from shuup_wishlist.models import Wishlist, WishlistPrivacy

from canvas.constants import CANVAS_STORE_OWNER_PERMISSION_GROUP_IDENTIFIER
from canvas.models import CanvasStoreExtra
import functools
from django.core.cache import cache

def get_supplier_shops(supplier: Supplier) -> "QuerySet[Shop]":
    """Return all the shops that the vendor is selling in."""
    if not supplier:
        raise ValueError(f"Supplier cannot be {supplier!r}")
    return Shop.objects.filter(pk__in=supplier.vendor_users.values("shop")).order_by("pk")


def get_request_supplier_shops(request: HttpRequest) -> "QuerySet[Shop]":
    """Return all the shops that the currently active vendor is selling in."""
    return get_supplier_shops(get_supplier(request))


def is_canvas_supplier(supplier: Supplier) -> bool:
    """Return True if the given vendor is a Canvas brick and mortar store owner."""
    return supplier.vendor_users.filter(user__groups__name=CANVAS_STORE_OWNER_PERMISSION_GROUP_IDENTIFIER).exists()


def is_request_canvas_supplier(request: HttpRequest) -> bool:
    """Return True if the active vendor is a Canvas brick and mortar store owner."""
    return is_canvas_supplier(get_supplier(request))


def get_canvas_brick_and_mortar_suppliers() -> "QuerySet[Supplier]":
    """Return all of the Suppliers that represent Canvas's physical stores."""
    return Supplier.objects.filter(
        vendor_users__user__groups__name=CANVAS_STORE_OWNER_PERMISSION_GROUP_IDENTIFIER
    ).distinct()


def product_is_available_in_stores(shop_product) -> "QuerySet[Supplier]":
    """Return all the stores that sell this product."""
    return get_canvas_brick_and_mortar_suppliers().filter(id__in=shop_product.suppliers.values("id"))


def ensure_store_extra(vendor) -> Optional[CanvasStoreExtra]:
    """Create a CanvasStoreExtra instance for `vendor` if it doesn't exist."""
    if not hasattr(vendor, "canvas_store") or vendor.canvas_store is None:
        return CanvasStoreExtra.objects.create(supplier=vendor)
    else:
        return vendor.canvas_store


def formfield(model: Type[models.Model], field: str, **kwargs) -> forms.Field:
    """Return a form field made from the given model field."""
    return model._meta.get_field(field).formfield(**kwargs)


def create_wishlist(contact, shop):
    """Create a wishlist for the contact - available for user registration, Firebase registration, and vendor
    registration.
    """
    Wishlist.objects.get_or_create(
        shop=shop,
        customer=contact,
        name=_("My wishlist"),
        privacy=WishlistPrivacy.PRIVATE,
    )


def get_canvas_orderability_errors(shop_product, request=None, customer=None, quantity=None):
    """Just because a brand doesn't have the item im stock, it doesn't mean it's completely not orderable."""
    if request is not None:
        customer = request.customer
    if quantity is None:
        quantity = 1
    errors = []
    for vendor in shop_product.suppliers.all():
        # Note: Since a store may ship the whole order, one store must have the whole quantity.
        errors = list(
            shop_product.get_orderability_errors(
                supplier=vendor, quantity=quantity, customer=customer, ignore_minimum=True
            )
        )
        if not errors:
            # Not unorderable after all - a store has it in stock.
            break
    return errors


def get_canvas_product_context(request, product, language=None, supplier=None):
    shop_product = product.get_shop_instance(request.shop)
    supplier = shop_product.suppliers.exclude(id__in=get_canvas_brick_and_mortar_suppliers().values("id")).first()
    context = get_default_product_context(request, product, language=language, supplier=supplier)

    if len(context["orderability_errors"]) == 1 and context["orderability_errors"][0].code == "stock_insufficient":
        context["orderability_errors"] = get_canvas_orderability_errors(shop_product, request=request)

    return context



def flat_categories(shop):
    flat_categories = cache.get("flat-categories", None)
    if not flat_categories:
        from shuup.core.models import Category

        flat_categories = list(Category.objects.select_related("image").filter(parent__isnull=True))
        qs = Category.objects.all_visible(shop=1).select_related("image", "parent").filter(parent__isnull=False).order_by("level", "ordering")
        current_category = None
        for category in qs:
            current_category = category
            idx = 0
            parent_found = False
            sibling_found = False
            for cat in flat_categories:
                if cat.id == category.parent.id:
                    parent_found = True
                if parent_found:
                    if getattr(cat.parent, "id", None) == getattr(category.parent, "id"):
                        sibling_found = True
                    if not sibling_found:
                        break
                    if category.ordering < cat.ordering:
                        break
                
                idx += 1

            flat_categories.insert(idx, current_category)
            print(flat_categories)
        




    return flat_categories
