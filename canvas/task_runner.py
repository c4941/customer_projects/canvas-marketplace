# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.core.tasks import Task, TaskRunner

from canvas.tasks import run_task as celery_run_task


class CeleryTaskRunner(TaskRunner):
    """
    Run tasks using Celery
    """

    def create_task(self, function, **kwargs):
        return Task(function, **kwargs)

    def run_task(self, task):
        celery_run_task.delay(task.function, task.kwargs)
