# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Inc. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from django.conf.urls import url

from canvas.front.views.discover_our_stores import DiscoverOurStoresView
from canvas.square_sync.event_handlers import sync_customers, sync_inventory, update_store_price
from canvas.square_sync.views import RequestTokenView, authenticate_canvas

from .views import SetTimezoneView, ShopBySDGView, VendorRegisterInfoView, wishlist_card

urlpatterns = [
    url(r"^set-timezone/", SetTimezoneView.as_view(), name="set_timezone"),
    url(r"^join-the-canvas/$", VendorRegisterInfoView.as_view(), name="vendor_register_info"),
    url(r"^sdg/$", ShopBySDGView.as_view(), name="shop-by-sdg"),
    url(r"^discover/$", DiscoverOurStoresView.as_view(), name="discover-our-stores"),
    url(r"^update-store-price", update_store_price, name="update-store-price"),
    url(r"^sync-inventory/$", sync_inventory, name="sync-inventory"),
    url(r"^authenticate-canvas/$", authenticate_canvas, name="authenticate"),
    url(r"^request-token/$", RequestTokenView.as_view(), name="request-token"),
    url(r"^wishlist-card/$", wishlist_card, name="wishlist-card"),
    url(r"^sync-customers/$", sync_customers, name="sync-customers"),
]
