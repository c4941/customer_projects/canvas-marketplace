from typing import Any

from django.conf import settings
from django.contrib.auth.models import User
from django.db import transaction
from django.utils.translation import gettext as _
from shuup.core.models import Shop, ShopStatus, get_person_contact
from shuup.front.apps.registration.forms import PersonRegistrationForm
from shuup_multivendor.forms import VendorRegistrationBaseForm, VendorRegistrationForm
from shuup_multivendor.models import SupplierUser
from shuup_multivendor.signals import vendor_registered

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.forms import CanvasSupplierExtraForm
from canvas.models.sync import SyncedContact
from canvas.utils import create_wishlist


class CanvasVendorRegistrationBaseForm(VendorRegistrationBaseForm):
    class Meta(VendorRegistrationBaseForm.Meta):
        labels = {"name": _("Brand name")}


class CanvasVendorRegistrationForm(VendorRegistrationForm):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.add_form_def("vendor_base", form_class=CanvasVendorRegistrationBaseForm)
        self.add_form_def("canvas_vendor_extra", form_class=CanvasSupplierExtraForm)

    @transaction.atomic
    def save(self, commit=True) -> User:
        """
        Customizes the super to add the vendor to all of the enabled shops
        and to handle saving of the `canvas_vendor_extra` form.
        """
        shops = Shop.objects.filter(status=ShopStatus.ENABLED)
        main_shop = shops.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER).first()

        if self.is_admin_user():
            raise Exception("Can't register as superuser or shop staff.")

        if not self.request.user.is_authenticated:
            vendor_user = self.forms["vendor_user"].save(commit=False)
            vendor_person = self.forms["vendor_person"].save(commit=False)
        else:
            vendor_user = self.request.user
            vendor_person = self.forms["vendor_person"].save(commit=False)

        vendor_user.first_name = vendor_person.first_name
        vendor_user.last_name = vendor_person.last_name
        vendor_user.email = vendor_person.email
        vendor_user.is_staff = settings.SHUUP_MULTIVENDOR_GIVE_VENDOR_USERS_ADMIN_ACCESS
        vendor_user.is_active = not settings.SHUUP_MULTIVENDOR_VENDOR_REQUIRES_APPROVAL
        vendor_user.save()

        vendor_person.user = vendor_user
        vendor_person.save()
        vendor_person.shops.add(*shops)

        vendor = self.forms["vendor_base"].save(commit=False)
        vendor_address = self.forms["vendor_address"].save(commit=False)
        vendor_address.name = vendor.name
        if not vendor_address.email and vendor_person.email:
            vendor_address.email = vendor_person.email
        vendor_address.save()

        vendor.contact_address = vendor_address
        vendor.save()
        vendor.shops.add(
            *shops, through_defaults=dict(is_approved=(not settings.SHUUP_MULTIVENDOR_VENDOR_REQUIRES_APPROVAL))
        )

        canvas_vendor_extra = self.forms["canvas_vendor_extra"].save(commit=False)
        canvas_vendor_extra.supplier = vendor
        canvas_vendor_extra.save()
        self.forms["canvas_vendor_extra"].save_m2m()

        for shop in shops:
            SupplierUser.objects.update_or_create(
                user=vendor_user, shop=shop, defaults=dict(supplier=vendor, is_owner=True)
            )
            create_wishlist(vendor_person, shop)

        vendor_registered.send(
            sender=type(self), shop=main_shop, vendor=vendor, user=vendor_user, person_contact=vendor_person
        )

        return vendor_user


class CanvasCustomerRegistration(PersonRegistrationForm):
    def save(self, commit=True, *args, **kwargs):
        user = super().save(commit=commit, *args, **kwargs)
        contact = get_person_contact(user)
        create_wishlist(contact, self.request.shop)
        # TODO: add something here for online users, or possibly fetch existing in-store user instead
        return user

