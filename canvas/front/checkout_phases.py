from django.utils.translation import ugettext_lazy as _
from shuup.front.checkout.addresses import AddressesPhase
from shuup.front.checkout.checkout_method import CheckoutMethodPhase
from shuup.front.checkout.confirm import ConfirmPhase
from shuup.front.checkout.methods import MethodsPhase
from shuup_multivendor.checkout import MultivendorMethodsPhase
import logging

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView
from logging import getLogger

from shuup.apps.provides import get_provide_objects
from shuup.core.models import OrderStatus
from shuup.front.basket import get_basket_order_creator
from shuup.front.checkout import CheckoutPhaseViewMixin
from shuup.front.signals import checkout_complete

LOG = logging.getLogger(__name__)

SHIPPING_METHOD_REQUIRED_CONFIG_KEY = "checkout_required_method:shipping"
PAYMENT_METHOD_REQUIRED_CONFIG_KEY = "checkout_required_method:payment"
SUPPLIER_SHIPPING_METHOD_KET_FMT = "supplier_{}:shipping_method_id"




class FinalizeYourOrderPhase(ConfirmPhase):
    title = _("Click to Confirm Your Order")
    def form_valid(self, form):
        for key, value in form.cleaned_data.items():
            self.storage[key] = value

        self.process()
        self.basket.save()
        self.basket.storage.add_log_entry(self.basket, _("Starting to create order."))

        order = self.create_order()
        self.checkout_process.complete()  # Inform the checkout process it's completed

        # make sure to set marketing permission asked once
        if "marketing" in form.fields and order.customer:
            if not order.customer.options or not order.customer.options.get("marketing_permission_asked"):
                order.customer.options = order.customer.options or {}
                order.customer.options["marketing_permission_asked"] = True
                order.customer.save(update_fields=["options"])

        if order.require_verification:
            response = redirect("shuup:order_requires_verification", pk=order.pk, key=order.key)
        else:
            response = redirect("shuup:order_process_payment", pk=order.pk, key=order.key)

        checkout_complete.send(sender=type(self), request=self.request, user=self.request.user, order=order)

        return response


class ChooseYourCheckoutMethod(CheckoutMethodPhase):
    title = _("Choose Checkout Method")


class ChooseShippingAndPay(MethodsPhase):
    title = _("Choose Shipping Method and Pay")


class MultivendorChooseShippingAndPay(MultivendorMethodsPhase):
    title = _("Choose Shipping Method and Pay")
   

class AddAddresses(AddressesPhase):
    title = _("Add Addresses")
