# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.views.generic import TemplateView

from canvas.utils import get_canvas_brick_and_mortar_suppliers


class DiscoverOurStoresView(TemplateView):
    template_name = "canvas/views/discover_our_stores.jinja"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # TODO: optimize query
        context["stores"] = get_canvas_brick_and_mortar_suppliers()
        return context
