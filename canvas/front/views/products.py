# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.front.themes.views._product_price import ProductPriceView

from canvas.utils import get_canvas_orderability_errors


class CanvasProductPriceView(ProductPriceView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        errors = get_canvas_orderability_errors(context["shop_product"], request=self.request)
        quantity = self._get_quantity(context["shop_product"])
        if quantity is not None:
            if (not context["product"] or errors) or quantity is None:
                self.template_name = "shuup/front/product/detail_order_section_no_product.jinja"
                return context
            else:
                # There are two potential errors. `qty` is None, and the product is not orderable.
                # `qty` is not None, and we need a special orderability check (which passed)
                # because a store has the product in stock.
                self.template_name = "shuup/front/product/detail_order_section.jinja"
        return context


def canvas_product_price(request):
    return CanvasProductPriceView.as_view()(request, pk=request.GET["id"])
