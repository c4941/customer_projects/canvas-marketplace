# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.db.models import QuerySet
from django.views.generic import TemplateView
from shuup.core.models import Category, Product, Supplier
from shuup.front.utils.sorts_and_filters import (
    ProductListForm,
    get_product_queryset,
    get_query_filters,
    post_filter_products,
    sort_products,
)
from shuup.front.utils.views import cache_product_things


def filter_by_sdg(products: QuerySet, sdgs: list) -> QuerySet:
    """Filter `products` by their assigned SDGs."""
    return products.filter(shop_products__product_sustainability_goals__goals__in=sdgs)


def get_context_data(context, request, category, product_filters):
    data = request.GET
    context["form"] = form = ProductListForm(request=request, shop=request.shop, category=category, data=data)
    form.full_clean()
    data = form.cleaned_data
    if "sort" in form.fields and not data.get("sort"):
        # Use first choice by default
        data["sort"] = form.fields["sort"].widget.choices[0][0]

    # TODO: Check if context cache can be utilized here
    sdgs = data.pop("sustainability_goals", None)
    products = (
        Product.objects.listed(customer=request.customer, shop=request.shop)
        .filter(**product_filters)
        .filter(get_query_filters(request, category, data=data))
        .prefetch_related("sales_unit", "sales_unit__translations")
    )

    products = get_product_queryset(products, request, category, data).distinct()
    products = post_filter_products(request, category, products, data)
    if sdgs:
        products = filter_by_sdg(products, sdgs)
    products = cache_product_things(request, products)
    products = sort_products(request, category, products, data)
    context["page_size"] = data.get("limit", 12)
    context["products"] = products

    if "supplier" in data:
        context["supplier"] = data.get("supplier")

    return context


class CanvasAllCategoriesView(TemplateView):
    """A Canvas-specific version of AllCategoriesView that includes SDG filtering."""

    template_name = "shuup/front/product/category.jinja"

    def get_product_filters(self):
        category_ids = Category.objects.all_visible(
            customer=self.request.customer,
            shop=self.request.shop,
        ).values_list("id", flat=True)
        return {
            "shop_products__shop": self.request.shop,
            "variation_parent__isnull": True,
            "shop_products__categories__id__in": category_ids,
            "shop_products__suppliers__in": Supplier.objects.enabled(shop=self.request.shop),
        }

    def get_context_data(self, **kwargs):
        context = super(CanvasAllCategoriesView, self).get_context_data(**kwargs)
        context["category"] = None
        return get_context_data(context, self.request, None, self.get_product_filters())
