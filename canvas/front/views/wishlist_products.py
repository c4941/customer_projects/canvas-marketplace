# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Inc. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from django.http import HttpResponse
from django.template.loader import render_to_string


def wishlist_card(request):
    return HttpResponse(
        render_to_string(
            "canvas/macros/wishlist.jinja",
            request=request,
        )
    )
