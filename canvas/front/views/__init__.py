# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Inc. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.

from canvas.front.views.shop_by_sdg import ShopBySDGView
from canvas.front.views.timezone import SetTimezoneView
from canvas.front.views.vendor_register_info import VendorRegisterInfoView
from canvas.front.views.wishlist_products import wishlist_card

__all__ = ["SetTimezoneView", "ShopBySDGView", "VendorRegisterInfoView", "wishlist_card"]
