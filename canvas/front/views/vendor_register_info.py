# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Inc. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from django.views.generic import TemplateView


class VendorRegisterInfoView(TemplateView):
    template_name = "canvas/views/vendor_register_info.jinja"

    def get_context_data(self, **kwargs):
        context = super(VendorRegisterInfoView, self).get_context_data(**kwargs)

        return context
