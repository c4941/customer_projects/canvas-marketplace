# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.db import models
from django.utils.translation import gettext_lazy as _
from shuup.core.models import ShuupModel

from canvas.models.sustainability_goals import SustainabilityGoal


class CanvasSupplierExtra(ShuupModel):
    supplier = models.OneToOneField("shuup.Supplier", related_name="canvas_supplier", on_delete=models.CASCADE)
    brand_statement = models.CharField(
        max_length=1000,
        help_text=_("Please provide 3-5 sentences about your brand and mission."),
    )
    sustainability_goals = models.ManyToManyField(
        SustainabilityGoal,
        blank=True,
        related_name="canvas_extra_sustainability_goals",
        verbose_name=_("Sustainable Development Goals"),
        help_text=_("Which United Nations' Sustainable Development Goals does your brand address?"),
    )
    sustainability_goals_description = models.CharField(
        max_length=1000,
        verbose_name=_("Please outline in detail how you address the goals selected"),
    )
    other_sustainability_initiatives = models.CharField(
        max_length=1000,
        verbose_name=_("Do you have any other sustainability initiatives you would like us to know about?"),
    )
    social_media_handles = models.CharField(
        max_length=300,
        verbose_name=_("Instagram handles"),
        help_text=_(
            "You can list multiple handles by separating them with commas or spaces. "
            "Example: @mybrand, @my_other_brand, @my.best.brand"
        ),
    )
    website = models.CharField(max_length=200, verbose_name=_("Website URL"))
    referrer = models.CharField(
        max_length=200,
        verbose_name=_("How did you hear about The Canvas?"),
        help_text=_("If a member of our team invited you to apply, please put their name."),
        blank=True,
    )

    def __repr__(self) -> str:
        return "<{}:{}-{}>".format(self.__class__.__name__, self.pk, self.supplier.name)

    def set_sustainability_goals(self, goals):
        """Clear any existing goals, and then set the current ones."""
        self.sustainability_goals.clear()
        self.sustainability_goals.add(*goals)


class CanvasStoreExtra(ShuupModel):
    supplier = models.OneToOneField("shuup.Supplier", related_name="canvas_store", on_delete=models.CASCADE)
    spotify_uri = models.CharField(max_length=128, blank=True, null=True)


class CanvasStoreEvent(ShuupModel):
    date = models.DateField()
    location = models.CharField(max_length=64)
    eventbrite_url = models.CharField(max_length=256)  # Eventbrite urls can be super long
    store = models.ForeignKey(CanvasStoreExtra, related_name="store_events", on_delete=models.CASCADE)
