# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import re
from typing import Generator, Iterable, Union

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from shuup.core.models import Order, Service, ServiceBehaviorComponent, Supplier
from shuup.core.order_creator import OrderSource


class CanvasPickupStockBehaviorComponent(ServiceBehaviorComponent):
    name = _("Check stocks on pickup")
    help_text = _(
        "Only allow using the method when the canvas brick and mortar store linked "
        "to the method has all the selected products in stock."
    )

    def get_unavailability_reasons(
        self, service: Service, source: OrderSource
    ) -> Generator[ValidationError, None, None]:
        canvas_supplier_pk = re.search(r"store-(\d+)", service.identifier).group(1)
        canvas_supplier = Supplier.objects.filter(pk=canvas_supplier_pk).first()
        if canvas_supplier is None:
            yield ValidationError(f"Canvas store id {canvas_supplier_pk} does not exist.", code="missing-store")

        stocks = canvas_supplier.get_stock_statuses(source.product_ids)

        for product, qty in source._get_products_and_quantities().items():
            logical_count = getattr(stocks.get(product.pk), "logical_count", 0)
            if logical_count < qty:
                msg = _(
                    "The logical stock {canvas_supplier} for {product!r} is "
                    "{logical_count}, but the basket has {qty} units."
                ).format(
                    canvas_supplier=canvas_supplier,
                    product=product,
                    logical_count=logical_count,
                    qty=qty,
                )
                yield ValidationError(msg, code="no-canvas-store-stock")


class CanvasStoreShipStockBehaviorComponent(ServiceBehaviorComponent):
    name = _("Check destinations")
    help_text = _(
        "Only allow using the method when the customer is in the US and the brand is not, and the canvas brick and "
        "mortar store linked to the method has all the selected products in stock."
    )

    def get_unavailability_reasons(
        self, service: "Service", source: Union["OrderSource", "Order"]
    ) -> Iterable[ValidationError]:
        if isinstance(source, Order):
            supplier = source.supplier_order.supplier
        else:
            supplier = source.supplier

        if supplier:
            from_address_country = supplier.contact_address.country
            to_address_country = source.shipping_address.country

            if to_address_country.code == "US" and to_address_country == from_address_country:
                # If shipping from US to US, see if ship-from-store is available (always available to non-US brands)
                canvas_supplier_pk = re.search(r"store-(\d+)", service.identifier).group(1)
                canvas_supplier = Supplier.objects.filter(pk=canvas_supplier_pk).first()
                if canvas_supplier is None:
                    yield ValidationError(f"Canvas store id {canvas_supplier_pk} does not exist.", code="missing-store")

                brand_stocks = canvas_supplier.get_stock_statuses(source.product_ids, suppliers=[supplier])

                for product, qty in source._get_products_and_quantities().items():
                    # Does the brand carry it right now?
                    brand_logical_count = getattr(brand_stocks.get(product.pk), "logical_count", 0)

                    # The brand keep stock, so ship-from-store is not valid.
                    if brand_logical_count > 0:
                        msg = _(
                            "The {canvas_supplier} for {product!r} has "
                            "{logical_count} units, so do not offer to ship from store."
                        ).format(
                            canvas_supplier=canvas_supplier,
                            product=product,
                            logical_count=brand_logical_count,
                        )
                        yield ValidationError(msg, code="brand-keeps-stock")
