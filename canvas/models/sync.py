# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from datetime import timedelta
from enum import Enum

from django.conf import settings
from django.db import models
from django.db.models import QuerySet
from django.utils import timezone
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from encrypted_fields import fields
from enumfields import EnumIntegerField
from shuup.core.fields import CurrencyField, MoneyValueField, QuantityField
from shuup.core.models import Shop, ShopProduct, ShuupModel
from shuup.core.pricing import Priceful
from shuup.utils.money import Money
from shuup.utils.properties import PriceProperty, TaxfulPriceProperty, TaxlessPriceProperty

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.square_sync.fields import CallableHashKeyEncryptedSearchField
from canvas.utils import get_canvas_brick_and_mortar_suppliers

###
# Shuup Category -> Square Catalog of type Category
# Shuup ShopProduct -> Square Catalog of type Item (not actually sellable)
###


class StorePurchaseLineType(Enum):
    PRODUCT = 1
    DISCOUNT = 2
    REFUND = 3

    class Labels:
        PRODUCT = _("product")
        DISCOUNT = _("discount")
        REFUND = _("refund")


class SyncedObject(ShuupModel):
    square_id = models.CharField(
        max_length=200, null=True, help_text=_("Square's identifier for the record (unknown till after upsert)")
    )
    is_synced = models.BooleanField(default=False, help_text=_("Indicate if the object needs to be synced."))
    version = models.BigIntegerField(null=True)

    @staticmethod
    def _get_shop():
        """Get the appropriate shop. For now, only the US shop uses square."""
        return Shop.objects.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER).first()


class SyncedStoreLocation(SyncedObject):
    supplier = models.OneToOneField("shuup.Supplier", related_name="synced_location", on_delete=models.CASCADE)
    shop = models.ForeignKey("shuup.Shop", related_name="synced_shop", on_delete=models.CASCADE, null=True)

    @classmethod
    def get_sync(cls, store):
        if store in get_canvas_brick_and_mortar_suppliers():
            if hasattr(store, "synced_location"):
                return store.synced_location
            else:
                return cls.objects.create(supplier=store, shop=cls._get_shop())

    @classmethod
    def create_sync(cls, store):
        return cls.objects.update_or_create(supplier=store, shop=cls._get_shop(), defaults={"is_synced": False})


class SyncedCategory(SyncedObject):
    category = models.OneToOneField("shuup.Category", related_name="synced_category", on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = _("synced categories")

    @classmethod
    def get_sync(cls, category):
        if hasattr(category, "synced_category"):
            return category.synced_category
        else:
            return cls.objects.create(category=category)

    @classmethod
    def create_sync(cls, category):
        return cls.objects.update_or_create(category=category, defaults={"is_synced": False})


class SyncedShopProduct(SyncedObject):
    shop_product = models.OneToOneField(
        "shuup.ShopProduct", related_name="synced_shop_product", on_delete=models.CASCADE
    )
    standalone_square_id = models.CharField(
        max_length=50,
        null=True,
        help_text=_("Identifier for the sellable part of the product, even if there is no Shuup variation parent."),
    )
    standalone_version = models.BigIntegerField(
        null=True,
        help_text=_(
            "The Item (non-sellable) version is different from the ItemVariation (sellable) version. "
            "So a ShopProduct that is not a parent or child will need to maintain both versions."
        ),
    )

    def __str__(self):
        return f"{self.shop_product} is {'not' if not self.is_synced else ''} synced."

    @classmethod
    def get_sync(cls, shop_product):
        if hasattr(shop_product, "synced_shop_product"):
            return shop_product.synced_shop_product
        else:
            return cls.objects.create(shop_product=shop_product)

    @classmethod
    def create_sync(cls, shop_product):
        return cls.objects.update_or_create(shop_product=shop_product, defaults={"is_synced": False})

    def get_parent_shop_product(self) -> ShopProduct:
        parent_product = self.shop_product.product.variation_parent
        if parent_product:
            return parent_product.get_shop_instance(self.shop_product.shop)
        else:
            return self.shop_product

    def get_child_shop_products(self) -> "QuerySet[ShopProduct]":
        return ShopProduct.objects.filter(
            product_id__in=self.shop_product.product.variation_children.all().values("id"), shop=self._get_shop()
        )

    def get_syncable_children(self) -> "QuerySet[ShopProduct]":
        return self.get_child_shop_products().filter(synced_shop_product__is_synced=False)


class SyncedContact(SyncedObject):
    contact = models.OneToOneField("shuup.PersonContact", related_name="synced_contact", on_delete=models.CASCADE)
    shop = models.ForeignKey("shuup.Shop", related_name="synced_contact_shop", on_delete=models.CASCADE, null=True)

    @classmethod
    def get_sync(cls, contact):
        if hasattr(contact, "synced_contact"):
            return contact.synced_contact
        else:
            return cls.objects.create(contact=contact, shop=cls._get_shop())

    @classmethod
    def create_sync(cls, contact):
        return cls.objects.update_or_create(contact=contact, defaults={"is_synced": False})


class SquareCatalog(ShuupModel):
    """From the documentation: you should store the latest_time value returned by the SearchCatalogObjects endpoint,
    so you can use it in your next call to SearchCatalogObjects. Using a locally generated timestamp might cause you
    to miss updates as a result of time skew between the Square server and your application.
    """

    last_update = models.DateTimeField(
        auto_now_add=True,
        help_text=_(
            "From the documentation: when you receive a `catalog.version.updated` notification, call the "
            "`SearchCatalogObjects` endpoint and set the `begin_time` field to this value. When you call the "
            "`SearchCatalogObjects` endpoint, store the timestamp of the call here."
        ),
    )


def get_hash_key():
    return settings.FIELD_ENCRYPTION_KEYS[0]


class CanvasAuth(ShuupModel):
    access_token = fields.EncryptedCharField(max_length=255, help_text=_("Access token to Canvas Square app."))
    access_expiry = fields.EncryptedDateTimeField()
    refresh_token = fields.EncryptedCharField(max_length=255, help_text=_("Refresh token to Canvas Square app."))
    _merchant_id = fields.EncryptedCharField(max_length=64, help_text=_("The Canvas app's Square id."))
    merchant_id = CallableHashKeyEncryptedSearchField(hash_key=get_hash_key, encrypted_field_name="_merchant_id")


class SyncedStockCount(ShuupModel):
    """Keep records of inventory changes from Square to make sure that no event is synced more than once."""

    item = models.ForeignKey(SyncedShopProduct, related_name="stocked_shop_product", on_delete=models.CASCADE)
    store = models.ForeignKey(SyncedStoreLocation, related_name="stocking_location", on_delete=models.CASCADE)
    event_id = models.CharField(
        max_length=64, help_text=_("Square's id for the sync event, which is used for idempotency.")
    )
    timestamp = models.DateTimeField(help_text=_("Square's timestamp for when the event occurred."))
    qty_change = models.DecimalField(max_digits=10, decimal_places=5)

    @classmethod
    def delete_old_records(cls):
        """No need to keep these forever. Just keep them long enough to be able to trace any problems."""
        time_range = now() - timedelta(days=settings.INVENTORY_SYNC_DELETE_CUTOFF)
        cls.objects.filter(timestamp__gte=time_range).delete()


class UnsyncedStockCount(ShuupModel):
    """Keep records of inventory syncs to Square that somehow fail, so it can be tried again later."""

    shop_product = models.ForeignKey(
        "shuup.ShopProduct", related_name="unsynced_shop_product", on_delete=models.CASCADE
    )
    store = models.ForeignKey("shuup.Supplier", related_name="unsynced_location", on_delete=models.CASCADE)
    new_qty = models.CharField(max_length=26, help_text=_("The new total qty on hand."))
    timestamp = models.DateTimeField(help_text=_("Timestamp when the inventory change occurred."))

    def __str__(self):
        return f"{self.shop_product} - {self.store} ({self.new_qty} at {self.timestamp.strftime('%Y-%m-%d %H:%M:%S')})"


class FailedSyncs(ShuupModel):
    """Instead of repeatedly trying to sync something that doesn't want to sync, optionally keep it here."""

    class Meta:
        verbose_name_plural = _("Failed Syncs")

    shop_product = models.ForeignKey("shuup.ShopProduct", related_name="failed_shop_product", on_delete=models.CASCADE)
    problem = models.CharField(max_length=1000, help_text=_("The Square error message, or the exception message."))
    timestamp = models.DateTimeField(auto_now_add=True, help_text=_("The first time this object failed to sync"))


class StorePurchase(SyncedObject):
    shop = models.ForeignKey("shuup.Shop", related_name="store_purchases", on_delete=models.CASCADE, null=True)
    created_on = models.DateTimeField(auto_now=True, editable=False, db_index=True, verbose_name=_("created on"))
    modified_on = models.DateTimeField(auto_now=True, editable=False, db_index=True, verbose_name=_("modified on"))
    reference_number = models.CharField(max_length=64, db_index=True, verbose_name=_("reference number"))
    customer = models.ForeignKey(
        "shuup.Contact",
        related_name="store_purchases",
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("customer"),
    )
    currency = CurrencyField(verbose_name=_("currency"))
    store = models.ForeignKey("shuup.Supplier", related_name="store_purchases", on_delete=models.CASCADE, null=True)

    taxful_total_price = TaxfulPriceProperty("taxful_total_price_value", "currency")
    taxless_total_price = TaxlessPriceProperty("taxless_total_price_value", "currency")

    taxful_total_price_value = MoneyValueField(editable=False, verbose_name=_("grand total"), default=0)
    taxless_total_price_value = MoneyValueField(editable=False, verbose_name=_("taxless total"), default=0)

    includes_tax = False


class StorePurchaseLine(ShuupModel, Priceful):
    order = models.ForeignKey(StorePurchase, related_name="lines", on_delete=models.CASCADE)
    product = models.ForeignKey(
        "shuup.Product",
        blank=True,
        null=True,
        related_name="store_purchase_lines",
        on_delete=models.PROTECT,
        verbose_name=_("product"),
    )
    supplier = models.ForeignKey(
        "shuup.Supplier", related_name="store_purchase_lines", on_delete=models.CASCADE, null=True
    )
    type = EnumIntegerField(StorePurchaseLineType, default=StorePurchaseLineType.PRODUCT, verbose_name=_("line type"))
    quantity = QuantityField(verbose_name=_("quantity"), default=1)
    parent_line = models.ForeignKey(
        "self",
        related_name="child_lines",
        blank=True,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("parent line"),
    )

    base_unit_price = PriceProperty("base_unit_price_value", "order.currency", includes_tax="order.includes_tax")
    base_unit_price_value = MoneyValueField(verbose_name=_("unit price amount (undiscounted)"), default=0)

    discount_amount = PriceProperty("discount_amount_value", "order.currency", includes_tax="order.includes_tax")
    discount_amount_value = MoneyValueField(verbose_name=_("total amount of discount"), default=0)

    created_on = models.DateTimeField(auto_now_add=True, editable=False, db_index=True, verbose_name=_("created on"))
    modified_on = models.DateTimeField(
        default=timezone.now, editable=False, db_index=True, verbose_name=_("modified on")
    )

    accounting_identifier = models.CharField(max_length=32, blank=True, verbose_name=_("accounting identifier"))

    @property
    def tax_amount(self):
        """
        :rtype: shuup.utils.money.Money

        This property is expected by reports. But since these items were sold in a brick-and-mortar store,
        the taxes are the responsibility of the store, not the brand. Therefore, tax is always zero.
        """
        return Money(0, self.order.currency)


class CanvasVendorLineRevenue(models.Model):
    """Just like normal VendorOrderLineRevenue, but allowing for store purchases."""

    shop = models.ForeignKey("shuup.Shop", related_name="canvas_vendor_revenue", on_delete=models.CASCADE)
    order_line = models.OneToOneField(
        "shuup.OrderLine",
        verbose_name=_("order line"),
        related_name="online_vendor_revenue_percentage",
        on_delete=models.CASCADE,
        null=True,
    )
    purchase_line = models.OneToOneField(
        StorePurchaseLine,
        verbose_name=_("store purchase line"),
        related_name="pos_vendor_revenue_percentage",
        on_delete=models.CASCADE,
        null=True,
    )
    percentage = models.DecimalField(verbose_name=_("percentage"), max_digits=5, decimal_places=2)
    revenue = TaxlessPriceProperty("revenue_value", "shop.currency")
    revenue_value = MoneyValueField(editable=False, verbose_name=_("funds"), default=0)

    def __str__(self):
        online_order = bool(self.order_line)
        line = self.order_line if online_order else self.purchase_line
        title = f"{line.product} by {line.supplier}"
        if not online_order:
            title += f" from {self.purchase_line.order.store}"
        return title

    @classmethod
    def get_or_create(cls, **kwargs):
        line = kwargs.get("order_line")
        if isinstance(line, StorePurchaseLine):
            kwargs.pop("order_line")
            kwargs["purchase_line"] = line
        return cls.objects.get_or_create(**kwargs)
