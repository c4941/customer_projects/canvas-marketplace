# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.db import models
from shuup.core.models import ShuupModel


class CanvasProductExtra(ShuupModel):
    product = models.OneToOneField("shuup.Product", related_name="canvas_product", on_delete=models.CASCADE)
    material_and_fit = models.CharField(
        max_length=1000,
        blank=True,
    )
    care = models.CharField(
        max_length=1000,
        blank=True,
    )
    about_brands = models.CharField(
        max_length=1000,
        blank=True,
    )

    def __repr__(self) -> str:
        return "<{}:{}-{}>".format(self.__class__.__name__, self.pk, self.product.name)
