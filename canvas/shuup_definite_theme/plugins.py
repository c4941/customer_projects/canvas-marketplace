# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from django import forms
from django.utils.translation import ugettext_lazy as _

from shuup.front.template_helpers.general import (
    get_best_selling_products, get_newest_products, get_random_products
)
from shuup.xtheme.plugins.forms import TranslatableField
from shuup.xtheme.plugins.products import ProductHighlightPlugin


class DefiniteProductHighlightPlugin(ProductHighlightPlugin):
    identifier = "shuup_definite_theme.product_highlight"
    name = _("Definite Product Highlights")

    fields = [
        ("title", TranslatableField(label=_("Title"), required=False, initial="")),
        ("type", forms.ChoiceField(label=_("Type"), choices=[
            ("newest", "Newest"),
            ("best_selling", "Best Selling"),
            ("random", "Random"),
        ], initial="newest")),
        ("count", forms.IntegerField(label=_("Count"), min_value=1, initial=4)),
        ("cutoff_days", forms.IntegerField(label=_("Cutoff days"), min_value=1, initial=30)),
        ("orderable_only", forms.BooleanField(label=_("Only show in-stock and orderable items"),
                                              initial=True,
                                              required=False))
    ]

    def get_context_data(self, context):
        type = self.config.get("type", "newest")
        count = self.config.get("count", 4)
        cutoff_days = self.config.get("cutoff_days", 30)
        orderable_only = self.config.get("orderable_only", True)
        if type == "newest":
            products = get_newest_products(context, count, orderable_only)
        elif type == "best_selling":
            products = get_best_selling_products(context, count, cutoff_days, orderable_only)
        elif type == "random":
            products = get_random_products(context, count, orderable_only)
        else:
            products = []

        return {
            "request": context["request"],
            "title": self.get_translated_value("title"),
            "products": products
        }
