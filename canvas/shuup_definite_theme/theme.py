# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
import datetime
from collections import defaultdict

import jinja2
import pytz
from django import forms
from django.conf import settings
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from django.utils.translation import get_language
from django.utils.translation import ugettext_lazy as _

from shuup.admin.forms.widgets import TextEditorWidget
from shuup.core import cache
from shuup.core.models import ProductMode
from shuup.core.utils import context_cache
from shuup.front.themes import BaseThemeFieldsMixin
from shuup.front.utils.views import cache_product_things
from shuup.utils.djangoenv import has_installed
from shuup.utils.numbers import get_string_sort_order
from shuup.xtheme import Theme

WISHLIST_BUTTON_TEMPLATE = """
<button href="#" class="btn btn-default add-to-wishlist" data-shop-product-id="%d" data-url-override="%s">
<i class="fa fa-fw fa-heart"></i> <span class="text">Add to wishlist</span></button>
"""

COMPARISON_BUTTON_TEMPLATE = """
<button href="#" class="btn btn-default shuup-product-compare-add" data-product-id="%d">
<i class="fa fa-fw fa-bar-chart"></i> <span class="text">Add to compare</span></button>
"""

BODY_FONT_CUSTOM_RULES = [
    ".category-sidebar .sidebar-block .title",
    ".category-sidebar .sidebar-block .sidebar-block-title",
    ".category-sidebar .sidebar-block h2",
    ".category-sidebar .sidebar-subtitle",
    ".site-nav .language-nav .dropdown > a",
    ".site-nav .secondary-nav .cart > a",
    ".site-nav .user-nav li a",
    ".nav-toggler",
    ".main-menu ul li a",
    ".product-preview-modal .labels .sale-badge",
    ".product-card .labels .label-sale",
    ".product-card .labels .label-new",
    ".product-image .labels .label-sale",
    ".product-image .labels .label-new",
    ".xt-snippet-injection"
]


class ShuupDefiniteTheme(BaseThemeFieldsMixin, Theme):
    identifier = "shuup_definite_theme"
    name = _("Shuup Definite Theme")
    author = "Shuup Team"
    template_dir = "shuup_definite_theme/"

    guide_template = "shuup_definite_theme/shuup_definite_theme/guide.jinja"

    stylesheets = [
        {
            "stylesheet": "shuup_definite_theme/classic/shuup_definite_theme.css",
            "name": _("Classic"),
            "images": []
        },
        {
            "identifier": "luxury_matress",
            "stylesheet": "shuup_definite_theme/luxury_matress/shuup_definite_theme.css",
            "name": _("Luxury Matress"),
            "images": ["shuup_definite_theme/luxury_matress/default.jpg"]
        },
        {
            "identifier": "my_fluffy_friend",
            "stylesheet": "shuup_definite_theme/my_fluffy_friend/shuup_definite_theme.css",
            "name": _("My Fluffy Friend"),
            "images": ["shuup_definite_theme/my_fluffy_friend/default.jpg"]
        },
        {
            "identifier": "cosme_co",
            "stylesheet": "shuup_definite_theme/cosme_co/shuup_definite_theme.css",
            "name": _("Cosme Co"),
            "images": ["shuup_definite_theme/cosme_co/default.jpg"]
        },
        {
            "identifier": "mega_electronics",
            "stylesheet": "shuup_definite_theme/mega_electronics/shuup_definite_theme.css",
            "name": _("Mega Electronics"),
            "images": ["shuup_definite_theme/mega_electronics/default.jpg"]
        },
        {
            "identifier": "greatronics",
            "stylesheet": "shuup_definite_theme/greatronics/shuup_definite_theme.css",
            "name": _("Greatronics"),
            "images": ["shuup_definite_theme/greatronics/default.jpg"]
        },
        {
            "identifier": "kick_it_real",
            "stylesheet": "shuup_definite_theme/kick_it_real/shuup_definite_theme.css",
            "name": _("Kick it Real"),
            "images": ["shuup_definite_theme/kick_it_real/default.jpg"]
        },
        {
            "identifier": "organic_juicery",
            "stylesheet": "shuup_definite_theme/organic_juicery/shuup_definite_theme.css",
            "name": _("Organic Juicery"),
            "images": ["shuup_definite_theme/organic_juicery/default.jpg"]
        },
        {
            "identifier": "private_wineyard",
            "stylesheet": "shuup_definite_theme/private_wineyard/shuup_definite_theme.css",
            "name": _("Private Wineyard"),
            "images": ["shuup_definite_theme/private_wineyard/default.jpg"]
        },
        {
            "identifier": "coutore_flowers",
            "stylesheet": "shuup_definite_theme/coutore_flowers/shuup_definite_theme.css",
            "name": _("Couture Flowers"),
            "images": ["shuup_definite_theme/coutore_flowers/default.jpg"]
        },
        {
            "identifier": "patio_and_garden",
            "stylesheet": "shuup_definite_theme/patio_and_garden/shuup_definite_theme.css",
            "name": _("Patio And Garden"),
            "images": ["shuup_definite_theme/patio_and_garden/default.jpg"]
        },
        {
            "identifier": "love_and_light",
            "stylesheet": "shuup_definite_theme/love_and_light/shuup_definite_theme.css",
            "name": _("Love And Light"),
            "images": ["shuup_definite_theme/love_and_light/default.jpg"]
        },
        {
            "identifier": "bigwave_surf",
            "stylesheet": "shuup_definite_theme/bigwave_surf/shuup_definite_theme.css",
            "name": _("Bigwave surf"),
            "images": ["shuup_definite_theme/bigwave_surf/default.jpg"]
        },
    ]

    _theme_fields = [
        ("allow_company_linkage", forms.BooleanField(
            required=False, initial=False, label=_("Allow Linking Accounts to Company"))),
        ("product_new_days", forms.IntegerField(
            required=False, initial=14, label=_("Consider product new for days")
        )),
        ("show_sales_units", forms.BooleanField(
            required=False, initial=True, label=_("Show sales units in product boxes"),
            help_text=_("If you enable this the sales units are going to "
                        "be shown next to the quantity in product boxes."))),
        ("render_category_page_menu", forms.BooleanField(
            required=False, initial=True, label=_("Render categories on category page"),
            help_text=_("If you enable this the categories are going to be "
                        "shown next to products on category page."))),
        ("group_items_by_supplier", forms.BooleanField(
            required=False, initial=False,
            label=_("Group items by supplier"),
            help_text=_("Group items by the supplier in basket and order"))),
        ("product_card_extra_content", forms.CharField(
            widget=TextEditorWidget(),
            required=False, label=_("Product card extra content"),
            help_text=_("Enter extra content for the product card."))),
        ("show_product_card_actions", forms.BooleanField(
            required=False, initial=True, label=_("Show product quick actions in product cards on mouse over"),
            help_text=_("Whether to show product quick actions on mouse over in product cards on category page. Does not affect products displayed in carousels."))),
        ("show_product_card_actions_in_carousel", forms.BooleanField(
            required=False, initial=False, label=_("Show product quick actions in carousels on mouse over"),
            help_text=_("Whether to show product quick actions on mouse over in product cards displayed in carousels."))),
        ("show_wide_banners_on_home_page", forms.BooleanField(
            required=False, initial=False, label=_("Show 100% wide banners on home page"),
            help_text=_("Frontpage carousel and Frontpage images placeholders are shown with full width, expanded from screen edge to edge."))),
        ("search_on_top", forms.BooleanField(
            required=False, initial=False, label=_("Show search on top of the user and basket links"),
            help_text=_("If you enable this the search is going to be shown on top of the user and basket elements."))),
        ("full_width_menu_bar", forms.BooleanField(
            required=False, initial=False, label=_("Show full width menu bar on the navigation"),
            help_text=_("Main menu links element is extended to be full width"))),
        ("show_homepage_link_in_nav", forms.BooleanField(
            required=False, initial=True, label=_("Display homepage link in the main menu"))),
        ("homepage_icon_in_nav", forms.BooleanField(
            required=False, initial=True, label=_("Display homepage icon instead of text in the main menu"))),
        ("show_child_elements_in_megamenu", forms.BooleanField(
            required=False, initial=False, label=_("Show child categories in megamenu"),
            help_text=_("Level 3 categories are shown in the main navigation's megamenu. This doesn't affect mobile or category menus."))),
        ("collapsed_category_filters", forms.BooleanField(
            required=False, initial=True, label=_("Collapse category page sidebar filters"),
            help_text=_("As a default, show category filters as collapsed rather than displaying them all open at page load."))),
        ("focused_checkout", forms.BooleanField(
            required=False, initial=False, label=_("Hide main navigation and footer in checkout"))),
    ]

    @property
    def fields(self):
        fields = self._theme_fields + super(ShuupDefiniteTheme, self).get_base_fields()

        if has_installed("shuup_product_reviews"):
            fields.extend([
                ("show_product_review", forms.BooleanField(
                    required=False, initial=True,
                    label=_("Show product reviews rating in product card.")
                ))
            ])

        if has_installed("shuup_typography") and self._shop:
            from shuup_typography.models import FontFamily
            from shuup_typography.admin_module.widgets import QuickAddFontFamilySelect
            font_choices = [
                (custom_font.pk, custom_font.name)
                for custom_font in FontFamily.objects.filter(shop=self._shop)
            ]
            font_choices.insert(0, ("", "------"))

            fields.extend([
                ("body_font_family", forms.ChoiceField(
                    required=False, initial=None,
                    label=_("Body font family"),
                    choices=font_choices,
                    widget=QuickAddFontFamilySelect(editable_model="shuup_typography.FontFamily")
                )),
                ("header_font_family", forms.ChoiceField(
                    required=False, initial=None,
                    label=_("Header font family"),
                    choices=font_choices,
                    widget=QuickAddFontFamilySelect(editable_model="shuup_typography.FontFamily")
                )),
                ("base_font_size", forms.CharField(
                    label=_("Base font size"),
                    required=False,
                    help_text=_("Enter a valid CSS font-size value, e.g: 12px, 1.2em.")
                ))
            ])

        if has_installed("shuup_multivendor"):
            fields.extend([
                ("show_vendor_link", forms.BooleanField(
                    required=False,
                    initial=True,
                    label=_("Show vendors page link in main menu")
                )),
                ("vendor_link_menu_title", forms.CharField(
                    required=False,
                    label=_("Main menu title for Vendors link"),
                    help_text=_("Enter menu title for vendors link. Defaults to Vendors.")
                )),
            ])

        return fields

    def render_custom_font_style(self):
        if not has_installed("shuup_typography"):
            return ""

        from shuup_typography.models import FontFamily
        from shuup_typography.renderer import render_custom_font_style
        body_font_family_id = self.get_setting("body_font_family")
        header_font_family_id = self.get_setting("header_font_family")
        base_font_size = self.get_setting("base_font_size")
        body_font_family = None
        header_font_family = None

        if body_font_family_id:
            body_font_family = FontFamily.objects.filter(pk=body_font_family_id).first()

        if header_font_family_id:
            header_font_family = FontFamily.objects.filter(pk=header_font_family_id).first()

        return render_custom_font_style(
            body_font_family, header_font_family, base_font_size, body_font_custom_rules=BODY_FONT_CUSTOM_RULES)

    def get_view(self, view_name):
        import shuup_definite_theme.views as views
        return getattr(views, view_name, None)

    def _format_cms_links(self, shop, **query_kwargs):
        if "shuup.simple_cms" not in settings.INSTALLED_APPS:
            return
        from shuup.simple_cms.models import Page
        for page in Page.objects.visible(shop).filter(**query_kwargs):
            yield {"url": "/%s" % page.url, "text": force_text(page)}

    def get_wishlist_button(self, product, url_override):
        if "shuup_wishlist" not in settings.INSTALLED_APPS:
            return ""
        return mark_safe(WISHLIST_BUTTON_TEMPLATE % (product.pk, url_override))

    def get_comparison_button(self, product):
        if "shuup_product_comparison" not in settings.INSTALLED_APPS:
            return ""
        return mark_safe(COMPARISON_BUTTON_TEMPLATE % product.pk)

    def get_cms_navigation_links(self, request):
        return self._format_cms_links(request.shop, visible_in_menu=True)

    def get_variation_information(self, request, product):
        language = get_language()

        key, val = context_cache.get_cached_value(
            identifier="shop_product", item=product, context=request,
            language=language, theme="definite_theme")
        if val is not None:
            return val

        info = {}
        info["variation_children"] = []
        if product.mode == ProductMode.SIMPLE_VARIATION_PARENT:
            info["variation_children"] = cache_product_things(
                request,
                sorted(
                    product.variation_children.language(language).all(),
                    key=lambda p: get_string_sort_order(p.variation_name or p.name)
                )
            )
            info["orderable_variation_children"] = [
                p for p in info["variation_children"]
                if p.get_shop_instance(request.shop).is_orderable(
                    supplier=None,
                    customer=request.customer,
                    quantity=1
                )
                ]
        elif product.mode == ProductMode.VARIABLE_VARIATION_PARENT:
            from shuup.front.utils.product import get_orderable_variation_children
            variation_variables = product.variation_variables.all().prefetch_related("values")
            orderable_children, is_orderable = get_orderable_variation_children(product, request, variation_variables)
            info["orderable_variation_children"] = orderable_children
            info["variation_orderable"] = is_orderable
            info["variation_variables"] = variation_variables
        context_cache.set_cached_value(key, info)
        return info

    def product_is_new(self, request, product):
        past = datetime.datetime.now(tz=pytz.UTC) - datetime.timedelta(days=self.get_setting("product_new_days", 14))
        return (product.created_on >= past)

    def show_product_card_actions(self):
        return self.get_setting("show_product_card_actions", True)

    def show_product_card_actions_in_carousel(self):
        return self.get_setting("show_product_card_actions_in_carousel", False)

    def allow_company_linkage(self):
        return self.get_setting("allow_company_linkage", False)

    def show_product_detail_section(self):
        return self.get_setting("show_product_detail_section", False)

    def show_sales_units(self):
        return self.get_setting("show_sales_units", True)

    def show_wide_banners_on_home_page(self):
        return self.get_setting("show_wide_banners_on_home_page", False)

    def render_category_page_menu(self):
        return self.get_setting("render_category_page_menu", False)

    def full_width_menu_bar(self):
        return self.get_setting("full_width_menu_bar", False)

    def show_homepage_link_in_nav(self):
        return self.get_setting("show_homepage_link_in_nav", True)

    def homepage_icon_in_nav(self):
        return self.get_setting("homepage_icon_in_nav", True)

    def bigger_cart_text(self):
        return self.get_setting("bigger_cart_text", False)

    def search_on_top(self):
        return self.get_setting("search_on_top", False)

    def show_child_elements_in_megamenu(self):
        return self.get_setting("show_child_elements_in_megamenu", False)

    def get_vendor_menu_link(self):
        if not has_installed("shuup_multivendor"):
            return ""

        if not self.get_setting("show_vendor_link"):
            return ""

        menu_text = _("Vendors")

        if self.get_setting("vendor_link_menu_title"):
            menu_text = self.get_setting("vendor_link_menu_title")

        return menu_text

    def collapsed_category_filters(self):
        return self.get_setting("collapsed_category_filters", True)

    def focused_checkout(self):
        return self.get_setting("focused_checkout", False)

    def get_product_filter_fields(self, form, exclude):
        return [field for field in form if field.name not in exclude]

    def render_product_review_rating(self, product, customer_ratings_title=None,
                                     show_recommenders=False, minified=False):
        if not has_installed("shuup_product_reviews"):
            return ""

        cache_key = "_product_review_rendered_rating_%d" % product.pk
        cached_rating = cache.get(cache_key)

        if cached_rating:
            return cached_rating

        from shuup_product_reviews.utils import render_product_review_ratings
        rendered = render_product_review_ratings(product, customer_ratings_title, show_recommenders, minified)

        if rendered:
            cache.set(cache_key, rendered)
            return rendered

        return ""

    def group_lines_by_supplier(self, lines):
        groupped_lines = defaultdict(list)
        if lines:
            for line in lines:
                groupped_lines[line.supplier].append(line)
        return groupped_lines

    @jinja2.contextfunction
    def render_extra_product_card_content(self, context, product, shop_product, supplier=None):
        if self.get_setting("product_card_extra_content"):
            context = dict(
                request=context["request"],
                product=product,
                shop_product=shop_product,
                supplier=supplier
            )
            from shuup.xtheme.resources import JinjaMarkupResource
            return JinjaMarkupResource(self.get_setting("product_card_extra_content"), context).render()
        return ""
