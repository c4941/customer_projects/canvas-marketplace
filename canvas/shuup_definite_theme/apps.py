# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
import shuup.apps


class AppConfig(shuup.apps.AppConfig):
    name = "shuup_definite_theme"
    label = "shuup_definite_theme"
    provides = {
        "xtheme": "shuup_definite_theme.theme:ShuupDefiniteTheme",
        "xtheme_plugin": [
            "shuup_definite_theme.plugins:DefiniteProductHighlightPlugin",
        ],
        "simple_cms_template": [
            "shuup_definite_theme.templates:SimpleCMSDefiniteSidebar",
        ],
    }

    def ready(self):
        from django.db.models.signals import post_save
        from shuup.xtheme.models import ThemeSettings
        from .signal_handler import handle_settings_save
        post_save.connect(handle_settings_save, sender=ThemeSettings)
