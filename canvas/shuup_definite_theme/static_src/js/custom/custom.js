function slowScrollToTop() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
}

function isOffcanvasNavOpen() {
    return document.body.classList.contains("offcanvas-open");
}

function openOffcanvasNav() {
    window.scrollTo(0, 0);
    $("#offcanvas-nav-toggler").attr("aria-expanded", "true");
    $("#offcanvas-nav").attr("aria-expanded", "true");
    document.body.classList.add("offcanvas-open");
}

function closeOffcanvasNav() {
    $("#offcanvas-nav-toggler").attr("aria-expanded", "false");
    $("#offcanvas-nav").attr("aria-expanded", "false");
    document.body.classList.remove("offcanvas-open");
    document.body.classList.add("offcanvas-animating");
    setTimeout(function () {
        document.body.classList.remove("offcanvas-animating");
    }, 400);
}

function toggleClearAll() {
    var showClearAll = false;
    var $inputs = $(".filter-block :input");
    for(var i = 0; i < $inputs.length && !showClearAll; i++) {
        var $el = $($inputs[i]);
        var type = $el.prop("type");
        if(type === "checkbox" || type === "radio") {
            if($el.prop("checked")) {
                showClearAll = true;
            }
        } else if($el.val().length > 0){
            showClearAll = true;
        }
    }
    if(showClearAll) {
        $("#clear-all-filters").removeClass("hidden");
    } else {
        $("#clear-all-filters").addClass("hidden");
    }
}

$(function() {

    var isMobile = false;
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
         isMobile = true;
    }

    $("#scroll_top").click(function(event) {
        event.preventDefault();
        slowScrollToTop();
    });

    // Set up frontpage carousel
    $(".frontpage-carousel").carousel({
        interval: 6000,
        cycle: true,
        pause: false
    });

    // Set up owl carousel for product list with 5 items
    $(".owl-carousel.five").owlCarousel({
        margin: 20,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsiveClass: true,
        responsive: {
            0: { // breakpoint from 0 up
                items : 1,
                slideBy: 1
            },
            425: { // breakpoint from 425px up
                items : 2,
                slideBy: 2
            },
            640: { // breakpoint from 640 up
                items : 4,
                slideBy: 2
            },
            1200: { // breakpoint from 992 up
                items : 5,
                slideBy: 3
            }
        }
    });

    //add tooltip triggers to data-attribute html with data-toggle=tooltip
    $("[data-toggle='tooltip']").tooltip({
        delay: { "show": 750, "hide": 100 }
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() > 400) {
            $("#scroll_top").addClass("visible");
        } else {
            $("#scroll_top").removeClass("visible");
        }
    });

    // Toggle subcategories if category has children
    $("#offcanvas-nav .toggle-icon").on("click", function toggleNavItems(e) {
        e.stopPropagation();
        if ($(this).parent().find("ul").length) {
            e.preventDefault();
            if ($(this).parent().hasClass("is-open")) {
                $(this).parent().removeClass("is-open");
                $(this).attr("aria-expanded", "false");
            } else {
                $(this).parent().addClass("is-open");
                $(this).attr("aria-expanded", "true");
            }
        }
    });

    // Go through all the current nodes and open their parents
    $("#offcanvas-nav li.current").each(function () {
        $(this).parents("li").addClass("is-open");
    });

    $(document).trigger('offcanvasClassNamesGiven');

    /* Close offcanvas navigation if user clicks outside the nav element
    when the navigation is open */
    $(document).on("click", function(e) {
        if (isOffcanvasNavOpen() && !$(e.target).closest("#offcanvas-nav").length) {
            closeOffcanvasNav();
        }
    });

    // Toggle the visibility of offcanvas navigation when the menu icon is clicked
    $("#offcanvas-nav-toggler").click(function(e) {
        e.stopPropagation();
        isOffcanvasNavOpen() ? closeOffcanvasNav() : openOffcanvasNav();
    });

    // if user has mobile client, first click only opens the product card, second redirects to product page
    if (isMobile) {
        $(document).on("click", ".product-card.load-on-hover", function(e) {
            if ($(this).hasClass("touched")){
                return;
            } else {
                e.preventDefault();
                $(".touched").trigger("mouseleave").removeClass("touched");
                $(this).addClass("touched");
            }
        });
    }

    // product card hovering
    $(document).on("mouseenter", ".product-card.load-on-hover", function() {
        var $actions = $(this).find(".actions").first();
        if ($actions.length && $actions.is(":empty")) {
            window.fetchProductActions($(this));
        } else {
            window.adjustProductCardOnMouseEnter($(this));
        }
    });

    $(document).on("mouseleave", ".product-card.load-on-hover", function() {
        window.adjustProductCardOnMouseLeave($(this));
    });

    $(".category-collapse").on("shown.bs.collapse", function (e) {
        if ($(this).is(e.target)) {
            $(this).closest("li").addClass("open");
        }
    });

    $(".category-collapse").on("hidden.bs.collapse", function (e) {
        if ($(this).is(e.target)) {
            $(this).closest("li").removeClass("open");
        }
    });

    $(".filter-block :input").on("change", function() {
        toggleClearAll();
    });
    toggleClearAll();
});

$(document).ready(function() {
    $(".selectpicker select").selectpicker();
    $(".category-link").on("mouseenter mouseleave", function () {
        if (!$(this).find(".mega-menu-container").length) {
            return;
        }
        var $container = $(this).find(".mega-menu-container");

        var off = $container.offset();
        var l = off.left;
        var w = $container.width();
        var docW = $(".content-wrap").width();

        var isEntirelyVisible = (l + w <= docW);

        if (!isEntirelyVisible) {
            $(this).addClass("edge");
        } else {
            $(this).removeClass("edge");
        }
    });
    $("button.btn-remove").click(function(e) {
        e.preventDefault();
        var form = $(this).parent("form[name='basket_partial_remove']");
        $("<input />").attr("type", "hidden")
                  .attr("name", $(this).attr("name"))
                  .attr("value", "")
                  .appendTo(form);
        form.submit();
    });

    if(navigator.userAgent.match(/Windows Phone/i)){
        $(".content-wrap").addClass("windows-phone");
    }

    // Our footer Flexbox solution just does not work
    // with webkit or ms "flex-support". So just detected
    // "real" flex here so we can adjust the footer for
    // modern browsers. Did not want to activate Modernizr
    // just for this.
    var doc = document.body || document.documentElement;
    var style = doc.style;
    if ("flex" in style || "WebkitFlex" in style || "msFlex" in style) {
        $(".content-wrap,#main").addClass("supports-flex");
    }

    window.addEventListener("Shuup.ProductListLoaded", function() {
        if (window.initializeWishlist) {
            window.initializeWishlist();
        }
        if (window.initializeProductComparison) {
            window.initializeProductComparison();
        }
    });
});

$(document).bind('offcanvasClassNamesGiven', function () {
    // Make sure that we have accessible menu toggler buttons by defining
    // the aria-expanded state after we have given the <li> elements the
    // "is-open" class.
    $("#offcanvas-nav li.is-open").each(function () {
        $(this).find(".toggle-icon").first().attr("aria-expanded", "true");
    });
});
