# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from django.utils.translation import ugettext as _


class SimpleCMSDefiniteSidebar(object):
    name = _("Page with sidebar (Definite Theme)")
    template_path = "shuup_definite_theme/page_sidebar.jinja"
