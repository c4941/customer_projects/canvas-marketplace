# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from django.db.models.signals import post_save

from shuup import configuration
from shuup.utils.djangoenv import has_installed


def handle_settings_save(sender, instance, **kwargs):
    configuration.set(
        instance.shop,
        "allow_company_registration",
        instance.get_setting("allow_company_linkage", False))


def handle_product_review_post_save(sender, instance, **kwargs):
    from shuup.core import cache
    cache.bump_version("_product_review_rendered_rating_%d" % instance.product_id)


if has_installed("shuup_product_reviews"):
    from shuup_product_reviews.models import ProductReview
    post_save.connect(handle_product_review_post_save, sender=ProductReview)
