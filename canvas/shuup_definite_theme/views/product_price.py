# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from shuup.front.themes.views._product_price import ProductPriceView
from shuup.front.views.product import ProductDetailView


class ProductListPriceView(ProductPriceView):
    template_name = "shuup_definite_theme/product/detail_order_section_list.jinja"


def product_list_price(request):
    return ProductListPriceView.as_view()(request, pk=request.GET["id"])


def product_price(request):
    return ProductPriceView.as_view()(request, pk=request.GET["id"])


class ProductActionsView(ProductDetailView):
    template_name = "shuup_definite_theme/product/product_actions.jinja"


def product_actions(request):
    return ProductActionsView.as_view()(request, pk=request.GET["id"])
