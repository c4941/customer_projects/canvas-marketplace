# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2019, Shoop Commerce Ltd. All rights reserved.
#
# This source code is licensed under the SHUUP® ENTERPRISE EDITION -
# END USER LICENSE AGREEMENT executed by Anders Innovations Inc. DBA as Shuup
# and the Licensee.
from shuup.front.themes.views import basket_partial, product_preview
from shuup.utils import update_module_attributes

from .product_price import product_actions, product_list_price, product_price

__all__ = [
    "basket_partial",
    "product_actions",
    "product_list_price",
    "product_preview",
    "product_price"
]

update_module_attributes(__all__, __name__)
