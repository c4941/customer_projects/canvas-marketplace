var gulp = require("gulp");
var less = require("gulp-less");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var plumber = require("gulp-plumber");
var minifycss = require("gulp-cssnano");
var gutil = require("gulp-util");
var merge = require("merge-stream");
var autoprefixer = require("gulp-autoprefixer");
var PRODUCTION = gutil.env.production || process.env.NODE_ENV === "production";

var NODE_PATH = "node_modules/";
var SOURCE_PATH = "static_src/";
var DEST_PATH = "static/shuup_definite_theme/";

var themes = [
    "bigwave_surf",
    "classic",
    "cosme_co",
    "coutore_flowers",
    "greatronics",
    "kick_it_real",
    "love_and_light",
    "luxury_matress",
    "mega_electronics",
    "my_fluffy_friend",
    "organic_juicery",
    "patio_and_garden",
    "private_wineyard",
];

gulp.task("less", function() {
    var autoprefixerConfig = {
        browsers: ["last 20 versions", "> 1%"],
        remove: false,
        cascade: false,
    };
    var tasks = themes.map(function(folder) {
        return gulp.src([
            NODE_PATH + "dropzone/dist/dropzone.css",
            NODE_PATH + "owl.carousel/dist/assets/owl.carousel.min.css",
            NODE_PATH + "simplelightbox/dist/simplelightbox.min.css",
            NODE_PATH + "select2/dist/css/select2.css",
            SOURCE_PATH + "less/theme_variations/" + folder + "/style.less"
        ])
            .pipe(plumber({}))
            .pipe(less())
            .pipe(concat("shuup_definite_theme.css"))
            .pipe((PRODUCTION ? minifycss({autoprefixer: autoprefixerConfig}) : autoprefixer(autoprefixerConfig)))
            .pipe(gulp.dest(DEST_PATH + folder + "/"));
    });
    return merge(tasks);
});

gulp.task("js", function() {
    return gulp.src([
        NODE_PATH + "simplelightbox/dist/simple-lightbox.js",
        SOURCE_PATH + "js/custom/update_price.js",
        SOURCE_PATH + "js/custom/product_actions.js",
        SOURCE_PATH + "js/custom/custom.js",
    ])
        .pipe(plumber({}))
        .pipe(concat("shuup_definite_theme.js"))
        .pipe((PRODUCTION ? uglify() : gutil.noop()))
        .pipe(gulp.dest(DEST_PATH + "js/"));
});

gulp.task("owl-images", function() {
    var tasks = themes.map(function(folder) {
        return gulp.src(NODE_PATH + "owl.carousel/dist/assets/owl.video.play.png")
            .pipe(gulp.dest(DEST_PATH + folder + "/"));
    });
    return merge(tasks);
});

gulp.task("copy_fonts", function() {
    return gulp.src([
        NODE_PATH + "bootstrap/fonts/*",
        NODE_PATH + "font-awesome/fonts/*"
    ]).pipe(gulp.dest(DEST_PATH + "fonts/"));
});

gulp.task("default", gulp.parallel(["js", "less", "copy_fonts", "owl-images"]));

gulp.task("watchFiles", function() {
    gulp.watch([SOURCE_PATH + "less/**/*.less"], gulp.parallel(["less"]));
    gulp.watch([SOURCE_PATH + "js/**/*.js"], gulp.parallel(["js"]));
});

const watch = gulp.series(["default"], ["watchFiles"]);

exports.watch = watch;
