# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2022, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import csv
import json
from argparse import ArgumentTypeError

from django.core.management import BaseCommand

from canvas.models import SyncedShopProduct


def json_file(filename):
    """Validate that `filename` has a json extension."""
    if not filename.endswith(".json"):
        raise ArgumentTypeError("Please provide the path to a json file for data input.")
    return filename


ORDER_ID = "order_id"
ORDER_DATE = "order_date"
ORDER_TYPE = "type"
SHOULD_IMPORT = "should_have_imported"
REASON_NOT_IMPORTED = "reason_not_imported"
NOTES = "notes"


class Command(BaseCommand):
    """Find out why each order would or would not have imported."""

    reasons = (ORDER_ID, ORDER_DATE, ORDER_TYPE, SHOULD_IMPORT, REASON_NOT_IMPORTED, NOTES)
    data = []
    item_variation_ids = SyncedShopProduct.objects.all().values_list("square_id", flat=True)
    item_standalone_ids = SyncedShopProduct.objects.all().values_list("standalone_square_id", flat=True)

    def add_arguments(self, parser):
        parser.add_argument("json_file", type=json_file, help="The path and filename of the Sqare purchase data dump.")

    def print_csv(self, filename):
        with open(filename, "w") as file:
            writer = csv.DictWriter(file, fieldnames=self.reasons)
            writer.writeheader()
            for row in self.data:
                writer.writerow(row)

    def analyze_purchase(self, order):
        data = {
            ORDER_ID: order["id"],
            ORDER_DATE: order["updated_at"],
            SHOULD_IMPORT: True,  # imported yes/no
            REASON_NOT_IMPORTED: "",  # reason not imported
            ORDER_TYPE: "purchase",
            NOTES: "",
        }
        lines = order.get("line_items")
        if lines is None:
            data[SHOULD_IMPORT] = False
            data[REASON_NOT_IMPORTED] = "No line items to import"
        for line in lines:
            square_id = line.get("catalog_object_id")
            if not square_id:
                # Todo: is there a sample of a missing catalog object id in a return? to put in specifics?
                data[
                    REASON_NOT_IMPORTED
                ] += f"A line item did not sync due to missing catalog object id ({line['note']}); "
            if square_id and square_id not in self.item_standalone_ids and square_id not in self.item_variation_ids:
                data[REASON_NOT_IMPORTED] += (
                    f"A line item did not sync because the product "
                    f"({line['name']} - {square_id}) wasn't in the marketplace, or "
                    f"there is something wrong with it; "
                )

        if data[REASON_NOT_IMPORTED] and data[SHOULD_IMPORT]:
            data[SHOULD_IMPORT] = False

        self.data.append(data)

    def analyze_refund(self, order):
        data = {
            ORDER_ID: order["id"],
            ORDER_DATE: order["updated_at"],
            ORDER_TYPE: "refund",
            SHOULD_IMPORT: True,  # imported yes/no
            REASON_NOT_IMPORTED: "",  # reason not imported
            NOTES: "source_order_id = ",
        }
        # Check for expected key.
        returns = order.get("returns")
        if not returns:
            data[SHOULD_IMPORT] = False
            data[REASON_NOT_IMPORTED] = "Missing 'returns' key"

        # Check the line items
        for store_return in returns:
            data[NOTES] += f"{store_return.get('source_order_id')}; "
            for line in store_return["return_line_items"]:
                square_id = line.get("catalog_object_id")
                if not square_id:
                    # Todo: is there a sample of a missing catalog object id in a return? to put in specifics?
                    data[REASON_NOT_IMPORTED] += "A refund line did not sync due to missing catalog object id; "
                if square_id and square_id not in self.item_standalone_ids and square_id not in self.item_variation_ids:
                    data[REASON_NOT_IMPORTED] += (
                        f"A refund line did not sync because the product "
                        f"({line['name']} - {square_id}) wasn't in the marketplace, or "
                        f"there is something wrong with it; "
                    )
        if data[REASON_NOT_IMPORTED] and data[SHOULD_IMPORT]:
            data[SHOULD_IMPORT] = False

        self.data.append(data)

    def handle(self, *args, **options):
        filename = options["json_file"]
        with open(filename) as file:
            orders = json.load(file)

        for order in orders["orders"]:
            if order.get("refunds"):
                self.analyze_refund(order)
            else:
                self.analyze_purchase(order)

        path = "/".join(filename.split("/")[:-1])
        new_name = filename.split("/")[-1].split(".")[0]
        self.print_csv(f"{path}/{new_name}-analysis.csv")
