# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from datetime import time
from decimal import Decimal

from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.sites.models import Site
from django.core.management import BaseCommand
from django.db.transaction import atomic
from django.utils.translation import activate
from shuup import configuration
from shuup.core.defaults.order_statuses import create_default_order_statuses
from shuup.core.models import (
    Category,
    Contact,
    ContactGroup,
    PersonContact,
    ProductType,
    SalesUnit,
    Shop,
    ShopProduct,
    ShopStatus,
    Supplier,
    SupplierModule,
    SupplierShop,
    SupplierType,
    TaxClass,
    get_person_contact,
)
from shuup.xtheme import get_current_theme, set_current_theme
from shuup_multivendor.models import SupplierPrice, SupplierUser
from shuup_multivendor.utils.configuration import set_product_tax_class_options, set_shipping_method_tax_class_options
from shuup_multivendor.utils.funds import (
    set_minimum_withdraw_amount,
    set_paypal_withdraw_enabled,
    set_vendor_default_revenue_percentage,
    set_vendor_funds_enabled,
    set_wire_transfer_eu_withdraw_enabled,
    set_wire_transfer_withdraw_enabled,
)
from shuup_opening_hours.models import SupplierOpeningPeriod, Weekday
from shuup_wishlist.models import Wishlist, WishlistPrivacy

from canvas.constants import (
    CANVAS_EU_SHOP_IDENTIFIER,
    CANVAS_MAIN_SHOP_IDENTIFIER,
    CANVAS_SHIPPING_TAX_CLASS_IDENTIFIER,
    CANVAS_STORE_OWNER_PERMISSION_GROUP_IDENTIFIER,
    CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER,
)
from canvas.models import SquareCatalog, SustainabilityGoal, SyncedCategory, SyncedStoreLocation
from canvas.utils import ensure_store_extra, get_canvas_brick_and_mortar_suppliers


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--reset-vendor-funds", action="store_true", help="Reset vendor funds to the project's default setup"
        )

    def handle(self, *args, **options):
        activate(settings.PARLER_DEFAULT_LANGUAGE_CODE)

        create_default_order_statuses()

        ProductType.objects.update_or_create(identifier="default", defaults=dict(name="Standard Product"))
        SalesUnit.objects.update_or_create(identifier="pcs", defaults=dict(name="Pieces", symbol="pcs"))
        product_tax_class, __ = TaxClass.objects.update_or_create(
            identifier="product", defaults=dict(name="Product Tax Class")
        )
        shipping_tax_class, __ = TaxClass.objects.update_or_create(
            identifier=CANVAS_SHIPPING_TAX_CLASS_IDENTIFIER, defaults=dict(name="Shipping Tax Class")
        )
        TaxClass.objects.update_or_create(identifier="payment", defaults=dict(name="Payment Tax Class"))

        # Create main shop
        us_shop, __ = Shop.objects.get_or_create(
            identifier=CANVAS_MAIN_SHOP_IDENTIFIER,
            defaults=dict(
                name="Canvas US",
                public_name="Canvas US",
                domain="canvas-us",
                currency="USD",
                maintenance_mode=False,
                status=ShopStatus.ENABLED,
            ),
        )
        eu_shop, __ = Shop.objects.get_or_create(
            identifier=CANVAS_EU_SHOP_IDENTIFIER,
            defaults=dict(
                name="Canvas EU",
                public_name="Canvas EU",
                domain="canvas-eu",
                currency="EUR",
                maintenance_mode=False,
                status=ShopStatus.ENABLED,
            ),
        )
        if not get_current_theme(us_shop):
            set_current_theme("canvas", us_shop)
        if not get_current_theme(eu_shop):
            set_current_theme("canvas", eu_shop)

        us_site = Site.objects.order_by("pk").first()
        if not us_site:
            us_site = Site()
        if us_site:
            us_site.name = us_shop.public_name
            us_site.domain = us_shop.domain
            us_site.save()

        eu_site = Site.objects.order_by("pk")[1:].first()
        if not eu_site:
            eu_site = Site()
        if eu_site:
            eu_site.name = eu_shop.public_name
            eu_site.domain = eu_shop.domain
            eu_site.save()

        # Create admin user
        admin_user, admin_created = User.objects.get_or_create(
            username="admin",
            defaults=dict(
                email="admin@example.com",
                first_name="Admin",
                last_name="Superuser",
                is_staff=True,
                is_active=True,
                is_superuser=True,
            ),
        )
        if admin_created:
            admin_user.set_password("admin")
            admin_user.save()

        staff, staff_created = User.objects.get_or_create(
            username="staff",
            defaults=dict(
                email="staff@example.com",
                first_name="First",
                last_name="Staff",
                is_staff=True,
                is_active=True,
            ),
        )
        if staff_created:
            staff.set_password("staff")
            staff.save()

        us_shop.staff_members.add(staff)
        eu_shop.staff_members.add(staff)

        # Create a singled vendor for managing vendor menus.
        # Keep it non-approved so that it won't show up in the frontend.
        vendor_user, vendor_user_created = User.objects.get_or_create(
            username="vendor",
            defaults=dict(
                email="vendor@example.com",
                first_name="First",
                last_name="Vendor",
                is_staff=True,
                is_active=True,
            ),
        )

        if vendor_user_created:
            vendor_user.set_password("vendor")
            vendor_user.save()

        vendor, __ = Supplier.objects.get_or_create(identifier="canvas-vendor", name="Canvas Vendor", enabled=True)
        SupplierShop.objects.update_or_create(shop=us_shop, supplier=vendor, defaults={"is_approved": False})
        SupplierShop.objects.update_or_create(shop=eu_shop, supplier=vendor, defaults={"is_approved": False})
        vendor.shops.set([us_shop, eu_shop])
        SupplierUser.objects.get_or_create(shop=us_shop, supplier=vendor, user=vendor_user)
        SupplierUser.objects.get_or_create(shop=eu_shop, supplier=vendor, user=vendor_user)
        contact = get_person_contact(vendor_user)
        contact.shops.set([us_shop, eu_shop])

        # Setup permission groups.
        staff_group, __ = Group.objects.get_or_create(name=settings.STAFF_PERMISSION_GROUP_NAME)
        staff_menu_group, __ = Group.objects.get_or_create(name="Staff Menu Edit Permissions")
        vendor_group, __ = Group.objects.get_or_create(name=settings.VENDORS_PERMISSION_GROUP_NAME)
        vendor_menu_group, __ = Group.objects.get_or_create(name="Vendor Menu Edit Permissions")

        staff.groups.add(staff_group)
        staff.groups.add(staff_menu_group)
        vendor_user.groups.add(vendor_group)
        vendor_user.groups.add(vendor_menu_group)

        for shop in (us_shop, eu_shop):
            configuration.set(shop, "staff_user_permission_group", staff_group.pk)
            configuration.set(shop, "vendor_user_permission_group", vendor_group.pk)

            set_product_tax_class_options(shop, [product_tax_class])
            set_shipping_method_tax_class_options(shop, [shipping_tax_class])

            # Configure vendor funds
            if options["reset_vendor_funds"]:
                set_vendor_funds_enabled(shop, True)
                set_wire_transfer_withdraw_enabled(shop, True)
                set_wire_transfer_eu_withdraw_enabled(shop, True)
                set_paypal_withdraw_enabled(shop, True)
                set_minimum_withdraw_amount(shop, Decimal("0"))
                set_vendor_default_revenue_percentage(shop, Decimal("100"))

        self._create_canvas_suppliers(us_shop, eu_shop)
        self._create_sustainability_goals()
        contact_ids = PersonContact.objects.filter(user_id__in=[staff.id, vendor_user.id, admin_user.id]).values_list(
            "contact_ptr", flat=True
        )
        self._create_wishlists(contact_ids, [us_shop, eu_shop])
        self._create_syncs()
        self._add_custom_module()

    @staticmethod
    def _add_custom_module():
        """Replace Simple Supplier with Canvas Simple Supplier, if needed."""
        canvas_module, created = SupplierModule.objects.get_or_create(module_identifier="canvas_supplier")
        original_simple_supplier = SupplierModule.objects.filter(module_identifier="simple_supplier").first()
        canvas_name = "Canvas Simple Supplier"
        if canvas_module.name != canvas_name:
            canvas_module.name = canvas_name
            canvas_module.save()
        for supplier in Supplier.objects.all():
            supplier.supplier_modules.remove(original_simple_supplier)
            supplier.supplier_modules.add(canvas_module)

    @staticmethod
    def _create_canvas_suppliers(*shops: Shop) -> None:
        permission_group, __ = Group.objects.get_or_create(name=CANVAS_STORE_OWNER_PERMISSION_GROUP_IDENTIFIER)
        discount_group, created = ContactGroup.objects.get_or_create(
            identifier=CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER
        )
        if created:
            discount_group.name = "Store Purchasing"
            discount_group.save()

        suppliers = (("canvas-bowery", "Canvas Bowery"),)
        for identifier, name in suppliers:
            supplier, supplier_created = Supplier.objects.update_or_create(
                identifier=identifier,
                defaults=dict(
                    type=SupplierType.INTERNAL,
                ),
            )
            if supplier_created:
                supplier.name = name
                supplier.save()
            ensure_store_extra(supplier)

            if supplier.opening_periods.count() == 0:
                for weekday in Weekday:
                    SupplierOpeningPeriod.objects.get_or_create(
                        supplier=supplier, weekday=weekday, start=time(hour=8), end=time(hour=20)
                    )

            vendor_user, vendor_user_created = User.objects.get_or_create(
                username=identifier,
                defaults=dict(
                    email=f"{identifier}@example.com",
                    first_name="First",
                    last_name="Last",
                    is_staff=True,
                    is_active=True,
                ),
            )

            if vendor_user_created:
                vendor_user.set_password(identifier)
                vendor_user.save()

            for shop in shops:
                SupplierShop.objects.update_or_create(shop=shop, supplier=supplier, defaults={"is_approved": True})
                SupplierUser.objects.get_or_create(shop=shop, supplier=supplier, user=vendor_user)

            supplier.shops.set(shops)
            contact = get_person_contact(vendor_user)
            contact.shops.set(shops)

            vendor_user.groups.add(permission_group)
            discount_group.members.add(contact)

    @staticmethod
    def _create_sustainability_goals():
        """Create sustainability goals if they don't exist."""
        with atomic():
            _create_sdg("01", "Goal 1: No Poverty", "End poverty in all its forms everywhere.")
            _create_sdg(
                "02",
                "Goal 2: Zero Hunger",
                "End hunger, achieve food security and improved nutrition and promote sustainable agriculture.",
            )
            _create_sdg(
                "03",
                "Goal 3: Good Health and Well-Being",
                "Ensure healthy lives and promote well-being for all at all ages.",
            )
            _create_sdg(
                "04",
                "Goal 4: Quality Education",
                "Ensure inclusive and equitable quality education and promote lifelong learning opportunities for all.",
            )
            _create_sdg("05", "Goal 5: Gender Equality", "Achieve gender equality and empower all women and girls.")
            _create_sdg(
                "06",
                "Goal 6: Clean Water and Sanitation",
                "Ensure availability and sustainable management of water and sanitation for all.",
            )
            _create_sdg(
                "07",
                "Goal 7: Affordable and Clean Energy",
                "Ensure access to affordable, reliable, sustainable and modern energy for all.",
            )
            _create_sdg(
                "08",
                "Goal 8: Decent Work and Economic Growth",
                "Promote sustained, inclusive and sustainable economic growth, full and productive employment "
                "and decent work for all.",
            )
            _create_sdg(
                "09",
                "Goal 9: Industry, Innovation and Infrastructure",
                "Build resilient infrastructure, promote inclusive and sustainable industrialization and foster "
                "innovation.",
            )
            _create_sdg("10", "Goal 10: Reduced Inequalities", "Reduce inequality within and among countries.")
            _create_sdg(
                "11",
                "Goal 11: Sustainable Cities and Communities",
                "Make cities and human settlements inclusive, safe, resilient and sustainable.",
            )
            _create_sdg(
                "12",
                "Goal 12: Responsible Consumption and Production",
                "Ensure sustainable consumption and production patterns.",
            )
            _create_sdg("13", "Goal 13: Climate Action", "Take urgent action to combat climate change and its impacts.")
            _create_sdg(
                "14",
                "Goal 14: Life Below Water",
                "Conserve and sustainably use the oceans, sea and marine resources for sustainable development.",
            )
            _create_sdg(
                "15",
                "Goal 15: Life on Land",
                "Protect, restore and promote sustainable use of terrestrial ecosystems, sustainably manage forests,"
                "combat desertification, and halt and reverse land degradation and halt biodiversity loss.",
            )
            _create_sdg(
                "16",
                "Goal 16: Peace, Justice and Strong Institutions",
                "Promote peaceful and inclusive societies for sustainable development, provide access to justice for "
                "all and build effective, accountable and inclusive institutions at all levels.",
            )
            _create_sdg(
                "17",
                "Goal 17: Partnerships for the Goals",
                "Strengthen the means of implementation and revitalize the global partnership for sustainable "
                "development.",
            )

    @staticmethod
    def _create_wishlists(contact_ids, shops):
        """Create wishlists for staff users if they're completely missing."""
        contacts = Contact.objects.filter(id__in=contact_ids, wishlist__isnull=True)
        Wishlist.objects.bulk_create(
            [
                Wishlist(shop=shop, customer=contact, name="My wishlist", privacy=WishlistPrivacy.PRIVATE)
                for contact in contacts
                for shop in shops
            ]
        )

    @staticmethod
    def _create_syncs():
        """The following records need to be synced:
        1. brick-and-mortar suppliers,
        2. categories,
        3. shop products that have been made available to brick-and-mortar suppliers,
        4. and supplier prices for the brick-and-mortar suppliers.

        A single instance of SquareCalendar should keep track of the timestamps needed by the webhook notification
        handlers.
        """
        for store in get_canvas_brick_and_mortar_suppliers():
            if not SyncedStoreLocation.get_sync(store):
                SyncedStoreLocation.create_sync(store)

        has_syncs = SyncedCategory.objects.all().values("category_id")
        needs_syncs = Category.objects.exclude(id__in=has_syncs)
        for category in needs_syncs:
            # Note: can't bulk_create when inherited models are involved
            SyncedCategory.get_sync(category)

        # Create records for handling syncs from Square to Shuup
        if SquareCatalog.objects.count() == 0:
            SquareCatalog.objects.create()

        has_store_prices = SupplierPrice.objects.filter(supplier__in=get_canvas_brick_and_mortar_suppliers()).values(
            "product_id"
        )
        needs_store_prices = ShopProduct.objects.exclude(product_id__in=has_store_prices).filter(
            suppliers__in=get_canvas_brick_and_mortar_suppliers()
        )
        SupplierPrice.objects.bulk_create(
            [
                SupplierPrice(
                    shop=Shop.objects.filter(identifier="canvas-us").first(),
                    product=shop_product.product,
                    supplier=store,
                    amount_value=shop_product.default_price_value,
                )
                for shop_product in needs_store_prices
                for store in shop_product.suppliers.filter(id__in=get_canvas_brick_and_mortar_suppliers().values("id"))
            ]
        )


def _create_sdg(identifier, name, desc):
    sdg = SustainabilityGoal.objects.filter(identifier=identifier).first()
    if not sdg:
        sdg = SustainabilityGoal(identifier=identifier)
        sdg.name = name
        sdg.description = desc
        sdg.save()
