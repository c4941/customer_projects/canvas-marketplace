# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import logging
from typing import Callable, Optional, Type

from django.conf import settings
from django.core.management import BaseCommand
from django.db.models import Q
from django.utils.timezone import now
from shuup.core.models import Category, Contact, Shop, ShopProduct, Supplier  # noqa F401

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.models import SyncedCategory, SyncedContact, SyncedShopProduct, SyncedStoreLocation
from canvas.models.sync import CanvasAuth, FailedSyncs, SyncedObject
from canvas.square_sync.client import ContactClient, ProductClient

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = ProductClient()
        self.us_shop = Shop.objects.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER).first()

    @staticmethod
    def _handle_errors(sync_class, sync_ids, err_msg, err_code, err_detail) -> bool:
        """Something went wrong. Log relevant data and re-set the record to be tried again later. Decide whether or
        not the sync should stop for now.
        """
        api_limit_reached = err_msg == "RATE_LIMIT_ERROR"
        version_mismatch = err_code == "VERSION_MISMATCH"
        is_ssp = sync_class.__name__ == "SyncedShopProduct"

        logger.info(f"Sync failed: {err_msg}, {err_code}, {err_detail}. {sync_class.__name__} ids [{sync_ids}].")
        if settings.RETRY_FAILED_SYNCS or api_limit_reached or version_mismatch or not is_ssp:
            # If the api limit was reached, then it wasn't a problem on Shuup's end and is safe to retry.
            # A version mismatch isn't really a problem because it will be tried again with the correct version.
            # SyncedShopProducts are the ones causing issues, so it's safe to retry Categories and Locations.
            sync_class.objects.filter(id__in=sync_ids).update(is_synced=False)
        else:
            for sync_id in sync_ids:
                ssp = SyncedShopProduct.objects.filter(id=sync_id).first()
                failed = FailedSyncs.objects.filter(shop_product=ssp.shop_product, problem=err_detail)
                if not failed:
                    FailedSyncs.objects.create(shop_product=ssp.shop_product, problem=err_detail)

        # If the api limit is reached, then the sync needs to know to stop now.
        return api_limit_reached

    def _sync(
        self, column_name: str, sync_class: Type[SyncedObject], call_client: Callable, is_bad: Optional[Callable]
    ):
        bad_objects = []
        total_synced = 0

        while sync_obj := sync_class.objects.filter(is_synced=False).first():
            if sync_obj in bad_objects:
                # There are records that can't be synced, and we've already come full circle.
                break

            model_obj = getattr(sync_obj, column_name)
            if is_bad is not None and is_bad(model_obj):
                bad_objects.append(sync_obj)
                continue

            sync_class.objects.filter(id=sync_obj.id).update(is_synced=True)
            sync_obj.refresh_from_db()
            response = call_client(sync_obj)

            if response.is_success():
                total_synced += 1
            else:
                stop_now = self._handle_errors(
                    sync_class,
                    [sync_obj.id],
                    response.errors[0]["category"],
                    response.errors[0]["code"],
                    response.errors[0]["detail"],
                )
                if stop_now:
                    # Square has an arbitrary cap on how many calls they permit, the exact number of which is not
                    # officially documented. They simply recommend backing off.
                    break
                bad_objects.append(sync_obj)

        obj_class = eval("".join([word.title() for word in column_name.split("_")]))
        logger.debug(f"{obj_class} objects (not counting children) synced: {total_synced}.")
        if bad_objects:
            logger.info(f"{obj_class.__name__} objects not synced: {len(bad_objects)}")

    def sync_locations(self):
        """Sync a new store location to Square. This will be uncommon."""

        def is_not_ready_to_sync(store):
            """Certain things are required by Square that are not required by Shuup."""

            return store.opening_periods.count() == 0 and store.contact_address.city is not None

        self._sync("supplier", SyncedStoreLocation, self.client.sync_location, is_not_ready_to_sync)

    def sync_categories(self):
        """Sync new/updated Categories to Square."""

        self._sync("category", SyncedCategory, self.client.sync_category, None)

    def sync_shop_products(self):
        """Sync new/updated ShopProducts to Square."""
        bad_sync_ids = []
        total_synced = 0

        while sp_sync := (
            SyncedShopProduct.objects.filter(is_synced=False, shop_product__shop=self.us_shop)
            .exclude(id__in=bad_sync_ids)
            .first()
        ):
            if sp_sync.id in bad_sync_ids:
                break

            try:
                parent = SyncedShopProduct.get_parent_shop_product(sp_sync)
            except Exception as e:
                # Sometimes it tries to sync EU products and fails. Cause still undetermined. Meanwhile, don't break.
                self._handle_errors(
                    SyncedShopProduct,
                    [sp_sync],
                    "",
                    "",
                    e.args[0],
                )
                bad_sync_ids.append(sp_sync.id)
                continue

            if parent == sp_sync.shop_product and not sp_sync.shop_product.product.is_variation_parent():
                family = [sp_sync.shop_product]
                parent_sync = sp_sync
            else:
                family = ShopProduct.objects.filter(
                    Q(
                        product__variation_parent=parent.product,
                    )
                    | Q(id=parent.id),
                    shop=self.us_shop,
                )
                parent_sync = SyncedShopProduct.get_sync(parent)

            # Sync the whole family all at once, since they're considered by Square to be a single unit.
            SyncedShopProduct.objects.filter(shop_product__in=family).update(is_synced=True)
            try:
                response = self.client.sync_shop_product(parent_sync)
            except Exception as e:
                self._handle_errors(
                    SyncedShopProduct,
                    SyncedShopProduct.objects.filter(shop_product__in=family).values_list("id", flat=True),
                    "",
                    "",
                    e.args[0],
                )
                bad_sync_ids.append(sp_sync.id)
                continue
            if response is None:
                # We really shouldn't have tried to sync a child, so report it
                self._handle_errors(
                    SyncedShopProduct,
                    SyncedShopProduct.objects.filter(shop_product__in=family).values_list("id", flat=True),
                    "",
                    "",
                    "This is a child, and it tried to sync without its parent.",
                )
                bad_sync_ids.append(sp_sync.id)
                continue

            if response.is_success():
                total_synced += len(family)
            else:
                stop_now = self._handle_errors(
                    SyncedShopProduct,
                    SyncedShopProduct.objects.filter(shop_product__in=family).values_list("id", flat=True),
                    response.errors[0]["category"],
                    response.errors[0]["code"],
                    response.errors[0]["detail"],
                )
                if stop_now:
                    # Square has an arbitrary cap on how many calls they permit, the exact number of which is not
                    # officially documented. They simply recommend backing off. So, try again later.
                    break
                bad_sync_ids.append(sp_sync.id)

        logger.debug(f"ShopProduct objects (not counting children) synced: {total_synced}.")
        if bad_sync_ids:
            logger.info(f"ShopProduct objects not synced: {len(bad_sync_ids)}")

    def sync_contacts(self):
        """Sync new/updated Categories to Square."""

        self._sync("contact", SyncedContact, ContactClient().sync_contact, None)

    def handle(self, *args, **options):
        auth_object = CanvasAuth.objects.first()

        if auth_object and auth_object.access_token:
            logger.debug(f"Start sync: {now().strftime('%Y-%m-%d %H:%M:%S')} UTC")
            self.sync_locations()
            self.sync_categories()
            self.sync_shop_products()
            self.sync_contacts()
            logger.debug(f"End sync: {now().strftime('%Y-%m-%d %H:%M:%S')} UTC.")

        else:
            logger.info("Canvas has not granted permission to sync to Square.")
