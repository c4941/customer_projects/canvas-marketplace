# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import logging

from django.core.management import BaseCommand

from canvas.models import SyncedStockCount

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        deleted = SyncedStockCount.delete_old_records()
        deleted = deleted[1]["canvas.SyncedStockCount"] if deleted is not None else 0
        logger.debug(f"{deleted} old SyncedStockCount records were deleted.")
