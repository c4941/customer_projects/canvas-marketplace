# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2022, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import logging
from datetime import timedelta

from django.core.management import BaseCommand
from django.db.models import Q
from django.utils.timezone import now
from shuup.core.models import Category, Contact, Shop, ShopProduct, Supplier  # noqa F401
from shuup_multivendor.revenue_calculator import get_vendor_revenue_calculator

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.models import (
    CanvasAuth,
    StorePurchase,
    StorePurchaseLine,
    SyncedContact,
    SyncedShopProduct,
    SyncedStoreLocation,
)
from canvas.models.sync import StorePurchaseLineType
from canvas.square_sync.client import StorePurchaseClient, StorePurchaseRefundClient
from canvas.utils import get_canvas_brick_and_mortar_suppliers

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "square_order_id", nargs="?", type=str, help="The square id of the order to attempt to import."
        )
        parser.add_argument("--refund", nargs="?", type=str, help="If present, import `square_order_id` as a refund.")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.purchase_client = StorePurchaseClient()
        self.refunds_client = StorePurchaseRefundClient()
        # Square is used only for US stores. This may change if Square expands its offerings in the EU.
        self.us_shop = Shop.objects.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER).first()
        self.store_ids = get_canvas_brick_and_mortar_suppliers().values_list("id", flat=True)
        self.revenue_calculator = get_vendor_revenue_calculator()
        self.all_lines = set()

    def _find_purchase_ids(self):
        """The online site launched before the store integration did. Therefore, import all purchases that
        have occurred since go-live at 2021-09-15 16:17:00.
        """
        purchase_ids = []
        to_import = []
        cursor = None
        response, cutoff = self.purchase_client.get_purchase_imports(cursor)

        if response.is_success() and response.body.get("order_entries"):
            purchase_ids += [entry["order_id"] for entry in response.body["order_entries"]]
            if response.body.get("cursor"):
                while cursor:
                    response, cutoff = self.purchase_client.get_purchase_imports(cursor)
                    purchase_ids += response.body["order_entries"]
                    cursor = response.body.get("cursor")

            # Note: the lower-bound timestamp used by Square is inclusive. So filter out purchases that have been
            # imported already.
            cutoff = cutoff - timedelta(hours=2)  # check the last two hours, just to be safe
            existing = StorePurchase.objects.filter(created_on__gt=cutoff).values_list("reference_number", flat=True)

            for purchase_id in purchase_ids:
                if purchase_id not in existing:
                    to_import.append(purchase_id)

        return to_import

    @staticmethod
    def _get_synced_contact(order):
        customer_id = order.get("customer_id")
        if customer_id is None:
            tenders = order.get("tenders")
            if tenders:
                customer_id = tenders[0].get("customer_id")
        return SyncedContact.objects.filter(square_id=customer_id).first()

    @staticmethod
    def _get_synced_product(square_id):
        return (
            SyncedShopProduct.objects.filter(Q(square_id=square_id) | Q(standalone_square_id=square_id))
            .prefetch_related("shop_product", "shop_product__product")
            .first()
        )

    def _create_purchase(self, order, store):
        purchase = StorePurchase()
        purchase.square_id = order["id"]
        purchase.shop = self.us_shop
        purchase.reference_number = order.get("reference_id") or order["id"]
        purchase.store = store
        purchase.currency = self.us_shop.currency
        purchase.taxful_total_price_value = order["total_money"]["amount"] / 100
        purchase.taxless_total_price_value = (order["total_money"]["amount"] - order["total_tax_money"]["amount"]) / 100

        synced_contact = self._get_synced_contact(order)
        if synced_contact:
            purchase.customer = synced_contact.contact

        return purchase

    def _construct_store_purchase_refund(self, order, store, refund=None):
        if refund is None:
            refund = self._create_purchase(order, store)

        returns = order.get("returns")
        if returns is None:
            return None, None

        # Create refund lines.
        refund_lines = []
        for store_return in returns:
            for refund_item in store_return["return_line_items"]:
                refund_line = StorePurchaseLine()
                refund_line.order = refund
                if not refund_item.get("catalog_object_id"):
                    # This is a custom sale; we can't do anything with it
                    logger.info(
                        f"There was no `catalog_object_id` to find for a line in order {refund.reference_number}."
                    )
                    continue
                returned_synced_product = self._get_synced_product(refund_item["catalog_object_id"])
                if returned_synced_product is None:
                    logger.info(
                        "This item is not available in the online store. Catalog item id:",
                        refund_item["catalog_object_id"],
                        "Order id:",
                        order["id"],
                    )
                    # This item is not available in the online store (e.g. gift cards, etc. )
                    continue
                refund_line.product = returned_synced_product.shop_product.product
                refund_line.supplier = returned_synced_product.shop_product.suppliers.exclude(
                    id__in=self.store_ids
                ).first()
                refund_line.type = StorePurchaseLineType.REFUND
                refund_line.quantity = refund_item["quantity"]
                refund_line.base_unit_price_value = (refund_item["gross_return_money"]["amount"] / 100) * -1
                refund_lines.append(refund_line)

        return refund, refund_lines

    def _construct_store_purchase(self, order, store):
        purchase = self._create_purchase(order, store)

        line_items = order.get("line_items")
        if line_items is None:
            # We can't do anything if there are no line items
            return None, None

        lines = []
        for order_line in order["line_items"]:
            if not order_line.get("catalog_object_id"):
                # We can't do anything without a product.
                continue

            line = StorePurchaseLine()
            line.order = purchase
            synced_product = self._get_synced_product(order_line["catalog_object_id"])
            if synced_product is None:
                logger.info(
                    "This item is not available in the online store. Catalog item id:",
                    order_line["catalog_object_id"],
                    "Order id:",
                    order["id"],
                )
                # This item is not available in the online store (e.g. gift cards, etc. )
                continue
            brand = synced_product.shop_product.suppliers.exclude(id__in=self.store_ids).first()
            line.supplier = brand

            line.product = synced_product.shop_product.product
            line.quantity = order_line["quantity"]
            line.base_unit_price_value = order_line["base_price_money"]["amount"] / 100  # we want dollars, not cents
            lines.append(line)

            if order_line.get("total_discount_money") and order_line["total_discount_money"]["amount"] > 0:
                # Square sends the amount of discount to be collected, but we want the amount of the discount itself.
                discount_line = StorePurchaseLine()
                discount_line.order = purchase
                discount_line.discount_amount_value = order_line["total_discount_money"]["amount"] / 100
                discount_line.quantity = 1
                discount_line.supplier = brand
                discount_line.type = StorePurchaseLineType.DISCOUNT
                lines.append(discount_line)

        return purchase, lines

    def import_store_purchases(self, new_purchase_ids=None, action=None):
        if action is None:
            action = self._construct_store_purchase
        if new_purchase_ids is None:
            new_purchase_ids = self._find_purchase_ids()
        store_locations = SyncedStoreLocation.objects.all().prefetch_related("supplier")

        new_imports = []
        new_import_lines = []
        start = 0
        end = 99
        for location in store_locations:
            while start < len(new_purchase_ids):
                # Square will accept only 100 order ids at a time
                response = self.purchase_client.sync_purchases(new_purchase_ids[start:end], location.square_id)
                start = end
                end += 100
                if response.is_success() and response.body.get("orders"):
                    for order in response.body["orders"]:
                        purchase, lines = action(order, location.supplier)
                        if lines:
                            # E.g. if the only item was a gift card.
                            new_import_lines += lines
                            new_imports.append(purchase)

        for purchase in new_imports:
            # Can't bulk create an inherited model
            purchase.save()
        for line in new_import_lines:
            if hasattr(line, "order"):
                # Now that the StorePurchases have PKs, make sure the lines have those PKs.
                line.order_id = line.order.id
            else:
                logger.info(
                    f"A store purchase line (product {line.product} by {line.supplier} did not have a valid order"
                )
        lines = StorePurchaseLine.objects.bulk_create(new_import_lines)
        self.all_lines.update(lines)

        return new_imports

    @staticmethod
    def _get_refunds(response, purchase_ids: dict):
        if response.is_success() and response.body.get("refunds"):
            for refund in response.body["refunds"]:
                if refund["location_id"] in purchase_ids:
                    purchase_ids[refund["location_id"]].append(refund["order_id"])
                else:
                    purchase_ids[refund["location_id"]] = [refund["order_id"]]

    def import_refunds(self):
        purchase_ids = {}

        # Fetch refunds that have occurred since the last time we checked.
        cursor = None
        response = self.refunds_client.get_refunds(cursor)
        self._get_refunds(response, purchase_ids)

        cursor = response.body.get("cursor")
        while cursor:
            response = self.refunds_client.get_refunds(cursor)
            self._get_refunds(response, purchase_ids)
            cursor = response.body.get("cursor")

        orders_with_refunds = []
        for location_id, order_ids in purchase_ids.items():
            response = self.purchase_client.sync_purchases(order_ids, location_id)
            orders_with_refunds += response.body["orders"]

        # Find the parent purchases (and import if necessary).
        existing_purchases = {
            sp.square_id: sp
            for sp in StorePurchase.objects.filter(
                square_id__in=[order["id"] for order in orders_with_refunds]
            ).values_list("square_id", flat=True)
        }
        if len(existing_purchases) != len(orders_with_refunds):
            # Just in case, look for anything that has been imported already.
            to_import = [
                order_id
                for location, orders in purchase_ids.items()
                for order_id in orders
                if order_id not in existing_purchases
            ]
            purchases = self.import_store_purchases(
                new_purchase_ids=to_import, action=self._construct_store_purchase_refund
            )
            refund_lines = StorePurchaseLine.objects.filter(order__in=purchases)
            self.all_lines.update(refund_lines)

    def calculate_earnings(self):
        # Refresh the queryset before trying to create revenue records
        lines = StorePurchaseLine.objects.filter(id__in=[line.id for line in self.all_lines])
        for line in lines.iterator():
            self.revenue_calculator.ensure_order_line_revenue(line)

        # Now check for missed lines, just in case:
        missing_lines = StorePurchaseLine.objects.filter(pos_vendor_revenue_percentage__isnull=True)
        for line in missing_lines:
            self.revenue_calculator.ensure_order_line_revenue(line)

    def handle(self, *args, **options):
        auth_object = CanvasAuth.objects.first()
        square_id = options.get("square_order_id")

        if auth_object and auth_object.access_token:
            if square_id:
                if options.get("refund"):
                    raise NotImplementedError()
                else:
                    self.import_store_purchases([square_id])
                    self.calculate_earnings()
            else:
                logger.debug(f"Start sync: {now().strftime('%Y-%m-%d %H:%M:%S')} UTC")
                self.import_store_purchases()
                self.import_refunds()
                self.calculate_earnings()
                logger.debug(f"End sync: {now().strftime('%Y-%m-%d %H:%M:%S')} UTC.")

        else:
            logger.info("Canvas has not granted permission to sync to Square.")
