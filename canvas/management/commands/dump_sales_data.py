# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2022, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import json
from argparse import ArgumentTypeError
from datetime import datetime

import pytz
from django.core.management import BaseCommand
from shuup.core.models import Shop

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.square_sync.client import CanvasSquareClient
from canvas.square_sync.utils import get_purchase_batch_data, get_purchase_params
from canvas.utils import get_canvas_brick_and_mortar_suppliers


class DebugStorePurchaseClient(CanvasSquareClient):
    def __init__(self, start, end, *args, **kwargs):
        super().__init__()
        y, m, d = start.split("-")
        # Store is located in NYC.
        self.start_date = datetime(year=int(y), month=int(m), day=int(d), tzinfo=pytz.timezone("America/New_York"))
        y, m, d = end.split("-")
        self.end_date = datetime(year=int(y), month=int(m), day=int(d), tzinfo=pytz.timezone("America/New_York"))

    def get_order_ids(self, cursor):
        search_params = get_purchase_params(cursor, self.start_date)
        search_params["query"]["filter"]["date_time_filter"]["created_at"]["end_at"] = self.end_date.isoformat()
        return self.client.orders.search_orders(body=search_params)

    def get_purchase_data(self, square_ids, location_id):
        data = get_purchase_batch_data(square_ids, location_id)
        return self.client.orders.batch_retrieve_orders(body=data)


def formatted_string(arg):
    error = "Please enter a date in YYYY-MM-DD format."
    if type(arg) != str:
        raise ArgumentTypeError(error)
    date = arg.split("-")
    if len(date) != 3:
        raise ArgumentTypeError(error)
    for item in date:
        if not item.isnumeric():
            raise ArgumentTypeError(error)
    return arg


class Command(BaseCommand):
    """A command to dump all sales data from date X to date Y into a .json file. This will help since we
    don't have access to Canvas' Square data.
    """

    def add_arguments(self, parser):
        parser.add_argument("start_date", type=formatted_string, help="The start date in YYYY-MM-DD format")
        parser.add_argument(
            "end_date", type=formatted_string, help="The first date to NOT include in YYYY-MM-DD format"
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.purchase_client = None
        self.refunds_client = None
        # Just US shop, nothing to import for EU.
        self.us_shop = Shop.objects.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER).first()
        # Just Bowery store
        self.store_id = (
            get_canvas_brick_and_mortar_suppliers().values_list("synced_location__square_id", flat=True).first()
        )

    def _find_purchase_ids(self):
        """Find all the Square order IDs contained during the target timeframe."""
        purchase_ids = []
        cursor = None
        response = self.purchase_client.get_order_ids(cursor)

        if response.is_success() and response.body.get("order_entries"):
            purchase_ids += [entry["order_id"] for entry in response.body["order_entries"]]
            if response.body.get("cursor"):
                while cursor:
                    response = self.purchase_client.get_order_ids(cursor)
                    purchase_ids += response.body["order_entries"]
                    cursor = response.body.get("cursor")

        return purchase_ids

    def import_store_data(self):
        purchase_ids = self._find_purchase_ids()
        start_date = self.purchase_client.start_date
        filename = f"{start_date.strftime('%B_%Y')}_purchase_data.json"

        start = 0
        end = 99
        with open(filename, "w") as file:
            while start < len(purchase_ids):
                # Square will accept only 100 order ids at a time
                response = self.purchase_client.get_purchase_data(purchase_ids[start:end], self.store_id)
                start = end
                end += 100
                if response.is_success() and response.body.get("orders"):
                    json.dump(response.body["orders"], file, indent=2)
                    file.write("\n")

    def handle(self, *args, **options):
        self.purchase_client = DebugStorePurchaseClient(options["start_date"], options["end_date"])
        self.import_store_data()
