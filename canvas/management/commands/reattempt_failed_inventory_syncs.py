# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import logging

from django.core.management import BaseCommand

from canvas.models.sync import UnsyncedStockCount
from canvas.square_sync.client import InventoryClient

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = InventoryClient()

    def handle(self, *args, **options):
        for attempt in UnsyncedStockCount.objects.all().prefetch_related(
            "shop_product__synced_shop_product", "store__synced_location"
        ):
            response = self.client.retry_sync_inventory(attempt)

            if response is None:
                logger.warning(f"Deleted 24-hour-old sync: {attempt}")
            elif not response.is_success():
                logger.warning(f"Sync failed again: {attempt}")
            else:
                logger.debug(f"Sync re-try successful! {attempt}")
