# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import random

from django.conf import settings
from django.core.management import BaseCommand
from django.db.models import Q
from django.db.transaction import atomic
from django.utils.translation import activate
from shuup.core.models import Order, PersonContact, Supplier
from shuup_product_reviews.enums import ReviewStatus
from shuup_vendor_reviews.models import VendorReview


def get_random_reviewer(supplier):
    """
    Get a random reviewer that didn't review this supplier yet
    """
    return (
        PersonContact.objects.exclude(
            Q(supplier_reviews__supplier=supplier) | Q(user__is_staff=True) | Q(user__is_superuser=True)
        )
        .order_by("?")
        .first()
    )


def create_vendor_review_for_order_line(order_line, rating, comment=None, approved=True, reviewer=None, supplier=None):
    return VendorReview.objects.create(
        shop=order_line.order.shop,
        supplier=supplier,
        reviewer=reviewer,
        rating=rating,
        comment=comment,
        status=(ReviewStatus.APPROVED if approved else ReviewStatus.PENDING),
    )


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--order", type=str, required=True, help="Order ID to generate the review")

    def handle(self, *args, **options):
        activate(settings.PARLER_DEFAULT_LANGUAGE_CODE)
        order_id = options["order"]
        order = Order.objects.get(pk=order_id)
        order_line = order.lines.first()
        shop = order.shop

        with atomic():
            # select 50 random suppliers for this shop
            for supplier in Supplier.objects.filter(shops=shop).order_by("?")[:50]:
                print("Generating reviews for {}-{}".format(supplier.pk, supplier))

                # create reviews with comments
                for i in range(random.randint(5, 10)):
                    reviewer = get_random_reviewer(supplier)
                    comment = "This is random review from %s" % reviewer
                    create_vendor_review_for_order_line(order_line, i, comment, True, reviewer, supplier)

                # create reviews without comments
                for i in range(random.randint(5, 10)):
                    reviewer = get_random_reviewer(supplier)
                    comment = "This is random review from %s" % reviewer
                    create_vendor_review_for_order_line(order_line, i, comment, True, reviewer, supplier)
