# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.utils.translation import ugettext_lazy as _
from shuup.admin.forms.fields import ObjectSelect2MultipleField
from shuup.core.models import Contact, Shop
from shuup.reports.forms import BaseReportForm
from shuup.reports.report import ShuupReportBase
from shuup.utils.dates import to_datetime_range

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.models.sync import StorePurchase, StorePurchaseLineType


class StorePurchaseReportForm(BaseReportForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        customer_field = ObjectSelect2MultipleField(
            label=_("Customer"),
            model=Contact,
            required=False,
            help_text=_("Filter report results by customer."),
        )
        customers = self.initial_contacts("customer")
        if customers:
            customer_field.initial = customers
            customer_field.widget.choices = [(obj.pk, obj.name) for obj in customers]

        self.fields.pop("shop")

    def initial_contacts(self, key):
        if self.data and key in self.data:
            return Contact.objects.filter(pk__in=self.data.getlist(key))
        return []


class StorePurchaseReport(ShuupReportBase):
    identifier = "store_purchase_report"
    title = _("In-Store Purchases Report")
    form_class = StorePurchaseReportForm

    schema = [
        {"key": "reference_number", "title": _("Purchase no.")},
        {"key": "order_date", "title": _("Imported on")},
        {"key": "customer", "title": _("Customer")},
        {"key": "order_line_quantity", "title": _("Line item quantity")},
        {"key": "order_taxless_amount", "title": _("Total before tax")},
        {"key": "order_total_amount", "title": _("Total")},
    ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.shop = Shop.objects.get(identifier=CANVAS_MAIN_SHOP_IDENTIFIER)

    def get_objects(self):
        (start, end) = to_datetime_range(self.start_date, self.end_date)
        queryset = StorePurchase.objects.filter(shop=self.shop, created_on__range=(start, end))
        customer = self.options.get("customer")
        if customer:
            queryset = queryset.filter(customer__in=customer)
        return queryset.order_by("created_on")

    def get_data(self):
        data = []
        orders = self.get_objects()

        for order in orders:
            data.append(
                {
                    "reference_number": order.reference_number,
                    "order_date": order.created_on,
                    "order_line_quantity": order.lines.filter(type=StorePurchaseLineType.PRODUCT).count(),
                    "order_taxless_amount": order.taxless_total_price.amount,
                    "order_total_amount": order.taxful_total_price.amount,
                    "customer": order.customer,
                }
            )
        return self.get_return_data(data, has_totals=False)
