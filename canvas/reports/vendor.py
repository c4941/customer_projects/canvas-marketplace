from decimal import Decimal
from typing import Any, Dict

import shuup_multishop_admin.reports.sales
from django import forms
from django.db.models import Avg, Count, Q, QuerySet, Sum
from django.utils.translation import gettext_lazy as _
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Order, OrderLine, Shop
from shuup.core.pricing import TaxfulPrice, TaxlessPrice
from shuup.default_reports.forms import OrderLineReportForm
from shuup.default_reports.reports import OrderLineReport
from shuup.reports.forms import BaseReportForm
from shuup.utils.dates import to_datetime_range

from canvas.constants import CANVAS_MAIN_SHOP_IDENTIFIER
from canvas.models import StorePurchaseLine
from canvas.models.sync import StorePurchaseLineType
from canvas.utils import get_supplier_shops

__all__ = [
    "MultishopVendorAllOrders",
    "MultishopVendorTotalSales",
    "StorePurchaseLineReports",
]

from shuup_multivendor.reports.utils import OrderReportForm


class MultishopVendorTotalSales(shuup_multishop_admin.reports.sales.TotalSales):
    identifier = "multishop_vendor_total_sales_report"
    title = _("Multishop Vendor Total Sales")
    form_class = OrderReportForm

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.supplier = get_supplier(self.request)

    def get_objects(self, shop: Shop) -> "QuerySet[Order]":
        return super().get_objects(shop).filter(lines__supplier=self.supplier)

    def get_data(self):
        """Overridden to backport the fix from shuup/shuup-multishop-admin#7."""
        data = []
        for shop in get_supplier_shops(self.supplier):
            orders = self.get_objects(shop)
            aggregation = orders.aggregate(
                customers=Count("customer", distinct=True),
                total=Sum("taxful_total_price_value"),
                avg_sale=Avg("taxful_total_price_value"),
            )
            total_sales_value = aggregation["total"] or 0
            if not total_sales_value:
                continue

            customer_avg_sale = aggregation["avg_sale"] or 0
            customers = aggregation["customers"] or 0

            data.append(
                {
                    "name": shop.name,
                    "order_count": orders.count(),
                    "customers": customers,
                    "customer_avg_sale": shop.create_price(customer_avg_sale).as_rounded().value,
                    "total_sales": shop.create_price(total_sales_value).as_rounded().value,
                    "currency": shop.currency,
                }
            )

        return self.get_return_data(data)

    def get_totals(self, data) -> Dict[str, Any]:
        """
        Overridden to skip calculating total sales from all shops, since the shops
        use different currencies, and totalling them together doesn't make sense.
        """
        price_types = [TaxlessPrice, TaxfulPrice]
        simple_types = [int, float, Decimal]
        countable_types = price_types + simple_types
        totals = {}
        for datum in data:
            for c, val in zip(self.schema, self.read_datum(datum)):
                k = c["key"]

                if k in {"total_sales", "customer_avg_sale"}:
                    continue

                if k not in totals:
                    if type(val) in price_types or type(val) in simple_types:
                        cls = type(val)
                        if type(val) in price_types:
                            totals[k] = cls(0, currency=self.shop.currency)
                        else:
                            totals[k] = cls(0)
                    else:
                        totals[k] = None

                if type(val) in countable_types:
                    totals[k] = totals[k] + val if totals[k] else val

        return totals


class CanvasOrderLineReportForm(OrderLineReportForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop("shop")
        self.fields.pop("supplier")


class MultishopVendorAllOrders(OrderLineReport):
    identifier = "multishop_vendor_all_orders_report"
    title = _("Multishop Vendor Online Orders Report")
    form_class = CanvasOrderLineReportForm

    schema = [
        {"key": "order_ref", "title": _("Order Reference Number")},
        {"key": "order_line_sku", "title": _("Order Line SKU")},
        {"key": "order_line_text", "title": _("Order Line Text")},
        {"key": "order_line_quantity", "title": _("Quantity")},
        {"key": "taxless_unit_price", "title": _("Taxless Unit Price")},
        {"key": "taxful_unit_price", "title": _("Taxful Unit Price")},
        {"key": "taxful_price", "title": _("Taxful Price")},
        {"key": "created_on", "title": _("Created on")},
        {"key": "type", "title": _("Type")},
        {"key": "shipping_method", "title": _("Shipping")},
        {"key": "shop", "title": _("Shop")},
    ]

    def get_objects(self):
        (start, end) = to_datetime_range(self.start_date, self.end_date)
        queryset = OrderLine.objects.filter(created_on__range=(start, end))
        types = self.options.get("order_line_type")
        order_status = self.options.get("order_status")
        filters = Q(supplier=get_supplier(self.request))
        if types:
            filters &= Q(type__in=types)
        if order_status:
            filters &= Q(order__status__in=order_status)
        return queryset.filter(filters).order_by("created_on")

    def get_data(self):
        data = []
        order_lines = self.get_objects()[: self.queryset_row_limit]
        for line in order_lines:
            data.append(
                {
                    "order_ref": line.order.reference_number,
                    "order_line_sku": line.sku,
                    "order_line_text": line.text,
                    "order_line_quantity": line.quantity,
                    "taxless_unit_price": line.taxless_base_unit_price,
                    "taxful_unit_price": line.taxful_base_unit_price,
                    "taxful_price": line.taxful_price,
                    "type": line.type.name.capitalize(),
                    "created_on": line.created_on,
                    "shipping_method": line.order.shipping_method.name,
                    "shop": line.order.shop,
                }
            )
        return self.get_return_data(data, has_totals=False)


class StorePurchaseLineReportForm(BaseReportForm):
    purchase_line_type = forms.MultipleChoiceField(
        label=_("Line Item Type (Product or Refund)"),
        required=False,
        initial=[StorePurchaseLineType.PRODUCT.value],
        choices=[(line_type.value, line_type.name.capitalize()) for line_type in StorePurchaseLineType],
    )  # Because value of OrderLineType.PRODUCT is 1

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Note: US shop only. This may change if Square expands to the EU.
        self.fields.pop("shop", None)
        self.shop = Shop.objects.get(identifier=CANVAS_MAIN_SHOP_IDENTIFIER)


class StorePurchaseLineReports(OrderLineReport):
    identifier = "vendor_store_purchase_line_report"
    title = _("Brand In-Store Sales Line Item Report")
    form_class = StorePurchaseLineReportForm

    schema = [
        {"key": "ref", "title": _("Purchase Reference Number")},
        {"key": "sku", "title": _("Product SKU")},
        {"key": "product_name", "title": _("Product Name")},
        {"key": "quantity", "title": _("Quantity")},
        {"key": "taxless_unit_price", "title": _("Taxless Unit Price")},
        {"key": "taxless_price", "title": _("Taxless Price")},
        {"key": "created_on", "title": _("Created on")},
        {"key": "type", "title": _("Type")},
    ]

    def get_objects(self):
        (start, end) = to_datetime_range(self.start_date, self.end_date)
        queryset = StorePurchaseLine.objects.filter(created_on__range=(start, end)).prefetch_related("product", "order")
        types = self.options.get("order_line_type")
        order_status = self.options.get("order_status")
        filters = Q(supplier=get_supplier(self.request))
        if types:
            filters &= Q(type__in=types)
        if order_status:
            filters &= Q(order__status__in=order_status)
        return queryset.filter(filters).order_by("created_on")

    def get_data(self):
        data = []
        purchase_lines = self.get_objects()[: self.queryset_row_limit]
        for line in purchase_lines:
            data.append(
                {
                    "ref": line.order.reference_number,
                    "sku": line.product.sku,
                    "product_name": line.product.name,
                    "quantity": line.quantity,
                    "taxless_unit_price": line.taxless_base_unit_price,
                    "taxless_price": line.taxless_price,
                    "type": line.type.name.capitalize(),
                    "created_on": line.created_on,
                }
            )
        return self.get_return_data(data, has_totals=False)
