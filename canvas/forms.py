# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django import forms

from canvas.models import CanvasProductExtra, CanvasSupplierExtra, SustainabilityGoal

__all__ = [
    "CanvasSupplierExtraForm",
]

_textarea = forms.Textarea(attrs={"rows": 5})


class CanvasSupplierExtraForm(forms.ModelForm):
    class Meta:
        model = CanvasSupplierExtra
        exclude = ("supplier",)
        widgets = {
            "brand_statement": _textarea,
            "sustainability_goals_description": _textarea,
            "other_sustainability_initiatives": _textarea,
        }

    def __init__(self, *args, **kwargs):
        super(CanvasSupplierExtraForm, self).__init__(*args, **kwargs)
        self.fields["sustainability_goals"].to_field_name = "identifier"
        self.fields["sustainability_goals"].widget = forms.CheckboxSelectMultiple()
        self.fields["sustainability_goals"].widget.choices = [
            (sdg.identifier, sdg.name) for sdg in SustainabilityGoal.objects.all().order_by("identifier")
        ]

    def save(self, commit=True):
        if hasattr(self, "vendor"):
            # Note: vendors submitting registration forms don't exist yet
            self.instance.supplier = self.vendor
        return super(CanvasSupplierExtraForm, self).save(commit=commit)


class CanvasProductExtraForm(forms.ModelForm):
    class Meta:
        model = CanvasProductExtra
        fields = ["material_and_fit", "care", "about_brands"]
        widgets = {
            "material_and_fit": _textarea,
            "care": _textarea,
            "about_brands": _textarea,
        }

    # def save(self, commit=True):
    #     if hasattr(self, "product"):
    #         # Note: vendors submitting registration forms don't exist yet
    #         self.instance.product = self.supplied_product
    #     return super(CanvasProductExtraForm, self).save(commit=commit)
