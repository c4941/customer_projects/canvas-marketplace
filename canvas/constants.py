# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
CANVAS_MAIN_SHOP_IDENTIFIER = CANVAS_US_SHOP_IDENTIFIER = "canvas-us"
CANVAS_EU_SHOP_IDENTIFIER = "canvas-eu"

CANVAS_PICK_UP_CARRIER_IDENTIFIER = "canvas-pickup"
CANVAS_STORE_SHIPPING_CARRIER_IDENTIFIER = "canvas-store-shipping"

CANVAS_SHIPPING_TAX_CLASS_IDENTIFIER = "shipping"

CANVAS_STORE_OWNER_PERMISSION_GROUP_IDENTIFIER = "Brick and Mortar Store Owner"
CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER = "purchasing"
