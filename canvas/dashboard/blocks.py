# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.

from django.db.models import Count, ExpressionWrapper, IntegerField, OuterRef, Q, Subquery, Sum
from django.utils.translation import ugettext_lazy as _
from shuup.admin.dashboard import DashboardContentBlock, DashboardMoneyBlock, DashboardNumberBlock
from shuup.core.models import Category, Order, PersonContact, Product
from shuup.core.pricing import TaxlessPrice
from shuup_multivendor.dashboards.blocks import VendorValueChartDashboardBlock


def get_new_customers_block(order_ids, supplier, cutoff_date):
    return DashboardNumberBlock(
        id="new_customers_block",
        color="orange",
        title=_("New Customers"),
        value=_get_new_customers(supplier, cutoff_date),
        icon="fa fa-user",
        subtitle=_("based on %s orders") % len(order_ids),
    )


def get_returning_customers_block(order_ids, supplier, cutoff_date):
    return DashboardNumberBlock(
        id="new_customers_block",
        color="orange",
        title=_("Returning Customers"),
        value=_get_returning_customers(order_ids, supplier, cutoff_date),
        icon="fa fa-user",
        subtitle=_("based on %s orders") % len(order_ids),
    )


def get_favorite_categories_block(request, supplier, order_ids):
    block = DashboardContentBlock.by_rendering_template(
        "favorite_categories",
        request,
        "shuup_multivendor/admin/dashboard/favorite_categories_block.jinja",
        {"categories": _get_favorite_categories(supplier, order_ids), "title": _("Top Categories")},
    )
    block.size = "medium"
    return block


def get_favorite_products_block(request, supplier, order_ids):
    block = DashboardContentBlock.by_rendering_template(
        "favorite_products",
        request,
        "shuup_multivendor/admin/dashboard/favorite_products_block.jinja",
        {"products": _get_favorite_products(supplier, order_ids), "title": _("Top Products")},
    )
    block.size = "medium"
    return block


def get_lifetime_sales_block(order_ids, supplier, shop):
    return DashboardMoneyBlock(
        id="lifetime_sales_sum",
        color="green",
        title=_("{shop} - Total Sales ({currency})").format(shop=shop.name, currency=shop.currency),
        value=(_get_lifetime_sales(order_ids, supplier, shop.currency) or 0),
        currency=shop.currency,
        icon="fa fa-line-chart",
        subtitle=_("based on %s orders") % len(order_ids),
    )


class MultishopVendorValueChartDashboardBlock(VendorValueChartDashboardBlock):
    def get_chart(self):
        """Overridden to customize the chart title."""
        chart = super().get_chart()
        # Fine to use `request.shop` here, also the `__init__` of super uses it.
        shop = self.request.shop
        chart.title = _("{shop} - Sales per Month (past 12 months, {currency})").format(
            shop=shop, currency=shop.currency
        )
        return chart


def get_vendor_order_chart_dashboard_block(request, shop):
    return MultishopVendorValueChartDashboardBlock(id=f"vendor_order_value_chart_{shop.identifier}", request=request)


def _get_new_customers(supplier, cutoff_date):
    return (
        PersonContact.objects.annotate(
            last_order=Subquery(
                Order.objects.filter(
                    lines__supplier=supplier,
                    customer=OuterRef("contact_ptr"),
                )
                .exclude(lines__accounting_identifier="vendor_withdrawal")
                .order_by("-order_date")
                .values("order_date")[:1]
            )
        )
        .filter(last_order__gte=cutoff_date)
        .count()
    )


def _get_returning_customers(order_ids, supplier, cutoff_date):
    return len(order_ids) - _get_new_customers(supplier, cutoff_date)


def _get_favorite_categories(supplier, order_ids):
    return (
        Category.objects.filter(
            Q(
                primary_shop_products__product__order_lines__order_id__in=order_ids,
                primary_shop_products__product__order_lines__supplier=supplier,
            )
            | Q(
                primary_shop_products__product__variation_children__order_lines__order_id__in=order_ids,
                primary_shop_products__product__variation_children__order_lines__supplier=supplier,
            )
        )
        .annotate(
            total_sales=ExpressionWrapper(
                Count("primary_shop_products__product__order_lines__id", distinct=True)
                + Count("primary_shop_products__product__variation_children__order_lines__id", distinct=True),
                output_field=IntegerField(),
            )
        )
        .order_by("-total_sales")[:20]
    )


def _get_favorite_products(supplier, order_ids):
    return (
        Product.objects.filter(order_lines__order__id__in=order_ids, order_lines__supplier=supplier)
        .annotate(total_sales=ExpressionWrapper(Sum("order_lines__quantity"), output_field=IntegerField()))
        .order_by("-total_sales")[:20]
    )


def _get_lifetime_sales(order_ids, supplier, currency):
    taxless_total = TaxlessPrice(0, currency)
    for order in Order.objects.filter(id__in=order_ids):
        for line in order.lines.filter(supplier=supplier):
            taxless_total += line.taxless_price
    return taxless_total
