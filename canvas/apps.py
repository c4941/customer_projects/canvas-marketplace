# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import shuup.apps


class AppConfig(shuup.apps.AppConfig):
    name = "canvas"
    verbose_name = "Business Logic"
    label = "canvas"
    provides = {
        "xtheme": ["canvas.theme:CanvasTheme"],
        "front_urls": [
            "canvas.front.urls:urlpatterns",
        ],
        "admin_module": [
            "canvas.admin_module.modules:CanvasMessageModule",
            "canvas.admin_module.modules:MultishopProductModule",
            "canvas.admin_module.modules:MultishopVendorProductsAdminModule",
            "canvas.admin_module.modules:MultishopVendorSalesDashboardModule",
            "canvas.admin_module.modules:MultishopVendorServiceProviderModule",
            "canvas.admin_module.modules:MultishopVendorShippingMethodModule",
            "canvas.admin_module.modules:MultishopProductsApprovalAdminModule",
            "canvas.admin_module.modules:SustainabilityGoalModule",
            "canvas.admin_module.modules:CanvasProductVariationsModule",
            "canvas.admin_module.modules:FailedProductSyncsModule",
            "canvas.admin_module.modules:CanvasMultivendorOrdersAdminModule",
        ],
        "admin_product_form_part": [
            "canvas.admin_module.form_parts:CanvasProductExtraFormPart",
            "canvas.admin_module.form_parts:CanvasBarcodesFormPart",
            "canvas.admin_module.form_parts:CanvasSimpleSupplierFormPart",
            "canvas.admin_module.form_parts:ProductSustainabilityGoalFormPart",
        ],
        "admin_vendor_product_form_part": [
            "canvas.admin_module.form_parts:MultishopVendorShopProductFormPart",
            "canvas.admin_module.form_parts:MultishopVendorProductBaseFormPart",
            "canvas.admin_module.form_parts:CanvasProductExtraFormPart",
            "canvas.admin_module.form_parts:CanvasStoreStocksFormPart",
            "canvas.admin_module.form_parts:CanvasBarcodesFormPart",
            "canvas.admin_module.form_parts:VendorProductSustainabilityGoalFormPart",
        ],
        "admin_vendor_form_part": [
            "canvas.admin_module.form_parts:CanvasSupplierOpeningPeriodsFormPart",
            "canvas.admin_module.form_parts:CanvasSupplierExtraSettingsFormPart",
            "canvas.admin_module.form_parts:MultishopVendorPendingWithdrawalsFormPart",
            "canvas.admin_module.form_parts:MultishopVendorFundsWireEUFormPart",
            "canvas.admin_module.form_parts:MultishopVendorFundsWireFormPart",
            "canvas.admin_module.form_parts:MultishopVendorFundsPaypalFormPart",
            "canvas.admin_module.form_parts:CanvasStoreExtraFormPart",
            "canvas.admin_module.form_parts:CanvasStoreEventFormPart",
        ],
        "admin_product_toolbar_action_item": [
            "canvas.admin_module.toolbar:MultishopApproveProductAction",
            "canvas.admin_module.toolbar:MultishopRevokeProductApprovalAction",
        ],
        "reports": [
            "canvas.reports:MultishopVendorTotalSales",
            "canvas.reports:MultishopVendorAllOrders",
            "canvas.reports:StorePurchaseReport",
            "canvas.reports:StorePurchaseLineReports",
        ],
        "xtheme_plugin": [
            "canvas.plugins.accordion_default:AccordionPlugin",
            "canvas.plugins.newsletter_form_with_images:NewsletterFormWithImagesPlugin",
            "canvas.plugins.checkbox_item:CheckboxItemPlugin",
            "canvas.plugins.sustainability_goals:SustainabilityGoalPlugin",
            "canvas.plugins.canvas_title:CanvasTitlePlugin",
            "canvas.plugins.vertical_space:VerticalSpacePlugin",
            "canvas.plugins.full_width_image:FullWidthImagePlugin",
        ],
        "front_extend_product_list_form": [
            "canvas.front.filters:SustainabilityGoalsProjectListFilter",
        ],
        "simple_cms_template": [
            "canvas.templates:EmptyPage",
            "canvas.cms_template.CanvasBlogTemplate",
        ],
        "customer_dashboard_items": [
            "canvas.front.dashboard_items:CanvasWishlistItem",
        ],
        "multivendor_admin_order_section": [
            "canvas.admin_module.views.multivendor_orders.CanvasVendorOrderDetailsSection",
            "canvas.admin_module.views.multivendor_orders.CanvasVendorOrderShipmentSection",
        ],
        "supplier_module": ["canvas.suppliers.simple_supplier:CanvasSimpleSupplierModule"],
        "admin_product_section": ["canvas.suppliers.simple_supplier:CanvasProductVariationsSection"],
        "order_source_modifier_module": ["canvas.modifiers.store_purchasing:StorePurchasingModifierModule"],
    }

    def ready(self):
        import canvas.logging  # noqa
        import canvas.signal_handlers  # noqa
