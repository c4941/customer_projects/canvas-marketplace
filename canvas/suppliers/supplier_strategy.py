# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.core.models import Supplier
from shuup.simple_supplier.models import StockCount
from shuup_multivendor.models import SupplierPrice
from shuup_multivendor.utils.cache import cache_cheapest_supplier, get_cached_cheapest_supplier

from canvas.utils import get_canvas_brick_and_mortar_suppliers, is_canvas_supplier


class BrandCheapestSupplierStrategy:
    def get_supplier(self, **kwargs):
        # Here we did some trick and passed different
        # kwargs than is passed from shop product get
        # supplier. Shouldn't be issue as long as this
        # strategy is able to adjust to both set of
        # kwargs.
        product_id = kwargs.get("product_id")
        if not product_id:
            shop_product = kwargs.get("shop_product")
            if not shop_product:
                return
            product_id = shop_product.product_id

        shop_id = kwargs["shop"].pk if kwargs.get("shop") else None
        if not shop_id:
            shop_product = kwargs.get("shop_product")
            if not shop_product:
                return
            shop_id = shop_product.shop_id

        supplier = get_cached_cheapest_supplier(shop_id, product_id)
        if supplier is not None:
            if supplier == -1:
                return None
            return supplier

        # Supplier with best price and fallback to
        # first shop product supplier. Likely there
        # needs to be cache around this in real solution,
        # but this is just a testing strategy to help
        # test supplier prices and multiple suppliers
        # with front.
        results = (
            SupplierPrice.objects.filter(
                shop_id=shop_id,
                product_id=product_id,
                supplier__enabled=True,
                supplier__id__in=StockCount.objects.filter(product_id=product_id, physical_count__gte=1).values_list(
                    "supplier", flat=True
                ),
            )
            .order_by("amount_value")
            .select_related("supplier")
            .only("supplier")
        )

        # Skip the stores even though they carry the shop product.
        for result in results:
            if not is_canvas_supplier(result.supplier):
                supplier = result.supplier
                break

        if supplier is None:
            # So far, it's just stores; so just try to grab the shop product's brand
            supplier = (
                Supplier.objects.enabled(shop=shop_id)
                .filter(shop_products__product__id=product_id)
                .exclude(id__in=get_canvas_brick_and_mortar_suppliers().values("id"))
                .first()
            )

        # cache -1 as a fallback if the supplier is None so when we fetch the cached value, we don't
        # have to do the entire db query again
        cache_cheapest_supplier(shop_id, product_id, supplier or -1)
        return supplier
