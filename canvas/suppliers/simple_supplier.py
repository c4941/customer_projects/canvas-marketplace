# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from coreapi.compat import force_text
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import F
from django.utils.translation import ugettext_lazy as _
from shuup.admin.shop_provider import get_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import Product, ShopProduct, Supplier
from shuup.core.stocks import ProductStockStatus
from shuup.simple_supplier.models import StockCount
from shuup.simple_supplier.module import SimpleSupplierModule
from shuup.utils.djangoenv import has_installed
from shuup.utils.excs import Problem
from shuup_product_variations.sections import ProductVariationsSection

from canvas.constants import CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER
from canvas.models import SyncedShopProduct
from canvas.square_sync.client import InventoryClient
from canvas.utils import get_canvas_brick_and_mortar_suppliers


class CanvasSimpleSupplierModule(SimpleSupplierModule):
    identifier = "canvas_supplier"

    def get_stock_statuses(self, product_ids, *args, **kwargs):
        if "suppliers" in kwargs:
            suppliers = kwargs["suppliers"]
        else:
            suppliers = list(get_canvas_brick_and_mortar_suppliers()) + [self.supplier]
        stock_counts = (
            Product.objects.filter(
                pk__in=product_ids,
                simple_supplier_stock_count__supplier__in=suppliers,
                kind__in=self.get_supported_product_kinds_values(),
            )
            .annotate(
                physical_count=F("simple_supplier_stock_count__physical_count"),
                logical_count=F("simple_supplier_stock_count__logical_count"),
                stock_managed=F("simple_supplier_stock_count__stock_managed"),
            )
            .values_list("pk", "physical_count", "logical_count", "stock_managed")
            .order_by("simple_supplier_stock_count__supplier", "-logical_count")
        )

        values = {}
        for (product_id, physical_count, logical_count, stock_managed) in stock_counts:
            if product_id in values and logical_count < values[product_id][1]:
                continue
            values[product_id] = (physical_count or 0, logical_count or 0, stock_managed or False)

        null = (0, 0, self.supplier.stock_managed)
        stati = []
        for product_id in product_ids:
            stock_managed = values.get(product_id, null)[2]
            if stock_managed is None:
                stock_managed = self.supplier.stock_manage

            stati.append(
                ProductStockStatus(
                    product_id=product_id,
                    physical_count=values.get(product_id, null)[0],
                    logical_count=values.get(product_id, null)[1],
                    stock_managed=stock_managed,
                )
            )

        return dict((pss.product_id, pss) for pss in stati)

    def get_orderability_errors(self, shop_product: ShopProduct, quantity: int, customer, *args, **kwargs):
        """If a customer is a store purchaser, then ignore `backorder_minimum` if the brand has nothing in stock."""
        if shop_product.product.kind not in self.get_supported_product_kinds_values():
            return

        stock_status = self.get_stock_status(shop_product.product_id)

        if (
            customer
            and customer.groups.filter(identifier="purchasing").exists()
            and shop_product.product.simple_supplier_stock_count.first().logical_count <= 0
        ):
            # Members of the Store Purchasing group have no limits
            backorder_maximum = None
        else:
            backorder_maximum = shop_product.backorder_maximum
        if stock_status.error:
            yield ValidationError(stock_status.error, code="stock_error")

        if self.supplier.stock_managed and stock_status.stock_managed:
            if backorder_maximum is not None and quantity > stock_status.logical_count + backorder_maximum:
                yield ValidationError(
                    stock_status.message or _("Error! Insufficient quantity in stock."), code="stock_insufficient"
                )

    def get_stock_status(self, product_id: int, *args, **kwargs):
        statuses = self.get_stock_statuses([product_id], *args, **kwargs)
        if product_id in statuses:
            return statuses[product_id]

    @staticmethod
    def _determine_shipper(shipment):
        shipper = shipment.supplier
        if shipment.order.shipping_method.identifier and "store" in shipment.order.shipping_method.identifier:
            identifiers = shipment.order.shipping_method.identifier.split("-")
            index = identifiers.index("store") + 1
            store_id = identifiers[index]
            store = Supplier.objects.filter(id=store_id).first()
            if store is None:
                raise Problem(
                    _("The store that is supposed to ship this order %(store_id)s does not exist.") % store_id
                )
            shipper = store
        if shipper != shipment.supplier:
            shipment.supplier = shipper

        return shipper

    def ship_products(self, shipment, product_quantities, *args, **kwargs):
        """Ship the products and manage the inventory for the shipper."""
        all_stores = get_canvas_brick_and_mortar_suppliers()

        shipper = self._determine_shipper(shipment)

        # See if stock levels are important
        check_stock = self.supplier.stock_managed
        store_purchase = False
        if shipment.order.customer:
            store_purchase = shipment.order.customer.groups.filter(
                identifier=CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER
            ).exists()
        if store_purchase:
            ship_to_customer = StockCount.objects.filter(supplier=shipper, logical_count__gt=0).exists()
            if not ship_to_customer:
                # This is going straight to a store, and the brand isn't keeping online inventory. Stocks don't matter.
                check_stock = False

        # This is a normal shipment, so check stocks for the shipper.
        if check_stock:
            insufficient_stocks = {}

            for product, quantity in product_quantities.items():
                if quantity > 0 and product.kind in self.get_supported_product_kinds_values():
                    # Get stock status only for the brand, not stores that happen to carry it (or vice versa).
                    stock_status = self.get_stock_status(product.pk, suppliers=[shipper])
                    if stock_status.stock_managed and stock_status.physical_count < quantity:
                        insufficient_stocks[product] = stock_status.physical_count

            if insufficient_stocks:
                formatted_counts = [
                    _("%(name)s (physical stock: %(quantity)s)")
                    % {"name": force_text(product.name), "quantity": str(int(quantity))}
                    for (product, quantity) in insufficient_stocks.items()
                ]
                raise Problem(
                    _("Insufficient physical stock count for the following products: `%(product_counts)s`.")
                    % {"product_counts": ", ".join(formatted_counts)}
                )

        shop_products = {
            shop_product.product: shop_product
            for shop_product in ShopProduct.objects.filter(
                product__in=product_quantities.keys(), suppliers=shipper
            ).iterator()
        }
        for product, quantity in product_quantities.items():
            if quantity == 0 or product.kind not in self.get_supported_product_kinds_values():
                continue

            sp = shipment.products.create(product=product, quantity=quantity)
            sp.cache_values()
            sp.save()

        shipment.cache_values()
        shipment.save()

        # See if the new inventory needs to be synced back to Square.
        if shipper in all_stores:
            for product, shop_product in shop_products.items():
                SyncedShopProduct.get_sync(shop_product)
                with transaction.atomic():
                    new_count = StockCount.objects.filter(supplier=shipper, product=product).first()
                    InventoryClient().sync_inventory(shop_products[product], shipper, new_count.logical_count)


class CanvasProductVariationsSection(ProductVariationsSection):
    @classmethod
    def get_context_data(cls, product, request=None):
        # The following should all be true, but don't break in case of a misconfiguration.
        data = super().get_context_data(product, request=request)
        main_product = product.variation_parent if product.variation_parent else product
        shop = get_shop(request)
        main_shop_product = main_product.get_shop_instance(shop)
        supplier = get_supplier(request)
        if not supplier:
            supplier = main_shop_product.suppliers.first()

        is_simple_supplier_installed = has_installed("shuup.simple_supplier")
        data["stock_managed"] = bool(
            is_simple_supplier_installed
            and supplier.supplier_modules.filter(module_identifier="canvas_supplier").exists()
            and supplier.stock_managed
        )
        return data
