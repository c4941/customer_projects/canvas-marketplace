# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from uuid import uuid4

from django.utils.translation import ugettext_lazy as _
from shuup.core.models import OrderLineType
from shuup.core.order_creator import OrderSourceModifierModule
from shuup.core.order_creator._source import LineSource

from canvas.constants import CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER


class StorePurchasingModifierModule(OrderSourceModifierModule):
    identifier = "store_purchasing"
    name = _("Store Purchasing Discount")

    def get_new_lines(self, order_source, lines):
        if (
            order_source.customer
            and order_source.customer.groups.filter(identifier=CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER).count()
        ):
            store_user = order_source.customer.user.vendor_users.filter(shop=order_source.shop).first()
            if store_user is None:
                return []

            store = store_user.supplier
            for line in lines:
                discount = line.taxless_price
                if not line.base_unit_price.unit_matches_with(discount):
                    discount = line.taxful_price
                yield order_source.create_line(
                    line_id="discount_%s" % uuid4().hex,
                    type=OrderLineType.DISCOUNT,
                    quantity=1,
                    discount_amount=discount,
                    text=_(f"Store Purchasing Discount for {store}"),
                    line_source=LineSource.DISCOUNT_MODULE,
                    supplier=line.supplier,
                )

        return []
