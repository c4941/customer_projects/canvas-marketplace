# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.core.models import get_person_contact
from shuup_wishlist.models import Wishlist


def get_user_wishlist(request):
    """Make user wishlist items available to every page."""

    if getattr(request, "user", None) is not None and not request.user.is_anonymous:
        contact = get_person_contact(request.user)
        # TODO: see about caching?
        wishlist = Wishlist.objects.filter(shop=request.shop, customer=contact).first()
        if wishlist:
            wishlist_items = wishlist.products.all().prefetch_related("product")
        else:
            wishlist_items = []
        return {"wishlist_items": wishlist_items, "wishlist": wishlist}
    return {}
