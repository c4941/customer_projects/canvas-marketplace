const { getParcelBuildCommand, runBuildCommands } = require("shuup-static-build-tools");

runBuildCommands([
    getParcelBuildCommand({
        cacheDir: "canvas",
        outputDir: "static/canvas",
        outputFileName: "canvas",
        entryFile: "static_src/index.js"
    }),
    getParcelBuildCommand({
        cacheDir: "canvas",
        outputDir: "static/canvas",
        outputFileName: "canvas_admin",
        entryFile: "static_src/admin_index.js"
    }),
]);
