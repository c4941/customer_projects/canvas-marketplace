# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from shuup.core.models import Shop
from shuup.core.utils.shops import get_shop_from_host
from shuup.core import cache


class DefaultShopProvider(object):
    @classmethod
    def get_shop(cls, request, **kwargs):
        host = request.META.get("HTTP_HOST")
        shop = cache.get(f"shop-{host}")
        if shop is None:
            shop = get_shop_from_host(host) if host else None
            if not shop:
                shop = Shop.objects.first()
            cache.set(f"shop-{host}", shop)
        setattr(request, "_cached_default_shop_provider_shop", shop)
        return shop
