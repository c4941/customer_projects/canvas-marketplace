import re
import os
from collections import defaultdict
from typing import Dict, List, Union

import django.utils.timezone
import django_jinja.library
from django.http import HttpRequest
from shuup.core import cache
from shuup.core.models import Category, PersonContact, Supplier

from canvas.models.supplier import CanvasSupplierExtra
from canvas.utils import get_canvas_brick_and_mortar_suppliers, get_canvas_orderability_errors, is_canvas_supplier, flat_categories
from django import template
from mptt.templatetags.mptt_tags import RecurseTreeNode
from django.templatetags.static import static
import logging

from django.templatetags.static import static
import toml
import functools
from django.conf import settings
from django.utils import timezone
@django_jinja.library.global_function
def get_current_timezone_name() -> str:
    """Return the name of the current timezone, e.g. 'America/Los_Angeles'."""
    return django.utils.timezone.get_current_timezone_name()


@django_jinja.library.global_function
def get_suppliers_letter_dict(request: HttpRequest) -> Dict[str, List[Supplier]]:
    """
    Example return value:
    {
        "A": [<Supplier-Alfa>, <Supplier-Auto>],
        "G": [<Supplier-Golf>],
    }
    """
    
    suppliers = Supplier.objects.exclude(pk__in=get_canvas_brick_and_mortar_suppliers()).enabled(shop=request.shop)
    suppliers_dict = defaultdict(list)
    
    for supplier in suppliers:
        suppliers_dict[supplier.name[0].upper()].append(supplier)
    return suppliers_dict


@django_jinja.library.filter
def round_stock_value(value: float) -> Union[int, float]:
    return int(value) if value == int(value) else round(value, 2)


@django_jinja.library.global_function
def canvas_supplier_extra_detail_exists(vendor):
    return CanvasSupplierExtra.objects.filter(supplier=vendor).exists()


@django_jinja.library.global_function
def no_at_handle(handle):
    return handle.lstrip("@")


@django_jinja.library.global_function
def get_all_handles(handles):
    """
    Instagram usernames must contain only letters, numbers, periods and underscores.

    Example:
    handles = "no thanks@cool_carbon, @carbon.cool notthisone, @climate2degC notthiseither"

    all_handles = ['@cool_carbon', '@carbon.cool', '@climate2degC']
    """
    all_handles = re.findall(r"@[0-9a-zA-Z._][0-9a-zA-Z._]*", handles)
    return all_handles


@django_jinja.library.global_function
def get_category(name):
    """Find the category based on the name."""
    category = cache.get(f"category-{name}")
    if category is None:
        category = Category.objects.select_related("image").filter(translations__name=name, parent=None).first()
        if category is None:
            return ""
        cache.set(f"category-{name}", category)
    return category


@django_jinja.library.global_function
def is_brick_and_mortar(supplier):
    return is_canvas_supplier(supplier)


@django_jinja.library.global_function
def is_canvas_orderable(shop_product, customer: PersonContact, quantity: int):
    errors = get_canvas_orderability_errors(shop_product, customer=customer, quantity=quantity)
    return not errors


logger = logging.getLogger(__file__)


def get_static_file_path(file_path, request) -> str:
    url: str = ""
    url = static(production_static_file(file_path, request))
    return url

@functools.lru_cache
def get_app_version():
    toml_dict = toml.load(f"{settings.BASE_DIR}/pyproject.toml")
    return toml_dict["tool"]["poetry"]["version"]

def production_static_file(filepath: str, request) -> str:
    filepath_list = filepath.split("/")
    filename = filepath_list[-1]
    extension = None
    app_label = None
    if "gz" in filename:
        filename, extension, gz = filename.split(".")
        app_label = filename.split("-")[0]
        extension = ".".join([extension, gz])
    else:
        filename = filename.split(".")
        if len(filename) > 1:
            extension = filename[1]
            filename = filename[0]
        app_label = filename.split("-")[0]
    extension = process_stitic_file_for_header(extension, request)

    version = get_app_version()

    if version:
        filepath_list.pop()
        filepath_list.append(format_versioned_static_file(filename, version, extension))
        return "/".join(filepath_list)
    else:
        return filepath

def process_stitic_file_for_header(extension, request):
    if "iPhone" in request.headers["User-Agent"]:
        extension = extension.replace(".gz", "")
    if "Safari" in request.headers["User-Agent"]:
        extension = extension.replace(".gz", "")
    return extension


def format_versioned_static_file(filename, version, extension=None) -> str:
    formatted_file_name = ""
    if extension is not None:
        formatted_file_name = f"{filename}.{version}.{extension}"
    else:
        formatted_file_name = f"{filename}.{version}"
    return formatted_file_name


@django_jinja.library.global_function
def production_static(file_path: str, request: HttpRequest) -> str:
    path: str = get_static_file_path(file_path, request)
    return path

@django_jinja.library.global_function
def year():
    return timezone.now().year

@django_jinja.library.global_function
def get_flat_categories(shop):
    return flat_categories(shop)
    categories = Category.objects.select_related("image").all()