# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
# import datetime
# from collections import defaultdict
from django import forms
from django.conf import settings
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _
from shuup_definite_theme.theme import ShuupDefiniteTheme
from django.core.cache import cache
from jinja2 import Environment, MemcachedBytecodeCache
from shuup.xtheme.engine import XthemeEnvironment
from django.core.cache import caches


class CanvasTheme(ShuupDefiniteTheme):
    identifier = "canvas"
    name = _("Canvas Theme")
    author = "Shuup Team"

    guide_template = "canvas/guide.jinja"

    template_dir = "canvas/"
    default_template_dir = "shuup_definite_theme/"

    stylesheets = []

    fields = [
        (
            "allow_company_linkage",
            forms.BooleanField(required=False, initial=False, label=_("Allow Linking Accounts to Company")),
        ),
        ("product_new_days", forms.IntegerField(required=False, initial=14, label=_("Consider product new for days"))),
        (
            "show_sales_units",
            forms.BooleanField(
                required=False,
                initial=True,
                label=_("Show sales units in product boxes"),
                help_text=_(
                    "If you check this the sales units are going to be shown next to the quantity in product boxes."
                ),
            ),
        ),
        (
            "group_items_by_supplier",
            forms.BooleanField(
                required=False,
                initial=False,
                label=_("Group items by supplier"),
                help_text=_("Group items by the supplier in basket and order"),
            ),
        ),
    ]

    def get_product_tabs_options(self):
        product_detail_tabs = [
            ("description", _("Description")),
            ("details", _("Details")),
            ("attributes", _("Attributes")),
            ("files", _("Files")),
        ]
        return product_detail_tabs

    def _format_cms_links(self, shop, **query_kwargs):
        if "shuup.simple_cms" not in settings.INSTALLED_APPS:
            return
        from shuup.simple_cms.models import Page
        qs = Page.objects.visible(shop).filter(**query_kwargs)
        for page in qs.iterator():
            yield {
                "url": "/%s" % page.url,
                "text": force_text(page),
                "children": qs.visible(shop).filter(visible_in_menu=True, parent=page),
            }

    def get_cms_navigation_links(self, request):
        return self._format_cms_links(shop=request.shop, visible_in_menu=True, parent=None)


def environment(**options):
    env = XthemeEnvironment(trim_blocks=True, lstrip_blocks=True, **options)
    return env
