# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from decimal import Decimal

from django.db.models import Q, Sum
from shuup.core.models import OrderLine, OrderLineType, OrderStatusRole, Shop
from shuup_multivendor.models import WITHDRAW_ORDER_LINE_ACCOUNTING_IDENTIFIER, VendorOrderLineRevenue
from shuup_multivendor.revenue_calculator import DefaultRevenueCalculator
from shuup_multivendor.utils.funds import get_vendor_revenue_percentage, is_vendor_funds_enabled, is_withdrawal_line

from canvas.constants import CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER
from canvas.models import CanvasVendorLineRevenue, StorePurchaseLine
from canvas.models.sync import StorePurchaseLineType


class CanvasRevenueCalculator(DefaultRevenueCalculator):
    def ensure_order_line_revenue(self, order_line: OrderLine) -> VendorOrderLineRevenue:
        if (
            order_line.order.customer
            and order_line.order.customer.groups.filter(identifier=CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER).count()
        ):
            # This order is for a store. Brands don't get revenue just yet.
            return

        supplier = order_line.parent_line.supplier if order_line.parent_line else order_line.supplier
        if not supplier:
            return

        if is_withdrawal_line(order_line):
            return

        if is_vendor_funds_enabled(order_line.order.shop):
            vendor_revenue_percentage = Decimal(
                get_vendor_revenue_percentage(order_line.supplier, order_line.order.shop)
            )
        else:
            vendor_revenue_percentage = Decimal("0")

        attributes = {
            "shop": order_line.order.shop,
            "order_line": order_line,
            "defaults": {"percentage": vendor_revenue_percentage},
        }
        revenue, created = CanvasVendorLineRevenue.get_or_create(**attributes)

        if hasattr(order_line.order, "shipping_method"):
            shipping_method = order_line.order.shipping_method
            if (
                order_line.type == OrderLineType.SHIPPING
                and shipping_method.identifier
                and "store" in shipping_method.identifier
                and revenue.revenue_value >= 0
            ):
                # If 1. this is for an online order and 2. a store paid for shipping and 3. this particular
                # `order_line` is a shipping method line, then don't add shipping to the brand's earnings
                revenue.revenue_value = 0
                revenue.save()
                return revenue

        revenue.revenue_value = order_line.taxless_price.amount.value * (revenue.percentage / 100)
        revenue.save()

        return revenue

    def get_withdrawals_total(self, shop_id: int, vendor_id: int, **kwargs):
        online_total = super().get_withdrawals_total(shop_id, vendor_id, **kwargs)
        in_store_total = (
            StorePurchaseLine.objects.filter(
                accounting_identifier=WITHDRAW_ORDER_LINE_ACCOUNTING_IDENTIFIER,
                supplier_id=vendor_id,
                order__shop_id=shop_id,
            ).aggregate(total=Sum("base_unit_price_value"))["total"]
            or 0
        )
        return online_total + in_store_total

    def get_revenue_earned(self, shop_id: int, vendor_id: int, **kwargs) -> Decimal:
        from shuup_multivendor.utils.configuration import get_completed_status_label_for_shop

        shop = Shop.objects.get(pk=shop_id)
        complete_status = get_completed_status_label_for_shop(shop)

        if complete_status:
            status_filter = Q(
                Q(order_line__order__status__role=OrderStatusRole.COMPLETE) | Q(order_line__labels=complete_status)
            )
        else:
            status_filter = Q(order_line__order__status__role=OrderStatusRole.COMPLETE)

        # since withdrawals are negative those are automatically counted
        # once refund order is completed
        online_revenue_total_qs = CanvasVendorLineRevenue.objects.filter(
            Q(
                order_line__order__shop_id=shop_id,
                order_line__supplier_id=vendor_id,
            ),
            status_filter,
        ).exclude(order_line__type=OrderLineType.REFUND)
        store_revenue_total_qs = CanvasVendorLineRevenue.objects.filter(
            purchase_line__order__shop_id=shop_id,
            purchase_line__supplier_id=vendor_id,
        ).exclude(order_line__type=OrderLineType.REFUND)
        sales_revenue_total = (online_revenue_total_qs.aggregate(total=Sum("revenue_value"))["total"] or 0) + (
            store_revenue_total_qs.aggregate(total=Sum("revenue_value"))["total"] or 0
        )

        refunded_online_revenue_total_qs = CanvasVendorLineRevenue.objects.filter(
            Q(
                order_line__order__shop_id=shop_id,
                order_line__parent_line__supplier__id=vendor_id,
                order_line__type=OrderLineType.REFUND,
            ),
            status_filter,
        )
        refunded_store_revenue_total_qs = CanvasVendorLineRevenue.objects.filter(
            purchase_line__order__shop_id=shop_id,
            purchase_line__supplier_id=vendor_id,
            purchase_line__type=StorePurchaseLineType.REFUND,
        )
        refunded_sales_revenue_total = (
            refunded_online_revenue_total_qs.aggregate(total=Sum("revenue_value"))["total"] or 0
        ) + (refunded_store_revenue_total_qs.aggregate(total=Sum("revenue_value"))["total"] or 0)

        return sales_revenue_total + refunded_sales_revenue_total
