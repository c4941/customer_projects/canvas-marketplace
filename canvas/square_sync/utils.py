# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from datetime import datetime
from uuid import uuid4

import pytz
from shuup.core.models import ShopProduct

from canvas.models import SyncedCategory, SyncedShopProduct, SyncedStoreLocation
from canvas.models.sync import StorePurchaseLine, StorePurchaseLineType, SyncedContact
from canvas.utils import get_canvas_brick_and_mortar_suppliers


def get_price(shop_product: ShopProduct) -> int:
    """Get the source vendor's price (not the store's overridden price) and format for Square's API."""
    price = shop_product.product.vendor_prices.exclude(supplier__in=get_canvas_brick_and_mortar_suppliers()).first()
    return int(price.amount_value * 100)


def get_item_variation_data(shop_product: ShopProduct, square_id: str, version: int):
    """Produce a dictionary suitable to sync a Square Item Variation."""
    can_buy_here = shop_product.suppliers.filter(
        id__in=get_canvas_brick_and_mortar_suppliers().values("id")
    ).values_list("id", flat=True)

    data = {
        "id": square_id,
        "type": "ITEM_VARIATION",
        "present_at_locations_ids": [str(location_id) for location_id in can_buy_here],
        "item_variation_data": {
            "name": shop_product.product.name,
            "price_money": {
                "amount": get_price(shop_product),
                "currency": shop_product.shop.currency,
            },
            "pricing_type": "FIXED_PRICING",
            "sku": shop_product.product.sku,
            "stockable": True,
            "track_inventory": True,
            "upc": shop_product.product.barcode,
        },
        "version": version,
    }
    return data


def get_all_variations(shop_product: ShopProduct, children, shop_product_sync):
    """Create a list of dictionaries with the appropriate information for Square Item variations."""
    variations = []
    for child in children:
        child_sync = SyncedShopProduct.get_sync(child)
        variations.append(
            get_item_variation_data(child, child_sync.square_id or f"#child{child.id}", child_sync.version)
        )
    if not variations:
        variation_square_id = shop_product_sync.standalone_square_id
        variation_version = shop_product_sync.standalone_version
        variations.append(
            get_item_variation_data(shop_product, variation_square_id or f"#child{shop_product.id}", variation_version)
        )
    return variations


def get_item_data(shop_product_sync: SyncedShopProduct):
    """Create a dictionary suitable for upserting product data to Square."""
    shop_product = shop_product_sync.shop_product
    parent: ShopProduct = shop_product_sync.get_parent_shop_product()
    if parent == shop_product and not shop_product.product.is_variation_parent():
        children = []
    else:
        children = shop_product_sync.get_child_shop_products()

    shuup_category = shop_product.primary_category
    if not shuup_category:
        shuup_category = parent.primary_category
    category_square_id = SyncedCategory.get_sync(shuup_category).square_id
    brand = shop_product.suppliers.exclude(id__in=get_canvas_brick_and_mortar_suppliers().values("id")).first()
    # TODO: discuss finer points of "available_online". Is it not available online if it's out of stock?
    return {
        "object": {
            "type": "ITEM",
            "id": SyncedShopProduct.get_sync(parent).square_id or "#new",
            "product_type": "REGULAR",
            "item_data": {
                "name": parent.product.name,
                "description": f"{brand.name}. {parent.product.description}",
                "abbreviation": parent.product.name[:24],
                "available_online": True,
                "available_for_pickup": True,
                "category_id": category_square_id,
                "variations": get_all_variations(parent, children, shop_product_sync),
            },
            "version": shop_product_sync.version,
        },
        "idempotency_key": uuid4().hex,
    }


def get_category_data(category_sync: SyncedCategory):
    """Product a dictionary with information relevant to Square categories."""
    category = category_sync.category
    return {
        "object": {
            "type": "CATEGORY",
            "id": category_sync.square_id if category_sync.square_id else "#new",
            "product_type": "REGULAR",
            "category_data": {"name": category.name},
        },
        "idempotency_key": uuid4().hex,
    }


def get_store_opening_periods(store):
    """Produce a dictionary with information relevant to Square Location opening periods."""
    return {
        "periods": [
            {
                "day_of_week": period.weekday.name[:3],
                "start_local_time": period.start.isoformat(),
                "end_local_time": period.end.isoformat(),
            }
            for period in store.opening_periods.all()
        ]
    }


def get_location_data(store_sync: SyncedStoreLocation):
    store = store_sync.supplier
    data = {
        "location": {
            "address": {
                "address_line_1": store.contact_address.street,
                "address_line_2": store.contact_address.street2,
                "address_line_3": store.contact_address.street3,
                "country": store.contact_address.country.code,
                "postal_code": store.contact_address.postal_code,
            },
            "business_email": store.contact_address.email,
            "business_name": store.name,
        },
    }
    if store_sync.square_id:
        data["location"]["id"] = store_sync.square_id
    store_hours = get_store_opening_periods(store)
    data["location"]["business_hours"] = store_hours or None

    data["location"]["address"]["locality"] = store.contact_address.city
    data["location"]["address"]["administrative_district_level_1"] = store.contact_address.city
    data["location"]["name"] = " - ".join([store.name, store.contact_address.city])
    data["location"]["type"] = "PHYSICAL"
    data["location"]["status"] = "ACTIVE"
    # Note: the shop domain must include the ".com" suffix or Square will reject it as invalid.
    data["location"]["website_url"] = f"https://{store_sync.shop.domain}"

    return data


def get_inventory_data(shop_product, store, qty, timestamp):
    item_id = shop_product.synced_shop_product.standalone_square_id or shop_product.synced_shop_product.square_id
    return {
        "changes": [
            {
                "type": "PHYSICAL_COUNT",
                "physical_count": {
                    "catalog_object_id": item_id,
                    "state": "IN_STOCK",
                    "location_id": store.synced_location.square_id,
                    "quantity": qty,
                    "occurred_at": timestamp.isoformat(),
                },
            }
        ],
        "idempotency_key": uuid4().hex,
    }


def get_contact_data(sync: SyncedContact):
    contact = sync.contact
    data = {}
    if contact.first_name:
        data["given_name"] = contact.first_name
    if contact.last_name:
        data["family_name"] = contact.last_name
    if contact.user:
        data["email_address"] = contact.user.email
    if contact.phone:
        data["phone_number"] = contact.phone
    if len(data) == 0:
        # Square requires at least one of the above. If none are available, then the contact won't sync.
        return {}

    data["preferences"] = {"email_unsubscribed": not contact.marketing_permission}

    # Optional fields that will be made blank in Square if they aren't here.
    # Country is required if the address is provided.
    if contact.default_billing_address and contact.default_billing_address.country.code != "":
        data["address"] = {
            "address_line_1": contact.default_billing_address.street,
            "locality": contact.default_billing_address.city,
            "administrative_district_level_1": contact.default_billing_address.region_code,
            "postal_code": contact.default_billing_address.postal_code,
            "country": contact.default_billing_address.country.code,
        }
        if contact.default_billing_address.street2:
            data["address"]["address_line_2"] = contact.default_billing_address.street2
    if contact.birth_date:
        data["birthday"] = contact.birth_date.strftime("%Y-%m-%d")

    # Updating an existing customer
    if sync.square_id:
        data["version"] = sync.version

    return data


def get_purchase_last_timestamp(latest_purchase):
    if latest_purchase:
        return latest_purchase.created_on

    # Nothing has been imported yet. Import everything starting from December 1st.
    return datetime(year=2021, month=12, day=1, tzinfo=pytz.timezone("America/New_York"))


def get_purchase_params(cursor: str, start_here: datetime):
    """Ask for all Square orders that:
    1. are complete
    2. at all stores (which, at the time of initialization, was just a single location)
    3. occurred between the specified start time and right now.
    """
    location_ids = get_canvas_brick_and_mortar_suppliers().values_list("synced_location__square_id", flat=True)
    data = {
        "location_ids": list(location_ids),
        "query": {
            "filter": {
                "date_time_filter": {
                    "created_at": {
                        "start_at": start_here.isoformat(),
                    }
                },
            },
            "sort": {
                "sort_field": "CREATED_AT",
                "sort_order": "ASC",
            },
        },
        "return_entries": True,
    }
    if cursor:
        data["cursor"] = cursor

    return data


def get_purchase_batch_data(square_ids, location_id):
    return {
        "location_id": location_id,
        "order_ids": square_ids,
    }


def get_refund_last_timestamp():
    last_created = StorePurchaseLine.objects.filter(type=StorePurchaseLineType.REFUND).order_by("-created_on").first()
    last_modified = StorePurchaseLine.objects.filter(type=StorePurchaseLineType.REFUND).order_by("-modified_on").first()
    if last_created is None:
        # First time refund sync.
        timestamp = datetime(year=2021, month=12, day=1, tzinfo=pytz.timezone("America/New_York"))
    else:
        timestamp = (
            last_created.created_on if last_created.created_on > last_modified.modified_on else last_modified.created_on
        )
    return timestamp.isoformat()


def get_refund_params(cursor: str, start_here: datetime):
    data = {
        "begin_time": start_here,
        "status": "COMPLETED",
    }
    if cursor:
        data["cursor"] = cursor

    return data
