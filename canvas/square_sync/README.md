# Canvas/Square POS integration
## Developer setup instructions and HOWTO

1. Sign up for a Square account and create a new project/app.
2. Go to your new app, click on "OAuth", add the following to your `.env`: 
```
SQUARE_APP_ID=<sandbox app id>
SQUARE_APP_SECRET=<sandbox app secret>
SQUARE_ENVIRONMENT=sandbox
```
3. To test OAuth, you must create a new test seller account (because your default account is already authorized)
   - Follow the steps here: https://developer.squareup.com/docs/oauth-api/walkthrough#create-a-seller-sandbox-account.
   - Start localhost, go to `/request-token/` and follow the prompts.
4. After you run `init_project`, run the `sync_to_square` command to add your stores (and anything else that might 
be relevant, like existing products that are already available to stores) to your Square project.
5. Enable the Simple Supplier module for all your stores.   
   
See https://developer.squareup.com/docs/testing/create-and-authorize-sandbox-account 

### Webhook setup
In your canvas project, go to Webhooks and add new endpoints.
- **Inventory sync**
    - `<ngrok_url>/sync-inventory/` is the callback URL for `inventory.count.updated`.
    - Add `INVENTORY_SYNC_WEBHOOK_KEY` to your `.env` with the signature key.
    
To find the signature keys for your webhooks, go to Webhooks and click on the webhook name.

### Changing retail prices on the store level in Square
1. Open your sandbox test account dashboard (not your canvas project).
2. Click on the Items tile in the nav bar.
3. Click on an item.
4. Scroll down to "Variations" and click "Edit variation details".
5. Under the "Details" tab, click "Add price override".
6. Enter the new price and choose the store location to which it applies.

### Testing inventory changes
#### From the Square Sandbox dashboard
1. Open your sandbox test account dashboard (not your canvas project).
2. Click on the Items tile in the nav bar.
3. Click on an item variation.
4. Scroll until you find the location you wish to adjust, and make sure "Tracking" is enabled.   
5. Under the "Manage stock" tab, click "Select reason" (next to "Stock action") to enable editing the numbers
6. Enter the inventory adjustment according to the reason you selected.
