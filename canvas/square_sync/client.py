# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import logging
from datetime import timedelta
from typing import Optional

from django.conf import settings
from django.db.models import IntegerField
from django.db.models.functions import Cast
from django.db.transaction import atomic
from django.utils.timezone import now
from shuup.core.models import Product, ShopProduct
from shuup.simple_supplier.models import StockCount
from square.client import Client

from canvas.models import CanvasAuth, SyncedCategory, SyncedShopProduct, SyncedStoreLocation
from canvas.models.sync import StorePurchase, SyncedContact, UnsyncedStockCount
from canvas.square_sync.utils import (
    get_category_data,
    get_contact_data,
    get_inventory_data,
    get_item_data,
    get_location_data,
    get_purchase_batch_data,
    get_purchase_last_timestamp,
    get_purchase_params,
    get_refund_last_timestamp,
    get_refund_params,
)

logger = logging.getLogger(__name__)


def save_token(auth_code) -> Optional[str]:
    """Ask Canvas for an initial access token. If there are problems, return an error message."""
    client = Client(environment=settings.SQUARE_ENVIRONMENT)
    if auth_code is None:
        # The request to the Redirect URL did not include an authorization code. Something went wrong.
        return "Square was supposed to send a code to allow authorization, but didn't. Please try refreshing the page."

    # Provide the code in a request to the Obtain Token endpoint
    body = {
        "client_id": settings.SQUARE_APP_ID,
        "client_secret": settings.SQUARE_APP_SECRET,
        "code": auth_code,
        "grant_type": "authorization_code",
    }

    response = client.o_auth.obtain_token(body)

    if response.body:
        CanvasAuth.objects.update_or_create(
            merchant_id=response.body["merchant_id"],
            defaults={
                "access_token": response.body["access_token"],
                "access_expiry": response.body["expires_at"],
                "refresh_token": response.body["refresh_token"],
            },
        )
    else:
        return "Square did not send the requested access token."


class CanvasSquareClient:
    def __init__(self):
        # If the access token has expired or is going to expire sometime today, go ahead and refresh it.
        auth = CanvasAuth.objects.first()
        if auth is None:
            logger.error("The Square integration tried to run, but it doesn't have permission yet.")

        if auth.access_token and auth.access_expiry.date() <= now().date():
            self.refresh_token(auth)

        self.client = Client(access_token=auth.access_token, environment=settings.SQUARE_ENVIRONMENT)

    @staticmethod
    def refresh_token(auth: CanvasAuth):
        """Refresh the credentials and store them for later."""
        body = {
            "client_id": settings.SQUARE_APP_ID,
            "client_secret": settings.SQUARE_APP_SECRET,
            "grant_type": "refresh_token",
            "refresh_token": auth.refresh_token,
        }
        response = Client(environment=settings.SQUARE_ENVIRONMENT).o_auth.obtain_token(body)
        if response.body:
            CanvasAuth.objects.update_or_create(
                merchant_id=response.body["merchant_id"],
                defaults={
                    "access_token": response.body["access_token"],
                    "access_expiry": response.body["expires_at"],
                    "refresh_token": response.body["refresh_token"],
                },
            )
        else:
            logger.error(f"An attempt to refresh an expiring access code failed. Reason given: {response.errors}")


class ProductClient(CanvasSquareClient):
    def sync_location(self, synced: SyncedStoreLocation):
        """Attempt to sync the store to its Square Location counterpart."""
        data = get_location_data(synced)
        if synced.square_id:
            return self.client.locations.update_location(location_id=synced.square_id, body=data)
        else:
            response = self.client.locations.create_location(body=data)
            if response.is_success():
                SyncedStoreLocation.objects.filter(id=synced.id).update(square_id=response.body["location"]["id"])
            return response

    def sync_category(self, synced: SyncedCategory):
        """Attempt to sync the Category to its Square counterpart."""
        data = get_category_data(synced)
        response = self.client.catalog.upsert_catalog_object(body=data)
        if response.is_success():
            SyncedCategory.objects.filter(id=synced.id).update(
                square_id=response.body["catalog_object"]["id"],
                version=response.body["catalog_object"]["version"],
            )
        return response

    def sync_shop_product(self, synced: SyncedShopProduct):
        """Attempt to sync the ShopProduct (and its children, or itself as a variation, as appropriate) to Square.
        Square considers an item and its variations to be a single unit, though Shuup's child variations are
        sellable products each by themselves.
        """
        parent: ShopProduct = SyncedShopProduct.get_parent_shop_product(synced)
        if synced.shop_product != parent:
            # Never try to sync a child without its parent
            return

        data = get_item_data(synced)
        response = self.client.catalog.upsert_catalog_object(body=data)
        is_standalone = synced.shop_product == parent and not synced.shop_product.product.is_variation_parent()

        if response.is_success():
            for variation in response.body["catalog_object"]["item_data"]["variations"]:
                if is_standalone:
                    # Parent and child data all in one
                    SyncedShopProduct.objects.filter(shop_product=synced.shop_product).update(
                        square_id=response.body["catalog_object"]["id"],
                        version=response.body["catalog_object"]["version"],
                        standalone_square_id=variation["id"],
                        standalone_version=variation["version"],
                    )
                else:
                    # Just child data
                    SyncedShopProduct.objects.filter(
                        shop_product__product__sku=variation["item_variation_data"]["sku"]
                    ).update(
                        square_id=variation["id"],
                        version=variation["version"],
                    )

            if not is_standalone:
                # Just parent data. Clear out the standalone data on the off chance that it wasn't a parent last time.
                SyncedShopProduct.objects.filter(shop_product=synced.shop_product).update(
                    square_id=response.body["catalog_object"]["id"],
                    version=response.body["catalog_object"]["version"],
                    standalone_square_id=None,
                    standalone_version=None,
                )
        else:
            if response.errors[0]["code"] == "VERSION_MISMATCH":
                # Update the version(s) so the next pass over this record will be successful.
                if is_standalone:
                    updated = self.client.catalog.retrieve_catalog_object(object_id=synced.square_id)
                    SyncedShopProduct.objects.filter(id=synced.id).update(
                        version=updated.body["object"]["version"],
                        standalone_version=updated.body["object"]["item_data"]["variations"][0]["version"],
                    )
                else:
                    updated = self.client.catalog.retrieve_catalog_object(
                        object_id=parent.synced_shop_product.square_id
                    )
                    for variation in updated.body["object"]["item_data"]["variations"]:
                        SyncedShopProduct.objects.filter(square_id=variation["id"]).update(version=variation["version"])
                    SyncedShopProduct.objects.filter(square_id=updated.body["object"]["id"]).update(
                        version=updated.body["object"]["version"]
                    )

        return response


class InventoryClient(CanvasSquareClient):
    def sync_inventory(self, shop_product, store, updated_count: str):
        timestamp = now()
        data = get_inventory_data(shop_product, store, updated_count, timestamp)
        response = self.client.inventory.batch_change_inventory(body=data)
        if not response.is_success():
            logger.error(
                f"{shop_product.product.name} at {store} failed to sync its new inventory: "
                f"{response.errors[0]['category']} - {response.errors[0]['code']} - {response.errors[0]['detail']}"
            )
            # Record the attempt to try again later
            UnsyncedStockCount.objects.get_or_create(
                shop_product=shop_product, store=store, new_qty=updated_count, timestamp=timestamp
            )

        return response

    def retry_sync_inventory(self, failed_attempt):
        twenty_four_hours_ago = now() - timedelta(hours=24)
        if failed_attempt.timestamp < twenty_four_hours_ago:
            # Square will not sync anything with a timestamp more than 24h old, so give up now.
            failed_attempt.delete()
            return

        data = get_inventory_data(
            failed_attempt.shop_product, failed_attempt.store, failed_attempt.new_qty, failed_attempt.timestamp
        )
        response = self.client.inventory.batch_change_inventory(body=data)

        if response.is_success():
            failed_attempt.delete()
        return response


class ContactClient(CanvasSquareClient):
    def sync_contact(self, synced: SyncedContact):
        data = get_contact_data(synced)
        if synced.square_id:
            response = self.client.customers.update_customer(customer_id=synced.square_id, body=data)
        else:
            response = self.client.customers.create_customer(body=data)

        if response.is_success():
            synced.square_id = response.body["customer"]["id"]
            synced.version = response.body["customer"]["version"]
            synced.save()
        else:
            if response.errors[0]["code"] == "CONFLICT":
                # Version mismatch. Get the updated version for the next attempt.
                updated = self.client.customers.retrieve_customer(customer_id=synced.square_id)
                synced.version = updated.body["customer"]["version"]
                synced.save()

        return response


class StorePurchaseClient(CanvasSquareClient):
    def get_purchase_imports(self, cursor):
        latest_import = StorePurchase.objects.order_by("-created_on").first()
        start_timestamp = get_purchase_last_timestamp(latest_import)
        initial_search_params = get_purchase_params(cursor, start_timestamp)
        return self.client.orders.search_orders(body=initial_search_params), start_timestamp

    def sync_purchases(self, square_ids, location_id):
        data = get_purchase_batch_data(square_ids, location_id)
        response = self.client.orders.batch_retrieve_orders(body=data)
        return response


class StorePurchaseRefundClient(CanvasSquareClient):
    def get_refunds(self, cursor):
        start_timestamp = get_refund_last_timestamp()
        search_params = get_refund_params(cursor, start_timestamp)
        return self.client.refunds.list_payment_refunds(**search_params)


def set_barcode(product):
    if not product.barcode:
        products = Product.objects.exclude(barcode="")
        previous = None
        with atomic():
            if products:
                previous = products.annotate(as_int=Cast("barcode", IntegerField())).order_by("as_int").last()
            product.barcode = f"{(previous.as_int if previous else settings.CANVAS_INITIAL_BARCODE_INTEGER) + 1:011}"
            product.save()


def make_stockable(product, supplier):
    StockCount.objects.get_or_create(product=product, supplier=supplier)
