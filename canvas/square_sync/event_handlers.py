# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import base64
import hmac
import json
import logging
from decimal import Decimal
from hashlib import sha1

from dateutil.parser import ParserError, parse
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.transaction import atomic
from django.http import HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from shuup.core.models import MutableAddress, PersonContact, Shop, Supplier, get_person_contact
from shuup.simple_supplier.models import StockCount
from shuup_multivendor.models import SupplierPrice
from square.client import Client

from canvas.models import SquareCatalog, SyncedShopProduct, SyncedStockCount, SyncedStoreLocation
from canvas.models.sync import SyncedContact
from canvas.suppliers import CanvasSimpleSupplierModule

logger = logging.getLogger(__name__)


def is_valid_signature(request, signature_key: str):
    """Validate the webhook signature."""
    # https://developer.squareup.com/docs/webhooks-api/validate-notifications
    try:
        signature: bytes = request.headers["x-square-signature"].encode()
    except KeyError:
        return False
    url = "".join([request.scheme, "://", request.get_host(), request.path])
    combination: bytes = url.encode() + request.body
    hashed = hmac.new(signature_key.encode(), combination, digestmod=sha1)
    return hmac.compare_digest(base64.encodebytes(hashed.digest()).decode().strip("\n").encode(), signature)


def _update_prices(client, results, last_update):
    """Look through the catalog and see what (if any) store-specific prices have been updated, and copy those
    prices to Shuup.
    """
    while True:
        for obj in results["objects"]:
            location_overrides = obj["item_variation_data"].get("location_overrides")
            if not location_overrides:
                # The change wasn't anything that should sync.
                continue

            for override in location_overrides:
                store_location = override["location_id"]
                store_price_value = override["price_money"]["amount"]
                item_variation_id = obj["id"]
                # Note: Square is currently for US stores only.
                SupplierPrice.objects.filter(
                    Q(product__shop_products__synced_shop_product__square_id=item_variation_id)
                    | Q(product__shop_products__synced_shop_product__standalone_square_id=item_variation_id),
                    supplier__synced_location__square_id=store_location,
                    shop=Shop.objects.filter(identifier="canvas-us").first(),
                ).update(amount_value=store_price_value / 100)

        cursor = results.get("cursor")
        if not cursor:
            break

        updates = client.catalog.search_catalog_objects(
            body={"begin_time": last_update, "object_types": ["ITEM_VARIATION"], "cursor": cursor}
        )
        if updates.is_success() and "objects" in updates.body:
            results = updates.body
        else:
            print(updates.errors)
            break


@csrf_exempt
def update_store_price(request):
    """Canvas has been notified of a recent change to an item in the Square Catalog. See what it is, and if it
    turns out that a specific store's price has been overridden, confirm the update here in Shuup.
    """
    if request.method == "POST":
        valid = is_valid_signature(request, settings.PRODUCT_SYNC_WEBHOOK_KEY)
        if not valid:
            return HttpResponseForbidden

        payload = json.loads(request.body)
        new_timestamp = parse(payload["data"]["object"]["catalog_version"]["updated_at"])
        last_update = SquareCatalog.objects.first().last_update.isoformat()
        client = Client(access_token=settings.SQUARE_ACCESS_TOKEN, environment=settings.SQUARE_ENVIRONMENT)
        updates = client.catalog.search_catalog_objects(
            body={"begin_time": last_update, "object_types": ["ITEM_VARIATION"]}
        )
        if updates.is_success():
            SquareCatalog.objects.filter(last_update=last_update).update(last_update=new_timestamp)
            if "objects" in updates.body:
                _update_prices(client, updates.body, last_update)
            return JsonResponse({"status": "success"})
        else:
            logger.error("A store price update failed to sync:", updates.errors)
            return JsonResponse({"status": "failure"})
    else:
        return HttpResponseBadRequest()


@csrf_exempt
def sync_inventory(request):
    """Respond to the inventory change that just happened in one of the Canvas stores."""
    if request.method == "POST":
        valid = is_valid_signature(request, settings.INVENTORY_SYNC_WEBHOOK_KEY)
        if not valid:
            return HttpResponseForbidden()

        payload = json.loads(request.body)

        for count in payload["data"]["object"]["inventory_counts"]:
            new_state = count["state"]
            event_timestamp = parse(payload["created_at"])
            synced_shop_product = SyncedShopProduct.objects.filter(
                Q(square_id=count["catalog_object_id"]) | Q(standalone_square_id=count["catalog_object_id"]),
            ).first()
            synced_store = SyncedStoreLocation.objects.filter(square_id=count["location_id"]).first()
            synced, created = SyncedStockCount.objects.filter(
                Q(item__square_id=count["catalog_object_id"])
                | Q(item__standalone_square_id=count["catalog_object_id"]),
            ).get_or_create(
                item=synced_shop_product,
                store=synced_store,
                event_id=payload["event_id"],
                timestamp=event_timestamp,
                qty_change=count["quantity"],
            )

            # Check for duplicate notifications and ignore them.
            if not created:
                continue

            # Make sure this update is the latest one we know about, since it's not impossible to receive updates
            # out of order. Don't overwrite the stock count with old data.
            latest_update = SyncedStockCount.objects.filter(
                Q(item__square_id=count["catalog_object_id"])
                | Q(item__standalone_square_id=count["catalog_object_id"]),
                store__square_id=count["location_id"],
            ).first()
            if event_timestamp < latest_update.timestamp:
                continue

            shop_product = latest_update.item.shop_product
            supplier = Supplier.objects.filter(synced_location__square_id=count["location_id"]).first()

            canvas_supplier = CanvasSimpleSupplierModule(supplier, {})
            if new_state == "IN_STOCK":
                with atomic():
                    # Square sends the grand total of inventory in stock. Convert this to use OOTB stock functionality.
                    stock_count = StockCount.objects.filter(product=shop_product.product, supplier=supplier).first()
                    original_count = stock_count.physical_count
                    delta = Decimal(count["quantity"]) - original_count
                    adjustment = canvas_supplier.adjust_stock(shop_product.product_id, delta)
                logger.debug("Inventory adjustment:", new_state, "original:", original_count, "adjustment", adjustment)
            elif new_state in ("SOLD", "WASTE"):
                with atomic():
                    adjustment = canvas_supplier.adjust_stock(shop_product.product_id, Decimal(count["quantity"]) * -1)
                    logger.debug("Inventory adjustment:", new_state, "adjustment", adjustment)
            elif new_state in ("RETURNED_BY_CUSTOMER", "UNLINKED_RETURN"):
                # Add the returned items back into inventory
                adjustment = canvas_supplier.adjust_stock(shop_product.product_id, Decimal(count["quantity"]))
                logger.debug("Inventory adjustment:", new_state, "adjustment", adjustment)

        return JsonResponse({"status": "ok"})

    else:
        return HttpResponseBadRequest()


@csrf_exempt
def sync_customers(request):  # noqa: C901
    if request.method == "POST":
        valid = is_valid_signature(request, settings.CUSTOMER_SYNC_WEBHOOK_KEY)
        if not valid:
            return HttpResponseForbidden()

        # Find the customer if possible.
        payload = json.loads(request.body)
        customer = payload["data"]["object"]["customer"]
        contact = PersonContact.objects.filter(synced_contact__square_id=payload["data"]["id"]).first()
        if contact:
            user = contact.user
        else:
            user = User.objects.filter(email=customer.get("email_address")).first()
            if user:
                contact = get_person_contact(user)
            else:
                contact = PersonContact.objects.filter(email=customer.get("email_address")).first()

        # Don't overwrite up-to-date data.
        if contact:
            event_timestamp = parse(payload["created_at"])
            if event_timestamp < contact.modified_on:
                return JsonResponse({"status": "ok"})

        # Attempt to delete a deleted customer.
        if payload["type"] == "customer.deleted":
            if contact:
                contact.is_active = False
                contact.save()
                if contact.synced_contact:
                    contact.synced_contact.delete()

        # Create or update customer.
        else:
            if not contact:
                contact = PersonContact(user=user)

            marketing = customer.get("preferences")
            # Don't overwrite existing information.
            if customer.get("given_name"):
                contact.first_name = customer.get("given_name")
            if customer.get("family_name"):
                contact.last_name = customer.get("family_name")
            if marketing:
                contact.marketing_permission = not marketing["email_unsubscribed"] if marketing is not None else False
            if customer.get("email_address"):
                contact.email = customer.get("email_address")
            if customer.get("birthday"):
                try:
                    contact.birth_date = parse(customer.get("birthday"))
                except ParserError:
                    # Square sent something that dateutils doesn't know how to parse (e.g. year 0)
                    pass
            if customer.get("phone_number"):
                contact.phone = customer.get("phone_number")

            address = customer.get("address")
            # Note: `address_line_1` is not guaranteed, but it is required by Shuup
            if address and address.get("address_line_1"):
                billing_address = contact.default_billing_address or MutableAddress()
                billing_address.name = contact.full_name
                billing_address.street = address["address_line_1"]
                billing_address.city = address["locality"]
                billing_address.region_code = address.get("administrative_district_level_1") or ""
                billing_address.postal_code = address["postal_code"]
                billing_address.save()
                contact.default_billing_address = billing_address

            contact.is_active = True
            contact.save()
            sync = SyncedContact.get_sync(contact)
            sync.square_id = payload["data"]["id"]
            sync.version = customer["version"]
            sync.is_synced = True
            sync.save()
            contact.shops.add(sync.shop)

        return JsonResponse({"status": "ok"})

    else:
        return HttpResponseBadRequest()
