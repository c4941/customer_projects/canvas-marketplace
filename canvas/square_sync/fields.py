# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from encrypted_fields import fields

# Issue:
# https://gitlab.com/guywillett/django-searchable-encrypted-fields/-/issues/16

# Solution:
# https://github.com/City-of-Helsinki/open-city-profile/blob/a4963c19a5f1250e87e00a1bd172a5b8ff85c3ab/profiles/fields.py#L20-L25


class CallableHashKeyEncryptedSearchField(fields.SearchField):
    """encrypted_fields.fields.SearchField but modified to support callable hash_key"""

    def get_prep_value(self, value):
        if value is None:
            return value
        # coerce to str before encoding and hashing
        # NOTE: not sure what happens when the str format for date/datetime is changed??
        value = str(value)

        if fields.is_hashed_already(value):
            # if we have hashed this previously, don't do it again
            return value

        # Callable hash key custom code start
        if callable(self.hash_key):
            hash_key = self.hash_key()
        else:
            hash_key = self.hash_key
        # Callable hash key custom code end

        v = value + hash_key
        return fields.SEARCH_HASH_PREFIX + fields.hashlib.sha256(v.encode()).hexdigest()
