# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.conf import settings
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import TemplateView

from canvas.models import CanvasAuth
from canvas.square_sync.client import save_token


def authenticate_canvas(request):
    """Get credentials from Canvas at Square (using the authorization code they just gave us) and store them."""
    if request.method == "GET":
        authorization_code = request.GET.get("code")
        error = save_token(authorization_code)
        if error:
            return HttpResponseRedirect(f"{reverse('shuup:request-token')}?error={error}")
        return HttpResponseRedirect(reverse("shuup:request-token"))


class RequestTokenView(PermissionRequiredMixin, TemplateView):
    template_name = "canvas/grant_permission.jinja"
    permission_denied_message = (
        "You are not authorized to view this page. Please log in with another set of credentials."
    )

    def has_permission(self):
        return self.request.user.groups.filter(name="Staff").exists() or self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        kwargs["grant_url"] = self.url
        auth = CanvasAuth.objects.first()
        kwargs["is_authorized"] = auth is not None and auth.access_token is not None
        kwargs["error"] = self.request.GET.get("error")
        return super().get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        if settings.SQUARE_ENVIRONMENT == "sandbox":
            self.url = f"https://connect.squareupsandbox.com/oauth2/authorize?client_id={settings.SQUARE_APP_ID}"
        else:
            self.url = f"https://connect.squareup.com/oauth2/authorize?client_id={settings.SQUARE_APP_ID}"

        permissions = [
            "ITEMS_READ",
            "ITEMS_WRITE",
            "INVENTORY_READ",
            "INVENTORY_WRITE",
            "MERCHANT_PROFILE_READ",
            "MERCHANT_PROFILE_WRITE",
            "ORDERS_READ",
            "PAYMENTS_READ",
            "CUSTOMERS_READ",
            "CUSTOMERS_WRITE",
        ]
        permissions = "+".join(permissions)
        self.url += f"&scope={permissions}"
        return super().get(request, *args, **kwargs)
