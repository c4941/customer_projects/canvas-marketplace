# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.contrib.auth import get_user_model
from shuup_multivendor.models import SupplierUser


class CloudSupplierStrategy(object):
    def get_supplier(self, **kwargs):
        suppplier_user = SupplierUser.objects.filter(user=kwargs["user"]).first()
        return suppplier_user.supplier if suppplier_user else None


class CloudSupplierUserStrategy(object):
    def get_users(self, **kwargs):
        supplier = kwargs["supplier"]
        user_ids = SupplierUser.objects.filter(supplier=supplier).values_list("user_id", flat=True)
        return get_user_model().objects.filter(id__in=user_ids)
