from typing import Any, Type

from django.db.models.signals import post_save
from django.dispatch import receiver
from shuup.core.models import (
    Category,
    CustomCarrier,
    OrderLineType,
    Shipment,
    ShippingMethod,
    Shop,
    ShopProduct,
    Supplier,
    TaxClass,
    get_person_contact,
)

from canvas.constants import (
    CANVAS_MAIN_SHOP_IDENTIFIER,
    CANVAS_PICK_UP_CARRIER_IDENTIFIER,
    CANVAS_SHIPPING_TAX_CLASS_IDENTIFIER,
    CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER,
    CANVAS_STORE_SHIPPING_CARRIER_IDENTIFIER,
)
from canvas.models import CanvasPickupStockBehaviorComponent, SyncedCategory, SyncedShopProduct
from canvas.models.stock_behavior import CanvasStoreShipStockBehaviorComponent
from canvas.square_sync.client import make_stockable, set_barcode
from canvas.utils import (
    create_wishlist,
    get_canvas_brick_and_mortar_suppliers,
    get_supplier_shops,
    product_is_available_in_stores,
)


@receiver(post_save, sender=Supplier, dispatch_uid="canvas_ensure_shipping_methods")
def ensure_shipping_methods(
    sender: Type[Any],
    instance: Supplier,
    raw: bool,
    created: bool,
    **kwargs: Any,
) -> None:
    """Ensure that all suppliers have the special shipping methods handling the brick and mortar pickups, orders,
    and shipping.
    """
    canvas_suppliers = get_canvas_brick_and_mortar_suppliers()

    if instance in canvas_suppliers:
        if not created:
            return
        suppliers = Supplier.objects.exclude(pk__in=canvas_suppliers.values("pk"))
    else:
        suppliers = [instance]

    carrier, created = CustomCarrier.objects.update_or_create(
        identifier=CANVAS_PICK_UP_CARRIER_IDENTIFIER,
        defaults=dict(
            name="Canvas pickup carrier",
        ),
    )
    store_shipping_carrier, created = CustomCarrier.objects.update_or_create(
        identifier=CANVAS_STORE_SHIPPING_CARRIER_IDENTIFIER,
        defaults=dict(
            name="Canvas ship from store carrier",
        ),
    )

    component, __ = CanvasPickupStockBehaviorComponent.objects.get_or_create()
    ship_to_usa = (
        instance.contact_address.country.code == "US"
        if instance.contact_address and instance.contact_address.country
        else True
    )

    store_shipping_component, __ = CanvasStoreShipStockBehaviorComponent.objects.get_or_create()

    for supplier in suppliers:  # n is big
        for shop in get_supplier_shops(supplier):  # n ~ 2
            for canvas_supplier in canvas_suppliers:  # n ~ 3
                # O(6n) ~ O(n), should be reasonable.
                name = f"Pick up from {canvas_supplier}"
                if canvas_supplier.contact_address:
                    name += f" ({canvas_supplier.contact_address})"

                method, created = ShippingMethod.objects.get_or_create(
                    identifier=f"supplier-{supplier.pk}-store-{canvas_supplier.pk}-shop-{shop.pk}",
                    defaults=dict(
                        carrier=carrier,
                        shop=shop,
                        supplier=supplier,
                        name=name,
                        enabled=True,
                        tax_class=TaxClass.objects.get(identifier=CANVAS_SHIPPING_TAX_CLASS_IDENTIFIER),
                    ),
                )
                method.behavior_components.add(component)
                if not ship_to_usa:
                    # Offer to ship from store
                    store_shipping_method, created = ShippingMethod.objects.get_or_create(
                        identifier=f"ship-supplier-{supplier.pk}-store-{canvas_supplier.pk}-shop-{shop.pk}",
                        defaults=dict(
                            carrier=store_shipping_carrier,
                            shop=shop,
                            supplier=supplier,
                            name=f"Ship from {canvas_supplier}",
                            enabled=True,
                            tax_class=TaxClass.objects.get(identifier=CANVAS_SHIPPING_TAX_CLASS_IDENTIFIER),
                        ),
                    )
                    store_shipping_method.behavior_components.add(store_shipping_component)


@receiver(post_save, sender=ShopProduct, dispatch_uid="canvas_sync_shop_product")
def sync_shop_product(
    sender: Type[Any],
    instance: ShopProduct,
    raw: bool,
    created: bool,
    **kwargs: Any,
) -> None:
    # Note: Saving a ShopProduct from admin triggers this signal multiple times.
    us_shop = Shop.objects.filter(identifier=CANVAS_MAIN_SHOP_IDENTIFIER).first()
    if product_is_available_in_stores(instance).count() > 0 and instance.shop == us_shop:
        # Only ShopProducts that are sold in stores should be synced, and only US shop products should be synced.
        SyncedShopProduct.create_sync(shop_product=instance)


@receiver(post_save, sender=Category, dispatch_uid="canvas_sync_category")
def sync_category(
    sender: Type[Any],
    instance: ShopProduct,
    raw: bool,
    created: bool,
    **kwargs: Any,
) -> None:
    # Note: Saving a ShopProduct from admin triggers this signal multiple times.
    SyncedCategory.create_sync(instance)


@receiver(post_save, sender=Shipment, dispatch_uid="canvas_initiate_product_sync")
def sync_initial_shop_product(
    sender: Type[Any],
    instance: Shipment,
    raw: bool,
    created: bool,
    **kwargs: Any,
) -> None:
    order = instance.order
    customer = order.customer
    if customer is None:
        return

    if created and customer.groups.filter(identifier=CANVAS_STORE_PURCHASING_GROUP_IDENTIFIER).count():
        # Only new shipments created for orders made by users in the Store Purchasing contact group.
        shop = order.shop
        for product_line in order.lines.filter(type=OrderLineType.PRODUCT):
            sync_new_child = False
            set_barcode(product_line.product)
            shop_product = product_line.product.get_shop_instance(shop)
            # Add a new sync record if there isn't one.
            if not hasattr(shop_product, "synced_shop_product"):
                SyncedShopProduct.get_sync(shop_product)
                sync_new_child = True

            if shop_product.product.is_variation_child():
                parent_shop_product = shop_product.product.variation_parent.get_shop_instance(shop)
                if not hasattr(parent_shop_product, "synced_shop_product") or sync_new_child:
                    # Sync new parents, and also parents of previously-unsynced children.
                    SyncedShopProduct.get_sync(parent_shop_product)

            store_user = customer.user.vendor_users.filter(shop=shop).first()
            make_stockable(product_line.product, store_user.supplier)
            shop_product.suppliers.add(store_user.supplier)

