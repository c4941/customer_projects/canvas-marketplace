// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const fs = require('fs');
const toml = require('toml');
const pyproject = toml.parse(fs.readFileSync('../pyproject.toml', 'utf-8'));
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");

const isProduction = process.env.NODE_ENV == "production";

const config = {
  entry: {
		"canvas": {
			import: "./static_src/index.js",
			filename: `canvas.${pyproject.tool.poetry.version}.js`,
			publicPath: "auto"
		}
	},
  output: {
    path: path.resolve(__dirname, "static"),
  },
  plugins: [
    new CopyWebpackPlugin({
        patterns: [
            { from: 'static_src/canvas', to: 'canvas' },
        ]
    }),
    new CompressionPlugin({algorithm: "gzip"})
],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/i,
        loader: "babel-loader",
      },
      {
        test: /\.(less)$/i,
        use: ["style-loader", "css-loader", "less-loader"],
      },
      {
        test: /\.(css)$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
      },

      // Add your rules for custom modules here
      // Learn more about loaders from https://webpack.js.org/loaders/
    ],
  }
};

module.exports = () => {
  if (isProduction) {
    config.mode = "production";

    config.plugins.push(new MiniCssExtractPlugin());
  } else {
    config.mode = "development";
  }
  return config;
};
