# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import pytz
from django.conf import settings
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin
from django.utils.translation import ugettext_lazy as _
from shuup.admin.shop_provider import get_shop
from shuup.admin.supplier_provider import get_supplier
from shuup.core.models import get_person_contact
from shuup.utils.djangoenv import has_installed


class EnforceMiddleware(MiddlewareMixin):
    """
    Make sure the vendor is connected to Stripe before accessing any other part of the system
    and the is a plan selected
    """

    # list of views that user can access without enforcemente
    permitted_views = [
        "shuup_admin:home",
        "shuup_admin:tour",
        "shuup_admin:wizard",
        "shuup_admin:login",
        "shuup_admin:logout",
        "shuup_admin:stop-impersonating-staff",
        "shuup_admin:menu",
        "shuup_admin:menu_toggle",
        "shuup_admin:js-catalog",
        "shuup_admin:set-language",
        "shuup_admin:shuup_vendor_plans.subscribe",
        "shuup_admin:shuup_vendor_plans.pay",
        "shuup_admin:shuup_vendor_plans.sub_table",
        "shuup_admin:shuup_vendor_plans.success",
        "shuup_admin:shuup_vendor_plans.cancel_first",
        "shuup_admin:shuup_vendor_plans.cancel_second",
        "shuup_admin:shuup_vendor_plans.cancel_third",
        "shuup_admin:shuup_vendor_plans.cancel",
        "shuup_admin:shuup_stripe_multivendor.connect",
    ]

    def process_view(self, request, view_func, view_args, view_kwargs):
        if getattr(request.user, "is_superuser", False) or request.resolver_match.app_name != "shuup_admin":
            return

        if not request.resolver_match.view_name or request.resolver_match.view_name in self.permitted_views:
            return

        supplier = get_supplier(request)

        # enforce plan selection
        if (
            supplier
            and has_installed("shuup_vendor_plans")
            and supplier.identifier != "canvas-vendor"
            and getattr(settings, "SHUUP_VENDORS_ENFORCE_SUBSCRIPTION", False)
        ):
            from shuup_vendor_plans.utils import get_subscription

            if supplier and not get_subscription(request):
                messages.warning(request, _("Please, choose a plan to continue using."))
                return HttpResponseRedirect(reverse("shuup_admin:shuup_vendor_plans.subscribe"))

        if (
            supplier
            and has_installed("shuup_stripe_multivendor")
            and supplier.identifier != "canvas-vendor"
            and getattr(settings, "SHUUP_ENFORCE_STRIPE_CONNECT", False)
        ):
            from shuup_stripe_multivendor.models import StripeConnectedAccount
            from shuup_stripe_multivendor.utils import get_payment_processor_for_shop

            shop = get_shop(request)
            payment_processor = get_payment_processor_for_shop(shop)

            # enforce stripe connected
            if not StripeConnectedAccount.get_for_object(payment_processor, supplier):
                messages.warning(request, _("Please, connect to Stripe to continue using."))
                return HttpResponseRedirect(reverse("shuup_admin:shuup_stripe_multivendor.connect"))


class ActivateTimezoneMiddleware(MiddlewareMixin):
    def process_request(self, request):
        person = get_person_contact(request.user)

        if person.timezone:
            tz = person.timezone
        elif request.session.get("tz"):
            tz = pytz.timezone(request.session["tz"])
        else:
            tz = settings.TIME_ZONE

        timezone.activate(tz)
