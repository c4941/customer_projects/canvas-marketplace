# -*- coding: utf-8 -*-
import shuup.apps


class AppConfig(shuup.apps.AppConfig):
    name = "canvas_admin"
    label = "canvas_admin"
    provides = {
        # enter provides here
    }
