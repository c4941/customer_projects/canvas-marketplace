const { getParcelBuildCommand, runBuildCommands } = require("shuup-static-build-tools");

runBuildCommands([
    getParcelBuildCommand({
        cacheDir: "canvas",
        outputDir: "static/canvas_admin",
        outputFileName: "canvas_admin",
        entryFile: "static_src/index.js"
    }),
]);
