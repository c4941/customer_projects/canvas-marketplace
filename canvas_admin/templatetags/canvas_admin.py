import django_jinja.library
from shuup.core.models import Shop, ShopStatus


@django_jinja.library.global_function
def get_first_enabled_shop():
    return Shop.objects.filter(status=ShopStatus.ENABLED).first()
