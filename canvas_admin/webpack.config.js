// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const fs = require('fs');
const toml = require('toml');
const pyproject = toml.parse(fs.readFileSync('../pyproject.toml', 'utf-8'));

const isProduction = process.env.NODE_ENV == "production";

const config = {
  entry: {
		"canvas_admin": {
			import: "./static_src/index.js",
			filename: `canvas_admin.${pyproject.tool.poetry.version}.js`,
			publicPath: "auto"
		},
	},
  output: {
    path: path.resolve(__dirname, "static"),
  },
  plugins: [],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/i,
        loader: "babel-loader",
      },
      {
        test: /\.(less)$/i,
        use: ["css-loader", "postcss-loader", "less-loader"],
      },
      {
        test: /\.(css)$/i,
        use: ["css-loader", "postcss-loader"],
      },
      {
        test: /\.(scss)$/i,
        use: ["css-loader", "postcss-loader", "sass-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
        type: "asset",
      },

      // Add your rules for custom modules here
      // Learn more about loaders from https://webpack.js.org/loaders/
    ],
  }
};

module.exports = () => {
  if (isProduction) {
    config.mode = "production";

    config.plugins.push(new MiniCssExtractPlugin());
  } else {
    config.mode = "development";
  }
  return config;
};
