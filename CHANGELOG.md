# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.7.13] - 2022-02-16

### Fixed

- Fix store purchase discount

## [2.7.12] - 2022-02-15

### Added

- Additional data import/analysis tools for devs

### Fixed

- Fix negative StorePurchaseLine `discount_amount` value 

## [2.7.11] - 2022-01-24

### Changed

- Update addons.

### Fixed

- Bug in signal handler when shipping methods already exist but with a different carrier.

## [2.7.10] - 2021-12-20

### Fixed

- Assign store purchase to discount purchase lines

## [2.7.9] - 2021-12-17

### Fixed

- Refresh StorePurchaseLines queryset before trying to calculate vendor funds commissions

## [2.7.8] - 2021-12-17

### Fixed

- Fix incorrect set update call in POS data import

## [2.7.7] - 2021-12-15

### Changed

- Re-enabled automatic POS data import

## [2.7.6] - 2021-12-15

### Fixed

- There may be no POS data to import on a given attempt
- Various POS import fixes

## [2.7.5] - 2021-12-14

### Fixed

- Street information is not guaranteed by Square

### Removed

- Special in-store sales commission from VendorFunds calculation

## [2.7.4] - 2021-12-14

### Changed

- Turn vendor plans enforcement off
- Refactored POS data import to reuse for how refunds come from Square

### Fixed

- Various fixes regarding the POS purchase import management command
- Various fixes for POS refunds import command
- Expect potential invalid DOB from Square

## [2.7.3] - 2021-12-13

### Fixed

- If something goes wrong and there are no combinations, don't crash

## [2.7.2] - 2021-12-09

### Fixed

- Made POS import tweaks to better conform to Square
### Added

- Add vendor plans
- Add support for shippo

## [2.7.1] - 2021-12-07

### Changed

- Re-enable store purchase cronjob

### Fixed

- Check for customer before checking groups

## [2.7.0] - 2021-12-07

### Fixed

- Replace store purchasing Discount strategy with an order modifier module
- When creating shipments, distinguish between store and brand inventory
- During store purchase import, `tenders` is a list, not a dict

### Changed

- Import all store purchases starting from Nov. 1, not from go-live
- Require a shop product price for the US shop
- Bump Shuup version

## [2.6.3] - 2021-12-03

### Fixed

- Add a shop filter when querying for synced children

## [2.6.2] - 2021-12-02

### Changed

- Disabled store purchase import task

### Fixed

- Use empty string when `administrative_district_level_1` is missing

## [2.6.1] - 2021-12-01

### Added

- shuup-subscriptions addon

### Fixed

- Don't depend on `administrative_district_level_1` key when syncing customers

## [2.6.0] - 2021-11-08

### Fixed

- Sync process should not attempt to sync EU shop products

### Added

- Importer for Square POS purchases
- Reports for in-store purchases for markeplace staff, vendors
- Brand name to synced item description (to make it searchable in Square)

### Changed

- SustainabilityGoals are ordered by identifier

## [2.5.2] - 2021-10-14

### Changed

- Set barcode (if missing) when vendor saves product form
- Update local permissions

### Fixed

- Load product SDGs management for staff users

### Added

- Designer credit in Shuup Powered By text 
- New supplier strategy: choose cheapest brand if either it or the store has inventory

## [2.5.1] - 2021-10-14

### Fixed

- Fix migrations

## [2.5.0] - 2021-10-14

### Added

- Customer sync from Square
- PersonContact sync to Square

### Customer changes

- Customers/Contacts sync between Square and the marketplace

### Fixed

- Don't attempt to sync brand inventory
- Query error in inventory sync management command

## [2.4.9] - 2021-10-07

### Fixed

- Filter variation StockCount query by Supplier

### Changed

- Update suggested permissions for local development

## [2.4.8] - 2021-09-29

### Fixed

- Megamenu to calculate height based on inner content after page fully loaded
- Wishlist button not removing properly
- Wishlist button toggle to use vannila js selector for browser support

### Customer changes

- Megamenu overflowing fixed
- Wishlist button not removing properly

## [2.4.7] - 2021-09-27

### Fixed

- Incorrect query parameters

## [2.4.6] - 2021-09-24

### Fixed

- Allow for stores to have a backlog of unsynced inventory records
- Don't sync brand inventory to stores

## [2.4.5] - 2021-09-16

### Changed

- Checkout Phases overridden and given new titles
- Move order confirmation form to top of section, remove comment field

### Customer changes

- Checkout process features action-oriented titles for the sections
- "Confirm Order" button comes up immediately after payment

## [2.4.4] - 2021-09-15

### Fixed

- Override default product context function to exclude stores from product pages

### Customer changes

- Products will display the brand, not the store that sells it

## [2.4.3] - 2021-09-15

### Fixed

- Override `ProductVariationsSection` to check for `canvas_supplier`
- Check if supplier is store before attempting to sync inventory

### Customer changes

- Brands can update variation stock again

## [2.4.2] - 2021-09-15

### Added

- Mobile search modal display with toggle on body click

### Changed

- Close svg icon to brand icon
- Redirect fa icon to svg icons
- Wishlist styling desktop and mobile

### Fixed

- Don't clutter up alerts channel when syncing an ineligible product
- Newsletter footer icon src toggle on focus for homepage giftcard and footer inputs
- Search view mobile and desktop styling

## [2.4.1] - 2021-09-15

### Fixed

- Shipping Method identifier may be None in revenue calculator
- Vendor users and Firebase users also get automatic wishlists
- Viewing a store's vendor page redirects to "Discover our stores"

## [2.4.0] - 2021-09-14

### Added

- Vendor full orderline report
- VendorFunds decides whether the brand or Canvas gets the shipping fee

### Fixed

- Overrode admin stock management to use new CanvasSimpleSupplier
- Store Shipping stock component bug fix

### Changed

- Bump dependencies to latest

## [2.3.5] - 2021-09-14

### Added

- Add custom CMS page template to list only the last 3 children pages, used for blog template

### Fixed

- Add missing migration file
- Adjust `init_project` to correctly handle Supplier Modules

## [2.3.4] - 2021-09-14

### Added

- Vendor outside USA default to shipping from store to US customers.
- Ignore out-of-stock error for store purchases
- Newsletter icon on focus toggle

### Fixed

- Navigation alignment to have all columns in one row and image on right edge
- homepage mobile sizing
- Checkbox plugin spacing sizing
- Newsletter page sizing
- Newsletter page icon button
- Product detail input styling
- Wishlist positioning and button styling across all elements
- Product detail top description section
- Homepage element spacings and lineheights
- footer alignments
- Tote Bag All screen sizes
- Navigation alignment desktop

## [2.3.3] - 2021-09-11

### Added

- Stylings for wishlist cards and modal
- +/- buttons in basket work.
- Can delete items from basket view.
- Clicking favorite icon in the basket also removes the line from the basket and adds it to the wishlist.

### Fixed

- Wishlist await add to wishlist to finish before displaying html
- Change all instances of wishlist button text when removing a wishlist item from the wishlist
- Save selected goals in vendor registration.
- Add vendors staring by numbers to the vendors list.

## [2.3.2] - 2021-09-10

### Fixed

- Be sure to cache parent category

### Added

- Canvas images to replace dummy content

## [2.3.1] - 2021-09-10

### Added

- All wishlist functionalities
- Wishlist cards to side modal
- Asynchronous add and remove of wishlist products
- wishlist object to global context
- Add to cart form button to wishlist card
- Front page links

### Fixed

- context processor wishlist prods prefetch_related query params
- wishlist button macro to check for edge cases

### Customer changes

- Fix links on front page
- Wishlist (on front page) is fully functional

## [2.3.0] - 2021-09-10

### Changed

- Vendor Orders/Shipments can be managed by stores for their own orders

### Customer changes

- Stores can "ship" their own pickup orders

## [2.2.4] - 2021-09-02

### Changed

- Do not schedule syncs for EU shop products - US only

### Added

- Datepicker for store events admin
- Admin module for failed syncs

### Fixed

- Product edit page should not crash because of misconfigurations
- Admin Theme Configuration no longer being overrided.

### Removed

- SyncedShopProduct creation in init_project

### Customer changes

- Product edit page should not crash
- Store users have a datepicker for creating events
- Increased Square sync accuracy
- Fixed font glitch in the admin panel.

## [2.2.3] - 2021-08-31

### Fixed

- Corrected query for synced children

### Customer changes

- Continued increased Square sync accuracy

## [2.2.2] - 2021-08-31

### Removed

- Overridden store prices - only brands set prices

### Fixed

- Missing category during Square sync

### Customer changes

- Increased accuracy in Square sync

## [2.2.1] - 2021-08-31

### Fixed

- Wishlist image renders correctly

## [2.2.0] - 2021-08-30

### Customer Changes

- Wishlist button size fixed
- User dashboard handles the user's one wishlist

### Fixed

- Wishlist button size

### Changed

- Don't use xtheme to get wishlist button
- Use img tags for SVG files
- Users do not create or delete additional wishlists

## [2.1.2] - 2021-08-27

### Fixed

- Fix missing ProductSustainabilityGoals object on some existing products

### Customer changes

- Some products that were not viewable in the admin are viewable again

## [2.1.1] - 2021-08-24

### Fixed

- Fix product creation

### Customer Changes

- New products can be created without errors.

## [2.1.0] - 2021-08-24

### Removed

- Product Reviews tab from product detail page

### Fixed

- Do not print non-existent barcodes

### Added

- CanvasProductExtra model to handle additional information about products.

### Changed

- Manufacturer tab no longer visible on Product edit view.
- Social media handles now referenced as "Instagram Handles" in the admin.
- Vendor detail view shows only brand statement, location (city, ST only), sdg info, and instagram handles.
- Minor developer help in README.md
- Product view "Detail" section will never show dimensions or weight.
- Product extra details will display in "Detail" section, if they exist.
- Product SDGs default to their vendor's SDGs.

### Customer changes

- Social media handles now referenced as "Instagram Handles" in the admin.
- Vendor detail view shows only brand statement, location (city, ST only), sdg info, and instagram handles.
- Added fields for product material and fit, care, and about brands, and these will display in the "Details" section if they exist.
- "Manufacturer" tab removed from the product edit view.
- Vendor detail view shows only brand statement, location (city, ST only), sdg info, and instagram handles.
- Hide all but the basic info when a Vendor creates a new Product.
- When a vendor creates a new prodcut, they will fist enter and save the basic information, after which they can enter and save more detailed information.
- Offer to print barcodes only if they exist.
- Product SDGs default to the same SDGs as the vendor.
- Removed "Product Reviews" tab from product details page.

## [2.0.8] - 2021-08-19

### Changed

- De-escalate logging from `warning` to `info` in product sync

## [2.0.7] - 2021-08-19

### Fixed

- Fix incorrect method call for stocks

## [2.0.6] - 2021-08-19

### Fixed

- Fix error caused by updating stocks

## [2.0.5] - 2021-08-10

### Fixed

- Fix overridden login macro

### Removed

- shuup-shippo-multivendor from requirements

## [2.0.4] - 2021-07-28

### Fixed

- Malformed cronjob

## [2.0.3] - 2021-07-28

### Changed

- Removed Canvas Williamsburg from init_project
- Stores in init_project are pre-approved
- Use poetry for dependency management
- Use Python 3.8 for build jobs

### Fixed

- Add canvas_admin to installed apps
- Check if store exists during shipping phase

## [2.0.2] - 2021-06-24

### Added

- Admin Styles

## [2.0.1] - 2021-06-24

### Fixed

- Added missing __init__ file

## [2.0.0] - 2021-06-24

### Added

- Add automatic barcode generation
- Add setting for starting barcode number
- Add Store location sync to Square
- Add Category sync to Square
- Add ShopProduct sync to Square
- Add store-specific price override sync from Square to Shuup
- Add Square integration setup notes for developers
- Add Square OAuth
- Add Firebase authentication
- Add Inventory sync

## [1.8.5] - 2021-06-24

### Fixed

- Don't break if user has no wishlist

## [1.8.4] - 2021-06-24

### Added

- Add brand main page link to footer
- Installed shuup_wishlist addon
- Installed shuup_product_reviews addon
- Add registered user's wishlist items to global context
- Add basic wishlist management JS functions

### Changed

- Use bigger heading style for product carousel plugin
- Update styling for the navigation marquee element
- Increase margin for each canvas store in mobile sizes
- Add bottom margin for each brand card in brand list page
- Update footer logo size on mobile devices
- Reduce margin between homepage elements in mobile screen sizes
- Homepage: Update some of the links and button texts from hardcoded elements

### Fixed

- Fix font size for large headings in mobile
- Fix select2 width issue when reducing screen width
- Ensure CanvasStoreExtra instances in init_project
- Override `/c/` to filter by SDG (without crashing)
- Wishlist button in basket doesn't crash

### Changed

- Users automatically get a wishlist during registration

## [1.8.3] - 2021-06-23

### Fixed

- Store events handle correctly

## [1.8.2] - 2021-06-23

### Added

- Discover Our Stores infrastructure
- Add initial styling for discover our stores page
- Add full page width image plugin
- Add initial styling for customer dashboard
- Add custom styles and template for vendor list page
- Add custom styles and template for vendor detail page
- Homepage: Add images to scattered images section
- Homepage: Add placeholder for sdg carousel
- Installed shuup-mailchimp

### Changed

- Add two layout options for the SDG plugin
- Reduce spacing for accordion plugin items
- Homepage brand list: Change lorem ipsum to actual text content
- Product detail: Show brand description and fix accordion element layout issues
- Update stores page content and design
- Brand detail: Update design for brand logo

### Fixed

- Fix hardcoded url check from navigation template
- Fix dropdown visibility in mobile nav
- Fix homepage brand list styles for mobile devices
- Fix product card z-index when hovering images
- Fix megamenu background size when resizing window
- Fix mobile navigation page link visibility

## [1.8.1] - 2021-06-16

### Added

- SDG plugin (for carousel use)
- SDG shop product filter
- /SDG/ url routing to /c/
- Add plugin for adding vertical white space
- Add custom title plugin
- Add empty cms page template
- Add custom checkout and order page styles
- Add styles for 'btn as a link'

### Changed

- Overwrite simple_cms page.jinja template
- Add styling for filter popup
- Hide search suggestion from search popup
- Hide Giftcards from navigation
- Change default alert text color

### Fixed

- Fix filter dialog close button padding issue
- Fix product card hover effects
- Fix product image sizing in product page and cards

### Removed
- Remove category page image as it's not in the designs

## [1.8.0] - 2021-06-10

### Added

- SDG infrastructure
- Add asterisk on required inputs
- Add custom styles for brand register page
- Create a custom view/page 'join the canvas'
- Add custom carousel styles, templates and scripts
- Enable popovers and add custom styling
- Show selected SDGs on product page

### Fixed

- Check if product page image carousel element exists
- Fix navigation elements being hidden when logged in as admin

### Changed

- Remove browser styles from shopping cart quantity inputs
- Reduce pageload animation delay
- Move glide styles from product carousel to a universal glide.less file
- Move custom Glide arrow disabler function into it's own file
- Fix accordion plugin title text align
- User auth: Update login, password update and registration templates
- Refactor folder structure for styles
- Update functionality for product page images in mobile
- Update styles and functionality for mobile nav
- Change page transition/animation configs
- Update styles for navigation elements and custom button
- Change top marquee bar background value to a CSS variable
- Update main navigation styles
- Update wishlist popup title styles
- Update basket to change the product counter number on navigation elements
- Update filters dialog styles
- Update homepage hero banner
- Move frequently used javascript functions into their own files
- Update main navigation template
- stickynav: don't move the nav if search open

## [1.7.1] - 2021-05-20

### Added

- More vendor permissions

### Fixed

- Summernote activates

### Removed

- 'Status text' and 'available until' fields in vendor product admin
- 'Physical count' field from Stock Management

### Changed

- Product dimension field labels refer to packaging
- Shipping Methods 'enabled' field label
- "name" and "name_ext" labels
- Hide `purchase_multiple` from vendors and set default value

## [1.7.0] - 2021-05-17

- Add custom product page styling

## [1.6.4] - 2021-03-31

- Add animations and depth effects to the frontend.

## [1.6.3] - 2021-03-23

### Added

- Add a grayscale vendor map to the landing page.

### Changed

- Improve the design.

## [1.6.2] - 2021-03-23

### Added

- Allow vendors and Canvas stores to message each other.

## [1.6.1] - 2021-03-23

### Added

- Add an accordion plugin for the frontend.

## [1.6.0] - 2021-03-23

### Added

- Add a custom permissions for Canvas Canvas brick and mortar stores.
- Create initial accounts for Canvas Williamsburg and Canvas Bowery.
- Add the ability for Canvas stores to set their opening hours.

### Removed

- Remove unneeded vendor settings form parts from Canvas stores and vendors.

## [1.5.0] - 2021-03-17

### Changed

- Improve the design.

## [1.4.4] - 2021-03-15

### Fixed

- Fix missing theme static files.

## [1.4.3] - 2021-03-15

### Changed

- Improve the theme.

## [1.4.2] - 2021-03-02

### Added

- Add a vendor report for viewing total sales from all shops from specified date ranges.

## [1.4.1] - 2021-03-01

### Fixed

- Correctly round and truncate 'Current physical stock' value in the barcode product form part.

## [1.4.0] - 2021-03-01

### Added

- Add a product form part for viewing and printing product barcodes.
- Add a product form part for managing variation products and their stocks.

## [1.3.0] - 2021-02-17

### Added

- Show the stock count information in the admin product lists.
- Show the canvas store information in the admin product lists.
- Add a 'Login to brand portal' button to the navbar for anonymous users.

### Changed

- Show all choices in category and product type fields without requiring to type anything.
- By default select the 'ship to my billing address' option in the checkout.
- Rename 'Price' -> 'Retail price' in product forms.
- Change the 'Register' and 'Checkout as Guest' choices of the first checkout phase into two buttons.

### Removed

- Remove tax class, gtin, barcode, minimum purchase quantity, manufacturer, and visibility field from vendor product form.

## [1.2.0] - 2021-01-25

### Added

- Automatically add custom shipping methods for all suppliers, which handle allowing to pickup from the Canvas brick and mortar stores.

### Changed

- Make all vendor products be shippable by default.

## [1.1.1] - 2021-01-18

### Added

- Allow vendors to select which Canvas brick and mortar stores they sell their product in.
- Allow vendors to see the stock counts of their own products in the Canvas stores they sell in.

## [1.1.0] - 2021-01-13

### Added

- Allow vendors to manage their service providers and shipping methods across all shops.
- Show data across all shops on vendor dashboard.
- Allow admins to manage product approvals across all shops.
- Allow admins to manage product across all shops, similarly to vendors.

## [1.0.1] - 2020-12-31

### Added

- Allow vendors to manage their products across all shops.
- Add a bunch of custom fields to vendor registration and vendor settings.
- Make vendor registration immediately add the vendor to all shops.
- Allow vendors to view and withdraw their vendor funds balance from all shops at once.

## [1.0.0] - 2020-12-07

### Added

- Add initial version.
