FROM python:3.8-slim-buster as intermediate-python

RUN mkdir /app

COPY ./project /app/project
COPY ./canvas /app/canvas
COPY ./runtime /app/runtime
COPY ./canvas_admin /app/canvas_admin
COPY ./manage.py /app/manage.py
COPY ./pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock

WORKDIR /app

ARG SHUUP_REPOSITORY_USER
ARG SHUUP_REPOSITORY_PASSWORD
ENV VIRTUAL_ENV /env

WORKDIR /app
RUN set -ex \
    && apt-get update --yes --quiet && apt-get install --yes --quiet --no-install-recommends \
    git \
    build-essential \
    libpq-dev \
    libmariadbclient-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    libwebp-dev \
    libcairo2-dev \
    libpangocairo-1.0-0 \
    git \
    tzdata \
    && apt update && apt install -y \
    python3-cffi \
    python3-brotli \
    libpango-1.0-0 \
    libpangoft2-1.0-0 \
    libjpeg-dev \
    libopenjp2-7-dev \
    libffi-dev \
    && runDeps="$(scanelf --needed --nobanner --recursive /env \
        | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
        | sort -u \
        | xargs -r apk info --installed \
        | sort -u)" \
    && pip install --upgrade pip \
    && pip install poetry \
    && poetry config http-basic.shuup $SHUUP_REPOSITORY_USER $SHUUP_REPOSITORY_PASSWORD \
    && poetry config http-basic.shuup-cloud $SHUUP_REPOSITORY_USER $SHUUP_REPOSITORY_PASSWORD \
    && poetry config virtualenvs.create true \
    && poetry config virtualenvs.in-project true \
    && poetry config installer.parallel false \
    && poetry install \
    && rm /bin/sh && ln -s /bin/bash /bin/sh \
    && chsh -s /bin/bash
    

FROM node:16-buster-slim as intermediate-node

RUN mkdir /app

COPY ./canvas /app/canvas
COPY ./canvas_admin /app/canvas_admin
COPY ./pyproject.toml /app/pyproject.toml

WORKDIR /app/canvas

RUN apt-get update --yes --quiet \
    && DEBIAN_FRONTEND=noninteractive apt-get install --yes --quiet --no-install-recommends \
    git ssh \
    && npm i && npm run build:prod

WORKDIR /app/canvas_admin

RUN npm i && npm run build:prod \
    && mkdir /app/static

WORKDIR /app

FROM python:3.8-slim-buster
COPY --from=intermediate-python /app /app
COPY --from=intermediate-node /app/canvas /app/canvas
COPY --from=intermediate-node /app/canvas_admin /app/canvas_admin

RUN useradd appuser

RUN apt-get update --yes --quiet && DEBIAN_FRONTEND=noninteractive apt-get install --yes --quiet --no-install-recommends \
    build-essential \
    libpq-dev \
    libmariadbclient-dev \
    libjpeg62-turbo-dev \
    zlib1g-dev \
    libwebp-dev \
    libcairo2-dev \
    libpangocairo-1.0-0 \
    git \
    tzdata \
    && apt update && apt install -y \
    python3-cffi \
    python3-brotli \
    libpango-1.0-0 \
    libpangoft2-1.0-0 \
    libjpeg-dev \
    libopenjp2-7-dev \
    libffi-dev \
    && chmod 755 /app/runtime/entrypoint.sh \
    && chmod 755 /app/runtime/entrypoint_init.sh \
    && chmod 755 /app/runtime/entrypoint_prod.sh \
    && chmod 755 /app/runtime/entrypoint_celery.sh \
    && rm -rf /var/lib/apt/lists/*


WORKDIR /app

ENV TZ=America/New_York
ENV VIRTUAL_ENV /app/.venv
ENV PATH /app/.venv/bin:$PATH:

ENV PYTHONUNBUFFERED=1 \
    PORT=8080

EXPOSE 8080

RUN chown appuser:appuser /app

USER appuser
