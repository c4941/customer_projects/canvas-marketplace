
from .shuup import *
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_sdk.integrations.celery import CeleryIntegration

# DATABASES & CACHE


CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.path.join(REDIS_URL, "6"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        }
    }
}

# STATIC & FILE STORAGE
MEDIA_URL = env("MEDIA_URL", default="/media/")
STATIC_URL = env("STATIC_URL", default="/static/")

MEDIA_ROOT = root(env("MEDIA_LOCATION", default=os.path.join(BASE_DIR, "var", "media")))
STATIC_ROOT = root(env("STATIC_LOCATION", default=os.path.join(BASE_DIR, "var", "static")))
FILE_UPLOAD_PERMISSIONS = 0o644  # https://github.com/divio/django-filer/issues/1031
STATICFILES_DIRS = [
    f"{PROJECT_DIR}/static"
]

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"

AWS_STORAGE_BUCKET_NAME = env("AWS_STORAGE_BUCKET_NAME", default="canvasbucket")
AWS_ACCESS_KEY_ID = env("AWS_ACCESS_KEY_ID", default="")
AWS_SECRET_ACCESS_KEY= env("AWS_SECRET_ACCESS_KEY", default="")
STATICFILES_STORAGE = 'project.storage.StaticCloudStorage'
DEFAULT_FILE_STORAGE = 'project.storage.MediaCloudStorage'
AWS_S3_REGION_NAME = "nyc3"
AWS_DEFAULT_ACL = "public-read"
AWS_LOCATION = ""
AWS_PUBLIC_MEDIA_LOCATION = "/media"
AWS_PRIVATE_MEDIA_LOCATION = "/private"
AWS_STATIC_LOCATION = "/static"
AWS_S3_ENDPOINT_URL = f"https://{AWS_S3_REGION_NAME}.digitaloceanspaces.com"
AWS_S3_CUSTOM_DOMAIN = f"static.thecanvas.nyc"
STATIC_URL = f"{AWS_S3_CUSTOM_DOMAIN}/static/"
MEDIA_URL = f"https://{AWS_S3_CUSTOM_DOMAIN}/media/"


#SMTP
if env("EMAIL_HOST", default=None):
    EMAIL_CONFIG = env.email_url("EMAIL_URL")
    vars().update(EMAIL_CONFIG)
else:
    EMAIL_FILE_PATH = os.path.join(BASE_DIR, "var", "emails")
    EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"

DEFAULT_FROM_EMAIL = env("DEFAULT_FROM_EMAIL", default="no-reply@myshuup.com")


sentry_sdk.init(
    dsn=env("SENTRY_DSN", default=None),
    integrations=[DjangoIntegration(), RedisIntegration(), CeleryIntegration()],
    traces_sample_rate=1.0,
    send_default_pii=True,
    environment=env("SENTRY_ENVIRONMENT", default="production")
)

