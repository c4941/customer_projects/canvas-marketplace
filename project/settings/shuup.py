import os

import dj_database_url
import environ
from .base import *


SHUUP_HOME_CURRENCY = "USD"
SHUUP_SETUP_WIZARD_PANE_SPEC = []  # TODO: fix these

SHUUP_ERROR_PAGE_HANDLERS_SPEC = [
    "shuup.admin.error_handlers:AdminPageErrorHandler",
    "shuup.front.error_handlers:FrontPageErrorHandler",
]

SHUUP_SIMPLE_SEARCH_LIMIT = 50

# User registration
REGISTRATION_FORM = "canvas.front.forms.CanvasCustomerRegistration"

# Permissions
STAFF_PERMISSION_GROUP_NAME = "Staff"
VENDORS_PERMISSION_GROUP_NAME = "Vendors"

# Shuup (shuup.core)
SHUUP_ENABLE_MULTIPLE_SHOPS = True
SHUUP_ENABLE_MULTIPLE_SUPPLIERS = True
SHUUP_MANAGE_CONTACTS_PER_SHOP = True

SHUUP_PRICING_MODULE = "multivendor_supplier_pricing"
SHUUP_SHOP_PRODUCT_SUPPLIERS_STRATEGY = "canvas.suppliers.supplier_strategy:BrandCheapestSupplierStrategy"

SHUUP_REQUEST_SHOP_PROVIDER_SPEC = "canvas.core_shop_provider.DefaultShopProvider"
SHUUP_MULTIVENDOR_ADDRESS_FORM = "canvas.admin_module.forms.contact.CanvasMutableAddressForm"
SHUUP_ADDRESS_MODEL_FORM = "canvas.admin_module.forms.contact.CanvasMutableAddressForm"

SHUUP_FRONT_PRODUCT_CONTEXT_SPEC = "canvas.utils:get_canvas_product_context"

USA_TAX_DEFAULT_TAX_IDENTIFIER = CAN_TAX_DEFAULT_TAX_IDENTIFIER = "Default tax class"
USA_TAX_ADDITIONAL_TAX_CLASS_IDENTIFIERS = []

SHUUP_PROVIDES_BLACKLIST = {
    "admin_module": [
        "shuup.admin.modules.support:ShuupSupportModule",
        "shuup.testing.modules.sample_data:SampleDataAdminModule",
        "shuup.testing.modules.demo:DemoModule",
        "shuup.testing.modules.mocker:TestingAdminModule",
        # The project provides a replacement for these.
        "shuup.admin.modules.products:ProductModule",
        "shuup_messages.admin.MessageModule",
        "shuup_multivendor.admin_module:MultivendorProductsAdminModule",
        "shuup_multivendor.dashboards:SalesDashboardModule",
        "shuup_multivendor.admin_module.modules.services:MultivendorShippingMethodModule",
        "shuup_multivendor.admin_module.modules.service_providers:MultivendorServiceProviderModule",
        "shuup_multivendor.admin_module:MultivendorProductsApprovalAdminModule",
        "shuup_product_variations.admin:ProductVariationsOrganizer",
        "shuup_multivendor.admin_module:MultivendorOrdersAdminModule",
    ],
    "admin_vendor_product_form_part": [
        # The project provides a replacement for these.
        "shuup_multivendor.admin_module.form_parts.product:VendorShopProductFormPart",
        "shuup_multivendor.admin_module.form_parts.product:VendorProductBaseFormPart",
        "shuup_multivendor.admin_module.form_parts.location:ProductSupplierLocationFormPart",
        "shuup_multivendor.admin_module.form_parts.simple_supplier:SimpleSupplierFormPart",
    ],
    "admin_order_section": [
        "shuup.admin.modules.orders.sections:BasicDetailsOrderSection",
    ],
    "service_provider_admin_form": [
        "shuup.testing.service_forms:PseudoPaymentProcessorForm",
        "shuup.testing.service_forms:PaymentWithCheckoutPhaseForm",
        "shuup.testing.service_forms:CarrierWithCheckoutPhaseForm",
    ],
    "front_service_checkout_phase_provider": [
        "shuup.testing.simple_checkout_phase.PaymentPhaseProvider",
        "shuup.testing.simple_checkout_phase.ShipmentPhaseProvider",
    ],
    "admin_contact_toolbar_button": [
        "shuup.testing.modules.mocker.toolbar:MockContactToolbarButton",
    ],
    "admin_contact_toolbar_action_item": [
        "shuup.testing.modules.mocker.toolbar:MockContactToolbarActionItem",
    ],
    "admin_contact_edit_toolbar_button": [
        "shuup.testing.modules.mocker.toolbar:MockContactToolbarButton",
    ],
    "admin_shop_edit_toolbar_button": [
        "shuup.testing.modules.mocker.toolbar:MockShopToolbarButton",
    ],
    "admin_product_toolbar_action_item": [
        "shuup.testing.modules.mocker.toolbar:MockProductToolbarActionItem",
        # The project provides a replacement for these.
        "shuup_multivendor.admin_module.toolbar:ApproveProductAction",
        "shuup_multivendor.admin_module.toolbar:RevokeProductApprovalAction",
    ],
    "admin_contact_section": [
        "shuup.testing.modules.mocker.sections:MockContactSection",
    ],
    "importers": [
        "shuup.testing.importers.DummyImporter",
        "shuup.testing.importers.DummyFileImporter",
    ],
    "xtheme": [
        "shuup.testing.themes:ShuupTestingTheme",
        "shuup.testing.themes:ShuupTestingThemeWithCustomBase",
    ],
    "pricing_module": ["shuup.testing.supplier_pricing.pricing:SupplierPricingModule"],
    "admin_vendor_form_part": [
        # The project provides a replacement for these.
        "shuup_multivendor.admin_module.form_parts.funds_pending:VendorPendingWithdrawalsFormPart",
        "shuup_multivendor.admin_module.form_parts.funds:VendorFundsWireEUFormPart",
        "shuup_multivendor.admin_module.form_parts.funds:VendorFundsWireFormPart",
        "shuup_multivendor.admin_module.form_parts.funds:VendorFundsPaypalFormPart",
        "shuup_multivendor.admin_module.form_parts.vendor:VendorOpeningPeriodsFormPart",
        "shuup_opening_hours.admin_module.form_parts.vendor:SupplierOpeningPeriodsFormPart",
        # These are not needed in the project.
        "shuup_multivendor.admin_module.form_parts.vendor_media:VendorImageMediaFormPart",
        "shuup_multivendor.admin_module.form_parts.vendor_media:VendorMediaFormPart",
    ],
    "customer_dashboard_items": [
        # Doesn't work with shuup-stripe-multivendor.
        "shuup_stripe.dashboard_items:SavedPaymentInfoDashboardItem",
        "shuup_wishlist.dashboard_items:WishlistItem",
    ],
    "admin_product_section": [
        # Not needed in this project for vendors.
        "shuup.campaigns.admin_module.sections:ProductCampaignsSection",
        "shuup_product_variations.sections:ProductVariationsSection",
    ],
    "admin_product_form_part": [
        # Not needed in this project for admins.
        "shuup.customer_group_pricing.admin_form_part:CustomerGroupPricingFormPart",
        "shuup.customer_group_pricing.admin_form_part:CustomerGroupPricingDiscountFormPart",
        "shuup.simple_supplier.admin_module.forms:SimpleSupplierFormPart",
    ],
    "multivendor_admin_order_section": [
        "shuup_multivendor.admin_module.sections.VendorOrderDetailsSection",
        "shuup_multivendor.admin_module.sections.VendorOrderShipmentSection",
    ],
}

# Shuup (shuup.admin)
SHUUP_ADMIN_SHOP_PROVIDER_SPEC = "canvas.admin_shop_provider.AdminShopProvider"
SHUUP_ADMIN_SUPPLIER_PROVIDER_SPEC = "canvas.supplier_provider.MultivendorSupplierProvider"
SHUUP_ADMIN_ALLOW_HTML_IN_PRODUCT_DESCRIPTION = False
SHUUP_ADMIN_ALLOW_HTML_IN_VENDOR_DESCRIPTION = False

# Shuup (shuup.front)
# Basket and order creator settings
SHUUP_CHECKOUT_VIEW_SPEC = "canvas.views:CheckoutViewWithLoginAndRegisterVertical"
SHUUP_BASKET_CLASS_SPEC = "shuup_multivendor.basket.MultivendorBasket"
SHUUP_BASKET_ORDER_CREATOR_SPEC = "shuup_multivendor.order_creator.MultiOrderCreator"
SHUUP_FRONT_OVERRIDE_SORTS_AND_FILTERS_LABELS_LOGIC = {
    "manufacturers": "Brands",
    "supplier": "Filter by vendor",
}
SHUUP_CUSTOMER_INFORMATION_ALLOW_PICTURE_UPLOAD = True
SHUUP_MULTIVENDOR_VENDOR_REVENUE_CALCULATOR = "canvas.vendor_funds.vendor_funds.CanvasRevenueCalculator"

# Shuup Stripe Connect
STRIPE_WEBHOOK_SLUG = env("STRIPE_WEBHOOK_SLUG", default="callback")
STRIPE_WEBHOOK_KEY = env("STRIPE_WEBHOOK_KEY", default=None)
STRIPE_SECRET_KEY = env("STRIPE_SECRET_KEY", default=None)
STRIPE_PUBLIC_KEY = env("STRIPE_PUBLIC_KEY", default=None)
STRIPE_OAUTH_CLIENT_ID = env("STRIPE_OAUTH_CLIENT_ID", default=None)
STRIPE_SUBSCRIPTIONS_API_VERSION = env("STRIPE_SUBSCRIPTIONS_API_VERSION", default="2018")

# Square credentials
SQUARE_APP_ID = env("SQUARE_APP_ID", default=None)
SQUARE_APP_SECRET = env("SQUARE_APP_SECRET", default=None)
SQUARE_ENVIRONMENT = env("SQUARE_ENVIRONMENT", default="sandbox")
PRODUCT_SYNC_WEBHOOK_KEY = env("PRODUCT_SYNC_WEBHOOK_KEY", default=None)
INVENTORY_SYNC_WEBHOOK_KEY = env("INVENTORY_SYNC_WEBHOOK_KEY", default=None)
CUSTOMER_SYNC_WEBHOOK_KEY = env("CUSTOMER_SYNC_WEBHOOK_KEY", default=None)

# How many days to keep inventory sync records
INVENTORY_SYNC_DELETE_CUTOFF = 30

# Whether or not to automatically re-try failed synces
RETRY_FAILED_SYNCS = env("RETRY_FAILED_SYNCS", default=False)

# Key for encrypted model columns.
FIELD_ENCRYPTION_KEYS = env("FIELD_ENCRYPTION_KEYS", default=None).split(",")

# Shuup (shuup.front.apps.registration)
SHUUP_REGISTRATION_REQUIRES_ACTIVATION = False

# Store purchasing order modifier
SHUUP_ORDER_SOURCE_MODIFIER_MODULES = ["basket_campaigns", "discounts_coupon_codes", "store_purchasing"]

# Canvas initial barcode number
CANVAS_INITIAL_BARCODE_INTEGER = 76876
CRON_LOG_ROOT = env.str("CRON_LOG_ROOT", default=os.path.join(BASE_DIR, "log"))

# Scheduled jobs and Celery config
CRONJOBS = [
    ("*/5 * * * *", "django.core.management.call_command", ["sync_to_square"]),
    ("0 0 * * *", "django.core.management.call_command", ["delete_old_inventory_syncs"]),
    ("*/10 * * * *", "django.core.management.call_command", ["reattempt_failed_inventory_syncs"]),
    ("0 * * * *", "django.core.management.call_command", ["import_store_purchases"]),
    # Required for Shippo
    (
        "*/15 * * * *",
        "django.core.management.call_command",
        ["shippo_sync_transactions"],
        {},
        ">> {} 2>&1".format(os.path.join(CRON_LOG_ROOT, "shippo_sync_transactions.log")),
    ),
]



if not DEBUG:
    SHUUP_TASK_RUNNER = "canvas.task_runner.CeleryTaskRunner"

# Shuup Multivendor
SHUUP_ADDRESS_HOME_COUNTRY = "US"
VENDOR_CAN_SHARE_PRODUCTS = False
SHUUP_MULTIVENDOR_ENABLE_CUSTOM_PRODUCTS = True
SHUUP_MULTIVENDOR_VENDOR_REQUIRES_APPROVAL = True
SHUUP_MULTIVENDOR_PRODUCTS_REQUIRES_APPROVAL_DEFAULT = True
SHUUP_MULTIVENDOR_SUPPLIER_MODULE_IDENTIFIER = "canvas_supplier"
SHUUP_MULTIVENDOR_REGISTRATION_COUNTRIES = []
SHUUP_MULTIVENDOR_ENSURE_ADDRESS_GEOPOSITION = True
SHUUP_MULTIVENDOR_GOOGLE_MAPS_KEY = env("SHUUP_MULTIVENDOR_GOOGLE_MAPS_KEY", default=None)
SHUUP_MULTIVENDOR_GOOGLE_MAPS_KEY_FRONTEND = SHUUP_MULTIVENDOR_GOOGLE_MAPS_KEY
SHUUP_MULTIVENDOR_GOOGLE_MAPS_KEY_BACKEND = SHUUP_MULTIVENDOR_GOOGLE_MAPS_KEY
SHUUP_MULTIVENDOR_CALCULATE_VENDOR_DISTANCE = True
SHUUP_MULTIVENDOR_VENDOR_DISTANCE_UNIT = "mi"
SHUUP_MULTIVENDOR_SHOW_LOG_IN_AS_BUTTON_IN_VENDOR_LIST = True
SHUUP_MULTIVENDOR_SHOW_APPROVAL_BUTTON_IN_VENDOR_LIST = True
SHUUP_MULTIVENDOR_VENDOR_REGISTRATION_FORM = "canvas.front.forms.CanvasVendorRegistrationForm"
SHUUP_MULTIVENDOR_PRODUCTS_IS_SHIPPABLE_DEFAULT = True

# Shuup Stripe Multivendor settings
STRIPE_MULTIVENDOR_PAYMENT_MODE = "single-charge"
STRIPE_MULTIVENDOR_CAPTURE_MODE = "auto"

# Shuup Vendor Plans
SHUUP_VENDORS_ENFORCE_SUBSCRIPTION = False

# Shuup Stripe Connect
SHUUP_ENFORCE_STRIPE_CONNECT = False

# Shuup API
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.SessionAuthentication",
        "shuup_api.authentication.ExpiringTokenAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("shuup_api.permissions.ShuupAPIPermission",),
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
}

# Adjust default caches for demos so the bumping get better
# tests. Also ususally with demo there is not enough traffic
# to keep caches up for long time of period. If there is
# nobody browsing for 30 min all caches need to rebuild.
SHUUP_TEMPLATE_HELPERS_CACHE_DURATION = 60 * 420
SHUUP_DEFAULT_CACHE_DURATION = 60 * 420

SHUUP_LOGGING_ENABLE_BASIC_LOGGING = True
SHUUP_LOGGING_SKIP_MENU_ENTRY_URL_NAMES = []
SHUUP_NOTIFY_SCRIPT_RUNNER = "canvas.script_runner.run_event"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

SHUUP_MESSAGES_RECAPTCHA_ENABLED = env.bool("SHUUP_MESSAGES_RECAPTCHA_ENABLED", default=False)
RECAPTCHA_PUBLIC_KEY = env("RECAPTCHA_PUBLIC_KEY", default=None)
RECAPTCHA_PRIVATE_KEY = env("RECAPTCHA_PRIVATE_KEY", default=None)
if SHUUP_MESSAGES_RECAPTCHA_ENABLED and RECAPTCHA_PUBLIC_KEY and RECAPTCHA_PRIVATE_KEY:
    RECAPTCHA_REQUIRED_SCORE = env("RECAPTCHA_REQUIRED_SCORE", default=0.85)
    SHUUP_MESSAGES_USE_RECAPTCHA_V2 = env.bool("SHUUP_MESSAGES_USE_RECAPTCHA_V2", default=True)
    INSTALLED_APPS += ("captcha",)

BASIC_WWW_AUTHENTICATION_USERNAME = env("BASIC_WWW_AUTHENTICATION_USERNAME", default=None)
BASIC_WWW_AUTHENTICATION_PASSWORD = env("BASIC_WWW_AUTHENTICATION_PASSWORD", default=None)
if BASIC_WWW_AUTHENTICATION_USERNAME and BASIC_WWW_AUTHENTICATION_USERNAME:
    BASIC_WWW_AUTHENTICATION = True
    MIDDLEWARE += ("canvas.htaccess_middleware.BasicAuthenticationMiddleware",)

THUMBNAIL_NAMER = "easy_thumbnails.namers.alias"
SHUUP_CUSTOMER_INFORMATION_ALLOW_PICTURE_UPLOAD = True
SHUUP_SHIPPO_PROXY_CONNECT_URL = env(
    "SHUUP_SHIPPO_PROXY_CONNECT_URL", default="https://shuupnow.com/shippo-proxy-connect/"
)
SHUUP_SHIPPO_OAUTH_CLIENT_ID = env("SHUUP_SHIPPO_OAUTH_CLIENT_ID", default=None)
SHUUP_SHIPPO_OAUTH_CLIENT_SECRET = env("SHUUP_SHIPPO_OAUTH_CLIENT_SECRET", default=None)
# Shippo - These are required settings
# You must create an installation, get its UUID and Secret and then use them below
SHUUP_SHIPPO_PROXY_CONNECT_INSTALLATION_ID = env("SHUUP_SHIPPO_PROXY_CONNECT_INSTALLATION_ID", default=None)
SHUUP_SHIPPO_PROXY_CONNECT_SIGNING_KEY = env("SHUUP_SHIPPO_PROXY_CONNECT_SIGNING_KEY", default=None)


SHUUP_FRONT_POWERED_BY_CONTENT = """
    <p class="powered">Powered by <a target="_blank" href="https://shuup.com">Shuup</a> and
    Designed by <a target="_blank" href="https://serious.business/">Serious Business</a></p>
""".strip()