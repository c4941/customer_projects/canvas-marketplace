import os
from .shuup import *

# DATABASES & CACHE

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.path.join(REDIS_URL, "6"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        }
    }
}

# STATIC & FILE STORAGE
MEDIA_URL = env("MEDIA_URL", default="/media/")
STATIC_URL = env("STATIC_URL", default="/static/")

MEDIA_ROOT = root(env("MEDIA_LOCATION", default=os.path.join(BASE_DIR, "var", "media")))
STATIC_ROOT = root(env("STATIC_LOCATION", default=os.path.join(BASE_DIR, "var", "static")))
FILE_UPLOAD_PERMISSIONS = 0o644  # https://github.com/divio/django-filer/issues/1031
STATICFILES_DIRS = [
    f"{PROJECT_DIR}/static"
]

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"



#SMTP
if env("EMAIL_HOST", default=None):
    EMAIL_CONFIG = env.email_url("EMAIL_URL")
    vars().update(EMAIL_CONFIG)
else:
    EMAIL_FILE_PATH = os.path.join(BASE_DIR, "var", "emails")
    EMAIL_BACKEND = "django.core.mail.backends.filebased.EmailBackend"

DEFAULT_FROM_EMAIL = env("DEFAULT_FROM_EMAIL", default="no-reply@myshuup.com")

from .shuup import * #noqa
