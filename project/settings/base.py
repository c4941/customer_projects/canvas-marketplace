# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
import os

import dj_database_url
import environ

#OS
env = environ.Env(DEBUG=(bool, False))

root = environ.Path(__file__) - 3
proj = environ.Path(__file__) - 1

BASE_DIR = root()
PROJECT_DIR = proj()


#DJANGO OS
DEBUG = env("DEBUG")
SECRET_KEY = env("SECRET_KEY")
ALLOWED_HOSTS = ["*"]

BASE_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.redirects",
    "django.contrib.sitemaps",
    # external apps that need to be loaded before Shuup
    "easy_thumbnails",
]

INSTALLED_APPS = BASE_APPS + [
    # Shuup
    "canvas",
    "canvas_admin",
    "shuup.themes.classic_gray",
    "shuup_definite_theme",
    "shuup_multivendor",  # Overrides base for fun
    "shuup.admin",
    "shuup.core",
    "shuup.default_tax",
    "shuup.front",
    "shuup.front.apps.auth",
    "shuup.front.apps.carousel",
    "shuup.front.apps.customer_information",
    "shuup.front.apps.personal_order_history",
    "shuup.front.apps.registration",
    "shuup.front.apps.simple_order_notification",
    "shuup.front.apps.simple_search",
    "shuup.notify",
    "shuup.simple_cms",
    "shuup.discounts",
    "shuup.customer_group_pricing",
    "shuup.campaigns",
    "shuup.simple_supplier",
    "shuup.order_printouts",
    "shuup.utils",
    "shuup.xtheme",
    "shuup.reports",
    "shuup.default_reports",
    "shuup.regions",
    "shuup.importer",
    "shuup.default_importer",
    "shuup_api",
    "shuup_rest_api",
    "shuup_front_api",
    "shuup_category_organizer",
    "shuup_xtheme_extra_layouts",
    "shuup_stripe_multivendor",
    "shuup_typography",
    "shuup.gdpr",
    "shuup.tasks",
    "shuup_logging",
    "shuup.testing",
    "shuup_messages",
    "shuup_sent_emails",
    "shuup_sitemap",
    "shuup_multishop_admin",
    "shuup_product_variations",
    "shuup_opening_hours",
    "shuup_mailchimp",
    "shuup_wishlist",
    "shuup_product_reviews",
    "shuup_subscriptions",
    "shuup_shippo_multivendor",
    "shuup_stripe_subscriptions",
    "shuup_vendor_plans",
    # Externals
    "bootstrap3",
    "django_countries",
    "django_crontab",
    "django_jinja",
    "django_filters",
    "django_redis",
    "filer",
    "registration",
    "reversion",
    "rest_framework",
    "rest_framework.authtoken",
    "rest_framework_swagger",
    "encrypted_fields",
    "storages"
]

EXTRA_INSTALLED_APPS = env.list("EXTRA_INSTALLED_APPS", default=[])
if EXTRA_INSTALLED_APPS:
    INSTALLED_APPS += EXTRA_INSTALLED_APPS

MIDDLEWARE = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "shuup.front.middleware.ProblemMiddleware",
    "shuup.front.middleware.ShuupFrontMiddleware",
    "shuup.xtheme.middleware.XthemeMiddleware",
    "shuup.admin.middleware.ShuupAdminMiddleware",
    "canvas.middleware.EnforceMiddleware",
    "shuup_multivendor.middleware.ShuupMultivendorAdminMiddleware",
    "canvas.middleware.ActivateTimezoneMiddleware",
    "django.middleware.gzip.GZipMiddleware"
)

INTERNAL_IPS = [
    # ...
    "127.0.0.1",
    # ...
]


ROOT_URLCONF = "project.urls"
WSGI_APPLICATION = "project.wsgi.application"

def show_toolbar(request):
    return True

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': show_toolbar,
}


LOGIN_REDIRECT_URL = "/"
LOGIN_URL = "/login/"
SESSION_SERIALIZER = "django.contrib.sessions.serializers.PickleSerializer"
SITE_ID = env("SITE_ID", default=1)

_TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.template.context_processors.debug",
    "django.template.context_processors.i18n",
    "django.template.context_processors.media",
    "django.template.context_processors.static",
    "django.template.context_processors.request",
    "django.template.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "canvas.context_processors.get_user_wishlist",
]

TEMPLATES = [
    {
        "BACKEND": "django_jinja.backend.Jinja2",
        "APP_DIRS": True,
        "OPTIONS": {
            "match_extension": ".jinja",
            "context_processors": _TEMPLATE_CONTEXT_PROCESSORS,
            "newstyle_gettext": True,
            "environment": "shuup.xtheme.engine.XthemeEnvironment",
        },
        "NAME": "jinja2",
    },
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {"context_processors": _TEMPLATE_CONTEXT_PROCESSORS, "debug": DEBUG},
    },
]

#LANGUAGE

LANGUAGE_CODE = env("LANGUAGE_CODE", default="en")
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = [("en", "English")]

PARLER_DEFAULT_LANGUAGE_CODE = "en"

PARLER_LANGUAGES = {
    None: [{"code": c, "name": n} for (c, n) in LANGUAGES],
    "default": {"hide_untranslated": False},
}


# CELERY

CELERY_TASK_SERIALIZER = "pickle"
CELERY_ACCEPT_CONTENT = ["pickle", "json"]
CELERY_CACHE_BACKEND = env("CELERY_CACHE_BACKEND", default="default")

DATABASES = {"default": dj_database_url.config(default=env("DATABASE_URL", default=""))}
REDIS_URL = env("REDIS_URL", default="")

