from __future__ import absolute_import, unicode_literals

import os
import ssl
from celery import Celery

password = os.environ.get("REDIS_URL", "")

Broker = f"{password}/2?ssl_cert_reqs=none"
Backend = f"{password}/3?ssl_cert_reqs=none"

if not os.environ.get("DEBUG", ""):
    Broker = f"{Broker}" 
    Backend = f"{Backend}"

app = Celery("project")
app.conf.broker_url = Broker 
app.conf.result_backend = Backend 

app.autodiscover_tasks()
