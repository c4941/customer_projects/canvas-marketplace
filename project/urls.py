# -*- coding: utf-8 -*-
# This file is part of Shuup.
#
# Copyright (c) 2012-2021, Shuup Commerce Inc. All rights reserved.
#
# This source code is licensed under the Shuup Commerce Inc -
# SELF HOSTED SOFTWARE LICENSE AGREEMENT executed by Shuup Commerce Inc, DBA as SHUUP®
# and the Licensee.
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.contrib.sitemaps.views import sitemap
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from shuup.admin.modules.products.views import ProductMediaBulkAdderView
from shuup.admin.views.dashboard import DashboardView
from shuup.core.utils.maintenance import maintenance_mode_exempt
from shuup_sitemap.views import CategoriesSitemap, CMSPagesSitemap, ProductsSitemap, StaticViewSitemap, SuppliersSitemap

from canvas.front.views.all_categories import CanvasAllCategoriesView
from canvas.front.views.products import canvas_product_price
from canvas.views import CanvasMultiselectAjaxView
from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='Pastebin API')

sitemaps = {
    "categories": CategoriesSitemap,
    "cms_pages": CMSPagesSitemap,
    "products": ProductsSitemap,
    "static": StaticViewSitemap,
    "vendors": SuppliersSitemap,
}


def not_vendor(view):
    def f(request, *args, **kwargs):
        user = request.user
        is_anonymous = getattr(user, "is_anonymous", False)
        is_superuser = getattr(user, "is_superuser", False)
        is_staff_member = request.shop and request.shop.staff_members.filter(id=user.id).exists()
        if is_superuser or is_staff_member:
            return login_required(view, login_url="shuup_admin:login")(request, *args, **kwargs)
        elif is_anonymous:
            return login_required(view, login_url="shuup:login")(request, *args, **kwargs)
        return HttpResponseRedirect(reverse("shuup_admin:shuup_multivendor.dashboard.supplier"))

    return f


urlpatterns = [
    url('swagger', schema_view),
    url('__debug__/', include('debug_toolbar.urls')),
    url(
        r"^sitemap\.xml$",
        sitemap,
        {"sitemaps": sitemaps},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    url(r"^api/", include("shuup_api.urls")),
    url(
        r"^sa/products/(?P<pk>\d+)/media/add/$",
        ProductMediaBulkAdderView.as_view(),
        name="shop_product.add_media_sa",
    ),  # TODO: Revise! Shouldn't be needed.
    url(
        r"^admin/$",
        maintenance_mode_exempt(not_vendor(DashboardView.as_view())),
        name="dashboard",
    ),
    # TODO: When upgrading to Shuup 3, get rid of CanvasMultiselectAjaxView.
    url(r"select/", CanvasMultiselectAjaxView.as_view(), name="select"),
    url(r"^admin/", include("shuup.admin.urls", namespace="shuup_admin")),
    url(r"^c/$", csrf_exempt(CanvasAllCategoriesView.as_view()), name="all-categories"),
    url(r"^xtheme/product_price$", canvas_product_price, name="canvas-product-price"),
    url(r"^", include("shuup.front.urls", namespace="shuup")),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
