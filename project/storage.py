from storages.backends.s3boto3 import S3Boto3Storage, S3StaticStorage


class StaticCloudStorage(S3StaticStorage):
    location = "static"


class MediaCloudStorage(S3Boto3Storage):
    location = "media"
